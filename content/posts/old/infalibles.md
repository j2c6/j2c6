---
title: 'Infalibles'
date: 2019-12-13T13:14:00.003+01:00
draft: false
tags : [cuento, ficcion, relato, narrativa]
---

Alguien llama y cuelga al instante. Me hago otro sándwich. Y mientras leo un número antiguo de la revista _Vogue_, recibo otra llamada:

  

—El muy cabrón me estaba engañando

  

Una mujer al otro lado de la línea me cuenta _de pe a pa_ cómo ha descubierto que su marido le es infiel con su mejor amiga. Para eso estamos, para emergencias. Sobre el escritorio tengo una foto con mi hermano, esa en que vamos vestidos de duendes a las cuevas de Altamira. Últimamente se me olvida regar el poto, que ya empieza a hacer rápel por la pared de la mesa. Tengo la sensación de que todo lo que depende de mí está fracasando y lo que escapa a mi alcance está nominado a la Mejor Película de Habla No Inglesa.

  

Entra el supervisor. Coloco _Vogue_sobre mi regazo y trato de escuchar atentamente. Cruzo las piernas y miro la esquina de la habitación. La mujer habla como si aquello le hubiera pasado a otra persona. Entre sus muletillas se encuentran: “_en ese sentido_”, “_joder_” y la siempre infravalorada “_lo que pasa es que_”. Que no se me olvide comprar detergente y cervezas.

  

La revista está abierta por la página “**_101 Looks Infalibles_**”, más o menos por la mitad. Desde que me relaciono con el mueble revistero de la cocina he visto toneladas de carne bronceada sin estrías, chismorreos propios de las conclusiones del estudio del apareamiento de los grillos, un Fiat 500 rosa muy mono, productos de venta a domicilio que cuestionan la supervivencia de nuestra especie y algunos descuentos caducados de Groupon.

  

El supervisor se dirige a la puerta mientras me echa una mirada de _esas_ que hace cuando te echa una mirada. Es difícil de soportar. Procuro mantener el canal de comunicación con “mmm” y “huhum” regulares. Apenas noventa y tres minutos para que termine la jornada laboral. Jueves, _juernes_, o lo que sea. Poco después oigo por el auricular:

  

—Porque tú me entiendes ¿verdad?