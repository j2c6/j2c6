---
title: 'El espíritu de la colmena (1973)'
date: 2012-04-06T11:51:00.000+02:00
draft: false
tags : [James Whale, Isabel Tellería, Luis de Pablo, Victor Erice, Terrence Malick, Cine, 1970, Luis Cuadrado, Fernando Fernán Gómez, Ana Torrent]
---

[![](http://2.bp.blogspot.com/-R3nGkSYH2BU/T368jaybycI/AAAAAAAABVw/KD6w4wXnfEI/s400/rostard.jpg)](http://2.bp.blogspot.com/-R3nGkSYH2BU/T368jaybycI/AAAAAAAABVw/KD6w4wXnfEI/s1600/rostard.jpg)No hace tanto tiempo oí hablar del cine calificado como poético. Mi primer contacto fue desconcertante. '_**El árbol de la vida**_' de **Terrence Malick**, es una película bien curiosa, que tiene otra finalidad que no es narrar como estamos acostumbrados. De alguna manera me pone nervioso que este tipo de largometrajes se metan en la misma clasificación que la totalidad del cine, mientras acarrean títulos como '_la obra maestra del cine español_'.

  

El cine documental o como ahora cine poético, debería de estar bien diferenciado. No es que se trate de malas o buenas obras, sino que como cine narrativo se le puede colocar directamente un suspenso, así de sencillo. Y los críticos hablan de ella como una maravilla, quizás tengan razón pero es como si un crítico gastronómico le diera la máxima calificación a una manzana cogida del árbol, incomparable con el resto de recetas testadas durante su carrera.

  

La película de **Víctor Erice** habla sobriamente de la vida en un pueblo, donde viven dos hermanas. No hay un derroche de palabras ni de diálogos sino que los gestos y planos que hablan -aunque escasamente- por sí mismos. Dejan ver la situación de un pueblo en la España de los años 40, una vida sencilla y poco ajetreada, con mucha lentitud y laconismo. En la familia de la protagonista no hay mucha vida,  y exceptuando a las dos hermanas, existe un individualismo y separación casi total, hasta en la escena de la mesa todos los personajes son mostrados por separado. 

  

Una de las peculiaridades consiste en que los actores usan su respectivos nombres durante el rodaje. El director explica la confusión que sufrían las niñas, o mejor dicho la jovencísima **Ana Torrent**, que no comprendía porqué la gente se cambiaba el nombre y tenía esa maravillosa incapacidad de diferenciar la realidad de la ficción. Es un detalle de la auténtica interpretación de Ana -en uno de sus primeros papeles- que resulta completamente real. 

  

[![](http://1.bp.blogspot.com/-YAAejztyVg4/T368kRBa9oI/AAAAAAAABV0/2ccg7H_BtuM/s320/rostard1.jpg)](http://1.bp.blogspot.com/-YAAejztyVg4/T368kRBa9oI/AAAAAAAABV0/2ccg7H_BtuM/s1600/rostard1.jpg)

El personaje central es Ana, que tras ver '_**El doctor Frankestein**_' (1931, **James Whale**) siente esa singular fascinación que solo despierta la muerte. Su hermana **Isabel** (**Tellería**) tiene un espíritu más macabro, y se observa cuando le gasta una broma a su hermana o estrangula al gato. En cambio en Ana se refleja más como una contemplación, de modo que no cesa de hacerle preguntas a su hermana. La historia se irá desarrollando ligeramente, y no la detallaré aquí.

  

La idea de la niñez está reflejada casi en su totalidad, con imágenes muy representativas. Si no fuera el tema propiamente de la niñez -que a nadie le es indiferente- diría que se trata de un documental. La atracción por los espíritus, los fantasmas o incluso lo trenes se ve en el filme. Las canciones populares que se esconden en la banda sonora de **Luis de Pablo**, son todo un acierto.

  

'_**El espíritu de la colmena**_' es el enigmático título que solo es expresado con imágenes y silencios. Una curiosidad es que **Luis Cuadrado**, el director de fotografía se estaba quedando ciego durante el rodaje, posteriormente perdió la vista y se suicidó en 1980. De algún modo la vida que queda en este país tras la guerra, es apagada, desolada, seca... muerta. Esto es lo que retrata la película, las personas se refugian en una rutina regida por unas leyes misteriossa, analógicamente las mismas que rigen la vida de las abejas que casi actúan sin motivo movidas por estas inefables reglas. Un símbolo puede ser esos ventanales ámbar con el entramado del panal de abejas, que encierran esas atareadas pero pobres vidas. La sequedad, lentitud y aspereza del filme claman todo esto.

  

La niñez de la protagonista escapa de esta horrorosa colmena. Lo mencionado antes: el ansia de ver un espíritu, el impacto visual de un tren o la evasión del cine son los elementos mágicos para salir de esa agria realidad, con la confusión que despierta en la niñez. También de forma simbólica Ana huye de ese silencioso quehacer, de ese aislamiento. Las setas son otro modo de escapar, representan los sueños o las alucinaciones. **Fernando** (**Fernán Gómez**) es otro ser de la colmena vive aislado, y oculto en la colmena, incluso es mostrado como apicultor, se autocensura a pesar de que antes tenía un pasado creativo, se supone que esto también es consecuencia de la guerra, parece hasta disecado. Y podríamos parlotear más pero no me apetece. ¿Se nota?  
  

Una película muy particular, por supuesto no recomendable a la inmensidad de la expectación. Tiene un mensaje expresado de forma muy personal, quizá de modo deprimente y con mucha tristeza transformada en sequedad. No me parece algo muy comunicativo, me resulta más vanguardista y subjetivo, y en este caso no me resulta fácil disfrutarlo.