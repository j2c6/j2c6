---
title: 'Mark Twain - ''Las aventuras de Tom Sawyer'''
date: 2012-03-23T20:29:00.000+01:00
draft: false
tags : [Tía Polly, Mark Twain, Frank Capra, Samuel Clemens, Thomas Alva Edison, William Faulkner, Ernest Hemingway, Norman Rockwell, Tom Sawyer, Huckleberry Finn, Literatura, XIX, Norteamericana]
---

[![](http://4.bp.blogspot.com/-QABUYTYS2C0/T2zOyB6PlyI/AAAAAAAABTQ/L_xe1igAL6s/s400/rostard.jpg)](http://4.bp.blogspot.com/-QABUYTYS2C0/T2zOyB6PlyI/AAAAAAAABTQ/L_xe1igAL6s/s1600/rostard.jpg)'_Toda la literatura estadounidense procede de un libro escrito por **Mark Twain**_' así homenajeaba el excelente premio nobel, **Ernest Hemingway** así como **W. Faulkner** que lo tildó de '_padre de la literatura norteamericana_'. También se oyó '_un norteamericano ama a su familia. Si le sobra espacio para amar a otra persona por lo general escoge a **Mark Twain**_' y esto lo pronunció el inventor de la bombilla **T. Alva Edison**. Un personaje ilustre conocedor de su tiempo, amigo de los aristócratas, presidentes, mendigos, niños... Alguien influyente al que había que leer.

  

**Samuel Clemens** -como se llamaba en realidad- fue un gran orador, humorista, escritor y nefasto en las finanzas, escribió muchas novelas de éxito, pero '**_Las aventuras de Tom Sawyer_**' y su secuela de Hucklebberry Finn, se convirtieron en sus eternos clásicos. Innumerables infancias y vejeces han enfermado con esta fantástica novela. Pero me voy a centrar en Sawyer. 

  

Al principio todo me parecía como en las preciosas películas de **Frank Capra**, todo sencillo, agradable y sorprendente. Sin embargo -equilibrio perfecto- no sensiblero ni cursi sino auténtico y fiel. El personaje de **Tom Sawyer** es único. Me hubiese gustado leerme la novela hace algunos años, para fomentar todo contagio soñador y aventurero.Está lleno de irónicas críticas a la sociedad en apoyo de Tom, le da la razón a su curioso y espontáneo modo de hacer las cosas y sin embargo -parece típico- pero todos los lectores acaban siendo como el protagonista, se identifican con él.

  

Es asombrosa la imaginación de Twain en la mente de Tom, que está a la orden del día en cada página. Dota a su pensamiento de la lógica de un matemático, justificando y razonando todo, cuando en realidad es simplemente Tom Saywer sin complicaciones. Calibra muy bien el efecto que tiene en el resto de personajes la singularidad de Sawyer, y cómo se sobreponen -o eso intentan- a un carácter tan "rebelde" e indomable. A pesar de la magia y la profundidad con que expone los hechos lo hace con mucho desenfado y confiado, deja imprecisos ciertos detalles, como en el aire, al encanto y parecer del lector.

  

Una escritura ejemplar que refleja los valores humanos a través de la inocencia y al mismo tiempo consciencia terrena. La amistad, el valor, la traición, la verdad, la conciencia, la humildad, y toda esa pesada lista de insignias del género. Sin duda hay una enseñanza detrás de cada aventura y cada travesura, es una escuela inusual y muy reveladora.

  

Sus comparaciones son extraordinarias y muy ilustrativas, son como el paleolítico y maestría del comienzo de las metáforas en la literatura. Su narrativa resucita los objetos, y adapta milagrosamente su lenguaje a lo que quiere decir sin reparos. También consigue que cualquier escena sencilla o casual parezca irresistiblemente poética, un acuerdo entre las ilusiones, los deseos y la realidad. Sin duda una obra encantadora.  
  

Lo más importante es que nunca caerán en el olvido '**_Las aventuras de Tom Sawyer_**' y **tía Polly** y **Huck Finn** y el tesoro y cuando acudieron a su propio funeral y cómo se perdieron en la cueva y todas esas cosas que relata este libro que sirvieron para inspirar a la humanidad e inmortalizar la juventud de un personaje junto a su narrador: **Mark Twain**.

  

"_Esas gotas caían ya cuando las pirámides eran nuevas, cuando cayó Troya, cuando se echaron cimientos de Roma, cuando Jesucristo fue crucificado, cuando el conquistador creó el Imperio británico, cuando la matanza de Lexinton fue noticia. Ahora seguía cayendo, y continuará cayendo cuando todas las cosas se hayan hundido en el atardecer de la historia y en las sombras de la traición, y queden engullidas por la densa noche del olvido. ¿Tienen todas las cosas una finalidad y una misión? ¿Estuvo cayendo esa gota pacientemente durante cinco mil años para cubrir la necesidad de este efímero insecto humano? ¿Tendrá algún otro importante fin que cumplir dentro diez mil años?  No importa. Han transcurrido muchos, muchos años desde que el desdichado mestizo abrió un hoyo en la piedra para recoger  las gotas de incalculable valor, pero hoy día el turista mira largo rato la patética piedra y el lento gotear del agua cuando viene a contemplar las maravillas de la cueva de McDougal._"

  

'**_Las aventuras de Tom Sawyer_**', Mark Twain. Editorial Everest , 1999.   
Ilustración de **Norman Rockwell**.