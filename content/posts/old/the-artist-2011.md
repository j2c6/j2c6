---
title: 'The Artist (2011)'
date: 2012-02-28T02:00:00.000+01:00
draft: false
tags : [Michel Hazanavicius, Alfred Hitchcock, Jean Dujardin, Mel Brooks, Bérénice Bejo, Cine, Chuck Workman, 2010, Charles Chaplin]
---

[![](http://3.bp.blogspot.com/-AsMU5jJ5IIo/T00JcMozJ1I/AAAAAAAABPw/lhebAFAnfbI/s400/rostard.jpg)](http://3.bp.blogspot.com/-AsMU5jJ5IIo/T00JcMozJ1I/AAAAAAAABPw/lhebAFAnfbI/s1600/rostard.jpg)Hollywood en los años veinte, concretamente 1927 el aclamado George Valentin (**Jean Dujardin**) está en lo más alto de su carrera como actor representativo del cine mudo. Un día conoce por casualidad a una joven deseosa de triunfar en el cine, Peppy Miller (**Bérénice Bejo**) y tiene un pequeño detalle con ella. Por circunstancias de la vida, llega el sonido a las pantallas y con el declive del famoso actor comienza un exitoso triunfo de Peppy como actriz principal en las películas de la época.

  

Un largometraje mudo y en blanco y negro. Quizá una apuesta muy arriesgada en los tiempos que corren. Sin duda **Michel Hazanavicius** sabía lo que hacía, no solo se trataba de algo "insólito" sino de algo más: un homenaje. Y lo cierto es que adoramos los homenajes, y los recuerdos. Hace poco pude disfrutar de '**_Precious Images_**' (1989) de **Chuck Workman**, un cortometraje de ocho minutos que contenía pequeños fragmentos de un sinfín de películas, os lo recomiendo, además se apuntaba la teoría de que un breve instante evocaba recuerdos más intensos, muy interesante. 

  

Una apuesta con motivos personales. **Hazanavicius** llevaba pensando en esta joya algún tiempo, claro que era difícil tomarlo en serio. El mundo no conoció otra película muda desde 1976 con '_**Silent Movie**_' de **Mel Brooks**. Hubo un intenso estudio del cine clásico, y como se puede observar se adecua milimétricamente al corte cinematográfico en que se ambienta el filme.  La elección del drama fue fundamental por la popularidad que obtuvieron en su momento y progresivamente; así como el encanto y lo entrañable del género (_Veasé:_ '**_Luces de la ciudad_**' 1931, **Charles Chaplin**). La banda sonora está compuesta por **Ludovic Bource**, aunque aparece un tema con letra titulado '**_Pennies from Heaven_**' de **Rose Murphy**, no acreditado; así como "el cameo" de algo de **Bernard Herrmann** nacido en  '**_Vértigo_**' del director **Alfred Hitchcock**: '_Love Scene_'. 

  

A pesar de la visible sencillez y autenticidad del largometraje hay mucho trabajo detrás. Un ejemplo es que **Dujardin** confesó que el rodaje llevó únicamente 35 días, claro que para preparar el baile final vivieron cinco meses de pesados ensayos. El resultado es un ejemplar magnífico y para recordar. Las actuaciones, con la exigencia del silencio han sido estelares, especialmente las de los protagonistas: **Dujardin** y **Bejo,** con un retorno al mimetismo y a la expresividad gestual admirable. El guión trata con mucho afecto a este melodrama clásico y especial. Reconocido y recompensado por la academia con cinco **Oscars** por la dirección, actor principal (Dujardin), película, vestuario y banda sonora. Como homenaje: muy grande.