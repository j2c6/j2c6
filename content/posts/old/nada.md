---
title: 'Nada'
date: 2018-05-08T19:33:00.004+02:00
draft: false
tags : [planta, poto, jarabe, farmacia, muscosan, nada, aburrimiento, medicina, foto, intenciones]
---

Quería comunicar que hoy no me apetece hacer nada especialmente. Tal vez leer a Carver, él sabe. Quiero hacerme con un poto. Me voy a dar un paseo.  
  

[![](https://1.bp.blogspot.com/-vKet3-FrgX8/WvHfR_df_yI/AAAAAAAAVjw/WUDO4EDw0BokpGF8VyZy4fTujLs3JYDQACLcBGAs/s320/20180508_015422_0.jpg)](https://1.bp.blogspot.com/-vKet3-FrgX8/WvHfR_df_yI/AAAAAAAAVjw/WUDO4EDw0BokpGF8VyZy4fTujLs3JYDQACLcBGAs/s1600/20180508_015422_0.jpg)