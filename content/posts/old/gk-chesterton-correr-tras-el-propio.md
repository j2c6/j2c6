---
title: 'G.K. Chesterton - Correr Tras el Propio Sombrero'
date: 2011-03-28T13:33:00.000+02:00
draft: false
tags : [Norman Rockwell, Chesterton, Londres, Ensayo, Sentencias]
---

[![](http://1.bp.blogspot.com/-iX_Qlj7PsjA/TZBw-wSRSXI/AAAAAAAAA8g/QmPqjhOZsAg/s320/norman2.jpg)](http://1.bp.blogspot.com/-iX_Qlj7PsjA/TZBw-wSRSXI/AAAAAAAAA8g/QmPqjhOZsAg/s1600/norman2.jpg)

Norman Rockwell_"After the Prom"_

Tras numerosas audiciones de referencias a **Chesterton**, he decidido leer algo de su puño y letra: es un revolucionario. Su modo de escribir es amistoso, atractivo y alegre, apetece escucharle. Es uno de estos hombres elegantes y majestuosos, de exquisitas maneras, grandes admiradores de todo, detallistas y persuasivos.  **Montecristo, Lord Henry** o el mismísimo **conde Fosco**: personajes fascinantes. Este es el primer ensayo del libro que leo: "**_Correr tras el propio sombrero_**", y habla sobre -como dijeron- el lado romántico de la vida, la perspectiva "poética" de todo.  Como lo mires: "el vaso medio lleno o medio vacío" y esto señala hacia esa tierra de optimismo y -de nuevo- de disfrute, donde la vida es como la cuentan los libros y no en ocasiones un calendario lleno de compromisos monótonos y agotadores . Os apunto un par de citas de este breve pero excelente ensayo:

  

_"Algunos reputan esta romántica contemplación de inundaciones o incendios algo falta de realismo. Pero en realidad esta contemplación romántica de tales fenómenos es tan pragmática como cualquier otra. El optimista que ve en ellos una ocasión de disfrutar es tan lógico y mucho más sensato que el «indignado contribuyente» que ve una ocasión de quejarse. El verdadero dolor, como cuando a uno lo queman vivo en la plaza pública de Smithfield, o le duelen las muelas, es un hecho objetivo; soportable, aunque escasamente placentero. Pero al fin y al cabo, los dolores de muelas son algo excepcional, y que a uno lo quemen en Smithfield es cosa que sólo sucede cada mucho tiempo. __La mayoría de los inconvenientes que hacen blasfemar a los hombres y llorar a las mujeres son, en realidad, inconvenientes de índole sentimental o ficticia, pertenecientes todos al ámbito de la imaginación."_

  

_"No pienso, pues, que sea completamente absurdo o increíble suponer que también las inundaciones de Londres pueden ser vistas y disfrutadas de una manera poética. Parece que no han causado nada más que molestias; y las molestias, como he dicho, son solo un aspecto, el aspecto menos imaginativo y más accidental de unas circunstancias realmente románticas. **Una aventura no es más que un inconveniente convenientemente considerado**. Un inconveniente no es más que una aventura considerada equivocadamente."_

  

_Correr tras el propio sombrero (y otros ensayos). G.K.Chesterton. Acantilado 2005._