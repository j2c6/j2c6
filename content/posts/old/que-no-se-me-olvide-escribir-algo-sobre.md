---
title: 'Restos de fresas'
date: 2018-04-24T17:57:00.001+02:00
draft: false
tags : [azúcar moreno, plato, postre, fresas, retro, foto]
---

[![](https://3.bp.blogspot.com/-mFEDUiNFJHk/Wt9TNqwSoaI/AAAAAAAAU7Q/MJ4FoZTlbjAPIGfsjW_xV7msttxA2AK6gCLcBGAs/s320/photo_2018-04-24_17-53-43.jpg)](https://3.bp.blogspot.com/-mFEDUiNFJHk/Wt9TNqwSoaI/AAAAAAAAU7Q/MJ4FoZTlbjAPIGfsjW_xV7msttxA2AK6gCLcBGAs/s1600/photo_2018-04-24_17-53-43.jpg)

  
  

Que no se me olvide escribir algo sobre cómo quedó de manchado el plato con restos de fresas, sus cabelleras verdes y difíciles de agarrar sin tocar su carne roja, y los restos de azúcar moreno, húmedo en modo café y seco tierra. La app para que la foto haya quedado así es Huji Cam, tiene tirón en mi móvil estos días. Estaba buscando ese efecto de partículas de las películas antiguas, he encontrado la palabra Dusty, que espero que me sirva para futuras búsquedas.