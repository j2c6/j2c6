---
title: 'El festín de Babette (1987)'
date: 2012-04-03T18:22:00.000+02:00
draft: false
tags : [Jean Philippe Lafont, Gabriel Axel, Karen Blixen, Birgitte Federspiel, Isak Dinesen, Bodil Kjer, Cine, Jarl Kulle, Stéphane Audran, 1980]
---

[![](http://1.bp.blogspot.com/-PLrHQWm0h-o/T3sjs-egCGI/AAAAAAAABVY/nGdP9FicMRQ/s400/rostard.jpg)](http://1.bp.blogspot.com/-PLrHQWm0h-o/T3sjs-egCGI/AAAAAAAABVY/nGdP9FicMRQ/s1600/rostard.jpg)Hasta ahora no había visto una película tan sencilla y al mismo tiempo detallista. La poética voz de una mujer va narrando plácidamente lo que ocurre en una aldea de Dinamarca. La sobriedad y la pasividad con la que todo es mostrado puede desesperar a espectador, pero creo que solo se trata de una técnica para sentir de primera mano -en proporción- la sobriedad de esta devota secta danesa. Y el efecto es completo.

  

**Gabriel Axel** decide dirigir y adaptar este relato de Isak Dinesen (**Karen Blixen**), particularmente artístico y religioso.  Por un lado la vida de los dos impresionados pretendientes,  que se desvían -según su visión- uno hacia la fama como es la postura del cantante de ópera Achille Papin (**Jean Philippe Lafont**), y el hombre del ejército hacia la ambición y el triunfo, el general Lorens (**Jarl Kulle**). Las piadosas almas de las dos hermanas  Filippa (**Bodil Kjer**) y Martine (**Birgitte Federspiel**) permanecen practicando la caridad con los ancianos del pueblo en una vida sumisa y tranquila. Encuentran consuelo en la fe y en la bondad que ofrecen al pueblo, y si quedaran con el  melancólico recuerdo de haber rechazado el amor, parecen olvidarlo por su sencilla dedicación a los demás.

  

**El festín de Babette** (**Stéphane Audran**) se convierte finalmente en el cruce de caminos y conclusiones. Por un lado la secta religiosa, no sabe cómo actuar ante un banquete dado a los placeres mundanos que no saben si va contra la ley de Dios, saborear semejante cocina francesa y pecaminosa. Pero la maestría de Babette es muy superior ya que no saben que se encuentran en una mesa destinada a probar manjares exquisitos y únicos de manos de una excepcional chef francesa. Por lo que acaba por imprimir su propósito, el arte posee esa persuasión. Pone el orden donde en realidad, no lo había.

  

El espectador se plantea si a pesar de todo sabrán que están ante una delicia sin igual, y si serán capaces de disfrutarla. La caduca idea de que la religión insiste en ser infeliz en la tierra y no disfrutar de los placeres terrenales, para luego gozar eternamente en el cielo es lo que rompe el festín de Babette, que hasta consigue arrancar divinas alabanzas. De modo que además de amar, ser caritativos, hacer el bien, también se ha de poner por obra el talento y arte de cada persona que también es con creces capaz de generar felicidad,  y que por ese motivo fueron creados nuestros sentidos, para sufrir el dolor pero además exprimir el encanto de la belleza y el gusto.  
  

Babette después de recibir el amparo de esta aldea y de las dos hermanas, les devuelve el favor, un regalo que les enseñará a disfrutar en medio del puritanismo que profesan, de los pequeños placeres que parecían desconocidos en sus vidas. Esta moraleja que parece simple, es muy bien reflejada en el largometraje, y da pie a cierta reflexión. Ciertamente el talentoso festín gastronómico no solo es disfrutado por los doce comensales, sino que también llega al paladar del público con generosas tomas de la cena, y sus delicados ingredientes.