---
title: 'Big Fish,  Avatar'
date: 2010-01-28T22:43:00.000+01:00
draft: false
tags : [James Cameron, Oscars, Tim Burton, Titanic, Cine, Entretenidas, Buenas, Big Fish, Avatar]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/S2IEmvY4koI/AAAAAAAAAoY/m3_EZaWKrms/s400/big-fish.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/S2IEmvY4koI/AAAAAAAAAoY/m3_EZaWKrms/s1600-h/big-fish.jpg)  

Solo una cuestión: estamos hablando de **Tim Burton**. En este filme del 2003, Burton nos trae un cuento infantil, para los no tan niños. Will Bloom está harto de que su padre le cuente su vida en forma del más increíble cuento, quiere saber la verdad. Tardará en descubrir que gran parte de esas historias salidas de la boca de **Edward Bloom**, han sido (en su mayoría) vividas por él. En esta película se puede valorar y disfrutar de la literatura infantil, produciendo un encanto especial en el espectador. A mí me ha gustado, lo suficiente para reivindicar mi gusto por los largometrajes de Tim Burton: algo espectacular. Aunque no se contempla en el top ten Burton, es algo digno de mención. Solo una idea: **Espectro**.

  

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/S2IEi9xRePI/AAAAAAAAAoQ/MTMqW6uvx_4/s320/Avatar-pelicula.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/S2IEi9xRePI/AAAAAAAAAoQ/MTMqW6uvx_4/s1600-h/Avatar-pelicula.jpg)

  

El esperado **Guinness Record** del presupuesto. : Avatar. Así lo anunciaban los periódicos de todo el mundo. En efecto,  la película más cara de la historia de manos del director de la _oscarizada_ “**_Titanic_**”: **James Cameron**. No podía traer nada pequeño que pudiera pasar  desapercibido. Avatar se adentra en un mundo totalmente desconocido para el siglo XXI: Pandora, donde viven unos seres racionales, con forma similar a la humana pero con el doble de tamaño y mucho azul. Jake Sully, trata de conocer ese mundo desde dentro, por medio de un **Avatar**. La cosa no tardara en complicarse cuando se mezclen los conflictos coloniales. Una gran película, que acoge ideas de algunas otras, junto con un planteamiento ecologista y panteísta, propio de Cameron. La novedad triunfó: un peliculón.