---
title: '5 niveles de estudio en una biblioteca'
date: 2012-09-25T19:54:00.000+02:00
draft: false
tags : [bilbiotecas, Praga, cinco niveles de estudio]
---

Hoy no ha venido nadie a la biblioteca y eso es realmente aburrido. Aunque la biblioteca general de la Universidad Politécnica de Valencia es espaciosa, en invierno no hace calor, y por el ventanal entra mucha luz, vacía es como un cementerio sin misterio. Odio esa última rima casual. Si no estudio me es necesario observar a la gente, y aunque sea un nivel bajo de estudio es -después de todo- un nivel. Los cinco niveles de estudio que se pueden alcanzar en una biblioteca por si no los conoce, son:

>   
> 
> [![](http://1.bp.blogspot.com/-gYNlycSZzHU/UKp_r0NFfVI/AAAAAAAABlI/HMJQiq5Hh4U/s400/vernitosta.jpg)](http://1.bp.blogspot.com/-gYNlycSZzHU/UKp_r0NFfVI/AAAAAAAABlI/HMJQiq5Hh4U/s1600/vernitosta.jpg)1.-**Nivel 1:** Pasa muy pocas veces, todos tus sentidos están concentrados en absorber información, no puedes durar así mucho tiempo, aunque pocas horas antes del maldito examen alcanzas este preciado nivel. Algunas personas son capaces de llegar solo con su orgullosa fuerza de voluntad.
> 
> 2.-**Nivel 2:** Estudias, aprendes, avanzas, podría ser mejor pero estás aprovechando el tiempo, y no te puedes quejar.  Es lo que corrientemente se llama estudiar.
> 
>   
> 
> 3.-**Nivel 3:** Finges que estudias, copias ejercicios, revuelves papeles, ordenas apuntes, subrayas el libro, pero en realidad tú y yo sabemos que no estás haciendo nada, a la gente le funciona y habitualmente te sueles mentir y te sientes satisfecho aunque hayas pasado horas en este nivel. No desentonas en el ambiente.
> 
>   
> 
> 4.-**Nivel 4**: Observas cómo la gente estudia en la biblioteca, es poco eficiente pero placentero. La gente que acude a estudiar a la biblioteca suele ser interesante, y puedes pasar sencillamente horas disfrutando en este nivel. Hasta es posible que cierta satisfacción del estudio ajeno se te contagie.
> 
>   
> 
> 5.-**Nivel 5**: Pasas de todo y pones un juego o lees, y no haces lo que se dice nada relacionado con el estudio. Muy entretenido pero poco productivo profesionalmente.

  

Si al final -no se confunda- lo importante es pasar horas en la biblioteca y fingir, tal vez aprender y sobretodo observar. Buscar el disfrute en cada instante: esto sí que es constructivo.