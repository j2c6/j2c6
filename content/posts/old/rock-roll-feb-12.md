---
title: 'Rock & Roll. Feb 12'''
date: 2012-02-24T12:33:00.000+01:00
draft: false
tags : [Rock, Playlist, David Fincher, Black Rebel Motorcycle Club, Jesse Eisenberg, Jack White, White Stripes, Black Keys, Sex Pistols, The Subways, The Raconteurs, Rooney Mara, Música, Bon Jovi, Kings of Leon, Vines]
---

[![](http://2.bp.blogspot.com/-lHK5_wiUUxU/T0d1dZPNaSI/AAAAAAAABPQ/zMz2WJd8KFw/s400/ROSTARD.jpg)](http://2.bp.blogspot.com/-lHK5_wiUUxU/T0d1dZPNaSI/AAAAAAAABPQ/zMz2WJd8KFw/s1600/ROSTARD.jpg)Estaba pensando en algo de _**rock'n'roll**_ o _**garage rock**_ para este mes.

  

'**_La Red Social_**' (**David Fincher**, 2010) empieza --augurando un buen comienzo-- con '**_Ball and biscuit_**' de **The White Stripes**, y mientras empieza esa conversación entre **Rooney Mara** y **Jesse Eisenberg**. **Jack White** está loco y me cae bien aunque solo sea una pose, pero es mudo y hace hablar a su guitarra. Y con **Meg** hace un gran dúo sin grandes complicaciones. Por eso también he metido algo de los **Raconteurs**, '**Consolers of Lonely**' ( del homónimo de 2008) para hacerle el seguimiento. La de '**_Another way to die_**' era demasiado Hollywood.

  

En _fnac_ oí algo sucio y entretenido de **The Black Keys** y para sacar algo en claro de "**_El camino_**" (2011) elegí '_**Money Maker**_' que no está en mi ADN pero la disfruto. **The Vines** eran demasiado enérgicos para el _playlist_, pero había algo que robar en su discografía, sobretodo porque estos si que son  agresivos, no sé si más por dinero que por actitud, pero el resultado es bueno, con algo ochentero en la voz del principio: '**_Don't listen to de radio_**'.

  

Pensé en los **Sex Pistols** pero no quería una A mayúscula de anarquía en la portada del disco, sino algo así como "me da igual quién sea el número uno en la lista de **_Billboard_**". **Bon Jovi** fue descartado por ser demasiado importante: son grandes canciones las de este proyecto pero no les confío mi vida. 

  

De los abanderados del garage rock, **Black Rebel Motorcycle Club** escogí uno de sus himnos '**_Whatever happened to my rock and roll_**' de su primer LP, aunque se me está pasando por la cabeza la de '**_Ain't no easy way_**' que no está mal, algo campestre en la línea de lo que busco. **The Kings of Leon** no son tan potentes pero tienen algo considerablemente informal llamado: '**_Molly's Chambers_**'.

  

Todavía quiero añadir algo de **The Subways** o pirados de ese calibre. Os mantendré informados.