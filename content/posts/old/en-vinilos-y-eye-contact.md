---
title: 'En vinilos y eye-contact'
date: 2019-10-17T20:34:00.002+02:00
draft: false
tags : [poesía cotidiana]
---

Te ríes en la luz  
de los domingos  
Hay fruta bajo de casa  
y llegan las campanas  
de una iglesia en la  
plaza, donde está el Tobogán  
  
Tus esculturas de  
aire y verde agua  
me tienen meditando,  
las damajuanas en la ducha,  
el día en un solo beso  
partido y continuo  
en vinilos y eye-contact.  
  
Tuuu lámpara de sal  
adopta nuestra intimidad.