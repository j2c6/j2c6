---
title: 'Lo mejor de la década según Rolling Stone'
date: 2009-12-10T07:50:00.000+01:00
draft: false
tags : [Rock, Rolling Stone, Música, Listas, S.XXI]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SyHqgtRhrlI/AAAAAAAAAnY/Vkh3UcQrFxU/s200/31248774.jpg)](http://www.rollingstone.com/news/story/31248926/100_best_songs_of_the_decade/27)

  
  

Un artículo breve pero importante. Con el fin de año la famosa revista de música **_Rolling Stone_**,  ha publicado una lista con los mejores temas y discos de estos últimos diez años. Estas listas  siempre me han gustado, dan para descubrir nuevas canciones, nuevos grupos, y sobre todo para  discutir quién  falta y quién sobra. Haced clic en la foto para ver la lista. En mi opinión faltan claramente...ver comentarios.