---
title: 'Entusiasmo, Pablo d''Ors'
date: 2018-02-03T19:02:00.002+01:00
draft: false
tags : [libro, fe, Pablo d'Ors, dolor, Novela, entusiasmo, visión, ficción, vocación, Literatura, religión, autobiografía, silencio]
---

Creo que la amabilidad auténtica es sólo fruto de un pensamiento tolerante. La flexibilidad es una de las condiciones del pensamiento. Un pensamiento rígido no es, en consecuencia, más que doctrina o ideología. Busca un gran pensado que haya sido un fanático, no lo encontrarás. (p.19)

  

En la naturaleza, sea de ello consciente o no, el hombre recuerda de dónde viene. La naturaleza no invita al hombre a pensar, sino a contemplar y, en último término, a fundirse con ella. (p.24)

  

La necesidad de acumular experiencia. Cuando oyes una canción para estar triste. Tener ganas de vivir y dejarse afectar por todo.

  

Lo que en última instancia me cautivó de este escritor fue  que se pasó la vida contándonos la misma historia, la suya: la de la dualidad del espíritu humano, escindido entre la cabeza y el corazón, entre los ideales y los instintos,  entre el poder erótico y el místico.

  

Que no son nuestros esfuerzos los que nos llevan a Dios, pero que sin ellos, por alguna razón, no llegamos a Él. (p.56)

  

Aún en la más terrible de las calamidades el hombre puede ser feliz su se sabe en un camino. Como escritor no he hecho otra cosa que ofrecer a mis lectores los mapas de mis caminos. Vivir --ahora lo sé-- merece la pena, aunque sólo sea para dejar a quienes nos sobreviven el mapa de los que has vivido.(p.57)

  

ID: Aprender las cosas auténticas o llegar a ellas, por los motivos equivocados.

  

Toda nuestra resistencia al dolor, que es la misma que la que experimentamos frente al amor, proviene de nuestra dificultad para entender que estos dos misterios, son sólo uno.

  

Sabes escuchar es, a fin de cuentas, la clave de absolutamente todo. (p.72)

  

Todo hombre, pobre o rico, es un necesitado. (p.74) \[Nota: La gente se siente sola y está necesitada.\] 

  

  

Cualquier joven del mundo queda sin mecanismos de defensa cuando un adulto le dice, sencilla y abiertamente, que le respeta. Que no le quiere cambiar. Que ni siquiera le quiere educar. Que sólo está ahí para escucharle y abrirle algún camino, por si alguna vez quisiera transitarlo. \[...\] Y dejarnos a solas, en ese espacio de interpretación que es la soledad y que es la fragua de lo que llamamos ser humano. (p.75)

  

Descubrir una personalidad es siempre un invitación a buscar la propia o, dicho de otro modo, un incentivo a la libertad.Y no hay en el hombre un anhelo más profundo que aquel que le conduce a ser él mismo. (p.76)

  

Sólo cuando nos sorprendemos estamos vivos.

Estarme quieto en mi centro, y allí permanecer lo más abierto posible.

Intento prestar atención al joven que me habla, no a sus conflictos o dilemas. 

(p.79)

  

No comprendo cómo se puede vivir sin un secreto en nuestro hondón. (p.107)

  

Lo trágico no es la tragedia en sí, sino no tener a nadie a quien contarla, ¿no crees? Lo triste no es sufrir, sino no poder compartir el sufrimiento. (p.117)

  

Tampoco afirmo que la vida moral sea el único camino para el cultivo del espíritu, aunque sí, desde luego, una de sus sendas; lo que sí creo es que evitar hablar del mal puede ser la manera más sutil para que ese mal pueda infiltrarse en el corazón de los hombres para irlos así deteriorando hasta destruirlos y dejarlos así irreconocibles. (p.123)

  

Buena parte de nuestra vida interior depende de nuestra capacidad de silenciarnos y de afinar la escucha. (p.127)

  

Siempre que me he puesto en el lugar del otro, siempre que he visto el mundo desde una perspectiva distinta a la habitual, me han pasado cosas interesantes. (p.131)

  

Conforme aumentan los objetos que nos rodeas y de los que somos propietarios, aumentan también , y en mayor medida, lo que encadena nuestra mente: prejuicios, fantasías, preocupaciones, pensamientos... Todo lastre. 

  

Deberíamos comenzarlo todo periódicamente de nuevo. Para que una vida fuera cabal habría que vivir al menos tres o cuatro nacimientos. (p.137)

  

\[Esto vale para todas las ETIQUETAS\] Resistir como persona sin ser devorado por su función es para todo sacerdote uno de sus compromisos más difíciles. (p.171)

  

Casi nunca merece la pena discutir por ideologías. Cada vez que en aquellos conciliábulos no enredábamos en discusiones por ideas, perdíamos la ocasión de encontrarnos de verdad con ese misterio que es el otro. Fueron aquellos combates verbales los que le habían ido preparando para la verdadera pelea de la vida, que siempre es contra uno mismo. (p.176)

  

La admiración es la puerta del amor, es ya el amor mismo. (p.185)

  

Aristóteles considera que lo más relevante de la vida humana no es la actividad, sino la contemplación. (p.192)

  

Viajamos sin ver verdaderamente los lugares a los que hemos viajado. Y amamos sin conocer a la persona que decimos que amamos. Diría más: vivimos todo un año sin vivir de él más que, en el mejor de los casos, cuatro o cinco días. Es difícil que un joven se olvide de sí. Por eso todo joven es incapaz, casi por principio, de leer, viajar o amar. Están demasiado ocupados en sí mismos, como para pensar en los demás. (p.212)

  

El arte sólo puede nacer de los vacíos del alma. (p.213)

  

No hay desdeñar la imaginación; casi todo lo que nos sucede a los seres humanos nos sucede precisamente en esa sede. (p.219)

  

Filosofía frente al mundo: estar en él, pero sin pertenecerle. Aceptar las reglas del juego, pero reservar un buen espacio para mi conciencia. (p.273)

  

Mis debilidades me harían comprender las debilidades ajenas cuando me tocara escucharlas, no precipitándome a condenarlas. \[...\] Porque cuando alguien es comprendido hasta el fondo, siempre nace la esperanza. (p.287)

  

Antes o después escribirás como sólo tú puedes hacerlo. Para lograrlo únicamente tienes que **alimentar tu deseo y practicar**. (p.298)

  

Mucho peor que el sufrimiento, es la humillación, que la humillación es el gran tema del hombre y la literatura. (p.301)

  

Sin poesía el dolor no puede ser redimido y se enquista. (p.317) El sufrimiento puede ser hermoso, pues descubre en el ser humano una dignidad que de otro modo no puede ser descubierta. Más aún: sólo el dolor contiene la belleza necesaria. (p.376)

  

Estaba convencido de la imposibilidad de vivir una existencia auténticamente religiosa instalado en el círculo de confort propio de la vida burguesa. De que para vivir religiosamente había que salir de ese círculo y caminar por fuera del mismo. De que no hay religión verdadera sin riesgo. Los hombres profundamente religiosos han vivido existencias profundamente inestables, han sido itinerantes. No se han hecho fuertes en una convicción, estilo, o lugar. Han emigrado. Han cambiado. (p.344)

  

No dar la espalda al dolor y estar en la realidad. (p.366)

  

Los grandes ideales se demuestran en las acciones de cada día, no en el debate de las palabras, que pueden quedarse vacías. (p.388)

  

Sólo cuando estamos ante un ser necesitado e indefenso descubrimos qué es eso de la libertad. (p.428)

  

A los niños hay que tratarlos de igual a igual.

  

_**Entusiamo**_, Pablo d'Ors, Galaxia Gutenberg, 2017.

  

He encontrado grandes cantidades de material interesante en esta novela. No me parece que sea una escritura fantástica, el secreto es su contenido humano, su visión.