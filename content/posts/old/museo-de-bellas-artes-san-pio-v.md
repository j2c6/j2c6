---
title: 'Museo de Bellas Artes San Pío V'
date: 2010-06-04T12:45:00.000+02:00
draft: false
tags : [Arte, Calvario sin cruz, Museo San Pío V, Museos]
---

Tras los exámenes decidimos ir a una galería de arte como es el **Museo San Pío V,** la segunda pinacoteca mas completa del país. Ya la habíamos visitado pero siempre conviene recoger nuevas reflexiones, pues entre arte no solo se habla de pintura acrílica... A pesar del objetivo de nuestra visita, el tema principal no fue Sorolla, ni Juan de Juanes... Se habló de no saturarse y de vez en cuando quemar el tiempo y deshacer las miradas en alguna obra para bañarte en la belleza del retrato y en el pensamiento del artista. Nos desolamos  ante un **"Calvario" (sin Cruz)**, de un desconocido: los ojos de los vivos que aparecían en él, acariciados por el dolor y las lágrimas, enrojecían sus rostros por la angustia... Al salir de la galería, los rayos de un lejano atardecer escucharon nuestra conversación. Debíamos salvar el arte, entonces fue esconder algo de aquello que habíamos presenciado, para la eternidad. A veces estos rescates de arte son despreciados por quienes son destino de su revelación: como cuando te gusta la oscuridad y la sacas a la luz, pierde su encanto igual que el silencio cuando exclamas admiración por él. A lo mejor ciertos secretos solo deben ser revelados a los admiradores que saben realmente apreciar la recomendación de un amigo, loco tras una novela, una poesía o una canción.

.

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TAjXBAyFDjI/AAAAAAAAAto/j7OD0J_HiCk/s320/Pio+v+%281%29.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TAjXBAyFDjI/AAAAAAAAAto/j7OD0J_HiCk/s1600/Pio+v+%281%29.jpg)

CV y [MG](http://www.mihuecoenlared.com/) contemplando y reflexionando durante su estancia en el museo.

  

  

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TAjXEpZoSDI/AAAAAAAAAtw/6zVufVNJ6s8/s320/Pio+v.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TAjXEpZoSDI/AAAAAAAAAtw/6zVufVNJ6s8/s1600/Pio+v.jpg)

MG escuchando a Doriana.

  

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TAjYWB2diII/AAAAAAAAAt4/EnCR_pVSSoQ/s320/pio.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TAjYWB2diII/AAAAAAAAAt4/EnCR_pVSSoQ/s1600/pio.jpg)

CV absorbido por un retrato.

  

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/TAjYvQeHbXI/AAAAAAAAAuA/568oFLMj6mI/s320/30052010208.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/TAjYvQeHbXI/AAAAAAAAAuA/568oFLMj6mI/s1600/30052010208.jpg)

[MG](http://www.mihuecoenlared.com/) de nuevo ante un cuadro mundialmente conocido.

  

Mejoraremos la calidad fotográfica, hubo un problema técnico.