---
title: 'desdoblamiento del día'
date: 2020-03-20T09:05:00.003+01:00
draft: false
tags : [Uncategorized, desdoblamiento, tiempo]
---

así, misteriosamente, es viernes. no creo que sea por el confinamiento. he estado en algún agujero temporal. congelado en una dimensión intermedia. la semana han sido como un día desdoblado en varios paralelos, que se le parecen. la semana ha pasado volando. mi sensor del paso del tiempo, se ha apagado. 

  

[![](https://1.bp.blogspot.com/-w1zS5pYNyCk/XnR5GpOh_DI/AAAAAAAAfD8/6PQAW6J7nD0EYSzvo90Wy5FaQVsjTnUJQCNcBGAsYHQ/s320/_MG_1496.jpg)](https://1.bp.blogspot.com/-w1zS5pYNyCk/XnR5GpOh_DI/AAAAAAAAfD8/6PQAW6J7nD0EYSzvo90Wy5FaQVsjTnUJQCNcBGAsYHQ/s1600/_MG_1496.jpg)