---
title: 'Candilejas (1952)'
date: 2012-03-01T18:34:00.000+01:00
draft: false
tags : [1950, Buster Keaton, Claire Bloom, Cine, Charles Chaplin]
---

[![](http://1.bp.blogspot.com/-OJhAbjH2KiM/T0-zJyYJJHI/AAAAAAAABP4/gteOvgswioE/s400/rostard.jpg)](http://1.bp.blogspot.com/-OJhAbjH2KiM/T0-zJyYJJHI/AAAAAAAABP4/gteOvgswioE/s1600/rostard.jpg)

La etapa como gran cómico de Calvero (**Charles Chaplin**) se ha acabado. Un día mientras el alcohol ahogaba sus penas, se encuentra con una joven desmayada tras un intento de suicidio. Calvero la lleva a su casa y la cuida generosamente despertando su afán por vivir. Es la bailarina Thereza (**Claire Bloom**) cuya suerte parecía haber desaparecido. Este inesperado encuentro cambiará la situación de ambos.

  

**Charles Chaplin** es otro de los grandes genios del séptimo arte. Famoso por enloquecer al mundo con mudas tragicomedias, es sorprendente encontrárselo sin su habitual atuendo en una película con habla. La historia que nos trae '**_Candilejas_**' no es simplemente un guión cualquiera de Chaplin, pues alberga contenido más que personal y autobiográfico. Su propia representación del papel de fracasado Calvero da en qué pensar.  Tardó dos años y medio en construir el guión, añadiendo nueve meses de composición de una inolvidable banda sonora.

  

La melancolía es una de las esencias del largometraje, que acompaña a todas las profundas reflexiones acerca de la vida, el amor, la frustración personal, la juventud, la compasión y la interminable lista de cuestiones que atañen al hombre y espectador. Hay algo encantador en esa sincera tristeza que deja traslucir **Chaplin**. Es una despedida muy conmovedora y entrañable. Sin embargo cargada de elegancia en las mejores dotes cinematográficas. Con una puesta en escena y fotografía excelente, un guión exquisito repleto de frases inolvidables, así como una banda sonora sublime y preciosa como aquellas que se encuentran en las memorias de los amantes del cine. 

  

El filme posee una sensibilidad especial, capaz de unos diálogos llenos de fuerza y optimismo atrayente, efectivo, además de auténtico y verdadero, procedentes de uno de los  inigualables creadores de belleza, sencillez y bondad en el cine.  Chaplin lo puso todo en sus obras, se dedicó por entero a una obra de inconmensurable valor. Son sus tres hijos los que aparecen en la primera escena y también es el mismísimo **Buster Keaton** a quién Chaplin contrata a pesar de haber sido rivales en el pasado del cine mudo. Ahora nostálgico y acabado aparece en '**_Candilejas_**'.  
  

**Chaplin** fue repudiado por su país tras esta película, por cuestiones legales. Así como olvidado por los Oscar de la Academia durante toda su carrera --otra de las grandes equivocaciones de **Hollywood**\--. ¿Pero qué le importa a un genio si es despreciado por la vanidad y ensalzado por millones de corazones? la historia hizo justicia. Una vez acaba el largometraje el público queda sumido en una considerable cavilación sobre la existencia. De lo mejorcito de la gran pantalla.