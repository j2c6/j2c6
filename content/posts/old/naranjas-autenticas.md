---
title: 'Naranjas auténticas'
date: 2018-04-16T18:26:00.002+02:00
draft: false
tags : [naranjas, calle, mendigos, fruta, vivir en la calle]
---

Total que voy con la bolsa de cartón de la perfumería en el regazo, llena de naranjas. Cogidas del campo y se las llevo a mis abuelos. De camino me encuentro con dos que están en la calle charlando. Uno tendido y el otro sentado. Y les ofrezco y uno primero y el otro después tienen su naranja. Dan las gracias. Le digo alegremente que son 'de las buenas', traídas del campo. Y asienten con sabiduría.

  

Naranjas grandes y esféricas. Un poco elefánticas en cuanto al tamaño. Y de un tono naranja apagado, real, como las fotos en RAW antes del procesado habitual.

  

Qué gusto dar fruta por la calle. Tratar fruta, gestionarla.