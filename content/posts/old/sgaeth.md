---
title: 'sgaeth'
date: 2020-09-12T11:32:00.003+02:00
draft: false
---

Horas y horas más tarde, descansando de la vida metidos en Instagram, en TikTok, en Twitter con un filtrado de 10% de cosas que te interesan, pero que no dejan ningún rastro en tu vida. Qué pasará cuando algunos lleven unos 40 años viendo lo más destacado en YouTube, o lo que sea que reine entonces. Hasta qué punto merece la pena y qué hacemos en su lugar. Tu a este paso no vas a escribir nada, te dices. Y tienes razón. Sabes las respuestas, pero no te importan una mierda. Para qué sirve todo eso. Y ya no es por llenarla de algo mejor, si no, por no llenarla de eso. Porque plastifica el cerebro.