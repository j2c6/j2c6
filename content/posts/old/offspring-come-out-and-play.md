---
title: 'Offspring - Come out and play'
date: 2009-05-01T10:16:00.000+02:00
draft: false
tags : [Punk, Rock, Offspring, Musica]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SfqwYPk6FWI/AAAAAAAAAcA/y241huhWjrc/s320/offspring.gif)](http://3.bp.blogspot.com/_VT_cUiG4gXs/SfqwYPk6FWI/AAAAAAAAAcA/y241huhWjrc/s1600-h/offspring.gif)  

Offspring, offspring, offspring….”_**Come out and play**_”. Estamos en los 90’, **punk rock** del siglo pasado. Los californianos están en nuestros oídos desde 1984… Esta canción es un temazo, se nota en sus riffs de guitarra melódicos, y pegadizos, sus gritos a pelo, dentro del ritmo a toda caña. **Offspring** son míticos. Es una canción de fiesta sin control, te dice diviértete, haz lo que quieras, esto es lo mejor, y se nota ese punteo inicial con la guitarra rara de **Dexter Holland**, “_pásalo bien, nosotros nos ocupamos de todo_”. El resto, es sin más: la típica batería rock “a saco”, los clásicos “corillos” de todos a la vez…Una canción perfecta. De este grupo hay muchos “hits” que descubrir, si todavía no lo has escuchado, te recomiendo otra suya y más famosa: “**The kids aren’t all right**”, ¡¡ como una bomba nuclear!! O si quieres también “_**The staring at the sun**_”. Bueno dale al play… ¡Venga!