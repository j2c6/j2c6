---
title: 'Arthur Miller - Muerte de un Viajante'
date: 2011-08-14T21:39:00.000+02:00
draft: false
tags : [S.XX, Muerte de un Viajante, Teatro, Marilyn Monroe, Arthur Miller]
---

![](http://www.biografiasyvidas.com/monografia/marilyn_monroe/fotos/monroe_miller_2.jpg)

Arthur Miller y Marilyn Monroe

  
  

"WILLY—Nos tienen rodeados, sin aire, sin horizonte... No vemos más que ladrillos y ventanas.

LINDA—Debimos haber comprado el terreno de al lado. Te lo dije. Entonces estaba barato.  
WILLY—La calle está llena de coches. No se respira más que gasolina. Debía haber una ley contra esas casas tan altas. ¿Te acuerdas de los olmos que había alrededor? ¿Cuándo le hice el columpio a Biff?   
LINDA—Entonces, esto parecía que estaba a cien kilómetros de la ciudad...  
WILLY—Debieron arrestar al constructor que taló los árboles... (Perdido en su pensamiento.) Cada vez que pienso en aquellos días, Linda... En esta época del año, los tilos y las acacias... Luego, echaban flor los narcisos... ¿Te acuerdas cómo olía esta habitación?"  

  

Arthur Miller. _Muerte de un Viajante_. Tusquets.