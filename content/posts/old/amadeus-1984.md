---
title: 'Amadeus (1984)'
date: 2012-07-03T19:20:00.000+02:00
draft: false
tags : [Amadeus, Mozart, Cine, Milos Forman, Tom Hulces, Salieri, Aleksandr Pushkin, F. Murray Abraham, Peter Shaffer, 1980]
---

Es a veces admirable el empeño o interés de una persona en ver una película que no ha visto en su vida, pero que tiene enormes ganas de tragarse. La he empezado varias veces pero, en ese momento no todos estaban de acuerdo con que fuera la película más apetecible. Tengo ciertas reglas -totalmente flexibles o más bien simbólicas- para ver cine, como _no verlo en solitario_, n_o escoger el filme al azar_, abstenerse en _condiciones no aceptables de comodidad y calidad_, etc.

  

El emblemático director de la memorable '_**Alguien voló sobre el nido del cuco**_' (1975), dirige '**_Amadeu_**_**s**_' en su tierra natal Checoslovaquia. No se trata de una biografía de Mozart, aunque sí se ven ampliamente muchas facetas de su vida, hechos históricos y especialmente una versión del mito de su muerte. Pocos personajes tan famosos tienen una obra maestra que los eternice aún más todavía. Pero no solo una versión de los hechos, considerablemente subjetiva, bastan para alcanzar el éxito, **Milos Forman** compuso una obra maestra. Ganadora de 8 _Oscar_.

  

[![](http://2.bp.blogspot.com/-G0gWrsTTKHA/T_MpsGXHMRI/AAAAAAAABfg/QeWe_YRYrdk/s640/large_433125.jpg)](http://2.bp.blogspot.com/-G0gWrsTTKHA/T_MpsGXHMRI/AAAAAAAABfg/QeWe_YRYrdk/s1600/large_433125.jpg)

  

'**_Amadeus_**' está basada en una obra de teatro de **Peter Shaffer**, basada a su vez en la obra de **Aleksandr Pushkin**, '_**Mozart y Salieri**'_ que retrata ligeramente ciertos aspectos de los famosos compositores. Pero la historia la empieza Salieri (**F. Murray Abraham**) desde el manicomio, de modo increíble con el legado musical de ambos compositores: '_¿conoce esta?, esta no es mía, la compuso Mozart_'. Una manera excelente de introducir su historia, cómo Mozart era un joven talento y él se moría de envidia desde que oyó su nombre hasta su muerte.

  

El texto puesto en boca de **Murray Abraham** es prodigioso, describe los detalles con una literatura encantadora. Y tampoco cabe despreciar su actuación que es magnífica, hace que hasta la traición y la envidia sean atractivas y fascinantes, hasta tratándose de **Mozart**.  Una actuación espectacular. Tiene esta narración un buen soporte y es la música clásica, indiscutiblemente sublime. Esta historia -totalmente ficticia- hace muy humano el argumento y lo ofrece de forma muy atractiva a un público ansioso por conocer su historia.

  

El personaje de Mozart (**Tom Hulces**) no es tan elaborado, pero dentro de la historia es peculiar e imprescindible. Se le presenta como un excéntrico de una risa nerviosa y sonora muy particular, pero que parece encajar perfectamente con su carácter: un talento prodigioso y sin precedentes. Y a partir de ahí, su relación va desarrollándose inteligentemente, pero siempre desde el punto de vista de Salieri. Desde su ruptura con Dios por haber dotado de un don superior a un '_jactancioso, lujurioso, obsceno e infantil muchacho_', hasta su plan para asesinar a Mozart. Como he citado el texto es insuperable, consigue que los sentimientos de Salieri sean únicos, aunque se trate de aspectos vividos por toda la humanidad.

  

[![](http://1.bp.blogspot.com/-DnO15wqrJg0/T_MptKSQO5I/AAAAAAAABfk/2RGL8kT-xAs/s400/rostard.jpg)](http://1.bp.blogspot.com/-DnO15wqrJg0/T_MptKSQO5I/AAAAAAAABfk/2RGL8kT-xAs/s1600/rostard.jpg)No sé qué decir. En sí es fascinante, que el traidor sea el único capaz de admirar y valorar verdaderamente la obra de su víctima, o que Mozart le pida perdón inocentemente a su enemigo, es irrepetible. También es grandiosa la muerte de Mozart y el comienzo del '_Lacrimosa_' del Requiem o elementos fantásticos como el fantasma de su padre. La época atrapa por su gran representación pero es el tema y la forma lo que conmueven y absorben el alma del público en una historia incomparable admiración-envidia. Podría tratarse de una tragedia de **Shakespeare**.

  

Además de todo lo que se siente cuando se ve esta obra maestra quedan la infinitud de detalles cuidados por el director y la producción, como el impecable montaje de sonido, la dirección artística, etc. En definitiva una película muy atrayente, que toca géneros muy diferentes en una historia mítica e interesantísima sobre Mozart y el inolvidable Salieri.