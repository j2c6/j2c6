---
title: 'Diario de Iniciación para Principiantes a la Búsqueda del Sabor Tomate-Tomate I'
date: 2019-12-30T10:37:00.002+01:00
draft: false
tags : [verdura, obsesiones, tomate, fruta]
---

A priori, un tomate monstruoso y mutante, tumoral y feo, poco homogéneo y muy carnoso: es ese el tomate perfecto. Por favor, por encima de 3€ el Kg.  
  
  

[![](https://1.bp.blogspot.com/-8ixxRj6VfTk/XgnBQAl_c7I/AAAAAAAAe4k/jWiZ2YrpOGgupRleJbHc4pArHPdJsNQKQCNcBGAsYHQ/s320/photo_2019-12-30_10-16-59.jpg)](https://1.bp.blogspot.com/-8ixxRj6VfTk/XgnBQAl_c7I/AAAAAAAAe4k/jWiZ2YrpOGgupRleJbHc4pArHPdJsNQKQCNcBGAsYHQ/s1600/photo_2019-12-30_10-16-59.jpg)

  
  
  
  
**Tomate Corazón de Buey**  
Fácil de reconocer por su forma, se trata de un tomate clásico. Jugoso y con sabor. Por lo visto, se encuentra maduro ya en su fase verdosa. Piel fina y pocas semillas.  
  

[![](https://1.bp.blogspot.com/-c1ho4E4cQPQ/XgnApUNc4EI/AAAAAAAAe3w/s7CAvEq_sgge1unmeumFMW-MMBz1qv9JQCNcBGAsYHQ/s320/photo_2019-12-30_10-17-01.jpg)](https://1.bp.blogspot.com/-c1ho4E4cQPQ/XgnApUNc4EI/AAAAAAAAe3w/s7CAvEq_sgge1unmeumFMW-MMBz1qv9JQCNcBGAsYHQ/s1600/photo_2019-12-30_10-17-01.jpg)

  

[![](https://1.bp.blogspot.com/-lOMFPuwNyUE/XgnApTq_BvI/AAAAAAAAe30/OKfxKMQaJ3sSlcNxGroGLu_Es365Mr0ewCNcBGAsYHQ/s320/photo_2019-12-30_10-17-02.jpg)](https://1.bp.blogspot.com/-lOMFPuwNyUE/XgnApTq_BvI/AAAAAAAAe30/OKfxKMQaJ3sSlcNxGroGLu_Es365Mr0ewCNcBGAsYHQ/s1600/photo_2019-12-30_10-17-02.jpg)

  
**Tomate de Mar Azul**  
Con ese tono amoratado y violeta para distinguirlos de sus congéneres más rojizos, el tomate azul es fruto de experimentos de laboratorio, donde se añadieron (y nos gusta pronunciar) genes de la flor Boca de dragón (Antirrhinum majus). Se producen en Andalucía, en Reino Unido y en Estados Unidos, y se venden en supermercados corrientes, por lo que son fáciles de encontrar. De textura compacta, tienen un sabor frutal y fiel al concepto mental de tomate.  
  

[![](https://1.bp.blogspot.com/-nUN2siCw738/XgnAuGMHL0I/AAAAAAAAe38/S4oYyCMXKlgM80MXDrlfAfvR2g28TlFhQCNcBGAsYHQ/s320/photo_2019-12-30_10-17-03.jpg)](https://1.bp.blogspot.com/-nUN2siCw738/XgnAuGMHL0I/AAAAAAAAe38/S4oYyCMXKlgM80MXDrlfAfvR2g28TlFhQCNcBGAsYHQ/s1600/photo_2019-12-30_10-17-03.jpg)

  

[![](https://1.bp.blogspot.com/-8VbaP1DCZqQ/XgnAudFc2fI/AAAAAAAAe34/c5WmCFWhJBEXUR5mZyaSk1J--Kp3zKRjACNcBGAsYHQ/s320/photo_2019-12-30_10-17-04.jpg)](https://1.bp.blogspot.com/-8VbaP1DCZqQ/XgnAudFc2fI/AAAAAAAAe34/c5WmCFWhJBEXUR5mZyaSk1J--Kp3zKRjACNcBGAsYHQ/s1600/photo_2019-12-30_10-17-04.jpg)

  
  
**Tomate Rosa de Barbastro**  
Tambien conocido como piel de doncella, tiene deformidades y cicatrices. El top de los tomates. Acostúmbrate a que pese más de 500gr, tal vez 800 ó 900 gramos. Fácil de partir por su fina piel, en su interior encontramos las pepitas distribuidas por todo el carnoso tomate.  
  

[![](https://1.bp.blogspot.com/-pcZG62ocFLg/XgnA0OQlruI/AAAAAAAAe4A/3ow17VWKackr8k08fZHUypGPEXB1BC-TQCNcBGAsYHQ/s320/photo_2019-12-30_10-16-57.jpg)](https://1.bp.blogspot.com/-pcZG62ocFLg/XgnA0OQlruI/AAAAAAAAe4A/3ow17VWKackr8k08fZHUypGPEXB1BC-TQCNcBGAsYHQ/s1600/photo_2019-12-30_10-16-57.jpg)

  

[![](https://1.bp.blogspot.com/-wpGol2ZkUxs/XgnAzw0USAI/AAAAAAAAe4E/d2tLcgGCkgAJIIt5HSqCtqL0MUAhg3_wgCNcBGAsYHQ/s320/photo_2019-12-30_10-16-58.jpg)](https://1.bp.blogspot.com/-wpGol2ZkUxs/XgnAzw0USAI/AAAAAAAAe4E/d2tLcgGCkgAJIIt5HSqCtqL0MUAhg3_wgCNcBGAsYHQ/s1600/photo_2019-12-30_10-16-58.jpg)

  

[![](https://1.bp.blogspot.com/-Y-R3r2Q7O1U/XgnBVxVfjWI/AAAAAAAAe4o/LpMd05oTogoGXrjun-HrQzNzPDo1OcIDwCNcBGAsYHQ/s320/photo_2019-12-30_10-17-00.jpg)](https://1.bp.blogspot.com/-Y-R3r2Q7O1U/XgnBVxVfjWI/AAAAAAAAe4o/LpMd05oTogoGXrjun-HrQzNzPDo1OcIDwCNcBGAsYHQ/s1600/photo_2019-12-30_10-17-00.jpg)

  

  

**Tomate Rosa corriente**

Pues es un engaño, es considerablemente más pequeño, tal vez entre unos 200 y 300 gramos de media. Tiene ese aspecto michelín pero en el sabor ni se acerca a su hermano rosa de Barbastro o de Altea.

  

  

[![](https://1.bp.blogspot.com/-Ahuj8KOaMVk/XgnA7016x8I/AAAAAAAAe4M/ekrA6W6amu4-DimAcltqbAj86Ywu1HvagCNcBGAsYHQ/s320/photo_2019-12-30_10-16-56.jpg)](https://1.bp.blogspot.com/-Ahuj8KOaMVk/XgnA7016x8I/AAAAAAAAe4M/ekrA6W6amu4-DimAcltqbAj86Ywu1HvagCNcBGAsYHQ/s1600/photo_2019-12-30_10-16-56.jpg)

  

Claramente se ve la diferencia de calibre:

  

[![](https://1.bp.blogspot.com/-n8s9uGPYAbk/XgnA71CqYhI/AAAAAAAAe4I/FSafSYaTgI42HATu0OdpoYt7fmSSeC5BgCNcBGAsYHQ/s320/photo_2019-12-30_10-16-54.jpg)](https://1.bp.blogspot.com/-n8s9uGPYAbk/XgnA71CqYhI/AAAAAAAAe4I/FSafSYaTgI42HATu0OdpoYt7fmSSeC5BgCNcBGAsYHQ/s1600/photo_2019-12-30_10-16-54.jpg)

  

  

Así es como lo ingerimos actualmente. No me importa tanto qué mozzarella sea, todavía no encontramos una burrata de bufala decente:

  

  

[![](https://1.bp.blogspot.com/-TGtHS0GBMO0/XgnBLH2FFWI/AAAAAAAAe4Y/h0WRochUzzcGIVV0sd0fdIKpso1qY1gBQCNcBGAsYHQ/s320/photo_2019-12-30_10-17-00%2B%25282%2529.jpg)](https://1.bp.blogspot.com/-TGtHS0GBMO0/XgnBLH2FFWI/AAAAAAAAe4Y/h0WRochUzzcGIVV0sd0fdIKpso1qY1gBQCNcBGAsYHQ/s1600/photo_2019-12-30_10-17-00%2B%25282%2529.jpg)

  

Pimientas negra, verde, rosa y blanca, orégano, albahaca y hierbas provenzales en general. Un buen aceite de oliva y algo de vinagre de módena, ah, y sal del himalaya.