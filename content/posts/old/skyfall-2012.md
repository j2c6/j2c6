---
title: 'Skyfall (2012)'
date: 2012-11-19T20:20:00.000+01:00
draft: false
tags : [George Lazenby, Adele, Javier Bardem, Daniel Craig, Skyfall, Pierce Brosnan, Jack White, Sean Connery, Berenice Marlohe, Timothy Dalton, Alicia Keys, Cine, Roger Moore, 2010, Sam Mendes]
---

[![](http://1.bp.blogspot.com/-kpS8on7-vsg/UKqGZvlOVfI/AAAAAAAABlU/BagKusHPAhg/s400/rostard.jpg)](http://1.bp.blogspot.com/-kpS8on7-vsg/UKqGZvlOVfI/AAAAAAAABlU/BagKusHPAhg/s1600/rostard.jpg)No se si ya lo he mencionado, pero uno de los deportes nacionales es la crítica no constructiva -más común que la sencillamente constructiva-. Llevo algún tiempo sin hablar de las películas que veo porque me da la sensación de que lo que digo lo puede decir cualquiera y porque muchas veces me dejo llevar por prejuicios e ideas preconcebidas, y después de todo ¿qué sé yo sobre cine? Otra de las razones es porque casi lo hago para rellenar espacio y tampoco debería tener esa necesidad. En cambio si tengo la necesidad de escribir lo que me ha parecido la maldita película, y aunque muchas veces no tenga una opinión demasiado fundada, en cualquier caso, necesito expresarme.

  

No he visto ninguna película de las del famoso agente **James Bond**, y digo ninguna aunque he visto la de '_**Quantum of Solace**_', que a pesar de no contarla como si fuera de la saga el tema de la película de manos de **Jack White** y **Alicia Keys** es todo sensación, una obra valiosa: '_**Another way to die**_', muy recomendable.  Lo poco que sé de 007 es la sustitución de actores y la abundante cultura general que existe al respecto. Supongo que **Sean Connery** y **Pierce Brosnan** -así como **Timothy Dalton, Roger Moore, George Lazenby**\- supieron llevar la 'placa' con orgullo y elegancia, pero la representación de **Daniel Craig** no me parece en la misma línea característica que los anteriores, no me parece que le siente tan bien el papel. Tendría mejor acogida sí completara la trilogía de '**_Los Mercenarios_**' pero este no me convence como James Bond.  
  
Luego la película en sí no me pareció divertida, y ojalá se acercara a entretenida para perdonarla. Vi demasiada acción gratuita sin la magia e imaginación del Bond que se esperaba. Me molestó que fuera tan plana y sin sentido, claro que las críticas de los carteles son siempre del estilo de . "el mejor Bond". Y si **Javier Bardem** -que realmente consigue ser un villano repugnante- es el mejor villano, evidentemente el nivel no es demasiado alto.   
  
Seguiría criticando que el tema de **Adele** no fue especial, y que la elección del famoso personaje de 'la chica Bond' _(_**Berenice Marlohe**_)_ no me convenció, pero sinceramente no quiero perder más tiempo del que perdí en el cine. Claramente le han suplicado el proyecto al renombrado **Sam Mendes** (**_American Beauty_**, **_Revolutionary Road_**) o le hacía falta un trabajo. Estos proyectos no nos entiendo.