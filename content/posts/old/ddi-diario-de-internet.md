---
title: 'DDI: Diario de Internet.'
date: 2020-04-26T13:28:00.000+02:00
draft: false
tags : [codebreakers, palm, criptografía, david lynch, twin peaks, ddi, Series]
---

  

Para simplificar la rutina, he decidido --no sé cuánto tiempo durará--, empezar un Diario de Internet, de manera que pueda documentar y resumir lo que vagueo por la red, para poder revisarlo luego, recurrir a ello y vaciarme la cabeza. No sé quién dijo lo de, '_**si puedes tenerlo en un papel, sácalo de la cabeza**_'.

*   El Palm Compact Phone: ¿cuándo dejarán de hacer teléfonos enormes y empezarán de nuevo a hacer móviles compactos y confortables? Ese tipo de respuestas me gustan.
*   ¿Hay que ver las dos primeras temporadas de Twin Peaks para ver la nueva (2017)?Aparentemente, es un _must_, pero argumentalmente, por lo visto los que las vieron, siguen igual de perdidos.
*   Hay una [lista](https://www.imdb.com/chart/toptv?ref_=tt_awd) de las mejores series según IMDB.
*   _**The Codebreakers: The Comprehensive History of Secret Communication from Ancient Times to the Interne**_t de David Kahn. Parece un imprescindible para todos los que quieran conocer la historia de la criptografía.