---
title: 'Celda 211, Al Limite, Shutter Island'
date: 2010-03-20T13:46:00.000+01:00
draft: false
tags : [Leonardo DiCaprio, Mel Gibson, Luis Tosar, Cine Español, Shutter Island, Cine, Entretenidas, Celda 211, Martin Scorsese]
---

**Celda 211**

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/S6TDT6UU4bI/AAAAAAAAAp8/InGFUPGtqt0/s320/celda-211-fin.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/S6TDT6UU4bI/AAAAAAAAAp8/InGFUPGtqt0/s1600-h/celda-211-fin.jpg)

¿La ganadora de siete premios Goya  realmente es buena? Pues para la sorpresa de los amantes del buen cine, una película española como la **C_elda 211_** es realmente otra modalidad dentro del cine nacional, podríamos considerarla dentro del apartado de películas distintas, del degenerante cine español. En el thriller se nota una tensión, a lo largo de los 110 minutos que dura: Juan queda atrapado en una cárcel, en la que acaba de iniciarse un motín. Tendrá que hacer lo imposible para que el resto de los presos no se percaten de que es un guardia en  su primer día de trabajo. Lo cierto es que hay personajes muy bien desarrollados como es el de _"**Malamadre**"_ (**Luis Tosar**), que hace trabajar un buen guion con un final desconocido hasta el ultimo minuto. Si: es española; si: es buena; sorprendentemente... En resumen esta película, ha cambiado el horizonte (por suerte) del cine ibérico, y simplemente os la recomiendo solo por el hecho de que sea española, y sea interesante.

  

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/S6TDXUs1HtI/AAAAAAAAAqM/FOtHnq3UqdM/s320/mel-gibson-al-limite-poster.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/S6TDXUs1HtI/AAAAAAAAAqM/FOtHnq3UqdM/s1600-h/mel-gibson-al-limite-poster.jpg)  

**Al Limite**

Ocho años son los que llevaba **Mel Gibson** desaparecido del objetivo de la camara, lo cual nos hace estar expectantes... La ultima película de Mel como actor, nos nos ha dejado indiferentes, claro que estamos hablando de un thriller como los de siempre, que como siempre nos hará pasar un buen rato. El asesinato de la hija de  Thomas Craven (un detective como  Mel Gibson),  hace que la situación llegue hasta el extremo, ya que Thomas haría cualquier cosa por su hija. Este dato pone las cosas interesantes... A pesar de todo el montaje, presupuesto, y la esperada aparición del actor, no obtiene muy buenos resultados, resulta una película poco original, que simplemente nos entretendrá recordándonos otras mejores (_**El Fuego de la Venganza**_ de **Denzel Washington**; _**Venganza**_ de **Liam Neeson**)  : nada imprescindible. Sin embargo no esta mal...

  

**Shutter Island**

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/S6TDWNgBbtI/AAAAAAAAAqE/97RoI6N6gp8/s320/shutter_island_ver2_xlg.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/S6TDWNgBbtI/AAAAAAAAAqE/97RoI6N6gp8/s1600-h/shutter_island_ver2_xlg.jpg)  

Basada en la novela de **Dennis Lehane**, este filme de **Scorsese** nos presenta quizás otro tipo de miedo... Claro esta que el tema de una cárcel para enfermos mentales, influye mucho, sobretodo si está en una isla solo para ellos: _**Shutter Island**_. El detective _Teddy Daniels_ (**Di Caprio**), acompañado de _Chuck Aule_ (**Mark Ruffalo**), se encargará de la investigación de la desaparición de una de las presas conocida como Rachel. Pero esto no sera tarea fácil, ya que destapar esto incluirá el descubrimiento de muchas otras cosas que no todo el mundo quiere que se sepan. El largometraje a pesar de no ser el mejor dúo _Scorsese-DiCaprio_, (aunque muy bueno), desarrolla una atmósfera de  incertidumbre, miedo, tensión, y locura que hacen que el guión dé vuelcos terribles, que harán al espectador saltar de su asiento varias veces. Una gran película de la que se puede extraer su propia conclusión... Un peliculón a la sombra de la actuación de Leonardo, que lo hace único. _Peli_, palomitas, play...