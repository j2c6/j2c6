---
title: 'Acción Civil (1998)'
date: 2012-03-31T13:16:00.000+02:00
draft: false
tags : [Robert Duvall, 1990, Captain Beefheart, Conrad L. Hall, Steven Zaillian, Cine, Zeljko Ivanek, Tony Shalhoub, Danny Elfman, John Travolta, William H. Macy]
---

[![](http://1.bp.blogspot.com/-OkO4anb-SRs/T3bnd8kuZxI/AAAAAAAABUM/H19e3BJ8RlA/s400/rostard.jpg)](http://1.bp.blogspot.com/-OkO4anb-SRs/T3bnd8kuZxI/AAAAAAAABUM/H19e3BJ8RlA/s1600/rostard.jpg)"Una de las mejores películas sobre el derecho de daños". Pues si eso es así, me temo que el derecho de daños deja mucho que desear. No soy muy partidario de la justicia legal, y me gusta que se refleje en los filmes -sin abusos y alardes de corrupción-  lo que en realidad se mueve en los bolsillos de la 'justicia'.

  

Los personajes inteligentes y astutos -o al menos con esa fachada- abundan en este tipo de largometrajes, siempre atraen: elegantes, ambiciosos y dispuestos a lo que sea.  La partida que se juega durante los juicios puede ser muy interesante, el uso de las leyes adecuado a ciertos propósitos, la elección de los testigos y la pose, dan lugar a un auténtico desfile legal que atrae. La película de referencia puede ser '**_El jurado_**' (2003), que es notable, muy entretenida y con buenas interpretaciones. 

  

En '**_Acción civil_**' se pretende todo eso, aunque quizá es mucho más de lo que realmente han conseguido. **John Travolta** es sin duda una buena opción pero ha quedado poco poderoso. En cambio **Robert Duvall** se presenta impecable, un papel muy cuidado que le valió una nominación. Pero ¿qué nos importan las nominaciones? Estoy intentando quitarme las cadenas de la esclavitud a la crítica y a las galas de premios, no quiero ceder ante unos que se hacen llamar críticos solo porque han visto cosas mejores, o no disfrutaron lo que yo. Esto es subjetivo con cierto aire de realidad, pues como dice **Jerome Facher**  (Robert Duvall) "la verdad solo la encontrarás en un pozo hondo y oscuro" o algo así de revelador. _Dixi_.

  

El resto del reparto también tiene su presencia, hemos visto a **Tony Shalhoub, William H. Macy, Zeljko Ivanek**, pero tampoco haré especial hincapié en ellos. El largometraje de **Steven Zaillian**, experto guionista y esporádico director ('_**En busca de Bobby Fisher**_' o '**_Todos los hombres del rey_**'), tiene un prometedor principio: acaba un caso y suena el tema peliculero de '_**Hard Workin' man**_' de **Captain Beefheart**, así. Luego se va desinflando a pesar de las interesantes dosis acerca de como ganar el caso y la posesión del triunfo.  
  

Se supone que se opera un cambio radical en el protagonista pero en realidad es fruto de las irremediables circunstancias a las que lo ha arrojado su orgullo. Basada en hechos reales finalmente cuenta cómo el tiempo hace justicia en cada personaje. Es una desgracia a pesar de las citas del guión, la música de **Danny Elfman** que no recuerdo, la puesta en escena y fotografía de **Conrad L. Hall**. No pasa de ser algo sobrio y corriente.