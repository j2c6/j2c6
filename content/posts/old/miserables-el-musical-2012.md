---
title: '''Los miserables: el musical'' (2012)'
date: 2013-01-31T17:01:00.000+01:00
draft: false
tags : [Bigelow, Anne Hathaway, Eddie Redmayne, Los Miserables, Amanda Seyfried, Tom Hooper, Hugh Jackman, Bin Laden, Django, Tarantino, Russell Crowe]
---

[![](http://2.bp.blogspot.com/-zt0OFPncEYg/UQqVLnA6oVI/AAAAAAAABus/1UrF9Z7m2mA/s1600/rostard.jpg)](http://2.bp.blogspot.com/-zt0OFPncEYg/UQqVLnA6oVI/AAAAAAAABus/1UrF9Z7m2mA/s1600/rostard.jpg)

Uno va al cine con una idea -la que sea, hay donde elegir- y en la sala ya se decepciona. '**_Los miserables: el musica_l**' es una de esas obras muy exhibidas por la productora: filtran fotos del rodaje, que dan rienda suelta a una imaginación normalmente exigente, se observa a **Russell Crowe** archirrelacionado con '_**Gladiator**_' y otras por el estilo; se observa a un afectado **Hugh Jackman** y a una desapercibida -sorprendido por su aparición- **Anne Hathaway**. Esperaba que **Tom Hooper** ('_**El discurso del rey**_', 2010) tuviera alguna idea genial para adaptar la inadaptada novela de **Hugo**, ya que la versión de **Neeson** y **Rush** \-excelente este último en el papel de _Javert_\- no aporta demasiado. La expectativas crecen: primer error.

  

Y en efecto un tal vez no tan abortado _Valjean_ -siendo muy bondadosos- comienza de una forma desconcertante, pero obtiene el tiempo que donamos gratuitamente a las obras para que se expliquen. Nada asombra ni decepciona, merece sus treinta minutos de atención incondicional. Pero a continuación ¡oh terrible fortuna! a continuación una versión sin precedentes de _Fantine_, cuya cara es la de una trágica e irreconocible **Hathaway** -como debe ser- que interpreta el papel de la desgraciada madre que se vende por su hija. '_**I dreamed a dream**_' despierta esos escalofríos que tantas actrices mediocres habían adormecido con sus nimios trabajos. El listón del filme asciende como las almas en pena bajan a los infiernos.  
  
¿Y después de eso? Después nada, puede quemar la cinta si no tiene nada mejor que hacer. La ausencia de conexión entre escenas, la mediocridad musical y la falta de preparación de los intérpretes (**Crowe** va de mal en peor, **Seyfried** poco convincente y cómico el resultado de la actuación de**Redmayne**como si el papel de _Marius_ no fuera importante), la hiperactividad de los planos y la cámara aburren al espectador, le dan náuseas y le faltan al respeto al mismo tiempo que se llenan sus sucios bolsillos. No tiene importancia, pero lamento haber sido engañado por 'la crítica publicitaria'. Sí, a ti te pregunto espectador falto de exigencia, con esas caras, ¿cómo podrían no haberte satisfecho?

  

Realmente, si se trataba de satisfacer a los amantes del musical y escupir al séptimo arte sin servirse de sus numerosas posibilidades, ahora lo entiendo todo. Pero deciden hacer una feria musical y pusilánime con caras famosas, una elección horrible. El musical de Broadway es excelente para cumplir ese cometido, y ya que en esta nueva versión no existe ni siquiera esa ventaja o juego que tiene el cine, uno se da cuenta del fracaso cuando pensaba que sabía lo que iba a ver, ninguna transmisión de sensaciones finalmente -a excepción de la brillante decena de minutos de Hathaway, que me alegro de que al menos sirva para impulsar su carrera y hacer una gran aportación al mundo-. El desengaño es completo, cada vez es más difícil ir al cine y obtener nuevas ideas o no aburrirse con vulgaridades comunes. ¿Donde está el rescate de aquellas ideas, o la búsqueda de formas nuevas?  
  
Llevo una época mala de asistencia al cine. No me gusta que hasta **Quentin** **Tarantino** haya perdido su originalidad y el necio espectador perdone su estulticia riendo: "_es que se trata de Tarantino_", como en el caso de la modesta y sobrevalorada '_**Django unchained**_'. Tampoco me gusta que la crítica proteja fanáticamente el cine inhumano como en el caso de '**_Zero Dark Thirty_**', fría e insignificante, documental e insípida, nunca artística y en contra de los admiradores, llena de clichés y publicidad pro-americana.