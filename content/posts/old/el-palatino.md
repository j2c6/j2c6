---
title: 'El Palatino'
date: 2010-05-04T16:10:00.000+02:00
draft: false
tags : [Arte, Los Grandes, Literatura, La Escalera Que Quemaba Ríos de Tinta, El Palatino]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TAkJNJ6U5tI/AAAAAAAAAuQ/Punnizdu8Kk/s320/villa-deste.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TAkJNJ6U5tI/AAAAAAAAAuQ/Punnizdu8Kk/s1600/villa-deste.jpg)  
  
  

  
  

Siempre se ha hablado de las contribuciones que ha hecho el hombre a la propia humanidad. Se ha admirado a personas por sus palabras, sus discursos, sus trazos, sus hallazgos... Pero creo que no es suficiente, hay determinadas personas cuya aportación a la humanidad es tal, que constituye un bien y no tan solo un mérito. Estos héroes del **palatino** de la humanidad, entrarán en el reino de los cielos directamente, por el gran bien que han hecho: exacto, pasaporte libre. ¿Entonces el arte, y el humanismo no son únicamente caprichos, o novedades agradables y bellas? Me temo que no. El arte es el lenguaje de la belleza y la belleza la letra "B" de bien. (Se nota que no soy ningún experto). **El palatino** estará lleno de pensadores, músicos, artistas... Se oirán los acordes más sublimes descubiertos por la humanidad, la luz allí será más intensa... Desgraciadamente,  han sido rechazados por la humanidad y apartados, no han sido comprendidos por los de su tiempo, la locura y una melancolía ambiciosa se apoderaron de ellos, haciéndolos aparentemente insensibles a lo exterior... En fin el arte no es el único tipo bien, pero si una parte importante... Saborea cada instante. El palatino...