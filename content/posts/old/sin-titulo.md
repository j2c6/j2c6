---
title: 'Sin título'
date: 2020-04-14T18:11:00.000+02:00
draft: false
tags : [velada literaria, ficcion, relato]
---

Un periódico local la describió como ‘hecha a su manera’. La comparaban con la protagonista de ‘Una mujer bajo la influencia’. A la caída de la tarde, los vientos alisios refrescaban el prado y los campos. Ella, salía con su vestido negro de lunares y el bolso de cuero. Su sombra proyectada sobre la calzada era sobrepasada por los coches que a veces pitaban al verla de camino. Ella saludaba alegremente, pero nunca se detenía a charlar. La pendiente de la carretera descendente la obligaba a bajar acelerada, con prisa, pero no llegaba tarde a ningún sitio.

  

Tiempo después, cuando ni siquiera ella bajaba ya por la cuesta, la tradición sugería tocar la bocina al llegar a ese tramo. Esto lo sé porque yo misma pasé por esa carretera con el autobús del colegio muchas veces y la conductora del autobús, hacía sonar el cláxon con gran solemnidad. Por aquel entonces no tenía ni idea de qué era lo que pasaba. Durante años pensé que era una cuestión de visibilidad, alguna mierda de dobles sentidos y prevención de accidentes.

  

Yo estaba por aquel entonces obsesionada con Dublín, había que ir a Dublín como sea. La primera oportunidad la ofreció el colegio, con un programa de esos en los que vas a parar a una familia de acogida. Pero nada, no hubo suerte. Se fueron dos de la clase y volvieron pavas perdidas. Habían estrenado en el cine, ‘Bailar en la Oscuridad’ y mi color favorito era el azul. Ese año suspendí todas menos dos. De mayor quería ser abogada.

  

Con veintitrés, iba con mi novio en el coche de su hermano y subimos por aquella cuesta. Hacía sol y se oía el motor con sonido de alta fidelidad. Entonces vi el cartel y lo entendí todo. Entendí porqué todos, sin excepción, hacían ese homenaje sonoro, aunque hubieran olvidado el motivo. Sin embargo, nunca dejará de sorprenderme que esa mujer, la protagonista de esa historia, fuera mi madre.