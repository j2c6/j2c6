---
title: 'Misery'
date: 2010-07-19T06:05:00.000+02:00
draft: false
tags : [Misery, Cine, Muy Buenas, Stephen King, James Caan, Kathy Bates, Rob Reiner, Soy tu fan número uno]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TEB1lYrxJtI/AAAAAAAAAwE/MdFwGylpolY/s400/large_misery_blu-ray1.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TEB1lYrxJtI/AAAAAAAAAwE/MdFwGylpolY/s1600/large_misery_blu-ray1.jpg)

Ya la había visto pero disfruté muchísimo al verla otra vez. La pesadilla de **_Stephen King_** se hace realidad en la película de **_Rob Reiner_**. Mi teoría es que la primera vez que ves una película estás a los pies del director y a las órdenes de lo que quiera hacer contigo, eres un naufrago en un laberinto: nunca antes habías visto cosa semejante, por eso te asustas, gritas, lloras, ríes... Sin embargo a la segunda vez ya estás prevenido, admiras la obra eres consciente, y más maduro respecto a lo que estás viendo: me encanta. El famoso escritor de la saga "**_Misery_**" _Paul Sheldon_ (**_James Caan_**), ha tenido un accidente mientras iba en su _Mustang_ a publicar la última novela de la saga (en la que mata a **_Misery_**, ya que está harto de desperdiciar su talento en comedias románticas)... Por suerte la agradable _Annie Wilkes_ (**_Kathy Bates_**) ex-enfermera, lo encuentra y lo acoge en su casa. Cuando _Paul_ despierta del golpe, se encuentra atendido por una auténtica lectora de su saga y una de sus mayores fans. La cosa se complica cuando _Annie_ descubre que **_Misery_** muere en la próxima novela inédita...  Lo cierto es que ha pesar de la tensión y el suspense que crea esta obra maestra, no dejas de reír misteriosamente sin dejar de repetir: "**_Soy tu fan número uno_**". Una película de las buenas para recordar con palomitas.