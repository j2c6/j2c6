---
title: 'Supercomputadoras sin llaves para entrar en casa'
date: 2020-03-06T10:22:00.000+01:00
draft: false
tags : [Woody Allen, Barcelonaqbit, Quantum Flagship, llaves, Procesamiento del Lenguaje Natural, transhumanismo, yorokobu]
---

Antes de irnos a Lisboa, teníamos previsto, que yo, en mi cómodo teleciervo o telecé, hiciera unas berenjenas rellenas. Antes de salir de casa, allá las 13 horas para comprar, dudé de si dejar encendido el horno para precalentar, pero no lo hice. Con la buena suerte de que un minuto después, me encuentro con la puerta cerrada por fuera y las llaves dentro. Decidí, en mi inesperada aventura, no ir a comprar y acercarme al Matadero de Madrid, a mirar las cosas de otra forma. Un día soleado, nubes con forma de espuma grandiosa y caducada. Total que me encontré con un viejo número de la revista de Yorokobu: Nº110 - 2019 "Me interesa el futuro, porque es el sitio donde voy a pasar el resto de mi vida" Woody Allen. Y creo que fue providencial que me lo encontrara, pues tengo la sensación de que los temas de la revista, tendrían una gran influencia en el curso de mi vida. Los apunto brevemente:

*   Computación cuántica. Barcelonaqbit. Guerra entre EEUU y China. Quantum Flagship. Seguridad. Encriptación.
*   Procesamiento del Lenguaje Natural (PLN), la necesidad de progresión en este aspecto en español. Natural Language Understanding. Dail Software y LeoRobotIA.
*   Intermediador para supercomputadoras, introducir la humanidad y sensibilidad en estos sistemas inteligentes.
*   Anya Bernstein, The Future of Immortality: Remaking Life and Death in Contemporary Russia. Nikolai Fiódorov, el transhumanismo. Bogdanov. Criogenización humana. Mind uploading. Poshumanismo.