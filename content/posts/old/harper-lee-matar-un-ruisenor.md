---
title: 'Harper Lee - Matar un Ruiseñor'
date: 2011-02-11T11:08:00.000+01:00
draft: false
tags : [Atticus Finch, Harper Lee, Scout Finch, Muy Bueno, Premio Pulitzer, Literatura, Matar un Ruiseñor, Maycomb, Jem Finch, Novela]
---

[![](http://3.bp.blogspot.com/-wm2Vpw3BAb8/TVUKBl9tM3I/AAAAAAAAA5Y/l1olglz790A/s320/2472000003_764f9fddf5.jpg)](http://3.bp.blogspot.com/-wm2Vpw3BAb8/TVUKBl9tM3I/AAAAAAAAA5Y/l1olglz790A/s1600/2472000003_764f9fddf5.jpg)  

Hay personas que tienen un misión en la vida que en ocasiones se concreta en tener algo que decir. La escritora **Harper Lee** escribió su famosa novela "Matar un Ruiseñor" en 1960, y ganó el premio **Pulitzer**  en 1961 por un público muy sonriente ante semejante obra. Poco después Harper Lee concedió unas pocas entrevistas y ahora lleva una vida retirada tras una etapa de lucha contra los casos de racismo. **Scout** es una niña de seis años que vive en **Maycomb** una pequeña ciudad de Alabama. **Jem** es su hermano mayor con quien pasa la mayor parte del tiempo. Atticus es su padre, un abogado del condado de Maycomb que ahora defiende a un negro en el juzgado. 

  

  

La novela "**Matar un Ruiseñor**" ambientada en una zona muy "regional" y concreta, es una de esas novelas en las que necesariamente no pasa nada, nada es accesorio y al mismo tiempo nada es importante, es simplemente el placer de la lectura. Al igual que otras veces la perspectiva inocente de una niña  como Scout pinta los acontecimientos de forma sencilla, de modo que saltan sentimientos muy vivos contados con mucha gracia. A lo largo de su lectura va introduciendo descubrimientos de las aventuras de Jem, Dill y Scout especialmente sobre cómo debe ser **Boo Radley** y la vida de los vecinos de Maycomb con todas sus peculiaridades. La fascinación por Boo Radley está presente en todo el libro, de modo que se mitifica y se convierte en una alternativa constante al resto de los acontecimientos. Las cuestiones del valor, la fortaleza, la compasión, el perdón... tan profundas en el corazón de una encantadora niña como Scout ablandan al lector, explicando además las suposiciones sobre el pensamiento de Jem: narrando un aprecio fraternal entre hermanos.

  

**Atticus** quedó viudo a dar a luz su mujer a Scout, Jem todavía recuerda esta pérdida. Atticus es un hombre recto, honesto, caballeresco y admirable, podría ser el héroe del libro. Educa a sus hijos de una manera sencilla pero eficaz, que daría un gran vuelco a la sociedad actual. La educación la calificaría de conservadora y liberal al mismo tiempo (me pareció bien esto cuando lo oí), rompiendo por ello algunos tópicos y tendencias de la época, siendo justo ante todo, hablando con franqueza a sus hijos en todo momento, y mostrándoles de manera simple lo que está bien y lo que está mal con un talento inusual. Atticus humilde y entrañable, se reserva en un personaje aparentemente seco y lacónico, pero despierta sorprendentemente un cariño filial de Jem y Scout hacia él increíble, recordando así que lo bueno de algún modo prevalece aún silencioso. 

  

La cuestión de razas barniza el libro, con la defensa a manos de Atticus de Tom Robinson, un negro acusado de una violación. Todo el condado, junto con sus familiares están en contra de su defensa, ya que opinan que además de conservar sus ideales está cavando su propia tumba. Pero Atticus es un hombre de una pieza, y aunque esto ocasione ciertos altercados verbales a sus hijos en la escuela, es capaz de sobrellevar la situación y de ayudar a Jem y Scout a hacerlo también. Calpurnia lleva casi la "voz negra cantante", queriendo y sirviendo a la familia Finch y participando en la educación de los hermanos.

  

Salen a relucir de nuevo las diferencias sociales, encarnando casi esta cuestión Tía Alexandra con sus reuniones de damas de Maycomb y su carácter clasista: recordando en todo momento la genealogía de las familias de Maycomb y su ritmo de vida, así también el cómo debería ser el desarrollo y educación de Scout Finch aspirante a ser una dama y sus luchas para que accediera a llevar vestido y no ese mono masculino que tanto despreciaba.

  

A través de las páginas y una vez conocidas profundamente las personalidades de los protagonistas el libro los va desarrollando, y deja ver su evolución especialemente la de Jem y Scout, que mezclándolos con la cuestión del juzgado descubren el mundo y cómo es en realidad, comprenden que los cánones que siempre habían tenido presentes, son violados en ocasiones y las leyes y referencias de Atticus sobre cientos de cosas, por desgracia no siempre se respetan como debe ser. 

  

Sobre el título, solo se hace referencia a él dos o tres veces a lo largo de la novela, eso me gusta. Atticus les regala dos rifles a Jem y Scout y les dice que pueden disparar a cualquier pájaro pero que "matar a un ruiseñor es pecado", esto lo concreta Miss Maudie diciendo que los ruiseñores no hacen daño a nadie, solo agradan con su canto. Matar a un ruiseñor se verá metafóricamente a lo largo de la obra, denunciando el pecado de ir a por el inocente sin defensa como **Tom Robinson.**

  

Esta novela me ha encantado por el disfrute que conlleva leerla  y por su forma de decir las cosas, es preciosa. 

_"A veces pienso que como padre he fracasado en absoluto, pero soy el único que tienen. Antes de mirar a nadie más, Jem me mira a mí, y yo he procurado vivir de forma que siempre pueda devolverle la mirada sin desviar los ojos... Si consintiera en una cosa como ésta, francamente, no podría sostener su mirada, y sé que el día que no pudiera sostenerla le habría perdido. Y no quiero perder a Jem ni a Scout; son todo lo que poseo."_

  
Los JET de Plaza & Janes. Sin año.