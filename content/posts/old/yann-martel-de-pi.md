---
title: 'Yann Martel - ''Vida de Pi'''
date: 2012-03-13T15:23:00.000+01:00
draft: false
tags : [Pi Pattel, Supervivencia, XXI, Ang Lee, Naufragio, Yann Martel, Literatura]
---

**Yann Martel** es un escritor nacido en Salamanca, aunque es considerado canadiense. '**_Vida de Pi_**' ganó el premio **Booker** en 2002. Es un libro narrado de forma peculiar, pero de modo sencillo y claro como lo haría un niño. Dentro de las ocurrencias en las que se detiene se encuentra interés a todo, con cierto ingenio en sus observaciones.

Relata la historia de Pi, que empieza desde la lucha por la confusión a la que lleva su nombre, pasando por su interés por la religión y finalmente el naufragio que acarreará todo el libro. Claro que todo esto viene acompañado por una amplia dosis de zoología. Reconozco que al principio es para quedarse extrañado y poco después boquiabierto con la imaginación de este hombre, sus comparaciones y forma de entender las cosas.

Después descubres en primer grado que simplemente te cuenta una historia, e incluso es posible observarla con cierta profundidad, concretamente en los hallazgos y sufrimientos que le acontecen mientras cruza el Océano Pacífico en un bote salvavidas con un tigre de Bengala, algo inusual. Sin duda está llena de creatividad, y es para pasar un rato agradable por no decir intenso ya que se te contagia hasta el mareo padecido en alta mar.

Como colofón podría comentar hasta dónde llega la vida humana cuando se acaban los recursos básicos, cual es su progreso --o retroceso por sobrevivir-- y sus reflexiones y pérdidas mientras camina esperanzado hacia la salvación o hundido hacia la muerte. No sin un interesante final que deja la incertidumbre en el aire. Por cierto se espera para 2012 una adaptación cinematográfica de **Ang Lee**

_"Cuando has sufrido mucho en la vida, cada dolor adicional es tan intolerable como insignificante.  Mi vida es como un cuadro_ memento mori _del arte europeo: siempre aparece una calavera sonriente a mi lado para que nunca me olvide de la locura de la ambición humana. Yo me burlo de la calavera. La miro y le digo: "Te has equivocado de hombre. Tú quizás no creas en la vida, pero yo no creo en la muerte. ¡Aire!". La calavera se ríe y se me acerca todavía más, pero tampoco me sorprende. La razón por la que la muerte se aferra tanto a la vida no tiene nada que ver con la necesidad biológica; lo hace por envidia pura. La vida es tan bella que la muerte se a enamorado de ella, un amor celoso y posesivo que agarra todo cuanto puede. Pero la vida salta por encima de la muerte con facilidad y en el fondo, lo poco que pierde carece de importancia --como el cuerpo, por ejemplo-- y la melancolía no es más que la sombra de una nube pasajera."_

_'**Vida de Pi**'_, Yann Martel. Ediciones Destino, 2003.