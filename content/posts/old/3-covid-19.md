---
title: '#3 COVID-19'
date: 2020-03-13T08:26:00.000+01:00
draft: false
tags : [pandemia, PharmaMar, coronavirus, diario coronavirus, covid-19]
---

Han decretado el estado de alarma. Han cerrado todas las tiendas. Se quedan abiertas: tiendas de alimentación, farmacias, estancos, kioscos y servicios públicos y sanitarios. La Maldición del Papel Higiénico se extiende por todo lo vasto y ancho de la península. Sigue paseando gente por la calle y no se deja de hablar de ello. Muchos madrileños han huido irresponsablemente a la costa. Llegamos a más de 4,000 infectados, pero es evidente que no se hace la prueba todos los que la solicitan. Los contagiados certificados llegan al tercer grado de relación con nosotros. Hoy ha hecho un clima nublado que no ha ayudado nada a ver las cosas con claridad. Aprovechamos y llamamos a nuestra familia y amigos y nos ponemos al día. El régimen de _memes_ y vídeos por las redes sociales llega a niveles absurdos. C. tiene que seguir yendo a trabajar, ya que trabaja en una clínica. A mí me dan teletrabajo. No todo el mundo consigue mantener la calma. Los mayores que salen a la calle, están incrédulos y desconcertados. Colas en el estanco antes de cerrar. Esto va a simplificar nuestra vida, va a centrarnos en lo importante. ¿Qué es lo importante? Pronto lo sabremos. PharmaMar anuncia que su fármaco contra el coronavirus, está listo para probar. El uso de la mascarilla o guantes, no es mayoritario. El uso de los dispensadores, alcoholes y limpiadores de manos, se extiende.  
  

[![](https://1.bp.blogspot.com/-e428fcArHho/XmyJ5fWx2rI/AAAAAAAAfBs/S6ePGds2N9I6obhHOKo1BAuzdXcAdOJngCNcBGAsYHQ/s320/_R072812.jpg)](https://1.bp.blogspot.com/-e428fcArHho/XmyJ5fWx2rI/AAAAAAAAfBs/S6ePGds2N9I6obhHOKo1BAuzdXcAdOJngCNcBGAsYHQ/s1600/_R072812.jpg)