---
title: 'Lasaña de champiñones y mozzarella'
date: 2020-03-23T11:35:00.000+01:00
draft: false
tags : [vegetarianas., receta, lasaña]
---

**Ingredientes:** champiñones, espinacas, cebolla, ajo, hojas de lasaña, tomillo, parmesano, mozzarella. 

  

Fríe la cebolla y los ajos, añade los champiñones y luego las espinacas, ese es el relleno. Normalmente pones en agua 10 minutos las hojas de lasaña, para que se ablanden.

  

**Bechamel**

**Ingredientes bechamel:** 1/4 c harina; 1/4 c mantequilla, 2 c de leche vegetal y 2/3 c de parmesano.

  

En una olla a fuego medio mezcla la mantequilla y la harina. Añade la leche mientras se mezclan y deja reposar 5 minutos hasta que espese. Finalmente añade el parmesano y salpimienta, hasta que salga una mezcla cremosa.

  

**Montaje**

Para montar dos pisos: relleno-mozzarella-bechamel-dos-hojas-de-lasaña. Para terminar, añades un poco de bechamel sobre el segundo piso de hojas de lasaña y pones queso para fundir y al horno (180ºC) 20 minutos con 3 minutos de grill.

  

[![](https://1.bp.blogspot.com/-9F_IHuhk4IY/XnniY0wRFdI/AAAAAAAAfF4/i1a9JFBEU34HVMFBRyrBif7FAwXeO9aDwCNcBGAsYHQ/s320/photo_2020-03-24_11-27-06.jpg)](https://1.bp.blogspot.com/-9F_IHuhk4IY/XnniY0wRFdI/AAAAAAAAfF4/i1a9JFBEU34HVMFBRyrBif7FAwXeO9aDwCNcBGAsYHQ/s1600/photo_2020-03-24_11-27-06.jpg)