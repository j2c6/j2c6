---
title: 'El Laberinto del Fauno (2006)'
date: 2012-04-11T18:11:00.000+02:00
draft: false
tags : [Eugenio Caballero, Maribel Verdú, Ariadna Gil, Ivana Baquero, Cine, Javier Navarrete, Álex Angulo, 2000, Guillermo del Toro, Sergi López]
---

[![](http://4.bp.blogspot.com/-RzP7u9sFch8/T4WtoxulP7I/AAAAAAAABWk/0AVPP-b6xLE/s400/8098.jpg)](http://4.bp.blogspot.com/-RzP7u9sFch8/T4WtoxulP7I/AAAAAAAABWk/0AVPP-b6xLE/s1600/8098.jpg)

22 minutos de aplausos en **Cannes**. Tenía muchas ganas de ver esta película. **Guillermo del Toro** es sin duda un director interesante, a pesar de mi aversión por '_**Hellboy**_', que me parece patética. Del Toro es como una versión hispana del visionario **Peter Jackson**, y me alegro. Ahora se ha hecho guionista a las órdenes del neozelandés  por la famosa y esperada versión cinematográfica del '_**El Hobbit**_' que se encuentra en nuestra '_waiting room_'. El cine español hacía tiempo que no apostaba por la criticada imaginación, que tan hundidos nos tiene su ausencia. Este largometraje escrito, dirigido y producido por G. del Toro lo ha sacado del atolladero tras algunas apuestas no especialmente brillantes y lo ha elevado a cierta categoría, que esperamos seguir disfrutando.

  

Estamos hablando de un cuento de hadas para adultos, y especialmente fascinado por las criaturas mitológicas como son los faunos, así como las leyendas y creencias mágicas; del Toro nos salpica la realidad con una enorme creatividad. Admirables decorados y puesta en escena en lo que a misterio se refiere, y en paralelo a una conseguida España de la posguerra, se presenta una historia bien trabajada y llevada. Esto es todo un mérito y aunque la dolorosa existencia en esta dura etapa española no resulta tan interesante y sí más floja, el cuento de hadas es especialmente fantástico.

  

He disfrutado considerablemente con lo épico de esta historia, las tres pruebas son deliciosas o algo así, seguro que hay adjetivo adecuado para expresar esa atracción por las criaturas y misterios que a veces se entrecruzan en el férreo raciocinio del espectador. La música de **Javier Navarrete**, así como la ostentosa dirección artística de **Eugenio Caballero** son más que dignas de mención.

  

La violencia en la que se ensaña el filme en varias escenas es chocante y muy gráfica, algo que no me ha gustado ni he podido soportar con facilidad, demasiada sangre sin sentido que fortalece la maldad de un personaje a recordar con odio, como es el capitán Vidal (**Sergi López**). Por otro lado he conocido a una prometedora **Ivana Baquero** joven talento que sin duda ha conseguido ponerme en su piel, muy expresiva y creíble. Además de **Maribel Verdú**, **Álex Angulo** y la nimia interpretación de **Ariadna Gil**, debo resumir que el reparto está muy conseguido sin grandes halagos, pero de nuevo resalto a la prometedora actriz en el papel de Ofelia.  
  

En definitiva, un ejemplar a admirar y disfrutar, porque su legendaria historia y sus misteriosos detalles la hacen una película muy singular. Afecta ampliamente este argumento, tiene notas que llaman la atención, disonancias que hacen reflexionar. No queda exenta de causar una fuerte impresión, quizá se deba a la excelente realización. Claro que es importante no desilusionar al público futuro con naderías políticas, o expectativas pesimistas o muy positivas, tan solo merece la pena considerar lo notable de esta artística obra. Recompensada por la academia con tres Oscars.