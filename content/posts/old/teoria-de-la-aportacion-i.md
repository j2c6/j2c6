---
title: 'Teoría de La Aportación I'
date: 2010-04-30T08:00:00.000+02:00
draft: false
tags : [Teoría de la Aportación, Arte, La Escalera Que Quemaba Ríos de Tinta]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/S9pxuG-a7-I/AAAAAAAAArs/WhJyHl8RCPo/s320/and.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/S9pxuG-a7-I/AAAAAAAAArs/WhJyHl8RCPo/s1600/and.jpg)  

Hoy hay un tema del que quiero comentar algo. Es mi blog donde escribo. Tengo que preguntarme porqué escribo: ¿lo hago para que me escuchen?, ¿o para ser escuchado? (no es lo mismo), todavía no lo tengo muy claro. Creo que si escribiera para mí sería absurdo, no tendría sentido. Sé que a lo largo de la historia, el mundo y las personas que viven en él han ido creando una especie de magia, que ha constituido una forma máxima de expresión: desde esas poesías que te meten en la sangre el amor y el olvido, hasta esas pinceladas que recuerdan al romper de las olas contra la tierra. Lo único que quiero es contribuir. Actualmente no espero una gran aportación, pero es como un "yo también". Aún así el riesgo de abrir la boca demasiado está en el aire, solo tengo diecisiete años ¿que sabes tú? podrían decir. Puedo equivocarme y mucho, sin embargo todo el mundo es diferente y aunque solo sea la perspectiva del otro, supongo que ya aporta algo. Pero a pesar de todo (como rectifiqué), no todo vale, no todo es aportar, también se puede hacer daño, destruir con esa magia poderosa los corazones de los hombres... 

  

.

Es encantadora la magia de los versos, los violines, las lágrimas, la vida...