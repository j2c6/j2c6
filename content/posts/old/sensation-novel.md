---
title: 'Sensation Novel'
date: 2011-02-23T12:00:00.000+01:00
draft: false
tags : [Sensation Novel, La Dama de Blanco, Wilkie Collins, Islandia, La Escalera Que Quemaba Ríos de Tinta, Novela]
---

[![](http://3.bp.blogspot.com/-B2Q8vy_Eq8A/TWTotgA8hWI/AAAAAAAAA5s/xgAwqRzvxYI/s320/Wong-by-the-sea-9.jpg)](http://3.bp.blogspot.com/-B2Q8vy_Eq8A/TWTotgA8hWI/AAAAAAAAA5s/xgAwqRzvxYI/s1600/Wong-by-the-sea-9.jpg)

Como si no tuviera nada que decir aquí, tengo asuntos que decir, pero tengo que ordenarme, cosa que no hago muy a menudo. Se me olvidan, hay muchos asuntos. Hay amaneceres, zumo de naranja, música clásica, un día soleado... pero no hay tiempo. Y estas líneas todavía en mi cabeza hablan de planetas que merecen ser retratados.  En fin un día uno se decide a algo, y estoy leyendo "_La Dama de Blanco_" de Wilkie Collins, he oído decir que algo de policíaca, pero me gustó más el subgénero "_sensation novel_" que así lo bautizaron los ingleses. Lo de policíaca me atrae pero no demasiado, me recuerda a la Mary Higgins que disfruté hace unos años. Tengo entendido que este año _La Dama_ cumple 150 años desde su publicación, y todavía se mantiene fresca, pálida y joven. Y es un clásico. Solo la palabra clásico me transporta a otro espacio, la adoro. Pero no importa: he disfrutado ya doscientas cincuenta maravillosas páginas de Sir Percival, el señor Hartright, y la señorita Fairlie... Creo que no me es lícito hablar de estos personajes.

Supongo que este mundo en el que tan orgullosos sentimos la era de la información y comunicación, tiene más secretos que cualquier otro, simplemente la crisis tímidamente anunciada en los periódicos de la cual no oigo sino respuestas dubitativas y vacías a las preguntas sencillas como: ¿La crisis en Islandia?.¿He escuchado la palabra revolución? Es interesante lo acontecido en esa isla alejada de la vorágine europea y rodeada de frío mar en calma. Lo cierto es que no tengo muchos puntos fuertes y es que hablar de temas de actualidad no es uno de ellos.