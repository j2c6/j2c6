---
title: '#1 COVID-19'
date: 2020-03-11T13:00:00.000+01:00
draft: false
tags : [madrid, pandemia, coronavirus, oms, covid-19]
---

Hoy han cerrado los colegios, universidades y centros educativos en la Comunidad de Madrid. Han dado teletrabajo en muchas empresas. Hay unos 2000 contagiados verificados y 50 muertos. Han suspendido las Fallas. Prohibido los encuentros de gente de más de mil personas. En los supermercados la gente hace la compra del pánico. Nosotros la hemos hecho pero no se parece a lo que se reenvía por las redes sociales. No hay desabastecimiento. A última hora del día falta papel higiénico, legumbres en conserva, pollo y leche. No hay demasiada gente en el Metro. Nos comparan con Italia que vive un aislamiento total, donde todo traslado tiene que estar justificado y todos se quedan en casa en cuarentena y sólo abren algunos supermercados y los centros sanitarios. Se ven algunas personas con mascarillas o inventos extraños para evitar el contagio. Es exasperante ver a todo el mundo hablando de esto, y al mismo tiempo indignante quien publica temas no relacionados con ello. En la Península se vive en general con humor. Sin embargo, eso no quita que los más vulnerables al negocio del miedo en los Medios de Comunicación quedan más receptivos a recibir todas las malas noticias alarmantes. Todos los días, hay intentos de tranquilizar a la población y ser razonables de 'expertos' y eruditos anónimos que dicen que se trata de una variante de la gripe muy contagiosa y que solo afecta a inmunodeprimidos y pacientes de edad avanzada. Pero el pánico les alerta. Mi posición al respecto es que no hay que desperdiciar los días frente al Coronavirus, ser prudentes, pero la vida sigue, y hay que lavar la fruta y sacar la basura y alimentar la mente. La OMS ha declarado Pandemia Mundial.  
  

[![](https://1.bp.blogspot.com/-jyhzvHqTXow/XmyLui-uekI/AAAAAAAAfB4/SePrXgXeJ9oj3E-0jIM8sZRaOzZqidj1QCNcBGAsYHQ/s320/_R072781.jpg)](https://1.bp.blogspot.com/-jyhzvHqTXow/XmyLui-uekI/AAAAAAAAfB4/SePrXgXeJ9oj3E-0jIM8sZRaOzZqidj1QCNcBGAsYHQ/s1600/_R072781.jpg)

De la frutería de Mohammed, que nos deja una caja.