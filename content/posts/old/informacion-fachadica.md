---
title: 'Información fachádica'
date: 2018-07-06T20:54:00.001+02:00
draft: false
tags : [soledad, Thoureau, cabaña, retiro, crecer, de cara a la galería]
---

Ha empezado el verano en el lugar de retiro. Casi no salgo de casa. Después de los ratos en los que me siento solo y deprimido, me miraría desde fuera para verme como un pretencioso Thoureau. Algo debo de estar haciendo mal. Siempre me ha atraído la frase de Basil Hallward de haber aprendido a amar "el secreto". Deja de fluir, de revelar información fachádica (=de cara a la galería) sobre ti, te quedas con el sabor y el regusto del café, y te concentras en respirar tu propia respiración por un tiempo para después sentir todo aquello que tienes dentro, que entre tanto input exterior casi no hay tiempo para experimentarse a uno mismo,  y soltarlo todo e ti. No me entiendo ni yo.

  

Uno ha estado en los rincones del mundo, conoce a cientos de personas y a mismo tiempo no sabe quién es y tiene esa sensación de no haber conectado con nadie, de haber compartido esa estancia secreta que no se comparte con nadie o haberla compartido y 'fracasado' en algo en lo que no se fracasa, se crece, se evoluciona, pero tienes esa sensación.  
  
Puede que la soledad y la mentalidad solitaria sea uno de los grandes temas de la temporada.  
  

  

[![](https://1.bp.blogspot.com/-eqo-hncSxv4/Wz-52-wBtII/AAAAAAAAXvo/ut6Fq-y8gR8BpFNbOXGUiHfa2nnmCDbEwCLcBGAs/s320/_MG_0072.jpg)](https://1.bp.blogspot.com/-eqo-hncSxv4/Wz-52-wBtII/AAAAAAAAXvo/ut6Fq-y8gR8BpFNbOXGUiHfa2nnmCDbEwCLcBGAs/s1600/_MG_0072.jpg)

Panorama nocturno desde mi ventana con mosquitera (no visible en la foto).

Qué poderosa vanidad me invade mezclada con un orgullo tipo pecho-ruiseñor al compartir esta foto en este sitio solitario.