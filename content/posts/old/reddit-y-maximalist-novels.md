---
title: 'Reddit y Maximalist Novels'
date: 2020-04-13T12:18:00.002+02:00
draft: false
tags : [pynchon, cartarescu, foster wallace, novelas, maximalista, literatura engórdica]
---

Nos hemos estrenado en Reddit, a la búsqueda de Maximalist Novels. Después de la lectura de '_**Solenoide**_' de Mircea Cartarescu (tras el que tengo pendiente la búsqueda de un atlas de parasitología).

  

I think they have to be:

*   \*Long.
    
*   \*Reveal another universe in detail.
    
*   \*Paper and pen to analyze what's going on.
    
*   \*Quality.
    
*   \*Postmodernism.
    

Have already read :

*   "_Solenoid_" @ Mircea Cartarescu
    
*   "_House of Leaves_" @ Mark Z. Danielewski
    
*   "_The Lost Scrapbook_" @ Evan Dara
    

Typical pending:

*   "_Infinite Jest_" @ David Foster Wallace
    
*   "_2666_" @ Roberto Bolaño
    
*   "_Underworld_" @ Don Delillo
    
*   "_Gravity's Rainbow_" @ Thomas Pynchon
    

Not sure in this category:

*   "_White Teeth_" @ Zadie Smith
    
*   "_The Corrections_" @ Jonathan Franzen
    

Nominated?:

*   "_Royal Family_" @ William T Vollmann
    
*   "_Crime and Punishment_" @ Fiodor M. Dostoievsky
    
*   "_The Recognitions_" @ William Gaddis
    

Please, when you suggest one, explain why.

\---------------------------------------------------------------------------------------------------

Then I find interesting, on the comments:

*   "_Jerusalem_" @ Alan Moore
    
*   "_The Sot-Weed Factor_" @ John Barth
    
*   "_The Tunnel_" @ William H. Gass
    
*   "_A Naked Singularity_" @ Sergio de la Pava
    
*   "_You Bright and Risen Angels_" @ William T. Vollmann
    

More:

*   "_La Medusa_" @ Vanessa Place
    
*   "_The Instructions_" @ Adam Levin
    
*   "_The Combinations_" @ Louis Armand
    
*   "_Novel Explosives_" @ Jim Gauer
    
*   "_Tristram Shandy_" @ Laurence Stern
    
*   "_Women and Men_" @ Joseph McElroy
    
*   "_The Book of Disquiet_" @ Fernando Pessoa
    
*   "_Shadow Country_" @ Peter Matthiessen
    

Smells good but maybe not in the postmodernism:

*   "_Foucault’s Pendulum_" @ Umberto Eco’s
    
*   "_The Illuminatus! Trilogy_" @ Robert Shea & Robert Anton Wilson’s
    
*   "_Life, a User's Manual_" @ Georges Perec
    
*   "_The Education of Henry Adams_" @ Henry Adams
    

  

Además, en matemáticas, relacionadas con:

  

"_**Gödel, Escher, Bach: an Eternal Golden Braid**_" @ Douglas R. Hofstadter

  

Anoto todo esto:

  

Princeton Companion to Mathematics by Gowers

Cedric Villani about research mathematics

"Gödel, Escher, Bach: an Eternal Golden Braid" @ Douglas R. Hofstadter

Surreal Numbers by Donald Knuth.

The Nature of Computation by Christopher Moore and Stephen Mertens

 Discovering Modern Set Theory by Just and Weese

 'Flatland' @ Edwin A. Abbot

 'Mathematics: A very short introduction' @ Timothy Gowers

 James Gleick's "The Information" and "Chaos"

Thinking, Fast and Slow, Kahneman

 "I am a strange loop" by Douglas, also I am planning on reading "the minds I"

 "Metamagical Themas" @ Hofstadter

  

Ha sido muy emocionante ver los primeros comentarios para confeccionar la lista.