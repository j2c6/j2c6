---
title: 'Noël Crooners'
date: 2010-11-30T14:03:00.000+01:00
draft: false
tags : [Rock, Fuego, Velas, Musica, Jazz, Noviembre, La Escalera Que Quemaba Ríos de Tinta, Noël Crooners, Navidad, Frank Sinatra]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/TPT1w2Z-L7I/AAAAAAAAA1U/BvHNtvjSrCQ/s200/597976.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/TPT1w2Z-L7I/AAAAAAAAA1U/BvHNtvjSrCQ/s1600/597976.jpg)

Y allí estábamos. Hablaban de temas casuales, sin importancia. Ninguna luz dañaba directamente mis ojos, todas esas lamparitas como candiles, iluminaban de manera tenue la sala. Para escapar de la "arificialidad" de nuestro tiempo una llama ondeaba lentamente sin apenas moverse, tranquila. Mis pupilas brillaban ante aquella escena, en la que giraba un disco navideño: "**Noël Crooners**", que dejaba oír a **Frank Sinatra**. Con frío y envuelto en abrigo, disfrutaba de los últimos días de Noviembre.