---
title: 'Jesus'' 1927 Solvay Conference'
date: 2018-07-26T10:40:00.000+02:00
draft: false
tags : [iglesia, Solvay Conference, Einstein, jesucristo, Collage, Jesus, religión]
---

Collage digital, partiendo de la foto original:  
  

[![](https://2.bp.blogspot.com/-nKwV7zLj-gU/W7I23PUdFII/AAAAAAAAaO4/C_3XXLj02MQSQj5TxAtweXQnp6HvFb8MACLcBGAs/s320/450px-Solvay_conference_1927.jpg)](https://2.bp.blogspot.com/-nKwV7zLj-gU/W7I23PUdFII/AAAAAAAAaO4/C_3XXLj02MQSQj5TxAtweXQnp6HvFb8MACLcBGAs/s1600/450px-Solvay_conference_1927.jpg)

  
Y esto queda como:  
  

[![](https://3.bp.blogspot.com/-psfLj3TVcGU/W1mIoJGwi3I/AAAAAAAAYF4/W9YDD-c38_4Mt76VbqXJGhyVTPtQQ10HQCLcBGAs/s320/photo_2018-07-26_10-36-34.jpg)](https://3.bp.blogspot.com/-psfLj3TVcGU/W1mIoJGwi3I/AAAAAAAAYF4/W9YDD-c38_4Mt76VbqXJGhyVTPtQQ10HQCLcBGAs/s1600/photo_2018-07-26_10-36-34.jpg)

  
 Concebido en la Biblioteca Pública Municipal de l'Eliana.  
  

[![](https://4.bp.blogspot.com/-mvtqxVpPOeQ/W1mVJAVGMgI/AAAAAAAAYGM/iZ9Iz3R52t0EonSJBlV_WTgeDdiX4bczACLcBGAs/s320/photo_2018-07-26_11-31-45.jpg)](https://4.bp.blogspot.com/-mvtqxVpPOeQ/W1mVJAVGMgI/AAAAAAAAYGM/iZ9Iz3R52t0EonSJBlV_WTgeDdiX4bczACLcBGAs/s1600/photo_2018-07-26_11-31-45.jpg)