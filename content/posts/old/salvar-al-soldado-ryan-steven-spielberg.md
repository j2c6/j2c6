---
title: 'Salvar al soldado Ryan - Steven Spielberg, 1998'
date: 2014-01-03T00:04:00.000+01:00
draft: false
tags : [Tom Hanks, Matt Damon, Steven Spielberg, ryan]
---

  

  

  

  

2.i.2014//¿Qué tiene "[Saving Private Ryan](http://www.imdb.com/title/tt0120815/)"? Unos medios espectaculares -qué poco hacen ahora con 70 millones de dólares en este siglo-, un reparto mítico, escenas de guerra hiperrealistas, un guión muy bien estructurado y dramático, una [fotografía increíble](http://www.imdb.com/name/nm0001405/?ref_=ttfc_fc_cr11) (Janusz Kaminski), una banda sonora conquistante, y un [director de oro](http://www.imdb.com/name/nm0000229/?ref_=tt_ov_dr). Y así podríamos seguir, esta película tiene el qué y el cómo: ha sido diseñada con pasión. 

  

Haber elegido la historia de la trágica muerte de tres hermanos y la misión de salvar al cuarto, ya es otro cantar. Empieza con la antológica representación del [desembarco de Normandía](http://es.wikipedia.org/wiki/Playa_Omaha) (25 minutos de guerra pura con tal realismo que no recuerdo haber visto en otra película bélica), e introduciendo el uniforme con el 'RYAN' bordado. Continúa con una serie de escenas -cada cual más dramática- que atrapa desde el principio, con el primer disparo. No quiero resumir el filme, pero no me resisto a enumerar algunas escenas. La expresividad con que desde la central de telégrafos se comunican las muertes de los soldados y el contraste de la guerra con la idílica casa rodeada de campos dorados -grabado con un filtro y color memorables- y una madre que recibe postrada en el porche la noticia del fallecimiento de sus hijos. Así -dejando a un lado [las palabras de Lincoln](http://volveranacer.wordpress.com/2011/09/12/carta-de-lincoln-a-lydia-bixby-madre-de-cinco-hijos-muertos-en-la-guerra/)\- introducen el primer gran nudo de la trama tras haber presentado tímidamente a los protagonistas entre explosiones.

  

A partir de ahí y con Hanks a la cabeza, empieza la misión con [los mejores](http://www.imdb.com/title/tt0120815/fullcredits?ref_=tt_ov_st_sm). Cada escena es sorprendente: una familia que tiernamente intenta dejar a su niña francesa con los soldados; como se elimina la pared y quedan frente a frente alemanes y americanos; el punto de inflexión de la compañía cuando tras la dramática muerte del médico y el abandono del prisionero -que luego vuelve al campo de batalla- aparece la desesperación, la crisis y dudas sobre el sentido de la misión. Una deliciosa escena a contraluz y al atardecer; buscar a Ryan irreverentemente entre las medallas de los soldados muertos y dar la noticia de la muerte de sus fratelli a un 'falso' Ryan antes de encontrar a un joven [Damon](http://www.imdb.com/name/nm0000354/?ref_=ttfc_fc_cl_t9). Y sigue y sigue. Lo que cuenta, es tan sorprendente -no sé hasta que punto es mérito de Robert Rodat- y lo hace de un modo tan vivo y épico, que es improbable que no te absorba.

  

En fin, para qué comentar más la evidencia -olvidando el humor y sentimentalismo patriótico americano-. "Merézcalo" ("Make all of this worthwhile"), con ese imperativo trascendente e inspirador se cierra esta genial narración.

B

  

  

[](https://www.blogger.com/null)

###