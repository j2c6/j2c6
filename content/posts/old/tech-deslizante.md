---
title: 'Tech deslizante'
date: 2020-07-06T15:38:00.001+02:00
draft: false
tags : [iPhone]
---

La tecnología puede ser fantástica, da gusto usarla. Puede hacerlo todo más fácil y cómodo, puede ahorrar tiempo. Pero  igual que con el ahorro del tabaco y ese mítico ejemplo de '¿dónde está tu Ferrari con todo el dinero que te has ahorrado del tabaco?', ¿qué haces con toda esa tecnología? Te sientes menos solo, más lleno, creas algo, haces la vida más agradable a los demás, conoces el mundo, te sientes bien, tiene sentido.

  

Al igual que el trabajo, la tecnología no debería ser un fin, sino un medio, para hacer la vida de cierta manera, no ser tu vida. Está el iPhone, tu ordenador ultrafino, el aire acondicionado, la ducha, el horno. Todos ellos te hacen vivir más lujosamente que en la corte de Luis XVI, pero a cambio, la pregunta es: ¿qué le devuelves al mundo? Intentando no ser pretencioso, no se trata de grandes cosas si no de naderías supongo.

  

La vida no tiene ningún sentido y pensarlo y darle vueltas puede hasta ser todo un propósito. El resto es capacidad de asombro, y las cosas pequeñas, las vivencias de cada día. La tecnología no puede ser un agujero negro de vida, tal vez se parezca más a la hielo que hace posible que el trineo se deslice por él en lugar de caminar pesadamente por la nieve.