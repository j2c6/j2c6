---
title: 'La gata sobre el tejado de zinc (1958)'
date: 2012-02-04T11:39:00.000+01:00
draft: false
tags : [1950, Tennessee Williams, Richard Brooks, Vicente Minnelli, Burl Ives, Cine, Paul Newman, Pulitzer, Grace Kelly, Jack Carson, Elizabeth Taylor]
---

[![](http://4.bp.blogspot.com/-jRTbed7E5tg/Ty0KwHXVAnI/AAAAAAAABM4/5v3qYjqoRf0/s400/rostards.jpg)](http://4.bp.blogspot.com/-jRTbed7E5tg/Ty0KwHXVAnI/AAAAAAAABM4/5v3qYjqoRf0/s1600/rostards.jpg)

Finalmente rodada en color.

Se acerca la muerte del anciano padre (**Burl Ives**) un todopoderoso millonario que se acerca al final de su existencia sin que nadie le diga una palabra de ello. El mayor de los hijos Gooper (**Jack Carson**) se centra en la herencia, al contrario que Brick (**Paul Newman**) que parece quedarse impasible e inmerso en el alcohol, mientras su esposa (**Elizabeth Taylor**) inquieta por lo que le ocurre a su marido y la situación que la rodea no deja de preguntarse qué ocurre.

  

**Tennessee Williams** uno de los dramaturgos estadounidenses más reconocidos del siglo veinte, ganador de dos premios **Pulitzer**, escribió la entretenida obra de teatro: '_**La gata sobre el tejado de zinc**_'. Considerada su obra maestra, "_la gata_" presenta una historia que habla sobre amor, hipocresía, codicia, envidia, y la verdad; sobre la vida y sus problemas de principio a fin. A partir de unos diálogos en los que sobra ni falta una palabra, guía al espectador extraordinariamente hacia el ojo del huracán. En sí la obra es un clásico, a pesar de no haberla reproducido por entero suprimiendo del guión \-en concreto-  las referencias a la homosexualidad. La humanidad escondida en ella y el realismo reflejado son merecedores de todo.

  

Además de la realización de **Richard Brooks**, lo inconmensurable del largometraje son las profesionales interpretaciones de los actores, desde la "reina" **Elizabeth Taylor** en su papel de _Maggie_, pasando por el auténtico patriarca de la familia **Burl Ives** y finalizando -podría nombrar a los demás, pero vamos a lo esencial- con **Paul Newman** uno de los papeles más lúcidos de su extensa carrera. Es tan sincera la película y trata tan honestamente la vida de los personajes, siendo a la vez algo que no te deja indiferente por las actuaciones estelares transmisoras de emoción en todo momento, que es difícil que no haya encantado al espectador mientras la tilda de obra maestra.  
  

Sin embargo la **Academia** pareció olvidarse de este ejemplar, ya que a pesar de sus seis nominaciones no aterrizó ningún **Oscar**. Ganó aquel año 9 estatuillas una tal '_**Gigi**_' de **Vicente Minnelli** que no conozco a mucha gente que la recuerde. En fin, la historia dicta sentencia. El papel de Taylor también fue pensado para **Grace Kelly** y  Elizabeth a pesar de perder a su marido el primer día de rodaje continuó con él.