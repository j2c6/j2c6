---
title: 'La invención de Hugo (2011)'
date: 2012-03-18T17:49:00.000+01:00
draft: false
tags : [Christopher Lee, Elijah Wood, John Logan, Chloë Moretz, Homenaje, Asa Butterfield, Martin Scorsese, George Méliès, Michel Hazanavicius, Sacha Baron Cohen, Robert Richardson, Ben Kingsley, Cine, Jude Law, 2010]
---

[![](http://4.bp.blogspot.com/-DKOzo-fP6FU/T2YQnTDbdlI/AAAAAAAABSo/zGDLV5BIUIU/s400/r5.jpg)](http://4.bp.blogspot.com/-DKOzo-fP6FU/T2YQnTDbdlI/AAAAAAAABSo/zGDLV5BIUIU/s1600/r5.jpg)Ir a ver una película infantil sin sentirse ridículo, porque la dirige **Martin Scorsese** y la crítica no puede darle mejores puntuaciones, es hasta cierto punto agradable. Tienes justificada tu presencia en la amplia sala, ahora no solo llena de niños. No es de dibujos animados y la puedes ver en 3D. Es el primer proyecto del director con esta tecnología y el mismísimo **James Cameron** afirmaba que no había visto mejor uso del 3D en ningún ejemplar, incluyendo los suyos.

  

Hay filmes que se pagaron para lucir sus capacidades técnicas, pero con escasos resultados narrativos, como es el caso de la catastrófica --en todos los sentidos-- '**_2012_**' (2009). Por el contrario la conmovedora '**_Avatar_**'(2009) que ya poca gente recuerda a pesar de su impresionante forma de transmitir un mensaje, se encuentra entre las más expresivas, con un uso del software más avanzado para realizar un proyecto de descomunales dimensiones. Una de las aspiraciones del cine de la década que comienza es impresionar al espectador con efectos alucinantes, al estilo de un espectáculo de circo. En la mayoría de los proyectos solo se centran en eso y la idea o el fondo resultan escasos o poco profundos por lo que es probable que sean olvidadas cuando los acróbatas se eleven más alto.

  

En '**_Hugo_**' es indiscutible el trabajo sensorial. Los efectos visuales son increíbles, constituyen un alarde de creatividad, mostrando las infinitas posibilidades narrativas de la cámara. Nada más comenzar la cámara que sigue a Hugo (**Asa Butterfield**) por los distintos escondites de los relojes de la estación y el tobogán, así como la romántica imagen de París con el sol naciente, auguran un espectáculo magnífico. En ningún momento deja de ofrecerte esta premiada dirección artística, mostrando colores inéditos en las flores o simplemente intensos retazos de viveza en los mecanismos de las máquinas. La recreación de los escenarios, así como la puesta en escena de los años 30, parece recordar --como siempre hago referencia-- a la magia de la narrativa de **Charles Dickens**. Una fotografía muy inventiva  la del tres veces oscarizado **Robert Richardson**.

  

[![](http://4.bp.blogspot.com/-m8vYhwUGUbI/T2YRRFSzHOI/AAAAAAAABSw/KAVaxLJ5ib0/s640/r6.jpg)](http://4.bp.blogspot.com/-m8vYhwUGUbI/T2YRRFSzHOI/AAAAAAAABSw/KAVaxLJ5ib0/s1600/r6.jpg)

Algunas de las cifras que suelen acompañar a estos desorbitantes rodajes aparte del presupuesto (**170.000.000 $**), son los 1000 ordenadores que se utilizaban para realizar cada fotograma en el rodaje. A pesar de que el mérito no es ni la mitad de cualquier obra, es parte a considerar. Mencionar también la recreación muda en la estación de personajes como **Django Reinhardt**, **James Joyce** y **Winston Churchill.**

  

Que tenga cinco premios _Oscar_, y que haya sido candidata más nominada del año no tiene que distraer al espectador con falsas esperanzas, esto es imprescindible, de lo contrario puede salir con mal sabor de boca del cine. Quiero insistir mucho en esta cuestión ya que ha causado numerosas desgracias en la apreciación del cine. Daría lo que fuera por ver una película sin prejuicios, como si fuera la primera vez y sorprenderme con todo el detalle de una obra maestra. Lamentablemente esto ya no es posible, pero hay que hacer lo imposible para disfrutar y analizar el filme y sacar lo mejor de cada obra.

  

El motor fue la hija de **Scorsese**, **Francesca** que le regaló la novela de **Brian Selznick**  con la esperanza de que algún día hiciera una película de la misma. **John Logan** prolífico guionista nos ha regalado grandes obras como '**_Gladiator_**' (2000), el musical '**_Sweeney Todd_**' (2007), '**_El último samurái_**' (2003), así como su anterior trabajo junto a Scorsese: '**_El Aviador_**' (2004), y ahora corre a su cargo la adaptación de la novela de Selznick. Merece cierto respeto, a pesar de que sinceramente no es lo que todos esperábamos.

  

El guión es tremendamente lento en el sentido de que no existen grandes saltos que perturben la tranquilidad del espectador, es como un cuento con mucha descripción imaginativa, incluyendo la acusada ingenuidad y simplicidad creativa que acompaña a estos trances eternos. Un resultado en este aspecto bastante modesto. De modo que lo más importante yace en el _cómo_; el _qué_ es secundario aunque esencial. Nos encantan los homenajes, pero a **Hollywood** más todavía. En concreto este año con '**_The Artist_**' y '**_My Week with Marilyn_**' ya deberían de estar contentos, además de que la mayoria de los premios han ido a parar a este ejemplar compartiéndolos con el de **Hazanavicius.** Sin embargo no todo el mundo entiende y aprecia esta clase de eventos sobretodo si se trata de **George Méliès,** soñador y director archirreconocido en la posteridad por sus comienzos de la historia del cine con '**Viaje a la luna**' (1902). Supongo que estamos mal educados.

  

[![](http://3.bp.blogspot.com/-E-CsK5CvR_o/T2YQWe_Uf7I/AAAAAAAABSg/GjUidRUlUjw/s400/r4.jpg)](http://3.bp.blogspot.com/-E-CsK5CvR_o/T2YQWe_Uf7I/AAAAAAAABSg/GjUidRUlUjw/s1600/r4.jpg)

Scorsese tiene en el alma el talento y  la pasión por el cine, además de su interminable lista de galardones y la fundación de la **World Cinema Fundation**. La aparición de los hermanos **Lumière**, **Chaplin** o las múltiples referencias a la fantasía del cine, son la expresión de gratitud del director hacia el séptimo arte.

  

Era difícil ver trabajar a M. Scorsese sin el incondicional **DiCaprio**. En elenco de **Ben Kingsley**, **Jude Law**, el irreverente **Sacha Baron Cohen**, así como el inmortal **Christopher Lee** es fantástico, aunque moderado, pues los que brillan en este largometraje son los iniciados **Butterfield** y **Moretz**. Personalmente he descubierto a **Asa B**., pues aunque lo vi en '**_El niño con el pijama de rayas_**' (2008) no me llamo la atención, al contrario que en '**_Hugo_**' donde ha hecho gala de una expresividad inusual y cómo no, muy prometedora. Me ha recordado considerablemente al **Elijah Wood** de '**_Las Aventuras de Huckleberry Finn_**' o '**_El buen hijo_**' ambas de 1993. No me ha sorprendido tanto la alabada **Chloë Moretz**, quizá por un papel demasiado idealizado (ñoño) e incluso sobreactuado, pero aún es pronto para juzgar: se esperan grandes cosas de ambas carreras.

  

La distribuidora española o quién sea consideró muy oportuno cambiar el título, -cuando no existe ninguna necesidad de complicar las cosas- por razones que desconocemos, en realidad qué desgracia. ¿Cuál es la invención de Hugo? Solo añade un aspecto patético a la película, así son las manías.  
  
  

En conclusión una película magnánima y espléndida, que hace gala de un lado artístico muy trabajado y admirable a pesar de su ordinario lado narrativo por parte del comedido en sobresaltos, guión. Solo apta para cinéfilos en mi opinión, ya que profundiza en el cine con cierta veneración o en su honor y desprecia la clásica oferta aventurera o inquietante del género de entretenimiento.