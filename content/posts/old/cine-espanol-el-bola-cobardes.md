---
title: 'Cine Español: El Bola, Cobardes'
date: 2009-02-15T13:51:00.000+01:00
draft: false
tags : [Interesantes, Juan Cruz, Achero Mañas, El Bola, Cine Español, Cine, Cobardes, José Corbacho]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/SZ_7twsd-tI/AAAAAAAAAZc/wYI2kX7tuhE/s320/cine_club.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/SZ_7twsd-tI/AAAAAAAAAZc/wYI2kX7tuhE/s1600-h/cine_club.jpg)  

El **cine español** no tiene muy buena fama, no recibe muchos premios, y se centra y fija en cosas bastas, y viciosas, muestra el lado mas animal del hombre... Esto es lo que pensábamos, pero no podemos generalizar así, estos de lo que he hablado son los conocidos, los que "nos hacen famosos", pero yo hablo de otro cine he visto pocas películas españolas (_el orfanato, intacto, los otros._..) pero este mes he visto un par que están bien, y necesito decirlas, porque no quiero que nadie cierre los ojos al "cine español".Bien las dos películas son:

  

.

_TITULO ORIGINAL: **Cobardes**_

_AÑO: 2008_

_DURACIÓN: 89 min._

_PAÍS: España_

_DIRECTOR: José Corbacho, Juan Cruz_

_GUIÓN: José Corbacho, Juan Cruz MÚSICA: Pablo Sala_

_FOTOGRAFÍA: David Omedes_

_REPARTO: Lluís Homar, Elvira Mínguez, Paz Padilla, Antonio de la Torre, Javier Bódalo, Eduardo Espinilla, Eduardo Garé, Ariadna Gaya, Maria Molins_

_PRODUCTORA: Castelao Productions S.A., Filmax Entertainment, Antena 3 Films, Hospiwood, El Terrat_

_Estreno en España: 25 abril 2008. Preestreno Festival de Málaga 2008.  
.  
TITULO ORIGINAL : **El Bola**  
Dirección y guión: Achero Mañas._

_País: España._

_Año: 2000._

_Duración: 88 min._

_Interpretación: Juan José Ballesta (Pablo), Pablo Galán (Alfredo), Alberto Jiménez (José), Manuel Morón (Mariano), Ana Wagener (Laura), Nieves de Medina (Marisa), Gloria Muñoz (Aurora), Javier Lago, Omar Muñoz, Soledad Osorio._

_Producción: José Antonio Félez._

_Fotografía: Juan Carlos Gómez._

_Diseño de producción: Goldstein & Steinberg._

_Dirección artística: Satur Idarreta._

_Montaje: Nacho Ruiz Capillas._

_Música: Eduardo Arbide._

.

Las dos son películas que enseñan mucho, hay mucho que aprender, y son muy buenas, quizás tardes un poco en acostumbrarte al cine español, y así también cambies un poco tu idea acerca del cine nacional.