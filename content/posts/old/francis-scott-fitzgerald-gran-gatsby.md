---
title: 'Francis Scott Fitzgerald - ''El gran Gatsby'''
date: 2012-01-13T09:46:00.000+01:00
draft: false
tags : [Años 20, Gatsby, Jay Gatsby, XX, Conde Fosco, Edmundo Dantés, Literatura, Lord Henry, F. S. Fitzgerald]
---

[![](http://4.bp.blogspot.com/-H4m7SGeFSbw/T2i_6f85OLI/AAAAAAAABTI/P2A5OAo1LW8/s400/rostardgatsby.jpg)](http://4.bp.blogspot.com/-H4m7SGeFSbw/T2i_6f85OLI/AAAAAAAABTI/P2A5OAo1LW8/s1600/rostardgatsby.jpg)

Siempre he adorado los años veinte, y los disfruté muchísimo con '_**Midnight in Paris**_'. Llevaba esperándolos algún tiempo, nunca sabes cuándo van a aparecer y de repente te sumerges en el encanto y la elegancia de la década dorada, que ha dejado sus mejores recuerdos en canciones, novelas y largometrajes.  
  
  
'_**El gran Gatsby**_' se ambienta en Nueva York durante la era del _jazz_. La historia no es espectacular, se reduce a las luces, música y risas de --una vez más-- los años veinte. Tengo una lista de personajes cautivadores y ejemplares, como son: '**El Conde Fosco**' (_La dama de blanco_, W. Collins), '**Lord Henry Wotton**' (_El retrato de Dorian Gray_, O. Wilde), '**Edmundo Dantés**' (_El Conde de Montecristo_, A. Dumas). Únicamente por el título, esperaba a 'el gran Gatsby'. Gatsby representa sin reservas el ideal americano que no es carente de moraleja, todos quisieran ser como Jay Gatsby, pero a qué precio. Entre el lujo y la idolatría al materialismo hay algo que no encaja, esa es la idea del libro y quizá el problema del gran Gatsby.  
  
  

Las escenas descritas por **F. S. Fitzgerald**, el autor de la novela, son exquisitas: "_en sus azules jardines, hombres y mujeres, semejantes polillas, entre los susurros, el champaña y las estrellas._" Sin duda existe belleza en la vida festiva descrita por Fitzgerald, íntegramente dedicada a los placeres de la vida. Y momentos memorables como cuando se interpreta “_Jazz History of the_ **World**”. Una novela encantadore, pero creo que no es para subirla al top 10. En fin, un libro breve y atrayente.

  

  

  

"_Sonrió comprensivamente, mucho más que comprensivamente. Era una de esas raras sonrisas, con una calidad de eterna confianza, de esas que en toda la vida no se encuentran más que cuatro o cinco veces. Contemplaba, parecía contemplar por un instante el universo entero, y luego se concentraba en uno con irresistible parcialidad; comprendía a uno hasta el límite en que uno deseaba ser comprendido, creía en uno como uno quisiera creen en sí mismo, y aseguraba que se llevaba la mejor impresión que uno quisiera producir_."  

  
  

'**El gran Gatsby**', Francis Scott Fitzgerald, Random House Mondadori, 2006.