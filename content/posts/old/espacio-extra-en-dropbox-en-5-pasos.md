---
title: 'Espacio extra en Dropbox en 5 pasos'
date: 2012-05-22T13:49:00.000+02:00
draft: false
tags : [tecnología, Dropbox, informática]
---

[![](http://3.bp.blogspot.com/-Uj3fWpzGgEQ/T7t721rDhHI/AAAAAAAABeM/82Nn6fyJmwg/s320/dropbox-500x307.jpg)](http://3.bp.blogspot.com/-Uj3fWpzGgEQ/T7t721rDhHI/AAAAAAAABeM/82Nn6fyJmwg/s1600/dropbox-500x307.jpg)**Dropbox** es sin duda una aplicación muy útil, para la disponibilidad de archivos de gran tamaño en la red, en cualquier momento y desde cualquier parte. La posibilidad del intercambio de archivos en el trabajo, o simplemente películas entre amigos te hace feliz. Cuando el usuario empieza, está tan contento con sus profundos 2 Gb que no sabe, ni se plantea conseguir 5Gb, por ejemplo. O simplemente ya lo llevas utilizando cierto tiempo y lo estás llenando. En este artículo explicamos, cómo de forma rápida, sencilla y gratuita se consigue este espacio tan amplio para el disfrute.

  

**1º Hazte una cuenta Dropbox:**

Sigue este enlace, rellena los datos: [http://db.tt/0KEkBWyR](http://db.tt/0KEkBWyR)

  

**2º Instalar Dropbox en tu equipo:**

Descarga esta aplicación e instálala en tu ordenador: [https://www.dropbox.com/](https://www.dropbox.com/)

  

**3º Instala la versión Beta de Dropbox:**  
Según sea el sistema operativo de tu ordenador.

**Windows:**  [http://dl-web.dropbox.com/u/17/Dropbox%201.3.13.exe](http://dl-web.dropbox.com/u/17/Dropbox%201.3.13.exe)  
****Mac OS X:** [http://dl-web.dropbox.com/u/17/Dropbox%201.3.13.dmg](http://dl-web.dropbox.com/u/17/Dropbox%201.3.13.dmg)**

****Linux x86\_64:** [http://dl-web.dropbox.com/u/17/dropbox-lnx.x86\_64-1.3.13.tar.gz](http://dl-web.dropbox.com/u/17/dropbox-lnx.x86_64-1.3.13.tar.gz)**  
****Linux x86:** [http://dl-web.dropbox.com/u/17/dropbox-lnx.x86-1.3.13.tar.gz](http://dl-web.dropbox.com/u/17/dropbox-lnx.x86-1.3.13.tar.gz)**  
  
**4º  Utiliza un disco extraible para pasar imágenes al Dropbox: **En tu pendrive (por ejemplo), crea una carpeta llamada '**_DCIM_**' e introduce imágenes de gran tamaño, lo mejor es que descargues alguna de estas, según el tamaño que te interese y las descomprimes:

**500 Mb:**  [http://dl.dropbox.com/u/1501117/Imag…Mos0\_5.jpg.rar](http://dl.dropbox.com/u/1501117/Imagenes%20Pesadas/rotoMos0_5.jpg.rar)

**1 GB:**  [http://dl.dropbox.com/u/1501117/Imag…oto1GB.jpg.rar](http://dl.dropbox.com/u/1501117/Imagenes%20Pesadas/roto1GB.jpg.rar)  
**3 GB:**  [http://dl.dropbox.com/u/1501117/Imag…toMos3.jpg.rar](http://dl.dropbox.com/u/1501117/Imagenes%20Pesadas/rotoMos3.jpg.rar)  
**3’5 GB:**  [http://dl.dropbox.com/u/1501117/Imag…Mos3\_5.jpg.rar](http://dl.dropbox.com/u/1501117/Imagenes%20Pesadas/rotoMos3_5.jpg.rar)  
**4 GB:**  [http://dl.dropbox.com/u/1501117/Imag…toMos4.jpg.rar](http://dl.dropbox.com/u/1501117/Imagenes%20Pesadas/rotoMos4.jpg.rar)  
**6 GB:**  [http://dl.dropbox.com/u/1501117/Imag…toMos5.jpg.rar](http://dl.dropbox.com/u/1501117/Imagenes%20Pesadas/rotoMos5.jpg.rar)  
  
****5º Introduce de nuevo el dispositivo extraíble, y carga las fotos mediante la ventana que te aparece de Dropbox. ****La ventana habitual que aparece cuando el ordenador detecta un nuevo hardware, te ofrece una opción de cargar el contenido mediante Dropbox, utilízalo. 

  

[![](http://3.bp.blogspot.com/-AZk7AlisFH0/T7t6V7bnnHI/AAAAAAAABd0/jxtZYv14vG0/s320/drop.JPG)](http://3.bp.blogspot.com/-AZk7AlisFH0/T7t6V7bnnHI/AAAAAAAABd0/jxtZYv14vG0/s1600/drop.JPG)

  

Posteriormente la aplicación '_**Camera upload**_' te pide autorización y comienza a subir las imágenes al Dropbox. A continuación, todas las imágenes que subas con este sistema ampliarán tu espacio Dropbox hasta -en principio- 3Gb extras. 

  

  

  

  

[![](http://3.bp.blogspot.com/-okkAj0gs1tU/T7t7tSuuO9I/AAAAAAAABeE/Yzo0ThaVws0/s1600/sprost.bmp)](http://3.bp.blogspot.com/-okkAj0gs1tU/T7t7tSuuO9I/AAAAAAAABeE/Yzo0ThaVws0/s1600/sprost.bmp)

  

  

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

Enlaces, cortesía de David Serantes.