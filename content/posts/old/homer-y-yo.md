---
title: 'Homer y yo'
date: 2010-06-05T06:52:00.000+02:00
draft: false
tags : [Microrrelatos, La Escalera Que Quemaba Ríos de Tinta]
---

“Así son las cosas Fred”. Eso fue lo que dijo  mientras hacía sus sólidos aros de humo con la boca y tiraba la ceniza con indiferencia y gracia. Sus piernas enfundadas en sus vaqueros con el zapato apoyado en la pared, sostenían una sonrisa desafiante. Así fumaba _Paul Lestrade_ en su última película. Cuando miré a Homer supe en qué estaban pensando sus grandes ojos castaños y sus trece años. Éramos imparables y creíamos que nos comíamos el mundo. Homer me aseguró que podía conseguir los cigarrillos, yo añadí que cogería cerillas de la cocina...

.

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/TAkxHp95ZLI/AAAAAAAAAuY/4rzcgk3INcE/s320/sulocura.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/TAkxHp95ZLI/AAAAAAAAAuY/4rzcgk3INcE/s1600/sulocura.jpg)Se deslizó una leve sonrisa por mis labios al contemplar cómo Homer se colocaba el cigarrillo en la boca. Me miró como lo había hecho cientos de veces aquel verano y asintió como quién disfruta de un premio tras una larga carrera. Sacó una cerilla y la encendió. Las chispas que salían del fosforo se confundían con el atardecer anaranjado que despedía el día. Homer entrecerró los ojos aspirando el humo  mientras sujetaba el cigarrillo con los dos dedos con un aire de rebeldía; apoyó la suela de la zapatilla en la pared, y soltó un ensayado “Así son las cosas Fred”. Al expulsar el humo tosió sin remedio... Reí: era igual que _Lestrade_.

.

Por un momento pensé que no podría imitarlo como Homer. Extendió la mano y me ofreció el cigarrillo retándome. Lo cogí inseguro. Homer sonrió mientras bajaba la zapatilla de la pared, esperado a que comenzara mi actuación. La verdad es que temía lo que pudiera pasar. Miré a ambos lados y me decidí a acercarme el cigarrillo a la boca… Cuando tocó mis labios me derrumbé procurando no quemarme y cerré los ojos.  Homer asustado acudió rápidamente y me cogió de los hombros  tambaleándome: me gritaba. En unos segundos la desesperación llegó a mi amigo y agudizó el tono, suplicante. Me arrepentí y abrí los ojos de repente mientras me  oía decir un alegre: “Así son las cosas Fred”. Entonces eché a correr huyendo de una venganza amistosa, que me alcanzó en el río. Jadeantes nos sentamos en la hierba que bordeaba la ribera del río, y contemplamos el ocaso.  Homer y yo éramos amigos.