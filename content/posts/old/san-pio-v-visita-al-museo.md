---
title: 'San Pio V: Visita al museo'
date: 2008-11-22T14:50:00.000+01:00
draft: false
tags : [Arte, Museo San Pío V, Museos]
---

El otro día fuimos a ver el museo de **San Pío V** en Valencia, tiene muchos cuadros importantes es el segundo mas importante de España después del **Prado**, yo no se mucho de esto pero me gustó aquella visita, os dejo algunas fotos, como siempre en blanco y negro:  

  

![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SSgYoJ-mAWI/AAAAAAAAAWc/-ITo30oPG3A/s320/1.JPG)

Nuestro querido museo.

  
  
  
  
  

  

![](http://2.bp.blogspot.com/_VT_cUiG4gXs/SSgYpmlN5SI/AAAAAAAAAW0/L2I9GbH3tvQ/s320/4.JPG)

Un cuadro interesante

  
  
  
  
  
  

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SSgYpCpC3jI/AAAAAAAAAWs/aep0CEmWU20/s320/3.JPG)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SSgYpCpC3jI/AAAAAAAAAWs/aep0CEmWU20/s1600-h/3.JPG)

PM, MG y JC admirando una de las obras del museo.

  
  
  
  

  

![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SSgYosUypWI/AAAAAAAAAWk/OD7sxBvGpJY/s320/2.JPG)

El patio interior del museo, ya restaurado.

[  
  
  
](http://3.bp.blogspot.com/_VT_cUiG4gXs/SSgYosUypWI/AAAAAAAAAWk/OD7sxBvGpJY/s1600-h/2.JPG)  

  

![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SSgYn5k9hTI/AAAAAAAAAWU/fjijAgg27xk/s320/5.JPG)

Una de las pocas esculturas pero: impresionante.

[  
  
  
](http://3.bp.blogspot.com/_VT_cUiG4gXs/SSgYn5k9hTI/AAAAAAAAAWU/fjijAgg27xk/s1600-h/5.JPG)  

  

  
  
  
  

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/SSggA6cYbyI/AAAAAAAAAXE/rhXqtndBOso/s320/7.JPG)](http://2.bp.blogspot.com/_VT_cUiG4gXs/SSggA6cYbyI/AAAAAAAAAXE/rhXqtndBOso/s1600-h/7.JPG)

MG ADMIRANDO UN CUADRO MUNDIALMENTE CONOCIDO.

  
  
  
  

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SSggA9fPeuI/AAAAAAAAAW8/gQzBvHk6t0E/s320/6.JPG)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SSggA9fPeuI/AAAAAAAAAW8/gQzBvHk6t0E/s1600-h/6.JPG)  
  
  
  
  
  
  

![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SSggBZHLIoI/AAAAAAAAAXM/g8cKdJ5pZ2o/s320/Nueva+imagen.JPG)

Famoso cuadro del Greco.