---
title: 'Snatch, cerdos y diamantes (2000)'
date: 2012-05-03T23:03:00.000+02:00
draft: false
tags : [Jason Satham, Mike Reid, Lennie James, Benicio Del Toro, Robbie Gee, Jason Flemyng, Ewen Bremner, Vinnie Jones, Cine, Dennis Farina, Alan Ford, 2000, Rade Serbedzija, Brad Pitt, Guy Ritchie]
---

Es fundamental dejar la impronta de tu estilo en la obra y que así se haga reconocible. No es difícil reconocer al más canalla de Reino Unido: **Guy Ritchie**. Después de su relativo éxito de los noventa '**_Lock & Stock_**', llegó al nuevo milenio consagrándose en el séptimo arte con su propia firma: '**_Snatch_**'. Es el típico lío inicial de cuatro o cinco historias caóticas y plagadas de humor negro, violencia mostrada estrictamente y el 'vocabulario explícito' donde '_joder_' tiene entre doscientas y trescientas acepciones posibles.  
  

  

[![](http://4.bp.blogspot.com/-USbjTCArp_Q/T6LxeOFsJZI/AAAAAAAABcU/ruitihvFmV0/s640/rostard.jpg)](http://4.bp.blogspot.com/-USbjTCArp_Q/T6LxeOFsJZI/AAAAAAAABcU/ruitihvFmV0/s1600/rostard.jpg)

  

Este tipo de largometraje se lo podríamos haber atribuido a **Tarantino**, aunque personalmente creo que el famoso **Ritchie** no cruza el umbral de la locura. Como los grandes visionarios, él escribe, él dirige. No sé si existe el término que represente a estos inteligentes realizadores, pero ya lo buscaré. Es un hilo considerablemente enrevesado, diríase un laberinto en el modo de narrar. Empieza por el final, escenas simultáneas contadas desde distintos planos extraños y crudos, tiempos rápidos, detalles lentos, flashbacks... Es una forma muy hábil de narrar: lo que no le interesa lo resume y lo que le interesa lo extiende. Todo se entiende sin demasiada dificultad, pero no deja de sorprender el entrecruzamiento de las historias de los personajes.  
  

Los diálogos dentro de el excesivo uso de palabras malsonantes y repetitivas es ingenioso, es como componer en un piano de siete teclas la 5ª de Beethoven. Cada personaje tiene sus pirulas montadas y el resto intenta comprenderlas según estén arriba o abajo de la pirámide corrupta y mafiosa. Yo, el espectador se entretiene desde el principio, riéndose ante la perfecta definición de evidencia y ante la representación perfecta de la idiotez. Cada cara tiene una historia, un mote, todo fiel a su estilo narrativo.

  

El reparto se viste de trapicheos, estafas, robos, asesinatos y venganzas. Sorprende un peculiar **Brad Pitt** ininteligible, colaboradores ahora habituales como **Jason Satham, Jason Flemyng, ****Vinnie Jones** y muchos otros como **Benicio Del Toro, Dennis Farina,  Rade Serbedzija,  Alan Ford, Mike Reid, Robbie Gee, Lennie James, Ewen Bremner**, y algunas otras caras que no había visto en mi vida. Sin embargo todo está bajo la planificación y despliegue de la mente de G. Ritchie.  
  

En fin, tampoco es que sea una obra maestra pero tiene una increíble capacidad para llamar tu atención y obtiene una espontaneidad única que se convierte en el sello de la casa, no sabes por dónde va a salir y habla sin complejos y con mucho desparpajo de la realidad de la mafia, por buscar una escusa. '_**La liebre está jodida**_' puede ser una escena ejemplar de cómo es el dominio narrativo del director. En cualquier caso no olvidemos la portentosa banda sonora que no hace sino arañar los corazones del público para sentir más rabia y entrometerte en la historia. Una maestría irreverente propia del género.