---
title: 'Mommy'
date: 2018-04-10T17:35:00.000+02:00
draft: false
tags : [Canadá, Cine, Xavier Dolan]
---

He visto Mommy (Canadá, 2014, Xavier Dolan). Y me ha gustado mucho la idea de la anchura de la pantalla. Los colores. La vida que tiene esta película y su crudeza.

  

[![](https://4.bp.blogspot.com/-40UaXQrxiuI/WszZECbRygI/AAAAAAAATHw/1AJ7O6TZppE3A_H-Jtf5Hvo99ibtrvKzACLcBGAs/s320/vlcsnap-2018-04-10-17h30m24s152.png)](https://4.bp.blogspot.com/-40UaXQrxiuI/WszZECbRygI/AAAAAAAATHw/1AJ7O6TZppE3A_H-Jtf5Hvo99ibtrvKzACLcBGAs/s1600/vlcsnap-2018-04-10-17h30m24s152.png)

  

[![](https://3.bp.blogspot.com/-0mAOFau3NTk/WszZEJ6MNPI/AAAAAAAATHs/p6CTVyEp2oQw5mDF-DPbFbK28NYuVE1aACLcBGAs/s320/vlcsnap-2018-04-10-17h31m49s406.png)](https://3.bp.blogspot.com/-0mAOFau3NTk/WszZEJ6MNPI/AAAAAAAATHs/p6CTVyEp2oQw5mDF-DPbFbK28NYuVE1aACLcBGAs/s1600/vlcsnap-2018-04-10-17h31m49s406.png)