---
title: 'No se ___ para decir que uno ha ___'
date: 2020-05-04T19:36:00.002+02:00
draft: false
tags : [cuarentena, leer, vollmann, confinamiento]
---

**Puedes cambiar el hueco por el verbo que tú quieras.**  

> **_No se lee para decir que uno ha leído._**

Leyendo '_La Familia Real_' de nuestro colega Bill Vollmann, siento ese terror de estar frente a un mamotreto, inseguro de lo que me puede aportar. Las supuestas oportunidades que se van a perder mientras dure esta aventura, esta fiel aventura a un libro que sin duda, uno piensa (en contra de lo que le dijo su editor de 'un libro tiene que durar lo que necesite durar') podría ser más corta. Pero uno, como dicen en la novela, no se lo lee para decir que lo ha leído, no vale si uno no disfruta. En esta cuarentena aprendo a disfrutar de ello, a arriesgarme y dejarme llevar a donde quiere llevarme, el tiempo que sea necesario, lo cual me hace acelerar, para ver el final, para no perderme, para no quedarme sin oxígeno. Y tal vez sea por que en realidad, ¿qué tienen que aportarme todos esos libros que me están esperando más allá de la escalada solitaria de alcanzar su cumbre, estar absorto en cada paso y comprender lo que se ha ido irguiendo y elevándose frente a mí?

  

No es importante, pero mientras escribo estas líneas, previniendo con astucia que se me peguen las yemas de los dedos a las teclas del ordenador. Haciendo caso omiso de todo el calor que soporto en este momento y que está por venir. Luchando grado a grado por ser persona y dejar espacio para la virtud.