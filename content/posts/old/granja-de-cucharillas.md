---
title: 'Granja de cucharillas'
date: 2020-02-01T09:39:00.000+01:00
draft: false
tags : [poesía cotidiana]
---

Dejo entrar el sonido  
de cómo se despega la  
piel de la mandarina,  
el alboroto de los  
cubiertos que se unen  
a sus hermanos en el  
cajón.  
¿Hacen contigo este jaleo?  
Podríamos tener  
una granja de cucharillas de  
postre secuestradas  
de cafés que tengan té de invierno  
o esas con la punta recortada  
para volverlas inofensivas,  
si soy feliz aquí contigo  
ellas también.