---
title: 'Anastasia Romanov'
date: 2012-11-21T07:30:00.000+01:00
draft: false
tags : [Familia Imperial, mujer, Anastasia Romanov, Romanov, grandes duquesas, Historia, Anna Anderson, grandes leyendas]
---

Sin duda la historia es apasionante. Solo conocer lo que ocurrió, llena de un deleite especial nuestras ansias de saber. Una de las grandes leyendas del siglo XX  -una buena etiqueta para el caso- es la de la **Gran Duquesa de Rusia**: **Anastasia**. Únicamente el título de gran duquesa es poderoso y atractivo sabiendo que se trataba de una chica de diecisiete años. Es fantástico que la duda posea este momento de la historia. A pesar de la belleza y encanto de las grandes duquesas, la gestión de los **Romanov** en aquel momento no era igual de deslumbrante, y por eso estalló la revolución culminada con el magnicidio de la familia imperial. Reconozco que estas palabras  (Anastasia, duquesa, Romanov, imperial, Rusia...) tienen un sonido especialmente fascinante.

  

[![](http://3.bp.blogspot.com/-Llnua9vIhIA/UKvn4-f2MNI/AAAAAAAABmE/1wvSn74MJo0/s1600/rostard.jpg)](http://3.bp.blogspot.com/-Llnua9vIhIA/UKvn4-f2MNI/AAAAAAAABmE/1wvSn74MJo0/s1600/rostard.jpg)

  

Las hijas del zar Nicolás II, eran objetivo de los grandes elogios de la aristocracia y el pueblo ruso. Su singular atractivo las iconizó para la eternidad. Olvidando el marco histórico y las cuestiones de sociedad, resulta una verdadera tragedia el asesinato de aquella familia imperial, por su encanto y hermosura concentrados en las cuatro duquesas (**Olga, Tatiana, María y Anastasia**). Imagínate los bailes, el palacio de Invierno y la Rusia misma. No podían desaparecer de la tierra tan fácilmente cuando poseían la flor de la juventud. Tras la muerte de la familia Imperial, el mundo quedó conmocionado. Pero el momento fue cuando en distintos lugares de Europa estallaron algunas mujeres que afirmaban ser Anastasia, la gran duquesa menor.

  

La mera idea de pensar que la graciosa actriz de gran talento, ingeniosa y encantadora Anastasia se hubiera escapado misteriosamente de la muerte, era sencillamente una idea genial, solo con imaginarlo un escalofrío recorre el cuerpo. La más famosa de las mujeres que afirmaban ser Anastasia: **Anna Anderson** apareció en Alemania a punto de tirarse por un puente. Fue llevada a un centro psiquiátrico y allí revelo ser la gran duquesa. Mucha gente llegó a estar convencida de semejante noticia, ya que al parecer hablaba de detalles que tal vez solo la mismísima Anastasia podría conocer. 

  

[![](http://1.bp.blogspot.com/-ykskcSRNpR8/UKvoN3CtVeI/AAAAAAAABmM/iusNRK9UZjc/s1600/rostard+anast.jpg)](http://1.bp.blogspot.com/-ykskcSRNpR8/UKvoN3CtVeI/AAAAAAAABmM/iusNRK9UZjc/s1600/rostard+anast.jpg)El mundo aguantó la respiración cuando algunas comunidades científicas investigaron el asunto. En la tumba de los Romanov faltaban dos cadáveres, y posiblemente fueran los de la menor y el zarevich Alexis. Otros 'demostraron' que separarían dos cuerpos para que nunca se supiera por el número de cadáveres que ahí se encontraba la familia imperial, y posteriormente localizaron dos cadáveres que corresponderían a los desaparecidos, incinerados.  Supuestamente se demostró la muerte de Anastasia en 1918, así como que Anderson no se trataba de Anastasia con pruebas de ADN. 

  

Pero da igual, esto dio lugar a buenas adaptaciones cinematográficas, grandes novelas y la invitación a soñar con esta historia. Sin duda uno de los más fascinantes y hechizantes misterios del siglo pasado.