---
title: 'The Hives – Tick Tick Boom'
date: 2009-01-26T16:09:00.000+01:00
draft: false
tags : [Rock, Garage Rock, Musica, Rock Alternativo, The Hives]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SX3So2nK9wI/AAAAAAAAAYM/rffICAEvRXM/s320/hives.bmp)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SX3So2nK9wI/AAAAAAAAAYM/rffICAEvRXM/s1600-h/hives.bmp)  
Simplemente recordar a este grupo (**The Hives**), que vienen desde Suecia, la letra no tiene más, lo que me da la canción es diversión, rock en abundancia, es como una fiesta de cumpleaños hecha para guitarras y baterías y rock! Quería poner esta canción ahora que estamos de exámenes, porque la necesitamos, nos faltaba esto para las crisis, los agobios, la tensión, es como un grito de 20.000 decibelios para desahogarse. Bueno espero que os guste, y disfrutéis mucho con ella: ahí va...