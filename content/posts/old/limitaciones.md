---
title: 'Limitaciones'
date: 2018-02-19T18:59:00.001+01:00
draft: false
tags : [Crítica, Italia, Cine, deseo, Luca Gualdagnino, romanticismo, prejuicios, amor, sexualidad]
---

Es curioso pero muestra una nota en la sinfonía mundial. La última película de Luca Gualdagnino no va sobre homosexualidad, ese no es el tema. Como dicen en PlayGround:  
  

> '_es un film romántico, rabiosamente adolescente, sobre el primer amor y el despertar de la sexualidad, sobre el aprendizaje de la diferencia y el miedo al estigma. Pero es también una reflexión sobre la relación (ética, estética, epistemológica) con nuestro objeto de deseo. Los constantes paralelismos y referencias al mundo clásico acentúan el carácter ambiguo de una amistad que, a pesar de ser mucho más, nunca deja de serlo_'

Eudald Espluga, 1-feb-18, [PlayGround](https://www.playgroundmag.net/lit/5-libros-que-debes-leer-si-Call-me-by-your-name-te-ha-dejado-sin-respiracion_27599362.html)