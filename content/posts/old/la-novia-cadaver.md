---
title: 'La Novia Cadáver'
date: 2008-11-02T09:29:00.000+01:00
draft: false
tags : [Johnny Depp, Tim Burton, Cine, Buenas]
---

  

El otro día estuvimos viendo **la novia cadáver**, la película es muy buena. Tiene muchos valores, y mucha "filosofía" de **Tim Burton** (Director) detrás.Yo solo quiero remarcar algunos detalles.

  

[![](https://3.bp.blogspot.com/_VT_cUiG4gXs/SQ1mILWPUFI/AAAAAAAAAUQ/1hqQ9gSHsdg/s320/aaaaaaaaaNovia+xcadave.bmp)](http://3.bp.blogspot.com/_VT_cUiG4gXs/SQ1mILWPUFI/AAAAAAAAAUQ/1hqQ9gSHsdg/s1600/aaaaaaaaaNovia+xcadave.bmp)

  

  

Queda muy reflejado en la película que el mundo de los vivos es un aburrimiento: reglas, obligaciones, leyes, y por supuesto nada de sentir y todo eso, es como un mundo muerto según aparece en la película; el mundo de los muertos es un mundo donde simplemente los muertos continúan "muriendo", como otra vida (sin reglas ni leyes) estando felices, que en contraste con la otra vida, eso era autentica vida sin reglas ni leyes y con sentimientos.Pero en la película aparece un tercer lugar, un lugar para los que superan ambos mundos, la novia cadáver es capaz de amar sin poseer a Víctor (**Johnny Depp**), es capaz de superar el engaño que le hicieron, entonces con esa purificación es como pasa a la otra vida se redime porque ha superado el sufrimiento mortal y va camino a una vida plena (la luna en la película).Esta es la bonita historia: Víctor quiere a la novia cadáver y sigue con ella( porque se ha casado con ella) a pesar de la equivocación, la novia cadáver hace esto también, le quiere y por eso le deja casarse con Victoria (**Emily Watson**). 

  

En general en las películas de Tim Burton  queda visto que el esta por encima del bien y del mal quiero decir: está por encima de la sociedad, en un mundo de amor donde la apariencia física es mas bien insignificante, donde importa el corazón, los sentimientos.  
  
  
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_  
NOTA GRÁFICA: 28.11.17  
Así era mi careto cuando empecé mis andaduras como bloguero simple. 08.11.08.  
  

[![](https://2.bp.blogspot.com/-N6IccRqPDZs/Wh2xTaPjhlI/AAAAAAAASVE/z_idisEZdKMwgRxf84_sWW5828YMz8UrwCLcBGAs/s400/P1000724.JPG)](https://2.bp.blogspot.com/-N6IccRqPDZs/Wh2xTaPjhlI/AAAAAAAASVE/z_idisEZdKMwgRxf84_sWW5828YMz8UrwCLcBGAs/s1600/P1000724.JPG)

  
  

**.**