---
title: 'Enrique García-Máiquez - Variación Sobre Cardenal'
date: 2011-04-09T12:35:00.000+02:00
draft: false
tags : [Enrique García-Maiquez, Literatura, Poesía]
---

Algo que me leyeron de un libro de poemas.

  

  

  

VARIACIÓN SOBRE CARDENAL

  

Sensibles treintañeras, que algún día leáis

mis versos encendidos de pasión conyugal

y soñéis un poeta con cierto aire bohemio

pero a la vez monógamo y siempre muy romántico,

  

sabed que yo los hice a una como vosotras

\-idéntica- y que ella, sin embargo,

prefería, sin duda,

que pusiese la mesa y fregase los platos.

  

  

  

“Con el tiempo” Enrique García-Máiquez. Ed. Renacimiento 2010.