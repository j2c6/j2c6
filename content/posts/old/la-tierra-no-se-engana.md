---
title: '''La tierra no se engaña'''
date: 2017-09-29T00:01:00.003+02:00
draft: false
tags : [tierra, vida, frase, misterio]
---

Y eso es. Lo que se siembra se recoge. Por otro lado en la vida estamos solos ante el misterio.