---
title: 'El sol cubría todo: las flores...'
date: 2010-04-17T06:00:00.000+02:00
draft: false
tags : [El Vesubio, Microrrelatos, La Escalera Que Quemaba Ríos de Tinta]
---

![](http://2.bp.blogspot.com/_VT_cUiG4gXs/S8iXhe5i_QI/AAAAAAAAArE/RV6PTTXpApw/s320/396px-Nike_of_Samothrake_Louvre_Ma2369_n2.jpg)

Victoria de Samotracia

  
  
El sol cubría todo: las flores, el rocío de la mañana,  las sonrisas de los inadvertidos,  la vida, la simplicidad de aquella ciudad que se levantaba para honrar a la humanidad.  Las mariposas dibujaban  lo sublime  entre los castaños, robles y alisos que rodeados de pétalos simulaban el Edén. “Bendita ignorancia” se dijo después. Dulces cantos sonaban en la rutina de aquel ángel sin alas, que  caminaba hacia la fuente. Un agudo murmullo, se clavó en el fondo de su alma, revelándose  el pánico todavía desconocido en su palpitar. Su visión era un espejismo, que la había apartado de la realidad: una gran nebulosa  quemaba todo lo que antes era precioso en aquella colina. Y es que estaba previsto: hasta _La Casa del Fauno_ seria arrasada con todo su esplendor.  El hechizo la hipnotizó, no respondía. Quedó sumisa  en una pena terrible.  Intentó recordar la brevedad de sus días,  y pensó en los que llenaban su  corazón. La orquesta de tallos, y hierba que la rodeaba la acogió como a una hija. Finalmente lo asumió y contempló el destino con cierta admiración, vio como ardía el encanto de la vida. La sorpresa se había convertido en sentencia.  Su mirada se fijó en una sencilla margarita, que arrancó y observó con melancolía.  No  podía calmar el ardor de sus mejillas con el llanto. Mientras bajaba las escaleras delicadamente, la belleza se posó en sus ojos en forma de lágrimas de cristal, que no derramó, conservándolas como un juguete en manos de un niño, inocente víctima de la vida. Aquella pincelada de todo quedó grabada en piedra para la eternidad. Sería recordada por los herederos. El Vesubio de Pompeya.