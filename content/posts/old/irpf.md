---
title: 'irpf'
date: 2020-02-05T11:54:00.003+01:00
draft: false
tags : [poesía cotidiana]
---

Pensando en la renta,  
recuerdo que la vida son  
los tomates, las  
primaveras al sol  
los besos y ser persona.