---
title: 'Largo domingo de noviazgo (2004)'
date: 2012-01-31T19:22:00.000+01:00
draft: false
tags : [Cine, Guillaume Laurant, París, Bruno Delbonnel, 2000, Audrey Tautou, Jean-Pierre Jeunet, Gaspard Ulliel]
---

Manech (**Gaspard Ulliel**) se fue a la guerra, la primera guerra mundial y desapareció. Mathilde (**Audrey Tautou**) su novia, recibe la noticia de que tras ser sometido a un consejo de guerra, hay pocas posibilidades de que haya sobrevivido al campo de batalla. La esperanza de Mathilde parece inquebrantable, y no satisfecha con esa mera suposición se embarca en la búsqueda de Manech con la profunda esperanza de que ha sigue vivo.

  

El director **Jean-Pierre Jeunet** y su guionista **Guillaume Laurant** vuelven a aparecer en los carteles tras su gran triunfo mundialmente conocido como '**Amelie**' (2001). Y parece ser que la fórmula les gustó, de modo que ¿porqué no hacer otro plato con tan exquisita receta? El resultado ha sido '**Largo domingo de noviazgo**' donde vuelve a aparecer ese filtro de colores que hace las escenas idílicas, siempre pertenecientes a la fantasía de una mente romántica e imaginativa. Los cuadros pintados por **Bruno Delbonnel **(fotografía), tienen su propia firma, pues en lo trágico de la historia, en la desgracia de los personajes siguen apareciendo los más radiantes rayos de sol, los campos más verdes de la primavera más joven. Y no pararía nunca de admirar esos tonos.

  

[![](http://2.bp.blogspot.com/-448iFt-c6Jo/TygxDNXPTYI/AAAAAAAABL8/0gB2Z5TV5RE/s1600/rostards.jpg)](http://2.bp.blogspot.com/-448iFt-c6Jo/TygxDNXPTYI/AAAAAAAABL8/0gB2Z5TV5RE/s1600/rostards.jpg)

  
  
Una fotografía así hace del guión una fábula, un cuento eterno que te sumerge en la esperanza de la protagonista. Un cine narrativamente poético y deliciosamente visual, donde los detalles son míticos y de leyenda, las frases de los diálogos son de un drama evocador mezclado con un humor inesperado y surrealista. A pesar de que no hay quién se aclare con los personajes: Bastoche, Six-Sous, Lavrouye, Biscotte... tampoco hay quién mire el reloj, el espectador simplemente disfruta. La recreación de la guerra, la decoración de la granja e incluso **París** en los años veinte son espectaculares, pero nada tan espectacular como el eje central de este drama.  
  
  

**Audrey Tautou** la encantadora protagonista de la película hace realidad este romance con una actuación inefable. Quizá el mérito solo sea el de una mirada tan inusual y cautivadora como la suya, que la presenta llena de inocencia y credibilidad. El atractivo de la actriz francesa es uno de los secretos de este largometraje.