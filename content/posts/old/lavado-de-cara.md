---
title: 'Lavado de cara'
date: 2017-11-28T20:00:00.000+01:00
draft: false
tags : [miblancoynegro, wayback machine, Historia, blog, diseño]
---

Pues sí, he decidido darle un lavado de cara al blog, buscando algo más retro. Porque aunque haya cambiado muchas veces de dirección, ahí donde lo ves tiene 9 años con cambios de nombre y esas cosas.  

  

  

  

[![](https://3.bp.blogspot.com/-vXkv6AKl-1k/Wh24lnYCDyI/AAAAAAAASVU/PhhlmNS8PBU6yyEzp1W6c4PG6k11TesEACLcBGAs/s400/pirulaselementales.PNG)](https://3.bp.blogspot.com/-vXkv6AKl-1k/Wh24lnYCDyI/AAAAAAAASVU/PhhlmNS8PBU6yyEzp1W6c4PG6k11TesEACLcBGAs/s1600/pirulaselementales.PNG)

  

  

  

  

  

Y de paso he decidido dejar constancia de la trayectoria bloguera.

  

En 2008 se abrió miblancoynegro del que conservo todas las entradas. Fue un hito. Disfruté mucho y aunque hay cosas que ahora suenan feas, lo miro con cariño, como Dickens a David Copperfield.

  

[![](https://3.bp.blogspot.com/-KX5_klu1Xik/Wh24lmHzGII/AAAAAAAASVY/TlmD_9V9FVIbAHoWY1cIf7AOMvWR8nCpwCLcBGAs/s400/miblancoynegro.PNG)](https://3.bp.blogspot.com/-KX5_klu1Xik/Wh24lmHzGII/AAAAAAAASVY/TlmD_9V9FVIbAHoWY1cIf7AOMvWR8nCpwCLcBGAs/s1600/miblancoynegro.PNG)

Mi Blanco y Negro allá por 2010.

  

Luego monté con mi padre una web de tonterías para aliviar el trabajo del personal, en plan finofilipino y compramos un dominio: malditolunes.es Subíamos montones de post y tuvo un pico cuando en el informer de la UPV pusimos una nota...900 visitas aquella hora. Funcionó un año escaso.

  

[![](https://2.bp.blogspot.com/-QpcL3dORkuI/Wh24mkHxaCI/AAAAAAAASVc/9VhlTFOamqoImiEjApv0sz2gazGdT3-rQCLcBGAs/s400/malditolunes.PNG)](https://2.bp.blogspot.com/-QpcL3dORkuI/Wh24mkHxaCI/AAAAAAAASVc/9VhlTFOamqoImiEjApv0sz2gazGdT3-rQCLcBGAs/s1600/malditolunes.PNG)

Esa pinta tenía, el fondo lo diseñó él. Mítico.

 En un intento de volver a empezar apareció Rostard, que sonaba como el nombre de un personaje del siglo XIX. Tuvo grandes momentos de redacción y se desinfló. Tal vez entre 2012 y 2013.  
  

[![](https://1.bp.blogspot.com/-8ChhMO2eoIQ/Wh24mpi67zI/AAAAAAAASVg/olyRNMhW9jktqIpwwplncilHlK8DxlmKQCLcBGAs/s400/rostard.PNG)](https://1.bp.blogspot.com/-8ChhMO2eoIQ/Wh24mpi67zI/AAAAAAAASVg/olyRNMhW9jktqIpwwplncilHlK8DxlmKQCLcBGAs/s1600/rostard.PNG)

Rostard con un diseño más despejado que MBYN.

  
Y eso es todo por hoy. Batallitas binarias.