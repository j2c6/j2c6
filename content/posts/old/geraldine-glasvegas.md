---
title: 'Geraldine - Glasvegas'
date: 2009-07-23T10:07:00.000+02:00
draft: false
tags : [Glasvegas, Musica, Rock Alternativo]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SmgdQBcqnlI/AAAAAAAAAik/l_Pk2R4E4-U/s320/glasvegas_glasvegas_tapa.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SmgdQBcqnlI/AAAAAAAAAik/l_Pk2R4E4-U/s1600-h/glasvegas_glasvegas_tapa.jpg)  

La verdad es que hace tiempo, que no escribo un artículo, en "**Mi blanco y negro**", pero como dicen mas vale tarde que nunca. El grupo del articulo, no es nada espectacular, saco su primer disco en septiembre de 2008, **_Glasvegas,_** y esta es una de las canciones que llego, a estar, en lista de exitos. La cancion es progresiva, y en su momento mas alto, tiene poder, la voz del vocalista, es insistente: suplica. Es un ritmo, sencillo, que te hace balancearte, lento...Es un estilo, un poco raro, es rock alternativo, de tipo indie, desde **Glasgow**, lugar donde hay mas bandas, por metro cuadrado. Estos saben. Solo hay que fijarse, es su primer album grabado en estudio que tienen, suficiente para llegar al Nº1 en Escocia...Bueno escuchadla, a ver qué tal...