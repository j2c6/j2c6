---
title: 'Estofado de Ternera'
date: 2020-05-29T10:15:00.000+02:00
draft: false
tags : [puchero, estofado, olla, receta, caldo, guisado, cuchara]
---

**Ingredientes**  

*   Un kilo de ternera, yo qué sé, gallega.
*   Dos cebollas.
*   Cuatro dientes de ajo.
*   Dos zanahorias.
*   Un vaso de vino tinto.
*   Sal, pimienta.
*   Laurel, orégano, perejil, tomillo y romero.
*   Dos tomates maduros.
*   Un litro de caldo de carne.

  
**Preparación**  

1.  Limpiamos la carne y la troceamos, la añadimos en aceite de una cazuela y doramos. Sacamos y reservamos.
2.  En el mismo aceite sofreimos las cebollas, los ajos. Y vamos añadiendo en orden esperando 8-10 minutos entre cada uno: Zanahorias, tomates picados, vino tinto, especias.
3.  Salpimentamos y añadimos la carne reservada, cubrimos con caldo. Esperamos que hierva y cocinamos dos horas a fuego medio con tapa.
4.  Cuarenta minutos antes de servir, añadimos las patatas en dados para que hiervan.

  

[![](https://1.bp.blogspot.com/-Rkf3MgRNlPE/XtDESXNq11I/AAAAAAAAfcE/o7Gsccs3lLYyqfBbm0cvlkZtvKxnK58EgCK4BGAsYHg/w400-h400/photo_2020-05-29_10-13-43.jpg)](https://1.bp.blogspot.com/-Rkf3MgRNlPE/XtDESXNq11I/AAAAAAAAfcE/o7Gsccs3lLYyqfBbm0cvlkZtvKxnK58EgCK4BGAsYHg/photo_2020-05-29_10-13-43.jpg)