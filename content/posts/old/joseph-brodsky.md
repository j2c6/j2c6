---
title: 'Joseph Brodsky'
date: 2019-05-24T13:23:00.004+02:00
draft: false
tags : [Rusia, Joseph Brodsky, Poesía]
---

Querida, era bien tarde hoy cuando salí de la casa  
para aspirar un poco de aire fresco del océano.  
Se quemaba el ocaso como un abanico chino estremecido  
y los nubarrones se elevaban como la cola  
de un piano negro de concierto.  
Veinticinco años atrás te ponían loca los dátiles,  
dibujabas con tinta, cantabas un poquito,  
te divertías conmigo y te fuiste con un ingeniero químico.  
Las cartas dicen que hoy eres tenazmente tonta,  
que te ven en las iglesias de provincia  
y en las de la capital.  
Vas a las misas por amigos comunes  
que no volverán a tu vecindad.  
Y estoy contento de que no haya en el mundo  
tanta distancia como la que nos separa a los dos  
No me entiendas mal. Ya nada me une  
a tu cuerpo, a tu nombre, a tu voz.  
Nadie los ha destruido. Fíjate.  
Pero por lo menos para olvidar una vida  
uno requiere de otra y para mi eso fue cosa vivida.  
Tuviste suerte también, ¿dónde más sino en la fotografía  
seguirás siendo siempre joven, sin arrugas, ligera?  
pues el tiempo enfrentado a los recuerdos  
se da cuenta de su falta de derechos.  
Fumo en las sombras y aspiro el hedor de la marea.  
  
(Traducido por Rubén Darío Flórez A)