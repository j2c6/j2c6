---
title: 'El Mercader de Venecia - William Shakespeare'
date: 2010-07-25T06:26:00.000+02:00
draft: false
tags : [Italia, Shakespeare, Sylock, Un Clásico, Teatro, Venecia, Literatura, Basanio, El Mercader de Venecia]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TERgWuSXGdI/AAAAAAAAAwU/uYWNMADI4Rw/s400/venecia1.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TERgWuSXGdI/AAAAAAAAAwU/uYWNMADI4Rw/s1600/venecia1.jpg)Dicen que es de los más grandes: **Shakespeare**. Todavía no me había leído nada de William, pero dicen que si conoces los clásicos conoces la humanidad, así que: ¿porqué no empezar ya?. Antonio tras fiar un préstamo pedido por su amigo Basanio a Sylock, debe firmar el contrato que dice que su incumplimiento le costará una libra de su carne. En esta famosa obra, he descubierto algunos de los encantos que existen entre los **dramas de Shakespeare**: una literatura llena de cultura y humanidad, con ciertos valores y un lenguaje artístico en boca de los protagonistas, y una línea argumental con elementos históricos, mitológicos y las situaciones llevadas al extremo. Al principio hay que acostumbrarse a su lenguaje, pero una vez dentro es maravilloso. Me ha gustado mucho. Seguiremos hablando.

_"BASANIO (p.56): Antes harán alianza el fuego y el hielo que mi amor y la traición._

  

_PÓRCIA (p.58):  El amor nace en los ojos, se alimenta de miradas y muere por desvíos de la misma donde nace._

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TERfcKj1b4I/AAAAAAAAAwM/-sZ7t0ip5Cw/s400/shakespeare_taming.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TERfcKj1b4I/AAAAAAAAAwM/-sZ7t0ip5Cw/s1600/shakespeare_taming.jpg)

_BASANIO (p.101): ¿Qué podré hacer? ¿Cortarme la mano izquierda y decir que perdí el anillo defendiéndome?_

_LORENZO (p.97): Por eso dicen los poetas que el tracio Orfeo arrastraba en pos de sí árboles, ríos y fieras: porque nada hay tan duro feroz y selvático que resista al poder de la música._

_PÓRCIA (p.98): El cantar del cuervo es tan dulce como el de la alondra, cuando no atendemos a ninguno de los dos, y de seguro que si el ruiseñor cantara de día, cuando graznan los patos, nadie le tendría por tan buen cantor. ¡Cuanta perfección tienen las cosas hechas a tiempo! ¡Silencio! Duerme Diana en brazos de Endimion, y no tolera que nadie turbe su sueño._

_BASANIO (p.59): Muchas veces engañan las apariencias. ¿Ha habido causa tan mala que un elocuente abogado no pudiera hacer probable, buscando disculpas para el crimen más horrendo? ¿Hay alguna herejía religiosa que no tenga sectarios, y que no pueda cubrirse con citas de la Escritura o con flores retóricas que disimulen su fealdad? ¿Hay vicio que no pueda disfrazarse con la máscara de la virtud?¿No habéis visto muchos  cobardes, tan falsos y movedizos como piedra sobre arena, y que por fuera muestran la belicosa faz de Hércules y las híspidas barbas de Marte, y por de dentro tienen los hígados tan blancos como la leche? Fingen valor, para hacerse temer. Medid la hermosura: se compra __al peso, y son más ligeras las que se atavían con los más preciados arreos de la belleza. ¡Cuantas __veces los áureos rizos, enroscados como sierpes al rededor de una dudosa belleza, son prenda de otra __hermosura que yace en olvidado sepulcro! Los adornos son como la playa de un mar proceloso; como __el velo de seda que oculta el rostro de una hermosura india; como la verdad, cuya máscara toma la __fraude para engañar a los más prudentes. Por eso desdeño los fulgores del oro, alimento y perdición __del avaro Midas, y también el pálido brillo de la mercenaria plata. Tu quebrado color, oh plomo que __pasas por vil y anuncias más desdichas que felicidad, me atrae más que todo eso. Por ti me decido._

_¡Quiera Dios cumplir mi amoroso deseo! _

  
_PÓRCIA: (Aparte). Como el viento disipa las nubes, así huyen de mi alma todos los recelos, tristezas y __desconfianzas. Cálmate, amor; ten sosiego: templa los ímpetus del alma, y dame el gozo con tasa, __porque si no, el corazón estallará de alegría._

_BASANIO:__(Abre la caja de plomo). ¿Qué veo? ¡El mismo rostro de la hermosa Porcia! ¿Qué pincel __sobrehumano pudo acercarse tanto a la realidad? ¿Pestañean estos ojos, o es que los mueve el reflejo __de los míos? Exhalan sus labios un aliento más dulce que la miel. De sus cabellos ha tejido el pintor __una tela de araña para enredar corazones. ¡Ay de las moscas que caigan en ellos! ¿Pero cómo habrá __podido retratar sus ojos, sin cegar? ¿Cómo pudo acabar el uno sin que sus rayos le cegaran de tal __modo que dejase sin acabar el otro? Toda alabanza es poca, y sería afrentar al retrato tanto como el __retrato al original. Veamos lo que dice la letra, cifra breve de mi fortuna. (Lee). «Tú a quien no __engañan las apariencias, consigues la rara  fortuna de acertar. Ya que tal suerte tuviste, no __busques otra mejor. Si te parece bien la que te ha dado la fortuna, vuélvete hacia ella, y con un beso __de amor tómala por tuya, siguiendo los impulsos de tu alma». ¡Hermosa leyenda! Señora, perdón. Es __necesario cumplir lo que este papel ordena. A la manera que el gladiador, cuando los aplausos __ensordecen el anfiteatro, duda si es a él a quien se dirigen, y vuelve la vista en torno suyo; así yo, __bella Porcia, dudo si es verdad lo que miro, y antes de entregarme al gozo, necesito que lo confirmen __vuestros labios."_

_Por último:_

_"La reputación es el agobio de los tontos." _