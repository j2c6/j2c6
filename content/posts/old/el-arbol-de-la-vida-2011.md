---
title: 'El Árbol de la Vida (2011)'
date: 2011-09-22T21:14:00.000+02:00
draft: false
tags : [Terrence Malick, Stravinski, Sean Penn, El Árbol de la Vida, Jessica Chastain, Zbigniew Preisner, Oscar Wilde, Alexander Desplat, Cine, Festival de Cannes, Emmanuel Lubezki, Kubrick, Brad Pitt]
---

[![](http://1.bp.blogspot.com/-AxJ1_Wj_TmU/TnuIswe6ZdI/AAAAAAAABAs/OGdaDReHe0Y/s320/elarboldelavida2.jpg)](http://1.bp.blogspot.com/-AxJ1_Wj_TmU/TnuIswe6ZdI/AAAAAAAABAs/OGdaDReHe0Y/s1600/elarboldelavida2.jpg)Las expectativas son siempre un obstáculo para apreciar las cosas. Desde el **Festival de Cannes 2011**, esta película ha prometido muchas grandezas. Únicamente ver trabajar a **Brad Pitt **y a** Sean Penn** en el mismo filme hace pelearse a cualquiera  por conseguir unas entradas. Siempre me ha gustado ir al cine, y más cuando lo que vas a ver puede ser bueno, va a hacer historia.

  

  

Empezó, y todo era perfecto cada plano cada toma era el preámbulo de una mejor, sin embargo tantos preparativos hacen esperar una buena historia, cargada de humanismo, vida y alma. Todo deseo, se vio confundido. No sabíamos qué íbamos a ver, quizá una introducción honesta, alejada del marketing y  fuerza publicitaria hubiera bastado para ver la película, es suficiente con que a un niño le prometas helado al final de la comida, para que se la trague a  toda velocidad sin saborear bocado para llegar a lo ansiado. Pero la obra era muy distinta. Apareciendo la cita del libro de Job y posteriormente todas las increíbles imágenes del planeta, desde el diminuto crecimiento humano, hasta la macroscopia del universo mientras se escucha la espeluznante "_**Lacrimosa**_" de **Zbigniew Preisner**, es difícil vivir el momento con toda su intensidad,  esperaba otra cosa, pero me rendí ante semejante escena. Imágenes del telescopio Hubble, ríos, océanos, montañas, volcanes, desiertos. El mundo no es un documental, es  vida, es un absoluto  milagro. Algunos compararon este espectáculo con el de **Kubrick** en "_2001: Una Odisea del Espacio_"_(1968)_. Este es otro tipo de cine, no es comparable, que no se emitan los juicios comerciales que se ejercieron sobre otras producciones. Es contemplativo, poético, abstracto en cuanto a lo que narrativo se refiere, un poema visual. Hasta las escasas escenas de interpretación una cámara inusual nos enseña tomas muy cuidadas, una exquisita fotografía de **Emmanuel Lubezki**. Teatro silencioso, palabras que no se dicen. El filtro que refleja los años 50: colores _polaroid_, inocentes, soñados. Dulce música de **Alexandre Desplat** ("_El Discurso del Rey" (2010)_).

  

 La familia O'Brien  ha perdido al joven Jack que ha muerto, su madre atenta, cariñosa, amable (**Jessica Chastain**), y el padre (**Brad Pitt**) severo y afectuoso pero muy autoritario. Los personajes van descubriendo el mundo y van obteniendo lecciones de la vida. Así como guardando en sus corazones las experiencias que da la existencia --los buenos y malos momentos--. Perder a un hijo hace que te plantees todas esas preguntas filosóficas que hacen temblar desde los comienzos de la historia a la humanidad.  Todo el énfasis se concentra en esa inquietud, toda la película es un detalle, una pregunta, una experiencia de un momento repetida infinitas veces, algo inefable: un poema.   
  
  

[![](http://2.bp.blogspot.com/-Kpz8eTwfoFk/TnuIr3c5MtI/AAAAAAAABAo/_G6DWQtubH8/s1600/el-arbol-de-la-vida.jpg)](http://2.bp.blogspot.com/-Kpz8eTwfoFk/TnuIr3c5MtI/AAAAAAAABAo/_G6DWQtubH8/s1600/el-arbol-de-la-vida.jpg)

  

La reiteración, el arriesgado modo de esta película experimental de** Terrence Malick**, profesor de filosofía, autor de _"**El Árbol de la Vida**"_, junto con su trayectoria --seis películas en 42 años-- de cine sensible, exigente, depurado, perfeccionista, da cierta explicación a las preguntas de los espectadores. La obsesión de Malick de retocar, cada pincelada en el montaje y rodaje de la obra es muy significativa. Quizá esta película es la promesa de la cercanía del séptimo arte al mismo arte. Sin duda no comprendí la película, no es una historia, no hay sinopsis, aquí el "cómo" es lo más importante. No sé si es una obra maestra pero sí es algo inusual: espectacular. Este director se ha hecho un espacio en el mundo del cine. El tema narrativo es archiconocido, sin embargo está película es el despliegue de la condensada pregunta de la vida: ¿porqué?.

  

Así pues, más de la mitad del largometraje son imágenes despersonalizadas, o personalizados silencios. La otra mitad es una lacónica pero rica visión de una familia. El público es ahora muy caprichoso, de ahí la osadía de **Malick**. Los espectadores no entienden y se enfadan, escupen a la obra, no están acostumbrados a escuchar, están habituados a oír lo que quieren en cualquier momento y permanentemente, como en un bufete de sensaciones muy valiosas que minusvaloran o desprecian por su actitud despótica con el arte, con el mundo. Ha perdido esa ingenuidad, humildad de oyente ante la soberbia de una obra. Tal vez la obra sea revolucionaria, o no apta para este tiempo abanderado contra la censura. La causa puede ser el dinero: algunas de las productoras se dedican a vender, hacen vulgar lo sublime, lo hacen pegajoso, superficial, no educan a su público, no lo enriquecen, lo embrutecen entreteniéndolo banalmente. O quizá es el abuso del arte del siglo XXI, o la hipocresía de algunas personas que se hace llamar artistas. Los comprensibles abucheos a esta película que empezaron en el **Grand Théâtre Lumière,** o la sensación de estafa de los espectadores podrían ser bien parecidos a los que recibió** Stravinski **con "**La Consagración de la Primavera**" (1913).

  

Se ha hablado mucho de esta película, unos se sienten engañados --sino que hablen de los escasos 5 minutos de aparición con **Sean Penn**\-- y otros están inclinados, de rodillas ante semejante obra. El cine está para disfrutar, por lo que odia esta mentira o ama esta maravillosa verdad. "_La diversidad de opiniones sobre una obra de arte muestra que esa obra es nueva, compleja y que está viva. Cuando los críticos disienten, el artista está de acuerdo consigo mismo_." **Oscar Wilde **(Prefacio del** Retrato de Dorian Gray**)