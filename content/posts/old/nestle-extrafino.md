---
title: 'Nestlé Extrafino'
date: 2020-04-25T12:53:00.002+02:00
draft: false
tags : [azúcar, Nestle, dulce, Chocolate, comida]
---

Algún día haremos un análisis de porqué el chocolate Nestlé Extrafino está tan bueno y es adictivo. Algunos tratarán de simplificar la respuesta con una palabra: azúcar. Y no hablarán de la forma que tienes de partir filas de cuadritos, cómo se abre el paquete como si fuera el sobre con una carta escrita a mano o cómo, a temperatura de ambiente, encuentra el estado ideal para derretirse y fundirse en tu boca, y sobre todo, el cómo se funde ¿dónde están los que estudian Mecánica de Fluidos?

Lo ideal hubiera sido subir una foto con esto.