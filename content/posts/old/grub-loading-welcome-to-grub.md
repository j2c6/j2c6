---
title: 'Grub loading. Welcome to GRUB!'
date: 2018-09-16T16:55:00.000+02:00
draft: false
tags : [ubuntu, grub, linux, PC, Boot Repair, grub rescue, arranque]
---

Enciendes el precioso ordenador de la empresa y:  
  
Grub loading.  
Welcome to GRUB!  
error: file '/grub/i386-pc/normal.mod' not found.  
Entering rescue mode...  
grub rescue>  
  
Total, que buscando, la solución que me vale es:  
  
cmdpath=(hd0)  
prefix=(hd1,msdos1)/boot/grub  
root=hd1,msdos1  
set prefix=(hd1,msdos1)/grub  
insmod normal  
normal  
  
Pero cada vez que lo enciendes tienes que 'solucionarlo'. Al final he encontrado una aplicación, Boot Repair, de seguir los pasos que te liquida, por el momento el problema. Para el Terminator:  
  
sudo add-apt-repository ppa:yannubuntu/boot-repair  
sudo apt-get update  
sudo apt-get install boot-repair  
  
Luego buscas 'Reparador de Arranque' en tus aplicaciones y eso es todo, aventuras.