---
title: '4 versiones diferentes'
date: 2018-05-16T12:19:00.000+02:00
draft: false
tags : [fe, ramas, JC, ruido, jesucristo, Verdad, cristianismo, oración, biblia, silencio]
---

> _Apuntad todo bien...no vayan a salir luego cuatro versiones diferentes_

JC en la imaginación popular 

  

Parece mentira que con todos los memes y las 'bromas' de semana santa te des cuenta, o hagas hincapié en lo \_\_\_\_\_\_\_\_\_ de la historia de la salvación. Un Dios (herencia cristiana esa mayúscula) que crea a los hombres en los que aparece el pecado original y deja embarazada a una virgen ya comprometida para que nazca su hijo que en realidad es él mismo para mandar su mensaje, pero sobre todo para morir crucificado por los hombres para salvarlos del pecado. Es una pirula espectacular. 

  

Si te 'limitas' a JC y sus enseñanzas (qué se yo: la otra mejilla, amar al prójimo, etc), en cualquier caso te ahorras todo el sistema que viene después. Donde no debes comer una hora antes de comulgar, si mueres en sábado con el escapulario de la Virgen del Carmen puesto vas directo al cielo, la confesión perdona los pecados pero no la huella que borra la Indulgencia Plenaria y toda la rigidez y ritualidad de los sacramentos, lo que vale y lo que no. El motor de la culpa. El infierno casi como método de justicia infantil.

  

Pero no es una cuestión de ahorro. La abundancia de obstáculos para alcanzar la verdad no brillante, la auténtica, la que no se mide en número de asistentes es apabullante. No puedo cargar con los preceptos y las diferencias escatológicas hasta tener mi relación con Dios (o dios, para distinguirlo del Yavéh), es más, creo que debo desprenderme de ellas y mirar dentro de mí, en silencio, sin ruido mandamentario.

  

Los primeros concilios se centran aparentemente en cuestiones complejas como la dualidad dios-hombre de JC. A medida que vas avanzando y pasas los dogmas de fe, te encuentras con toda una lista de leyes, si es que se pueden llamar así, acerca de, qué sé yo una vez más, la excomunión, la actividad de los templarios, y otras cosas más mundanas puramente legislativas.

  

Todavía no entiendo como puedes ser cautivado en una pompa de tales dimensiones. Es cierto que el espíritu del cristianismo tal cual lo entiendo y es, disfruta de muchas verdades universales. Por ejemplo la oración como mecanismo de recogimiento interior, muy similar a la meditación o el silencio y la paz personal. Pero todas las ramas, el ruido, no te dejan centrarte en lo esencial, en lo que se espera finalmente de un creyente o un cristiano. 

  

Pero en realidad sí lo entiendo. En mi caso al menos se trataba de una pompa social, que obedece a un código moral de catalogación. Se ahoga la intuición del corazón, todo tiene su método y su validación y es sometido al escrutinio. Eso, por supuesto en general. No me cabe duda de que existe gente que trata de penetrar hondo en el misterio y que vive las cosas de forma cercana a como las cree, pero son pocos los elegidos.