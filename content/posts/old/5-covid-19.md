---
title: '#5 COVID-19'
date: 2020-03-15T15:58:00.000+01:00
draft: false
tags : [pandemia, coronavirus, diario coronavirus, covid-19]
---

En la plaza de Rutilio Gacís ponen el himno con letra y reciben abucheos, la canción de "resistiré". Una pandilla de gente que parece que se pasa el día en el parque, bebe despreocupadamente. Aparecen de nuevo, algunos miembros del personal sanitario en audios y vídeos cargados de responsabilidad y conocimiento, soberbios y nobles, hablando de la ineptitud del populacho, de su comportamiento inadecuado, de lo que ignoran y es bochornoso frente al a la gran labor que están realizando y por la que se sienten superestrellas. En Francia aparece una nota de prensa alertando del efecto negativo del Ibuprofeno en pacientes jóvenes con coronavirus. 288 muertos, 7800 contagios, 520 altas médicas, 320 casos en la UCI.  
  
Parece que el aspecto desértico de las calles con un flujo escaso de caminantes en busca del supermercado o los paseadores de perros, se ajusta mejor a la crisis que estamos viviendo. Confirman que el confinamiento durará más de 15 días, se da por echo. El humor sigue en su momento álgido. No aparecen medidas contundentes para proteger una economía que va a estallar, ya que ¿quién va a compensar todas las pérdidas de este cierre continuado? Mientras tanto la gente sigue haciendo hincapié en el movimiento de quédate en tu puta casa, con las formas más comerciales posibles. Animando el aburrimiento de los demás, incapaces de estar consigo mismos, lo que resulta insoportable.  
  
Me gustaría que esta situación y el sufrimiento que está por venir me trajeran algo de conocimiento provechoso, algo que entender de la vida. Me sigo admirando de lo que cambia la vida y la situación de un país de un día para otro. Vivimos momentos históricos. Empiezo a masticar teorías conspiranoicas y de terrorismo biológico, la guerra comercial entre USA y China tras el 5G. Pero el Coronavirus no tiene porqué tener una explicación, un responsable total. Lo que sí me parece patético son los 5 minutos de gloria que conquista una nueva persona cada 5 minutos. Creo que no entiendo a la gente, no entiendo porqué tiene que hacer un esfuerzo especial para abordar su vida familiar o personal en casa. Las empresas de la atención deben de estar ganando lo suyo.  
  
Me pone nervioso que no haya un estado de alarma, más allá del 'estado de alarma' provocado por el gobierno. La sensación de trascendencia más allá de estas dos semanas. Sin embargo a la vez, pienso que no ayuda entrar en pánico. Corea parece un ejemplo espectacular de cómo actuar y que deja a España en evidencia total.  
  

[![](https://1.bp.blogspot.com/-CktE5OtKv_0/Xm-UZEe0eSI/AAAAAAAAfCs/KGV8mPN7mHQMi6I3HywCo1qDlBus6NwgQCNcBGAsYHQ/s320/_R072849.jpg)](https://1.bp.blogspot.com/-CktE5OtKv_0/Xm-UZEe0eSI/AAAAAAAAfCs/KGV8mPN7mHQMi6I3HywCo1qDlBus6NwgQCNcBGAsYHQ/s1600/_R072849.jpg)

Vitamina C para desayunar.