---
title: 'Deseo De Ser Punk - Belén Gopegui'
date: 2010-04-24T17:16:00.000+02:00
draft: false
tags : [Punk, Belén Gopegui, El Guardián Entre el Centeno, Musica, Deseo de ser punk, Iggy Pop, Literatura, Interesante, J.D. Salinger, Novela]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/S9MLnQt7PFI/AAAAAAAAArk/q9m60p1EkKE/s320/deseo-de-ser-punk.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/S9MLnQt7PFI/AAAAAAAAArk/q9m60p1EkKE/s1600/deseo-de-ser-punk.jpg)  

Realmente me molestó que la gente me mirara mal cuando llevé este libro entre las manos, aunque lo entiendo: un libro que tiene como titulo "_**Deseo De Ser Punk**_" con **_Iggy Pop_** en la portada... Tiene pinta de que cuando levantes la vista se te aparecerá una cabeza rapada a excepción de una cresta teñida, y algún que otro elemento estridente. Pero es que la filosofía _**punk**_, no se encuentra en crestas, metal y cuero...En la  novela en forma de carta, Martina va contando lo que le va aconteciendo desde su perspectiva adolescente, con todo lo que eso conlleva, que viene a ser en ese sentido como el joven **_Holden Cauldfield_ de "_El Guardián Entre El Centeno_"** (_J.D. Salinger_). Sin embargo en este caso se mezcla el tema musical.

  

.

Es genial porque habla de canciones que dicen lo que quieren decir, que son autenticas, y de como entra la música en la vida como si fuera un _tsunami_ o un terremoto que lo arrasa todo... A veces un _riff_ de guitarra es más poderoso que cien discursos políticos: sabes que quiere decir, y lo oyes gritándote: "_get free_", y entonces sientes como si toda tu alma se elevara a una dimensión en la que lo comprendes y piensas: "¿porqué no me lo habían dicho antes?". Así es la música, tiene ese poder para encerrarte y tirar la llave; para quemar todo y decirte que "éste" es tu momento. Y es que la diferencia entre lo grande y lo pequeño en la música es ** la actitud**, que es lo que te atrapa, lo que la hace única.

.

Por otro lado, los razonamientos críticos adolescentes, sorprenden cada página del libro, reflejando también el poder y la fuerza de esos dieciséis o diecisiete años en los que crees que el mundo es tuyo, y puedes hacer lo que quieras con él. Ademas los sentimientos se pueden ver claramente con sus lágrimas, sonrisas y risas...

Realmente muy interesante...