---
title: 'La Noche del Cazador - Davis Grubb'
date: 2011-06-21T11:29:00.000+02:00
draft: false
tags : [La Noche del Cazador, Literatura, Davis Grubb, Novela]
---

[![](http://www.decine21.com/EstructurasBd/Peliculas/N1521/Imagenes/Lanochedelcazador1.jpg)](http://www.decine21.com/EstructurasBd/Peliculas/N1521/Imagenes/Lanochedelcazador1.jpg)

Fotograma de _La Noche del Cazador_ (1955)

No estoy muy fresco, no obstante debía publicar cuanto antes.

  

**Ben Harper** harto de la miseria y necesidad que rodea su vida y la de Willa y sus dos hijos John y Pearl, decide **atracar un banco**, con la mala suerte de que lo atrapan y lo ejecutan en la horca. Para bien o para mal los diez mil dólares que robó no se sabe dónde están. ¿Empieza fuerte eh?.

  

Leer al desconocido **Davis Grubb** ha sido un placer, una **novela** de suspense que necesitaba hace ya tiempo. Contado con mucho talento y energía, la historia de **John y Pearl** es increíble: seduce de principio a fin. Vas esperando lo que va a pasar antes de que pase: es inevitable; luego te desconcierta y sufres como los protagonistas, aunque el autor sabe hacerte descansar de vez en cuando. Sin embargo debo decir que no es en exceso como una _hamburguesa_, quiero decir no va al grano, no es únicamente acción, se detiene en las costumbres de la zona con experiencia autobiográfica, el drama...   
  
  
La espera a través de las páginas se convierte en angustia. Todo un _thriller_.

  

"_Las luces se veían muy claramente: el vacilante fuego crecía detrás de los ojos velados del Predicador. Su rostro empezó a cambiar de repente, la sonrisa desapareció, la benevolente máscara de carne se convirtió en una torva e__xpresión de aviesa malevolencia._"

  

_**La Noche del Cazador**_, Davis Grubb. Ed. Anagrama, 2000.