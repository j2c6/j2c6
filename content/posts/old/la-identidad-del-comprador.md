---
title: 'La identidad del comprador'
date: 2019-02-07T17:09:00.004+01:00
draft: false
tags : [vida moderna, compras, comprar, identidad, dinero]
---

Ahora, en sociedad, para definir mi identidad tengo que comprar. Yo _soy_ porque compro y en la medida que compro. Con un poco de holgura económica hay que hacer un gran esfuerzo por no comprar lo que se te ocurre o te ofrecen para, en concreto, ninguna misión en particular, eso sí, bajo la promesa de que te hará más completo y feliz.