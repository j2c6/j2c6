---
title: 'El mago de Oz (1939)'
date: 2012-01-22T16:12:00.000+01:00
draft: false
tags : [Ray Bolger, Victor Fleming, L. Frank Baum, 1930, Over the Rainbow, Cine, Judy Garland, Dorothy Gale]
---

[![](http://1.bp.blogspot.com/-9fo8lctliU4/TxwmvEV8UjI/AAAAAAAABH8/tj9QMACrxOY/s400/Oz%252C+rostards.jpg)](http://1.bp.blogspot.com/-9fo8lctliU4/TxwmvEV8UjI/AAAAAAAABH8/tj9QMACrxOY/s1600/Oz%252C+rostards.jpg)  

Dorothy Gale (**Judy Garland**) se arrepiente de haberse escapado con su perro **Toto**. Cuando vuelve, todos han desaparecido y su casa es arrastrada por un huracán hasta "_Pequeñilandia_", donde se dará cuenta de que si quiere volver a Kansas tendrá que pedir favor al **Mago de Oz** (**Frank Morgan**).

  

Los zapatos de rubíes, "_**sigue el camino de baldosas amarillas**_", el espantapájaros (**Ray Bolger**), los Munchkins... El clásico cuento de hadas de **L. Frank Baum**, fue llevado al cine empaquetado en un precioso musical de partituras de **Harold Arlen** y **Herbert Stothart**. La creatividad y la locura de la literatura fantástica son tangibles en esta inolvidable película: los decorados salpicados de mil colores, el encanto de los personajes y lo entrañable de la historia. Sin duda los cientos de detalles que colman la felicidad del espectador la convierten en una obra maestra, junto con su retransmisión televisiva todas las navidades desde los años cincuenta.  
  

**Victor Fleming** ('**Lo que el viento se llevó**', 1939)  con la generosa ayuda de la producción de **Mervyn  LeRoy, **construye el país de Oz dentro de la gran pantalla, espectacular resultado de un rodaje difícil, además de los problemas de intoxicación con el maquillaje del hombre de hojalata (**Jack Haley**). Lo maravilloso de esta película es el cómo sigues también a **Dorothy**, y escuchas cantar '**_Over the rainbow_**' mientras ves la expresión de J. Garland, reviviendo la ilusión que has tenido siempre o que te ha faltado alguna vez. Entonces vuelves a imaginar que estás en otro mundo y todo te vuelve a sorprender como si fuera la primera vez. "_Los corazones nunca serán prácticos hasta que puedan hacerse irrompibles_." Antológica.