---
title: 'Velvet Underground - Femme Fatale'
date: 2010-01-13T00:35:00.000+01:00
draft: false
tags : [Rock, 60', Lou Reed, Andy Warhol, Musica, Velvet  Underground, Comienzos del Rock]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/S00G90v6UnI/AAAAAAAAAn0/ksGazsg6kT0/s200/Lou+Reed.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/S00G90v6UnI/AAAAAAAAAn0/ksGazsg6kT0/s1600-h/Lou+Reed.jpg)

_**Velvet Underground**_ son conocidos pero quizás no te suene lo suficiente. Es uno de los primeros grupos de rock: años 60, y su material es bueno pero escaso. Principalmente triunfaron por algunas piezas: creo que tienen no se sabe qué en sus canciones que siempre parece que estén  de vacaciones. La verdad es que cuesta descubrirlos de verdad, tienen un encanto inusual, y probablemente hable de ellos muchas veces. Todo lo traen exportado desde _**New York**_. Son capaces de decir cosas muy amargas en cucharadas de miel, y sencillamente así es. Sacaron  cuatro discos el primero con la famosa portada de _**Andy Warhol**,_ aquel plátano que los haría famosos. En concreto esta canción es muy relajante empieza con un ritmillo como una camioneta de viaje por el campo, una voz clara y  acabados agudos “_cause everybody  knows_”… “_she’s a femme fatale_”, y así se llama _**Femme Fatale**_ la tercera canción del primer disco. En la foto **Lou Reed**. Ahí va: