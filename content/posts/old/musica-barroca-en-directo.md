---
title: 'Música Barroca en Directo'
date: 2010-12-20T12:42:00.000+01:00
draft: false
tags : [Musica en directo, Haendel, Arcangelo Corelli, Adagio, Barroco, Concierto, Iglesia de la Compañía, Música, Musica Clásica, El Mesías, Concerto]
---

**_"El Mesías" (Obertura)_ de _Haendel_**  
**_Concerto grosso op.6 nº8,_**  
**_"Fatto per la Notte de Natale"_ de _Arcangelo Corelli_**  
  

[![](http://bp0.blogger.com/_xwXdx3eAeZ4/RjkR656aVYI/AAAAAAAAAPM/QaSK3mSTx24/s320/sch00343.jpg)](http://bp0.blogger.com/_xwXdx3eAeZ4/RjkR656aVYI/AAAAAAAAAPM/QaSK3mSTx24/s1600/sch00343.jpg)Ante la invitación a un concierto de música barroca, accedí sin dudarlo, no por mi experiencia satisfactoria proveniente de otras ocasiones -estaba exento de tales privilegios- simplemente la idea de abrirse a terrenos desconocidos que engrandecieron a famosos compositores como **_Arcangelo Corelli_** o _**Haendel**_ y quizás _**Bach**_, pues precisamente de ese caso se trataba, me atraía. Mi curriculum como "disfrutante" de las artes clásicas, aumentaba considerablemente entre las vibraciones del mismísimo _Mesías_ de _Haendel.  _Finalmente **J.S. Bach** desapareció del programa, suplantado por dos _Arias del **Oratorio de Haendel,**_ esto me pareció oír. La Obertura tiene cientos de definiciones, pero en esta ocasión me decanto por el adjetivo "espectacular", o mejor: "solemne". Con aquello que vibraba entre las cuerdas y tubos, se podía rezar, aquello se dirigía al sitio más alto que cabe en  el corazón de los hombres, Dios. La melodías perfectas y armónicas, anunciaban y contaban historias con finales grandiosos.

  

Luego apareció lo que habían predicho los doctos, **_Adagio_ del _Concierto Grosso_ de _Corelli_**. Después del **_Allegro_** que precedía al referido entró. Entraban con una melodía todas las cuerdas, y poco más tarde las agudas se afiliaban a una que iba deslizándose por toda la Iglesia de la Compañía, en cambio las graves confirmaban por debajo lo que decían los violines, pero con violas, convirtiéndose en una conversación celestial. Fue breve pero en un pequeño frasco de intenso perfume. Un privilegio.