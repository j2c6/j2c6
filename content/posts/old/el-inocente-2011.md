---
title: 'El inocente (2011)'
date: 2012-04-04T16:15:00.000+02:00
draft: false
tags : [Cliff Martinez, Marisa Tomei, Cine, 2010, Brad Furman, Michael Connelly, Matthew McConaughey, William H. Macy, Josh Lucas, Ryan Phillippe]
---

[![](http://4.bp.blogspot.com/-fXDs3AxxEe8/T3xXXe_RyDI/AAAAAAAABVg/KFgNvLLW9HE/s400/rostard.jpg)](http://4.bp.blogspot.com/-fXDs3AxxEe8/T3xXXe_RyDI/AAAAAAAABVg/KFgNvLLW9HE/s1600/rostard.jpg)'**_The Lincoln Lawyer_**' debió de llamarse así por el espectacular coche que luce Mick Haller (**Matthew McGonaughey**), el protagonista de la popular novela basada en un drama judicial de **Michael Connelly**. A este último le debemos supongo el interesante argumento del thriller. La película empieza con bastante ritmo (música de **Cliff Martinez**), incluso mientras salían los créditos me recordó considerablemente a una de las escenas de '_**Training day**_' (2001, **Antoine Fuqua**) cuando **Denzel W.** y **Ethan H.** montan en el coche; perdona pero no puedo evitar esta comparación.

  

Presentan al típico protagonista triunfador, pero convence lo suficiente como para disfrutar con sus detalles de chulería y soberbia. También tiene sus peculiaridades este personaje (**McGonaughey**) tan resuelto, como su caótico asiento trasero que casi usa como despacho,  y donde se muestra apasionado por su trabajo, aunque no todo en él es el afán por el dinero. En repetidas ocasiones expresa su temor por condenar a alguien que no es culpable, o de otro modo, por no saber ver la inocencia de su cliente. Estos detalles le dan otro cariz al filme.

  

Por otro lado la adaptación del guión por parte de **John Romano**, es brillante hasta donde llega esta historia. Es rápida e interesante pero no te ahoga, se trata de un ritmo muy disfrutable, quizá perfecto. No es que la historia sea descomunal pero mantiene el suspense en todo momento, despertando constantemente el interés con nuevas sorpresas y giros del guión. Además el thriller es complementado con ciertos aspectos de un drama moral como la justicia, el secreto profesional, cuando el caso se personaliza, cuando te puedes tomar la justicia por tu mano, o lo mencionado anteriormente de la creencia en la culpabilidad del cliente.

  

Si duda agradezco que -hasta cierto punto- haya una variación en el hilo argumental, me parece novedoso el problema que plantea esta película. El largometraje no pasa de ser otro juicio comercial pero con todo el mérito, es una obra para vibrar con el caso. También tiene unos personajes secundarios consistentes como es el caso de **William H. Macy** y **Marisa Tomei** que tampoco me convence tanto esta última. **Ryan Philippe** tiene algunos destellos pero a pesar del interés de su personaje, deja entrever sus lagunas en la interpretación, no obstante no desentona.  
  

Todo bien pagado por la productora **Aurum**, que confía este entretenido  y espectacular filme a **Brad Furman**, que da un buen salto con este segundo largometraje tras su primer contacto con '**_The Take_**' (2007) con el que no salió muy bien parado. El objetivo de la película no es otro que atarte a la butaca casi dos horas con una situación completamente absorbente, esto es el cine.