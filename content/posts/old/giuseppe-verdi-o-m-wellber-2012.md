---
title: 'Giuseppe Verdi - ''Rigoletto'' (O. M. Wellber, 2012)'
date: 2012-11-22T17:10:00.000+01:00
draft: false
tags : [Omer Meir Wellber, Palau de les Arts, Rigoletto, Ivan Magrì, Juan Jesús Rodriguez, Ópera, Valencia, Giuseppe Verdi]
---

[![](http://1.bp.blogspot.com/-EzlTzWKAtQM/UK5Nd1bm5GI/AAAAAAAABmk/PFMzo-HLGN8/s400/rostard.jpg)](http://1.bp.blogspot.com/-EzlTzWKAtQM/UK5Nd1bm5GI/AAAAAAAABmk/PFMzo-HLGN8/s1600/rostard.jpg)**La ópera** es un mundo inexplorado para mí. La humanidad la va exluyendo cada vez más de su sociedad, pero no deja de tener una abundante y poderosa oferta mundial. Muchas veces he leído como los personajes de muchas novelas -especialmente las del siglo XIX- asistían elegantemente a estos eventos y siempre los he seguido con curiosidad y cierta envidia, pensando que se trataban de cosas del pasado. Además el atractivo cóctel de artes es muy provocador: el teatro, la música, la danza e incluso la pintura y escultura vista en los espectaculares decorados; así como las grandes leyendas e historias escogidas para obras tan delicadamente elaboradas en un espectáculo tan exuberante y rico en matices.

  

He podido asistir a la inauguración de la temporada 2012-2013 en el **Palau de les Arts de Valencia**, y a su vez al estreno de '**_Rigoletto_**' de **_Giuseppe Verdi_**. Solo ver la sala principal del edificio en forma de barco -o lo que sea- era todo un enigma. El ambiente que se respiraba allí era muy elegante y es encantador que el público se adorne tanto para un ritual tan artístico: realza su importancia y genera mayor respeto. Como no tengo una educación formada sobre la ópera, aunque la cultura general haya regalado a mis oídos piezas famosas, pues fui sin estár preparado y no supe apreciar los matices, y es un propósito para la próxima vez hacerlo y aprender. Un espectáculo tan complejo y detallista merece un conocimiento superior sobre las artes para su máximo disfrute.

  

Olvidando lo que dice la crítica, mi primera visita al patio de butacas sería sin duda poco exigente. '_**Rigoletto**_' es una ópera muy fácil de escuchar, tiene un texto muy animado, y en sí creo que la ocasión era grandiosa. Afortunadamente conocía algunas de las piezas como el aria '_**La donna è mobile'**_ o _**'_E il sol dell'anima'_**_, y por supuesto descubrí muchas que me embriagaron. En fin no sé qué decir. Los decorados eran espectaculares, quizá demasiado ya que para cambiarlos necesitaban media hora de intermedio. Entre los artistas había más destacados y menos. Al parecer **Juan Jesús Rodriguez** era el más renombrado y más aplaudido, por ser 'Rigoletto' y por ser español, no dudo que tuviera una gran voz, pero a veces resultaba demasiado monótona y pesada. Personalmente y desde mi inexperiencia me cautivó el tenor siciliano **Ivan Magrì**, que hacía de Duque de Mantua, tal vez por el papel o por su voz más insegura y variable, todo un artista.

  

El director israelí _**Omer Meir Wellber**_ tuvo el detalle de pararse un instante antes de empezar con la obertura para leer el manifiesto protesta contra el proximo ERE. La crítica habla de un Rigoletto algo mediocre, pero notable al mismo tiempo. La producción de Deflo, exportada desde la Ópera Nacional de Polonia, tenía serios problemas en la puesta en escena, que si aún quedaban por madurar y perfeccionar las voces, demasiada velocidad y menos delicadeza, etc, etc. Yo de momento no entiendo nada, pero la experiencia fue desbordante, muy por encima de las expectativas. Siempre pensé que sería todo un evento asistir a la ópera y ahora lo confirmo con conocimiento de causa: una velada sensacional. Claramente es un arte para apreciar en directo, no solo escuchar. Ahora a aprender al respecto para disfrutar más y adquirir el pensamiento crítico.