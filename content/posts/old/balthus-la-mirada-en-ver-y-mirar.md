---
title: 'Balthus, la mirada en ver y mirar'
date: 2019-02-19T11:06:00.003+01:00
draft: false
tags : [Balthus, Setsuko Ideta, erotismo, pintura, Arte]
---

Leo que Balthus está en el Thyssen. Setsuko Ideta. La casa en Suiza. La cultura japonesa. Su erotismo. Leo un comentario en el periódico:  

> Hay una sutil diferencia entre ver a una mujer y mirar a una mujer. Ver que una mujer es atractiva y admitir que atrae es puro instinto, no podemos provocar inmediatamente nuestra ceguera. Otra cosa distinta es recrearse en los detalles e instrumentalizar a la mujer como objeto de cosas irrespetuosas. En términos puramente artísticos, desde luego, la mujer suscita profunda admiración, eso es indudable.  
> 
> [Raful Raful](https://elpais.com/cultura/2019/02/18/actualidad/1550503292_049008.html)