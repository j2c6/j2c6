---
title: 'Toy Story 3'
date: 2010-11-29T11:14:00.000+01:00
draft: false
tags : [Animación, PIXAR, Cine, Entretenidas, Disney, S.XXI, Toy Story]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/TPT25T2nfeI/AAAAAAAAA1Y/5tZJYMvCLJo/s320/Dibujo.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/TPT25T2nfeI/AAAAAAAAA1Y/5tZJYMvCLJo/s1600/Dibujo.jpg)

Disney una de las marcas más valoradas del mundo, arrasa con proyectos multimillonarios que conmocionan a millones de niños... y adultos. **Toy Story 3** la última película del dúo de animación, Disney/**Pixar** arrasa en taquilla, y opta al premio Oscar, a la mejor película. Ha obtenido cientos de halagos, y su recaudación llenará los bolsillos de **Disney** por mucho tiempo. Andy se marcha a la universidad y una limpieza general de su habitación genera la duda en los famosos juguetes, que aparecen por tercera vez en pantalla. ¿Será Andy muy mayor para jugar con Wooddy y Buzz?¿Qué serán del Sr. Patata, y Rex después? Por casualidades de la vida deciden quedarse en una "maravillosa" guardería, pero no tardarán en darse cuenta de que no todo es tan fantástico como en casa de su dueño. Una película muy divertida, donde las risas más oídas no serán las de los niños. La introducción de nuevos personajes, junto con los personajes de siempre y la modernidad del guión de la última entrega de la trilogía, la hacen una gran película.