---
title: 'Esa cosa piando'
date: 2019-09-25T09:39:00.000+02:00
draft: false
tags : [propuesta, ficcion, textos]
---

No podían tener a esa cosa piando todo el rato. Abrigada, Alicia iba de camino a la tienda de animales cuando se encontró con Roberto. Hola, cómo estás. Ey, cuánto tiempo, dijo él. Qué fuerte. Hubo un silencio como el que hay después de un gran golpe. Pues, oye, ¿te apetece tomar algo?…¿desde cuándo? ¿siete? ¿ocho años? ¿y ese canario? Había una cafetería cerca, esa donde su madre y la madre de Victoria las llevaban a cenar antes de la sesión del Cine de Verano. Dibujos con crayones de casas sobre colinas en el mantel de papel, huevo frito con patatas o croquetas o San Jacobos y coca-colas y mesas pegajosas.

  

Alicia se percató de que la cafetería tenía otro dueño. En una secuencia, vio tras la barra a una chica de su edad, de aspecto alternativo diría su madre, algunos adornos de navidad, un ambiente tufado y acogedor en el aire, enmarcaciones de recortes de periódicos y servilletas firmadas por desconocidos. Roberto hablaba solo mirando a Alicia. Un café corto. ¿Tenéis Nestea? ¿Sí? Pues uno. Roberto seguía hablando, tenía muchas cosas que decir. Crispín, el canario, hablaba también de vez en cuando, insoportable. Roberto comprobó el teléfono un par de veces sin desbloquearlo. Es mi mujer, explicó, ¡me he casado!

  

Alicia lo observó con atención. Tenía eso que llaman signos de la edad. Con el principio de las arrugas, su cara parecía más estirada y atractiva. Su piel morena había virado absurdamente hacia una coloración rojiza y apagada que lo convertían en un ser solemne y en constante sonrojo. Hablaba por los codos, hablaba hasta la muerte, hablaba y se le veían trozos de comida entre los dientes. Aunque no escuchaba lo que decía Roberto (sobre su hija, los domingos en casa de sus suegros, el viaje a Niza, pintar las rayas blancas de los azulejos de la cocina, borrarse de Canal + o cómo intentó quemarse la verruga plantar), su voz le relajaba, y mientras lo hacía, ella re-colocaba los cubiertos, plegaba las esquinas de la servilleta, hacía una fila india con los frascos de las vinajeras y pensaba en que tal vez podía darle otra oportunidad al canario.