---
title: 'Aum Shinrikyo'
date: 2018-07-26T10:57:00.001+02:00
draft: false
tags : [Japón, terrorismo, Aum Shinrikyo, Tatsuya Mori, Tokio, Shoko Asahara]
---

Aum Shinrikyo (Verdad Suprema) es una secta budista pseudoreligiosa fundada en 1984 por Shoko Asahara (Chizuo Matsumoto). En 1995 perpetraron un ataque terrorista en el metro de Tokio con gas Sarín. Las consecuencias fueron devastadoras. En 1998, Tatsuya Mori lanzó 'A', un documental sobre la secta en cuestión y su día a día.