---
title: 'Solos con nuestro tío (1989)'
date: 2012-01-01T18:47:00.000+01:00
draft: false
tags : [Cine, John Hughes, John Candy, 1980]
---

[![](http://4.bp.blogspot.com/-5eaabqpI9_4/TyQ0D16wO8I/AAAAAAAABKs/UC8eiIiLaTM/s400/rostards.jpg)](http://4.bp.blogspot.com/-5eaabqpI9_4/TyQ0D16wO8I/AAAAAAAABKs/UC8eiIiLaTM/s1600/rostards.jpg)  

Los padres tienen que irse de casa unos días debido a un problema familiar, pero nada tiene que preocupar a los niños, el tío Buck (**John Candy**) se hace cargo de ellos.

  

Fuera de lo flojo que es el largometraje de **John Hughes** ('**La pequeña pícara**', 1991), es gracioso volver a ver una película de uno de los guionistas más televisados por sus comedias infantiles de los noventa. Conocido por: '**Solo en casa**', '**101 Dálmatas**', '**Daniel el travieso**', '**Beethoven, uno más en la familia**' o '**Mejor solo que mal acompañado**', basta leer los títulos de las películas para colocar su etiqueta correspondiente. Las ideas fáciles, predecibles, absurdas y divertidas no dejan de entretener a un público poco exigente.  
  

Nunca se sabe cuando te va a tocar ver una de estas películas de serie B o C, pero la realidad es que en algún momento las colocan frente a ti y no hay otro remedio. Unas navidades, un domingo aburrido en el que nada importa. Cuando te pase, mírala con buenos ojos y ríete cada vez que llega John Candy con su apestoso coche y frena con un "petardazo". Quizá haya algo que no consigas olvidar.