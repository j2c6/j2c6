---
title: 'Estrellas'
date: 2010-10-31T19:27:00.000+01:00
draft: false
tags : [Cielo, Noche, La Escalera Que Quemaba Ríos de Tinta]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/TM204VR-bOI/AAAAAAAAAz4/Y6dGQUQyCyk/s200/estrellas%5B1%5D.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/TM204VR-bOI/AAAAAAAAAz4/Y6dGQUQyCyk/s1600/estrellas%5B1%5D.jpg)La noche... Realmente la noche en la ciudad es algo grandioso, lleno de luz (al mismo tiempo), que arrebata la atención de nuestros sentidos, el frío, las luces, el tráfico... Un instante de una noche de luna llena que querría vivir todo el mundo. 

  

Fuera de la legión de edificios, "ambiente", y ríos de personas, las estrellas reinan en el campo, una infinidad de pequeños focos que nos miran desde arriba. Vértigo, eso es lo que provoca ver la inmensidad del universo de semejante manera. En la quietud de la noche: la soledad, y el aparente desamparo caen en el olvido con su misteriosa contemplación, dejas de ser tú, para ser solo tus ojos, o incluso unas lágrimas huérfanas que escuchan  la calma de una **noche estrellada**. Entonces un escalofrío recorre tu cuerpo inmóvil, para despertarte.