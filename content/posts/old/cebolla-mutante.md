---
title: 'Cebolla mutante'
date: 2019-02-27T17:28:00.001+01:00
draft: false
tags : [planta, tarro, naturaleza, plantas, cebolla]
---

[![](https://2.bp.blogspot.com/-TyMZTS18sXQ/XHa6lBNS_HI/AAAAAAAAdBc/RLmjveVQH3cHnYp7bLNCaX6BH3eFT5hGwCLcBGAs/s320/WhatsApp%2BImage%2B2019-02-24%2Bat%2B9.05.17%2BPM.jpeg)](https://2.bp.blogspot.com/-TyMZTS18sXQ/XHa6lBNS_HI/AAAAAAAAdBc/RLmjveVQH3cHnYp7bLNCaX6BH3eFT5hGwCLcBGAs/s1600/WhatsApp%2BImage%2B2019-02-24%2Bat%2B9.05.17%2BPM.jpeg)

  

Cuando crezca, tal vez se escandalice de este título, pero esa fue la primera impresión que dejó cuando la abracé para sacarla de la malla, y apareció esta cola de dragón mutante. Si ha crecido así en una despensa oscura y aterradora, veremos cómo evoluciona en un tarro de cristal en agua.

  

[![](https://3.bp.blogspot.com/-H96OD1VgbV8/XHa6lEl_G7I/AAAAAAAAdBY/ZdADF3MgCokjOwqyvn9CHWYzMI9T_IHdwCLcBGAs/s320/WhatsApp%2BImage%2B2019-02-24%2Bat%2B9.57.47%2BPM.jpeg)](https://3.bp.blogspot.com/-H96OD1VgbV8/XHa6lEl_G7I/AAAAAAAAdBY/ZdADF3MgCokjOwqyvn9CHWYzMI9T_IHdwCLcBGAs/s1600/WhatsApp%2BImage%2B2019-02-24%2Bat%2B9.57.47%2BPM.jpeg)