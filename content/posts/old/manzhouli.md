---
title: 'Manzhouli'
date: 2019-06-06T13:33:00.006+02:00
draft: false
tags : [tipografía, Cine, Matadero Madrid, china, subtitulos, Cineteca]
---

[](https://www.blogger.com/blogger.g?blogID=6901977242163785486)  
En la película '_**An Elephant Sitting Still**_' en las 'EMES' mayúsculas (M) de los subtítulos aparecían unos cuernos misteriosos. ¿Qué significarán?  
  

[![](https://1.bp.blogspot.com/--neYB9xYw8Q/XPl9xBeUTyI/AAAAAAAAdL8/j7lXA3c3PZYEps4HgVNgpNO_VJGh7YadgCLcBGAs/s320/descarga.png)](https://1.bp.blogspot.com/--neYB9xYw8Q/XPl9xBeUTyI/AAAAAAAAdL8/j7lXA3c3PZYEps4HgVNgpNO_VJGh7YadgCLcBGAs/s1600/descarga.png)

He conseguido esta captura de YouTube.

Larga, muy interesante y deprimente (sin recrearse): exactamente lo que necesitaba. A la salida de la sala de la cesta de la Cineteca (Azcona) pude robar el póster de la película.  

  
  

[![](https://1.bp.blogspot.com/-5YN7PuVopNg/XPl9Ht-Nz6I/AAAAAAAAdLw/fs2XRY1Dcv4Z4qJgIuQ48Y7cecCR-D-QwCLcBGAs/s320/photo_2019-06-06_22-51-49.jpg)](https://1.bp.blogspot.com/-5YN7PuVopNg/XPl9Ht-Nz6I/AAAAAAAAdLw/fs2XRY1Dcv4Z4qJgIuQ48Y7cecCR-D-QwCLcBGAs/s1600/photo_2019-06-06_22-51-49.jpg)

Así queda el póster.

  

[](https://www.blogger.com/blogger.g?blogID=6901977242163785486)[![](https://1.bp.blogspot.com/-MqUjf-1BJCM/XPl9Hm7LIlI/AAAAAAAAdLs/WCijRsw95W8c1kFe70ORRslee-TQzSF5wCLcBGAs/s320/photo_2019-06-06_22-51-53.jpg)](https://1.bp.blogspot.com/-MqUjf-1BJCM/XPl9Hm7LIlI/AAAAAAAAdLs/WCijRsw95W8c1kFe70ORRslee-TQzSF5wCLcBGAs/s1600/photo_2019-06-06_22-51-53.jpg)

Sala Azcona (Cineteca, Matadero Madrid)