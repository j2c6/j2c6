---
title: 'Alicia En El País De Las Maravillas (Alice''s Adventures In Wonderland) - Lewis Carroll'
date: 2010-05-06T18:10:00.000+02:00
draft: false
tags : [Reina de Corazones, Lewis Carroll, Danny Elfman, Alicia, Johnny Depp, Un Clásico, el Sombrerero Loco, Sweeney Todd, Mia Wasikowska, Tim Burton, Cine, Entretenidas, Literatura]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/S-LpehQMQLI/AAAAAAAAAr8/JpMuDL-AA-A/s320/The-Mad-Hatters-Tea-Party,-Illustration-From-Alices-Adventures-In-Wonderland,-By-Lewis-Carroll,-1865.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/S-LpehQMQLI/AAAAAAAAAr8/JpMuDL-AA-A/s1600/The-Mad-Hatters-Tea-Party,-Illustration-From-Alices-Adventures-In-Wonderland,-By-Lewis-Carroll,-1865.jpg)  

_**Lewis Carroll**  _inventó un cuento para niños que revolucionó la época victoriana (_**Alice's Adventures In Wonderland**_). Realmente fantástico por su sin sentido, maravilloso por lo irracional. En los últimos meses, me lo estuve leyendo en su lengua original: El desconcierto aparecía al final de cada capítulo, ya que verdaderamente no siguen un orden lógico, lo que hace que el interés por la obra crezca, y te haga sonreír ante la incomprensión. El arte es incomprensible e "inútil", cualquier persona ajena a este lenguaje universal hablaría de un loco al referirse al autor de este cuento, pero resulta que realmente es así, maravillosamente loco. Hablaremos.

  

.

En cuanto a la película, debo hacer referencia a la predisposición. La esperaba desde hacia un par de años: el encargo de _Disney_ a _**Tim Burton**,_ salto a los noticiarios, aunque todavía tendríamos que esperar a ver la versión del director de  _**Sweeney Todd**_. Ciertamente los elementos "_Disney_" estuvieron muy presentes en la película,que por un lado desfavorecieron al carácter de la obra fantástica de simpleza argumental, esto fue fruto de lo comercial del asunto. El daño se vio reflejado en elementos "superfluos" como la "batalla final" o las escenas finales de moralejas destinadas al mundo victoriano, que después de todo molestaban en el filme quitándose parte del aire infantil del que presumía y añadiendo una línea argumental más definida.

.

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/S-LrNFbhtyI/AAAAAAAAAsU/FgsSlaTd2MU/s320/alice-in-wonderland-logo-tim-burton-Aama-copia.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/S-LrNFbhtyI/AAAAAAAAAsU/FgsSlaTd2MU/s1600/alice-in-wonderland-logo-tim-burton-Aama-copia.jpg)  

Sin embargo esto no fue todo. La protagonista fue la más brillante del largometraje desde el vestuario que lucía hasta los diálogos e interpretación que llevó a cabo **Mia Wasikowska**. Además el ámbito técnico-espacial de la película está muy bien respaldado: los escenarios, los elementos del paisaje (flores, personajes secundarios), la banda sonora (**Danny Elfman**) impresionante y los personajes de: la **Reina de Corazones, el Sombrerero Loco**, etc. Concluyo en que está muy bien ajustada en proporción a otras adaptaciones, aunque no es igual que el mundo de Lewis Caroll en la imaginación del lector. Una ejemplar que asombra a los inadvertidos.

  
.