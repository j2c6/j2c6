---
title: '#2 COVID-19'
date: 2020-03-12T13:14:00.001+01:00
draft: false
tags : [pandemia, coronavirus, covid-19]
---

Cuanto menos, cuando va pasando la sensación del pánico y la incertidumbre se estabiliza, el virus, nos hace reflexionar. Pone en jaque lo que tenemos y queremos mantener, nuestro sistema moderno de rutina diaria, el consumo, la producción. Se para la producción en China y poco a poco baja en todo el mundo. Caen las bolsas y la economía de todo el mundo. Los niños se quedan en casa para la pesadilla de los padres. Se bajan los niveles de contaminación. Para el fútbol y otros deportes. Tras un pico de consumo, se reduce considerablemente. Se fijan en que las empresas no están preparadas para la conciliación, y qué es importante. Todos los países que gozan del bienestar se vuelven vulnerables ante un mismo problema. Se gana perspectiva. La gente se queda en casa y podríamos pensar. Estamos en tiempos extraordinarios. Ahora mismo no es cuándo va a pasar, sino, qué significa esta meditación viral. Por algo, un virus, un ser pequeño y sencillo, pone en juego a toda la sociedad, nuestro sistema de vida, nuestra soberbia modernidad.  
  
2.968 personas contagiadas, 84 muertos. Creo que lo entiendo, no es el pánico, es una gripe, es el colapso del sistema sanitario. Si colapsa en exceso te puedes ver con muertos por todos lados. Hay un límite de atención sanitaria para los que necesitan.  
  
  

[![](https://1.bp.blogspot.com/-qfMP9pmz3b8/Xmo8MhnmjxI/AAAAAAAAfBQ/_2EszcYAxUk3Bfa9q7Aijm4i6wpXLTqowCNcBGAsYHQ/s320/111.PNG)](https://1.bp.blogspot.com/-qfMP9pmz3b8/Xmo8MhnmjxI/AAAAAAAAfBQ/_2EszcYAxUk3Bfa9q7Aijm4i6wpXLTqowCNcBGAsYHQ/s1600/111.PNG)

Colapso de los servicios sanitarios.

[![](https://1.bp.blogspot.com/-b4ks9LmtHbQ/XmyL9iuaT8I/AAAAAAAAfB8/5KU7iEOZhUM515em4FiyOVSLLb1eSbqXgCNcBGAsYHQ/s320/_R072775.jpg)](https://1.bp.blogspot.com/-b4ks9LmtHbQ/XmyL9iuaT8I/AAAAAAAAfB8/5KU7iEOZhUM515em4FiyOVSLLb1eSbqXgCNcBGAsYHQ/s1600/_R072775.jpg)

Supermercado Día, sobre el mercado de Guillermo de Osma