---
title: 'Sándor Márai  - El Último Encuentro'
date: 2011-06-05T16:30:00.000+02:00
draft: false
tags : [Muy Bueno, Literatura, Sándor Márai, Novela]
---

Leí "El Último Encuentro", una novela increíble. Lo cierto es que en su momento no comenté nada, y lleva la conciencia recordándomelo cada vez que pienso en cuanto he disfrutado leyendo y lo que queda si no me muero pronto. Es uno de esos libro que lees y acaba siendo como una parte de ti, tendré que volver a leerlo. Mientras tanto rescato algún fragmento de la novela del húngaro Sándor Márai, personaje interesantísimo:

  

  

  

“Uno está convencido, y mi padre todavía lo enten­día así, de que la amistad es un servicio. Al igual que el ena­morado, el amigo no espera ninguna recompensa por sus sentimientos. No espera ningún galardón, no idealiza a la persona que ha escogido como amiga, ya que conoce sus defectos y la acepta así, con todas sus consecuencias. Esto sería el ideal. Ahora hace falta saber si vale la pena vivir, si vale la pena ser hombre sin un ideal así. Y si un amigo nues­tro se equivoca, si resulta que no es un amigo de verdad, ¿podemos echarle la culpa por ello, por su carácter, por sus debilidades? ¿Qué valor tiene una amistad si sólo amamos en la otra persona sus virtudes, su fidelidad, su firmeza? ¿Qué valor tiene cualquier amor que busca una recompen­sa? ¿No sería obligatorio aceptar al amigo desleal de la mis­ma manera que aceptamos al abnegado y fiel? ¿No sería justamente la abnegación la verdadera esencia de cada rela­ción humana, una abnegación que no pretende nada, que no espera nada del otro? ¿Una abnegación que cuanto más da, menos espera a cambio? Y si uno entrega a alguien toda la confianza de su juventud, toda la disposición al sacrificio de su edad madura y finalmente le regala lo máximo que un ser humano puede dar a otro, si le regala toda su confianza ciega, sin condiciones, su confianza apasionada, y después se da cuenta de que el otro le es infiel y se comporta como un canalla, ¿tiene derecho a enfadarse, a exigir venganza? Y si se enfada y pide venganza, ¿ha sido un amigo él mismo, el engañado y abandonado? ¿Ves?, este tipo de cuestiones teóricas me han ocupado desde que me quedé solo.”

  

[![](http://2.bp.blogspot.com/-w7gG1Aw_Jcg/TekrJZ1TriI/AAAAAAAAA-s/ZFG-Ck_I9G8/s640/hungr%25C3%25ADa.jpg)](http://2.bp.blogspot.com/-w7gG1Aw_Jcg/TekrJZ1TriI/AAAAAAAAA-s/ZFG-Ck_I9G8/s1600/hungr%25C3%25ADa.jpg)

  

“—Porque en la vida de un hombre no solamente ocurren las cosas. (…) Uno también construye lo que le ocurre. Lo construye, lo invoca, no deja escapar lo que le tiene que ocurrir. Así es el hombre. Obra así incluso sabiendo o sintiendo desde el principio, desde el primer instante, que lo que hace es algo fatal. Es como si se mantu­viera unido a su destino, como si se llamaran y se crearan mutuamente. No es verdad que la fatalidad llegue ciega a nuestra vida, no. La fatalidad entra por la puerta que noso­tros mismos hemos abierto, invitándola a pasar. No existe ningún ser humano lo bastante fuerte e inteligente para evitar mediante palabras o acciones el destino fatal que le deparan las leyes inevitables de su propia naturaleza y ca­rácter.”

  

“¿Qué significa la fidelidad, qué esperamos de la persona a quien amamos? Yo ya soy viejo, y he reflexiona­do mucho sobre esto. ¿Exigir fidelidad no sería acaso un grado extremo de la egolatría, del egoísmo y de la vanidad, como la mayoría de las cosas y de los deseos de los seres humanos? Cuando exigimos a alguien fidelidad, ¿es acaso nuestro propósito que la otra persona sea feliz? Y si la otra persona no es feliz en la sutil esclavitud de la fidelidad, ¿amamos a la persona a quien se la exigimos? Y si no ama­mos a esa persona ni la hacemos feliz, ¿tenemos derecho a exigirle fidelidad y sacrificio? Ahora, al final de mi vida, ya no me atrevería a responder a estas preguntas, si alguien me las formulase (…). Pero, en fin, así es el hombre, que incluso siendo inteligente y experimenta­do puede hacer muy poco en contra de su naturaleza y de sus obsesiones.”

  

“Sobrevivir a al­guien a quien se quiere tanto como para llegar al homici­dio, sobrevivir a alguien por quien nos habríamos dejado matar por amor es uno de los crímenes más misteriosos e incalificables de la vida. Los códigos penales no reconocen este delito. Pero nosotros dos sí que lo hacemos (…). He visto la paz y la guerra, he visto la miseria y la grandeza, te he visto cobarde y me he visto a mí mismo vani­doso, he visto la confrontación y el acuerdo. Pero en el fondo, quizás el último significado de nuestra vida haya sido esto: el lazo que nos mantuvo unidos a alguien, el lazo o la pasión, llámalo como quieras. ¿Es ésta la pregunta? Sí, ésta es. Qui­siera que me dijeras —continúa, tan bajo como si temiera que alguien estuviera a sus espaldas, escuchando sus pala­bras— qué piensas de esto. ¿Crees tú también que el sentido de la vida no es otro que la pasión, que un día colma nuestro corazón, nuestra alma y nuestro cuerpo, y que después arde para siempre, hasta la muerte, pase lo que pase? ¿Y que si he­mos vivido esa pasión, quizás no hayamos vivido en vano? ¿Que así de profunda, así de malvada, así de grandiosa, así de inhumana es una pasión?… ¿Y que quizás no se concentre en una persona en concreto, sino en el deseo mismo?… Tal es la pregunta. O puede ser que se concentre en una persona en concreto, la misma siempre, desde siempre y para siempre, en una misma persona misteriosa que puede ser buena o mala, pero que no por ello, ni por sus acciones ni por su ma­nera de ser, influye en la intensidad de la pasión que nos ata a ella. Respóndeme, si sabes responder —dice elevando la voz, casi exigiendo.— ¿Por qué me lo preguntas? —dice el otro con cal­ma—. Sabes que es así.”