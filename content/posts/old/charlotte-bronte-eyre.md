---
title: 'Charlotte Brönte -  ''Jane Eyre'''
date: 2012-03-03T21:06:00.000+01:00
draft: false
tags : [Currer Bell, Charlotte Brönte, Literatura, Jane Eyre, XIX]
---

[![](http://2.bp.blogspot.com/-jONidPEvUdM/T1J4_KcI1mI/AAAAAAAABQI/h3l89x-mseM/s400/rostard.jpg)](http://2.bp.blogspot.com/-jONidPEvUdM/T1J4_KcI1mI/AAAAAAAABQI/h3l89x-mseM/s1600/rostard.jpg)

Existe un gran privilegio como es disfrutar del mismo deleite del que han disfrutado millones personas de nuestro tiempo y a lo largo de la historia: leer a los clásicos. El tiempo es un aval muy poderoso y encumbra a una verdadera obra maestra tras el reconocimiento de incontables lectores. Sentirse identificado y comprendido es imprescindible en esta vida, así como entender el mundo y llamar a las cosas por su nombre.  
  

  

'**_Jane Eyre_**' fue publicada en 1847 por _**Currer Bell**_, el seudónimo utilizado por **Charlotte Brönte**, una de las reverenciadas hermanas Brönte. Fue una novela revolucionaria en su tiempo por la perspectiva que evoca  el fascinante personaje de Jane Eyre. Independientemente de lo que narra la obra, la autora describe con tal precisión los acontecimientos, así como los detalles del temperamento y las más leves e inadvertidas impresiones de los personajes que simplemente la apreciación de tal disección del comportamiento humano es apasionante. Y es que Charlotte --y así la llamo para distinguirla de sus hermanas-- tiene esa capacidad para materializar las sensaciones y componer metáforas que detallan perfectamente los deseos, las desdichas, los pensamientos y emociones de los protagonistas.  
  

  

Una de las historias de amor más románticas de la rica época victoriana, con el imprescindible punto trágico y misterioso. El relato es impredecible y novedoso a pesar de su fecha de creación. No  se pierde la sofisticada y elegante atracción por la inusual Jane, que lleva las riendas del guión. Evita el despliegue del romance maquinalmente, sino que lo hace sutilmente y con mucho talento, introducciendo fenómenos persuasivos que embeben al lector en una lectura apasionada y placentera. El dinamismo de la obra se debe en gran medida a unos diálogos inteligentes y entretenidos que prometen constantemente, sin decepcionar en ningún momento y dejando traslucir el amplio conocimiento del mundo y de sus costumbres de la autora.

  

  

  

La elección de los acontecimientos repletos de sorpresas y giros facilitan una lectura rápida y ansiosa. Los sufrimientos de la desafortunada protagonista, capaz de disfrutar de los más sencillos placeres consiguen un apoyo pleno a la misma, de modo que el lector --a quién en numerosas ocasiones alude la novela-- acoge a  la señorita Eyre y la respalda en sus decisiones tras un completo conocimiento de la eterna y exquisita heroína.   
  

No quisiera revelar detalles que entorpezcan la labor enriquecedora de la novela, pero quiero remarcar la imaginación y dotes narrativas de la novelista, demostradas en este ejemplar con una creatividad muy delicada y sublime, donde un mundo imperfecto está lleno de belleza  hasta en los lugares más recónditos. Una novela excepcional.

  

_"Solías tener aquellos días un aire pensativo, Jane, no propiamente desanlentado, porque tu aspecto no era enfermizo, pero tampoco animoso; denotabas la ausencia de euforia propia de quien carece de esperanzas y disfruta poco de la vida. Me preguntaba qué pensarías de mí, si es que alguna vez mi recuerdo pasaba por tu cabeza, cosa que no sabía. Y para averiguarlo decidí hacerte caso de nuevo. Afloraba una cierta alegría en tu mirar y te volvías cordial y expresiva, a medida que hablábamos. \[...\] Empecé a permitirme el placer de ser amable contigo, y la amabilidad no tardó en tirar de la emoción. La expresión de tu rostro fue dulcificándose al unísono con el tono de tu voz, me encantaba oír de tus labios la palabra \[...\] con aquel acento tan grato y placentero. En esa época, Jane,  disfrutaba mucho de los encuentros que nos proporcionaba el azar: había en tu actitud una especie de rara vacilación, una turbación en la mirada, como si te mostrases recelosa o dubitativa, a la expectativa de mi caprichoso comportamiento. \[...\] cuando te extendía cordialmente la mano, los rasgos de tu rostro joven y anhelante se coloreaban con tal luminosidad que muchas veces tuve que hacer un esfuerzo para no estrecharte  allí mismo contra mi pecho."_

  

_**Jane Eyre**_,  Charlotte Brönte.  Alba Editorial, 2009.