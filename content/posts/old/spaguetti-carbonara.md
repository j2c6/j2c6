---
title: 'Spaguetti Carbonara'
date: 2020-05-29T10:39:00.002+02:00
draft: false
tags : [Italia, receta, carbonara, pasta]
---

**Ingredientes**  

*   Pecorino romano o parmesano rallado, 30-40 gr./persona  
    
*   Huevo por persona.
*   Spaguetti 100 gr./persona
*   Mantequilla
*   Pancetta o bacon en trozos, 50 gr./persona
*   Aceite

[![](https://1.bp.blogspot.com/-K-n2LigK1qo/XtDJzTN3-KI/AAAAAAAAfc8/_f5b4rViP3AhMcuYaBc96sOQInlWhCmDwCK4BGAsYHg/w400-h300/photo_2020-05-29_10-24-19.jpg)](https://1.bp.blogspot.com/-K-n2LigK1qo/XtDJzTN3-KI/AAAAAAAAfc8/_f5b4rViP3AhMcuYaBc96sOQInlWhCmDwCK4BGAsYHg/photo_2020-05-29_10-24-19.jpg)

  

  

**Preparación**  

1.  Añadimos sal al agua hirviendo y añadimos la pasta, conservando el agua después.
2.  En un bol, un poco de aceite, añadimos los huevos y mezclamos con el parmesano, con unos treinta-cuarenta gramos por huevo y añadimos pimienta y añadimos algo del agua de la cocción de la pasta. De esta forma se hace un poco el huevo.
3.  En la sartén añadimos un poco de mantequilla y hacemos la pancetta. Añadimos la pasta y la salsa del bol, removiendo activamente. Si es necesario, otro poco del agua de cocción de la pasta.
4.  Lo servimos enrollado añadiendo algo de pimienta y rallamos queso sobre la pasta.  
    

Esta ricetta pertenece al [Chef Stefano Barbato](https://www.youtube.com/watch?v=zXWGhFXH-Ss), menudo descubrimiento. Y esa cosa con nata que tomábamos hasta ahora, ¿qué era?