---
title: 'Coldplay - Yellow'
date: 2008-12-13T16:47:00.000+01:00
draft: false
tags : [Indie, Rock, Coldplay, Musica]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/SUPaDNAQaaI/AAAAAAAAAXU/P2Q2gis14ok/s320/Coldplay-705392%5B1%5D.JPG)](http://2.bp.blogspot.com/_VT_cUiG4gXs/SUPaDNAQaaI/AAAAAAAAAXU/P2Q2gis14ok/s1600-h/Coldplay-705392%5B1%5D.JPG)  

No es una de las canciones mas famosas de **Coldplay** (ojalá no la conozcáis) pero es buenísima, como siempre y por eso, es sencilla. Pero aparte de no entenderla la letra tiene la potencia (_potens_) de una bomba nuclear. Os he traído la letra aunque no es una muy buena traducción, creo que os podréis hacer una idea:

  

_"Mira las estrellas, mira como brillan por ti, y por todo lo que haces, si, era todo amarillo, también vine...escribí una canción para ti, y todas las cosas que haces, y fue llamada "**yellow**" entonces di un giro, o mira lo que he hecho, y todo era amarillo, tu piel, OH si tu piel y huesos, se volvieron algo hermoso, tu sabes....tu sabes que te amo tanto, nade alrededor... salte alrededor por ti, OH que cosas para hacer, por que eras toda amarilla, dibuje una línea, puse un limite por ti y por todo lo que haces, y era todo amarillo, tu piel oh! si tu piel y huesos, se volvieron algo hermoso tu sabes que por ti sangraría hasta secarme, es verdad mira como brillan por ti, mira como brillan por ti mira como brillan por ti y por todas las cosas que haces..."_