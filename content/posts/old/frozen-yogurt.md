---
title: 'Frozen Yogurt'
date: 2011-09-08T12:34:00.000+02:00
draft: false
tags : [Helado, Frozen Yogurt, Gastronomía, Placer, Plátano, Chocolate]
---

  

Cada cosa tiene su sito. Acabas de comer en un fétido restaurante de comida rápida. Te gusta, pero tú, te mereces algo mejor. Sales a la calle, son las 4 p.m. y el calor derrite tu cuerpo: caminas cabizbajo y tu ropa se adhiere a ti pegajosamente. La gente, bajo los mismos rayos del hiriente sol, te mira  con pesadez, incluso con asco. Su odio se refleja en el sudor de sus frentes, no tienen la culpa, pero tú también lo sufres. Entonces acude a tu mente la genial idea por la que cualquiera pagaría en tu situación... Por un momento tu boca sonríe pero en seguida vuelve a su posición de angustia.

  

  

El aire acondicionado del local deja en tu subconsciente un ápice de esperanza. Tú ya sabes lo que vas a pedir, no estás para sorpresas, lo has probado antes: sabes del triunfo. Vas viendo como van saliendo los afortunados que se encuentran en el pequeño comercio, y los tomas como grandes amigos, al elegir disfrutar el mismo **placer** que tú.

  

[![](http://3.bp.blogspot.com/-FFy6mSLykpc/TmiZWMAjXEI/AAAAAAAABAA/sG1u8iG90GI/s1600/RE_10374.jpg)](http://3.bp.blogspot.com/-FFy6mSLykpc/TmiZWMAjXEI/AAAAAAAABAA/sG1u8iG90GI/s1600/RE_10374.jpg)

La imagen solo bloquea a la imaginación.

Una receta sencilla: **yogurt helado, rodajas de plátano, galleta caramelizada batida y tu exquisito chocolate negro**. Tras la espera sales con tu postre como absuelto de un crimen del que eres culpable. Al volver a la calle el sol sigue abrasando tu cara y continúas con esa suciedad pegajosa del ambiente. Sin embargo tu boca se convierte en otra habitación, una nueva dimensión dominada por el frescor y el deleite. Los labios tiemblan ante frescor semejante, mezclado con finísimo **chocolate** fundido y pastoso --por el contraste de temperaturas-- que va masajeando allí por donde se desliza: las encías, la lengua. El cambio de texturas del áspero **yogurt** al suave **chocolate** produce un ligero cosquilleo en el paladar, y no evitas esa sonrisa. El **plátano** aparece poco después con un tacto deslizante y su sabor inconfundible, te alegras de haberlo elegido mezclado con la dulcísima **galleta caramelizada**, y mueves la mandíbula lentamente como bailando la última balada de la fiesta que contienen tus dientes. Ya no eres el mismo de antes, todo es distinto, ya nada importa. La gente que te rodea comprende, y se derrite, esto fortalece tu consciencia de privilegiado: has triunfado.