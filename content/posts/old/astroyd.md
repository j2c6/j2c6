---
title: 'Astroyd'
date: 2020-07-03T15:48:00.001+02:00
draft: false
tags : [vida]
---

Pues así, en la Puerta del Sol, sobre la tienda de Apple. Nuevo puesto. Creo que voy a aprender y que si me porto bien podré estar algún tiempo. Por el momento tengo las mañanas libres tengo que venir por las tardes. El día se estructura de forma relajada y tengo la sensación de que puedo llevar una vida de verdad. Obviamente, si las cosas se pusieran 'feas', también tendría esa opción, pero ahora, quiero decir, las siento especialmente favorable. La lectura, la vida lenta. Y nada, poco más. 

  

He empezado a leer las historias de Lorrie Moore y la primera, ese cuento de 'Autoayuda' de Cómo ser la otra, me ha parecido estupendo, con esos recursos sorprendentes y fáciles con los que te saca una sonrisa. Mientras tanto en casa, el salón se llena de plantas y nuestro pequeño aire acondicionado portátil hace la vida un poco más agradable. ¿Qué más se puede pedir?

  

Otro día, otro año más en la Tierra, viendo qué ocurre, cómo ocurre y qué será de nosotros.