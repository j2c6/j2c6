---
title: 'Un dios salvaje (2011)'
date: 2012-04-22T15:42:00.000+02:00
draft: false
tags : [Roman Polanski, John C. Reilly, Christopher Waltz, Cine, Jodie Foster, 2010, Kate Winslet, Yasmina Reza]
---

[![](http://3.bp.blogspot.com/-soRDspYQ3uE/T5QKt53FLNI/AAAAAAAABak/k01ofkyC7_M/s400/rostard.jpg)](http://3.bp.blogspot.com/-soRDspYQ3uE/T5QKt53FLNI/AAAAAAAABak/k01ofkyC7_M/s1600/rostard.jpg)

Me he leído ya una obra de teatro de **Yasmina Reza**, que no fue nada espectacular pero sí interesante: '_**Arte**_'. Me llamó la atención '**_Un dios salvaje_**' (_**Carnage**_) por los carteles de grandes actores y por alguna recomendación, pero tampoco recuerdo -exceptuando un tráiler- demasiada publicidad.

  

La ventaja de ver una obra de teatro en el cine o una adaptación es que la sigues en primera linea, la escenificación es más real y no pierdes detalle. **Roman Polanski** no tuvo más que hablar con la escritora y rodar con un guión perfecto de antemano. No es un proyecto demasiado ambicioso sino una realización pensada y medida. Incluso Polanski se pudo permitir hacer un _cameo_ apareciendo como el vecino que escucha con la puerta entrecerrada.

  

Creo que he disfrutado más incluso viendo a grandes estrellas que si fueran desconocidas, interpretan papeles muy bien definidos cada uno con sus peculiaridades, de modo que a medida que va avanzando este largometraje de escasos 79 minutos, se observan distintos choques entre ellos.

  

Todo empieza en el contexto 'políticamente correcto' donde dos matrimonios parecen entenderse perfectamente, respetando las normas preestablecidas y dando la impresión que cualquier otra pareja habría ofrecido en circunstancias normales. Dan a entender que ellos están por encima de las tonterías y las peleas de críos, tratando el caso como si fuera un parte amistoso tras un leve choque entre vehículos.

  

Empiezan a sacar distintos temas de conversación que se entrecruzan inteligentemente con el caso que los ha reunido: la pelea de sus respectivos hijos. Poco a poco se va desgajando la careta que exhibe esta sociedad, siendo incapaces de no mofarse de los gustos de los demás, o siendo poco tolerantes con sus errores o humanidades algo que al principio hubiera parecido imposible.

  

La obra de teatro lleva a nuestros personajes educados y sus amables y hospitalarias fachadas hasta el extremo. Se van mostrando cómo son por su susceptibilidad, pasividad, hipocresía y simplismo. Es muy interesante y llevadero ver como van cruzando las leyes y reglas de cortesía, cambiándolas por impertinencias, reproches e ironías.

  

El largometraje parece una sencilla comedia, pero tras una apariencia divertida hay una crítica dura, llena de humor negro que muestra perfectamente los fenómenos que en realidad mueven los hilos de la sociedad. Sin duda muy reveladora.  
  

El reparto ha sido fundamental. Ver a grandes como **Jodie Foster, Kate Winslet, Christopher Waltz y John C. Reilly** consigue que la esencia del drama de **Yasmina Reza** aporte toda la consistencia de su mensaje. Muy entretenida e irónicamente didáctica.