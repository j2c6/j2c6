---
title: 'En el dentista'
date: 2018-04-20T17:41:00.000+02:00
draft: false
tags : [poemas, Poesía]
---

No me duelen las muelas,  

es esa sensación de

aguantar el temporal,

ahí plantado como

un árbol

sin escapatoria.

  

Ya sabes, los árboles,

pues se quedan sin opción.

Y al principio jode

pero luego te haces un

experto

en eso de ser árbol.

  

Y si  no pasa nada,

en primavera, pronto,

aparecen tus flores,

sí, he dicho TUS flores,

claro que

mientras, tienes que _estar_

tragándote tu angustia,

esa soledad sorda

y las ganas de que te

abracen como si les

importaras

algo más que una mierda.

  
  
NOTA: Esto tendría algo que ver con la importancia de experimentar las sensaciones en lugar de ahogarlas con evasión.