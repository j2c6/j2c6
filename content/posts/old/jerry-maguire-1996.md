---
title: 'Jerry Maguire (1996)'
date: 2012-03-03T01:20:00.000+01:00
draft: false
tags : [Reneé Zellweger, 1990, Cuba Gooding Jr., Cine, Tom Cruise, Cameron Crowe]
---

[![](http://1.bp.blogspot.com/-rox5mgzimRs/T1FjsSW-MII/AAAAAAAABQA/hRkIzVbIVFQ/s400/rostard.jpg)](http://1.bp.blogspot.com/-rox5mgzimRs/T1FjsSW-MII/AAAAAAAABQA/hRkIzVbIVFQ/s1600/rostard.jpg)

Un exitoso representante deportivo como es Jerry Maguire (**Tom Cruise**) es despedido por su agencia tras una declaración de principios o algo similar. Después de la pérdida de casi todos sus clientes solo resta uno: Rod Tidwell (**Cuba Gooding Jr.**) un gran jugador en potencia y su incondicional seguidora desde el despido, Dorothy Boyd (**Renée Zellweger**).

  

El habitual columnista de la revista musical **_Rolling Stone, _Cameron Crowe**, se dedicó posteriormente al cine logrando un **Óscar** por el guión de '**_Casi famosos_**' (2000), así como su "consagración" mediante otro de sus éxitos conocido como '_**Vanilla Sky**_' (2001): un _remake_ de '**_Abre los ojos_**' (**Alejandro Amenábar** y **Mateo Gil**, 1997).  La temática de sus películas gira en torno a un mismo tema, abordado de distintas maneras. En el caso de la taquillera '**_Jerry Maguire_**', introduce al seguro de ganancias **Tom Cruise** y al deporte: ¿quién no vio este ejemplar? Los números fueron muy buenos en la caja y obtuvo 5 nominaciones y una estatuilla para el secundario **Gooding Jr**. Como se puede comprobar este año de sequía sobrevaloró a _Maguire_. 

  

Una comedia sin duda divertida, pero que hace aguas por todos lados. Tras el bombardeo de tópicos fílmicos y la admiración de la figura del **Cruise** típico, llega el posible y sentimental romance con un tacto blando y pastoso mezclado una presencia de **Zellweger** quizá con demasiado sirope de caramelo. El triunfo y el fracaso, la infidelidad y la lealtad... Es notable y sorprendente que obtuviera tan buenos resultados un largometraje tan ordinario, pero así funciona el público cuando no hay nada mejor. Lo que no es tan novedoso es el guión, que no hace sino recordar tenuemente el montaje de siempre. Sí: entretiene; sí "enséñame la pasta", pero ¿qué más?  
  
  

Argumentos baratos, fáciles pero graciosos y simpáticos gracias a **Cuba Gooding Jr**. Intenta pero no logra entusiasmar ni llegar con profundidad. A pesar de obtener el interés del espectador lo va desperdiciando en momentos poco convincentes y evidencias insatisfactorias. Entretenimiento de tercera fila, sin grandes aspiraciones: una historia de superación convertida en romance.