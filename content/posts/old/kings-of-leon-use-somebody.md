---
title: 'Kings Of Leon - Use Somebody'
date: 2010-10-08T12:46:00.000+02:00
draft: false
tags : [Indie, Rock, Garage Rock, Musica, Black Rebel Motorcyle Club, Kings of Leon]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TK711imBCCI/AAAAAAAAAy4/o-CTrHUxwLk/s320/kings_of_leon.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TK711imBCCI/AAAAAAAAAy4/o-CTrHUxwLk/s1600/kings_of_leon.jpg)

**_Garage Rock_**. Dentro de este género, podríamos decir que los grupos tienen estilo propio, dentro de un ámbito común: el _garage rock_. Siempre me ha recordado al impresionismo (de otro modo): como cuadros con acabados "sin acabar", no improvisados pero si más informales a los estudiados y "pincelados" anteriores. Son algo más alternativos (aunque este término no es el adecuado), distintos a los demás por su falta de precisión, por su obertura al pensamiento del subconsciente: te pueden decir cualquier cosa. En el caso de _[B.R.M.C](http://miblancoynegro.blogspot.com/2009/12/black-rebel-motorcycle-club.html)_ creo que esos acordes agresivos, y en ocasiones asesinos, en el fondo se ríen del _Pop_ quizás, están en su propio mundo. **Kings Of Leon** es un grupo venido desde los Estados Unidos, y llevan algo así como una década en funcionamiento. Tienen la esencia del Garage Rock, y una voz  algo dolida y cruda, seguida de ritmos limpios que esculpen el ritmo en hierro, alguna guitarra que grita de vez en cuando. En "**_Use Somebody_**" existen verbalmente otras voces de Nathan, Jared, Mathew en plan coro, además de la de Caleb el cantante: tres hermanos y un primo. Es un grupo que no se vende fácilmente, pero con este tema lo consiguieron además del premio **Grammy** de este año 2010 a la mejor grabación. Os recomiendo también: **_Camaro, Molly's Chambers..._**