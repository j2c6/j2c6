---
title: 'El rey de la comedia (1983)'
date: 2012-04-14T14:08:00.000+02:00
draft: false
tags : [Paul D. Zimmerman, Robert De Niro, Jerry Lewis, Cine, Martin Scorsese, 1980]
---

El mundo que circunda al cómico televisivo no me agrada en general. Tampoco me hacen especial gracia las películas en las que tienes que 'ir predispuesto a reírte', prefiero que sean ellas las que me arranquen las carcajadas así como que no hace falta volver de un funeral para llorar en una película trágica, sería una total desventaja y desconsideración. 

  

[![](http://1.bp.blogspot.com/-Gy7K_yRvxiQ/T4lpyFXbUfI/AAAAAAAABXw/e5MylqxvE6k/s1600/rostard.jpg)](http://1.bp.blogspot.com/-Gy7K_yRvxiQ/T4lpyFXbUfI/AAAAAAAABXw/e5MylqxvE6k/s1600/rostard.jpg)

  

Sin embargo esta película de Scorsese, que denominan 'obra menor' no me la tomé muy en serio, era tarde y era la opción menos apetecible. Pero funcionó. Resulta que **Robert de Niro** sale con un bigote muy interesante que parece su carné de cómico, un papel desconocido en su carrera. Sin duda conseguido y trabajado, se trata de un aspirante a cómico y triunfador, con la autoestima por las nubes, imparable ante las negativas o incluso sordo ante los reproches. Rupert Pupkin sabe lo que quiere y lo quiere por la vía rápida. 

  

A principio del principio conoce al famoso Jerry Langford (**Jerry Lewis**), y consigue hablar con él para tener una oportunidad de uno de los grandes. La escena da a conocer al personaje, y parece muy capaz hasta cuando le llama al despedirse una otra vez, y parece que nunca acaba: "_¡Jerry!... ...__¡Jerry!_". En fin es difícil describir la sensación del espectador. Se presenta en su oficina, en su casa, le "espera sentado", le llama por teléfono y finalmente lo secuestra. La muerte no solucionaba ese afán por salir en su programa. Y mientras se imagina como un lunático cómo serán las cosas cuando sea una celebridad.

  

Además el filme refleja muy bien, el efecto que tienen las masas sobre las figuras 'públicas' a principios de los 80, así como el funcionamiento del mundo televisivo, las nuevas promesas y oportunidades a caras nuevas. Quizá deja claro, que al final el público es el que manda. Ni los críticos, ni el prestigio, el que llega es aplaudido. En fin, después de seis años de cárcel consigue hacer su propio programa. 

  

Sale una historia disparatada pero real. Es posible -aunque increíble- que exista ser humano semejante y que tenga el coraje suficiente para hacer lo necesario para cumplir sus propósitos. Pone todas las reglas enfocadas a la victoria y contaba con ello.  Es grandioso que sea consciente de por dónde ha de pasar para realizar su sueño, que sea capaz. 

  

Reconozco que no es nada extraordinario, pero el guión (**Paul D. Zimmerman**) es completo y describe perfectamente al mencionado Rupert Pupkin optimista como el que más. No esperes grandes cosas de esta comedia, querido lector, solo disfruta sencillamente de cómo logra el triunfo uno que realmente quiere tocar la fama.