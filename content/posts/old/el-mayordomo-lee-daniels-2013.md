---
title: 'El Mayordomo - Lee Daniels, 2013'
date: 2014-01-02T00:03:00.000+01:00
draft: false
tags : [Alan Rickman, lee daniels, Forest Whitaker, Vanessa Redgrave, Terrence Howard, Lenny Kravitz, Oprah Winfrey, John Cusack, Cuba Gooding Jr., Jane Fonda, Mariah Carey, Cine, Robin Williams]
---

  

  
i.2014//Muchos ingredientes simplemente correctos en un batido típico e insípido. A parte del tema del maltrato racial archiconocido, junto con un toque azucarado y pasteloso, sin gancho, tratado superficialmente como un conjunto de anécdotas sin fuerza. Simplemente la oportunidad de ver caóticamente en pantalla el elenco que constituía el reclamo de la película: Forest Whitaker, Oprah Winfrey, Cuba Gooding Jr., John Cusack, Terrence Howard, Lenny Kravitz, Vanessa Redgrave, Alan Rickman, Robin Williams, Jane Fonda, Mariah Carey... Para qué hablar...como si eso bastara. No he visto todavía '[**_Precious_**](http://www.filmaffinity.com/es/film362411.html)', pero espero que sea algo mejor.  
**D**