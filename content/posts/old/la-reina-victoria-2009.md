---
title: 'La reina Victoria (2009)'
date: 2012-01-29T10:00:00.000+01:00
draft: false
tags : [Jean-Marc Vallée, reina Victoria, Paul Bettany, Emily Blunt, Rupert Friend, Cine, Donizetti, Julian Fellowes, Miranda Richardson, Hagen Bogdanski, 2000]
---

[![](http://1.bp.blogspot.com/-WcNzDjW0QBs/TyUKse7WtjI/AAAAAAAABK0/fTbFl_dkhak/s400/rostards.jpg)](http://1.bp.blogspot.com/-WcNzDjW0QBs/TyUKse7WtjI/AAAAAAAABK0/fTbFl_dkhak/s1600/rostards.jpg)La joven Victoria (**Emily Blunt**) es víctima de la verborrea y los intereses de los cortesanos ya que influenciar a la inexperta futura reina puede conllevar sus beneficios. Sin embargo a pesar de sentirse como una pieza de ajedrez, Victoria no será como se esperaba sino una mujer rebelde, con autoridad y pensamiento propios. Entretanto se enamora del príncipe Alberto (**Rupert Friend**).

  

Empezando por alabar la tarea de ambientación, puesta en escena histórica medida hasta el más mínimo detalle, junto con una fotografía (**Hagen Bogdanski**) y una dirección artística envidiables de la producción inglesa; diré que merece la pena esta película. Además de mostrar la personalidad de la singular **reina Victoria**, los diálogos y la trama creada por el guión (**Julian Fellowes**) son muy sugerentes, visible en las costumbres y quehaceres de la época y en el transcurso del argumento. También he de nombrar a los grandes talentos ingleses como la evidente **Emily Blunt**, así como a **Paul Bettany** o **Miranda Richardson**.  
  

La historia de amor a pesar de que es interesante,  creo que no está a la altura, sin embargo el interés que colocan los actores en ella capta la atención en lo sucesivo. Una producción cinematográfica orgullosa que da gusto ver en la gran pantalla por su historicidad y arte, aunque no será una película que recuerde eternamente. Un placer oír de nuevo a **Donizetti** en esta gran obra.