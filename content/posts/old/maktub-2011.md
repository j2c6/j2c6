---
title: 'Maktub (2011)'
date: 2012-05-05T17:33:00.000+02:00
draft: false
tags : [Rosa María Sardá, Aitana Sánchez Gijón, Paco Arango, Cine, 2010, Diego Peretti, Goya Toledo]
---

[![](http://1.bp.blogspot.com/-hwXeUR-7j3E/T6VH_FONPFI/AAAAAAAABdA/P-i9dTxO9OA/s320/maktub.jpg)](http://1.bp.blogspot.com/-hwXeUR-7j3E/T6VH_FONPFI/AAAAAAAABdA/P-i9dTxO9OA/s1600/maktub.jpg)

Todos lo años salen como por arte de magia más películas felices de 'se acerca la navidad'.  En general no me suelen gustar por su sentido básico de estas fiestas, y por la superpoblación de los tópicos en sus guiones. Cuando empezaba la película creo que ya no olía bien. Fuera de las casualidades que unen los acontecimientos, el hilo es posible y no desentona demasiado. Una producción española que sorprende por la ausencia de los típicos elementos que se incluyen en la mayoría de los sórdidos ejemplares peninsulares. El mensaje es claramente creyente.

  

**Diego Peretti, Aitana Sánchez Gijón, Goya Toledo, Rosa María Sardá**... un reparto inesperado para un drama así. Y eso es lo que quería discutir, lo de drama. Dentro de lo inconsciente que es la historia y por ese lado la acusaría de irreal, intenta hacer gracia a trompicones en plan comedia, cosa que no consigue casi nunca y luego apaga las luces para unas lágrimas con cuenta gotas del drama cancerígeno. La víctima (**Andoni Hernández**) lleva su enfermedad de modo poco habitual y eso hace que el resto de los problemas parezcan más sencillos y casi porque estamos en navidad. No tiene demasiada profundidad aunque trate temas delicados y trascendentes.

  

Los recursos del guión son más propios de capítulos de serie televisiva, y no ejercen excesiva influencia. Las casualidades citadas son escandalosas, y los recursos muy simples pero dejándolos a un lado y raptando al sentido del humor, puede arrancar algunas sonrisas mudas en el momento más álgido que podría ser la cena. No es una comedia/drama de carcajadas  o lágrimas simplemente, es simpática en familia de forma poco exigente.  
  

El filme de **Paco Arango** pretende dar a conocer al optimista Antonio que de forma heroica acepta la muerte a través de la fe, pero el acompañamiento se derrumba por los tópicos y la poca fuerza de sus golpes de efecto, que son insignificantes. Se muestra el caso de cáncer, así como el problema matrimonial con una visión plana y primordial de estos obstáculos, abusando del sentimentalismo. Sin embargo no se puede pedir más. Parte de modelos consistentes pero toda su estructura queda coja. El mensaje es diferente, pero el modo es de A B C, nada especial y olvidable.