---
title: 'darcotín'
date: 2020-05-09T21:24:00.002+02:00
draft: false
tags : [poema, corashe, poesía cotidiana, nathy peluso, Poesía, darcotín, poesía contemporánea]
---

con el darcotín se puede

tomar el sol.  

  

se llena algo en mi 

cerebro con el sonido de la

lavadora.

  

has traído a Lorenzo segundo

que se eleva junto al carveriano

poto de la damajuana.

  

quiero fabricar un tomoscopio

para verte a través de él

y la copa que rompí ayer 

me acusa desde la encimera

y su pose 

parece la misma 

que la de una farola 

en un parque que ya no recuerdo.

  

ocultarse es lo más brillante

que ha hecho Pynchon.

  

esto puede leerse con la misma  

entonación que nathy  

peluso en _corashe_