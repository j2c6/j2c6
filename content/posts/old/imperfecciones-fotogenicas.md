---
title: 'Imperfecciones fotogénicas'
date: 2017-11-28T20:56:00.002+01:00
draft: false
tags : [sangre, falange, corte, Kurt, dedo]
---

Mi amigo Kurt Cobain dejó escrito en su diario que entre las cosas que le interesaban, recuerdo que estaba: _el cultivo de una selecta legión de imperfecciones faciales_.

  

No tiene exactamente mucho que ver, pero el otro día me salió un corte en la mano y me sorprendí mirándolo. Creo que era extremadamente fotogénico:

  

[![](https://3.bp.blogspot.com/-Ht7UT7kduqY/Wh2-2tw-uRI/AAAAAAAASVw/sNqIHINVlLQKUOPXLpbZmfgwhLIQzAs5ACLcBGAs/s400/IMG_3150.jpg)](https://3.bp.blogspot.com/-Ht7UT7kduqY/Wh2-2tw-uRI/AAAAAAAASVw/sNqIHINVlLQKUOPXLpbZmfgwhLIQzAs5ACLcBGAs/s1600/IMG_3150.jpg)

  

No hay que darle muchas vueltas pero observa que sigue la línea de la falange pero como un principiante, ligeramente torcido. Justo antes de llegar a esa rotonda de pliegues que no sé cómo se llama. Y creo que es ese ingrediente el que lo convierte en auténtico, como genuino, un  objeto de mercadillo, un expositor de sangre.