---
title: 'Mary Ann Shaffer & Annie Barrows - ''La sociedad literaria y el pastel de piel de patata de Guernsey'''
date: 2012-04-02T17:32:00.000+02:00
draft: false
tags : [Mary Ann Shaffer, títulos, Annie Barrows, Shakespeare, XXI, Lectura, Guernsey, Literatura, G. K. Chesterton, Charles Lamb]
---

¡Oh! En efecto que las expectativas respecto a este ejemplar estaban como las estrellas una noche despejada. <<_Una delicia para los amantes de los libros_\>> clama el **Chicago Sun-Times**. Reconozco que el título era sencillamente un anzuelo hecho a medida para mí, ya que una parte de mis debilidades excéntricas se encuentra en la repetición y admiración de títulos largos y extraños. Pero no se preocupe querido lector, una vez acabada la novela, o selección de correspondencia, el título cobra todo el absoluto sentido que acostumbraba tener.

  

En efecto el libro no es una narración sabelotodo sobre algo que pasó entre **Guernsey** y Londres, sino de un modo original aunque no inédito de cartas y más cartas entre los protagonistas, como **Juliet** \-un nombre muy clásico a lo tragedia de **Shakespeare** que me gusta- y **Dawsey** por ejemplo, escogido intencionadamente ya que sé como termina esta historia.

  

Además de disfrutar considerablemente el primer tercio de la obra, creo que incluso he aprendido bastante. Sin embargo no era lo que esperaba, hasta lo confundí con una historia de carteo terminado en 'vivieron felices y comieron perdices' muy romántica. Pero lo dicho, a pesar de todo le he encontrado el punto sin elevarlo al canto de los ángeles.

  

Lo de 'para los amantes de los libros' es la mitad de la verdad. El ambiente editorial y de mentes que buscan la novela que les haga inmortales siempre me atrajo, así como las referencias a novelas que he leído o que por casualidad he oído nombrar y reconozco. Existe cierta satisfacción en darse cuenta de que comprendes de qué se está hablando. Sin embargo a pesar de que se trata hasta del tema principal, no es de una intensidad apasionada, pero sí tiene un sentido central. 

  

También habla de los gustos por los libros y cómo ocurre en algunos -y solo en algunos-  el fenómeno de encontrar tan solo un detalle que te encanta, y te parece un instante escondido y sublime o una particularidad genial que aprieta una tecla oculta y te lleva a otra novela y después a otra y  así sucesivamente.

  

Hay una frase maravillosa que consigue resumir con mucha elegancia el argumento y mensaje del libro y es propiedad del reverenciado señor **G. K. Chesterton**: <<nunca tuve una tristeza que una hora de lectura no haya conseguido disipar>>. Es increíble el poder de evasión de la lectura, incluso en la guerra. Podría ser una novela de agradecimiento a las lestras en un contexto desesperante pero contado con una amable posterioridad. Mi sensación ha sido la de tomar un excelente aperitivo antes de una gran cena, y que después los platos fuertes -la esencia de la velada- te hayan defraudado. Aún así ¡vaya entrantes!. Las primeras cien páginas prometieron algo que, luego no pasó de cerrar un círculo no carente de  cierto interés.  
No es una gran novela, pero tampoco algo mediocre. Quizá una buena idea con elegantes destellos. Hubo una frase aparentemente evidente en el libro, pero en realidad reveladora: "_**Leer buenos libros te impide disfrutar de los malos**_". Cuánta razón.

  

[![](http://1.bp.blogspot.com/-f3yocQ3GEbg/T3nGSee7WvI/AAAAAAAABU4/_I6erdeeBzE/s640/rostard.jpg)](http://1.bp.blogspot.com/-f3yocQ3GEbg/T3nGSee7WvI/AAAAAAAABU4/_I6erdeeBzE/s1600/rostard.jpg)

  

"_Me llamo Dawsey Adams y vivo en una granja en la parroqia de St. Martin's Parish en Guernsey. La conozco porque tengo un viejo libro que una vez le perteneció,_ Ensayos escogidos de Elia_, de una autor que en la vida real se llamaba Charles Lamb. Encontré su nombre y dirección en la cubierta interior del libro._

_Seré claro: me encanta Charles Lamb. El libro dice_ Ensayos escogidos_, así que supongo que debe de haber escrito otras cosas entre las que escoger. Me gustaría leerlo, pero a pesar de que los alemanes ya se han ido, no ha quedado ni una librería en Guernsey._

_Querría pedirle un favor. ¿Puede mandarme el nombre y la dirección de alguna librería de Londres? Me gustaría pedir por correo más libros de Charles Lamb. También querría preguntar si alguien ha escrito alguna vez la historia de su vida, y si lo han hecho, si me pueden mandar un ejemplar. Debido a su brillante y aguda inteligencia, creo que el señor Lamb debe de haber tenido una vida muy triste._"

  

 '_**La sociedad literaria y el pastel de piel de patata de Guernsey**_', Mary Ann Shaffer & Annie Barrows. RBA Narrativas, noviembre de 2011  
Ilustración de **Vincent Van Gogh**