---
title: '#10 COVID-19'
date: 2020-03-22T13:24:00.001+01:00
draft: false
tags : [pandemia, coronavirus, diario coronavirus, covid-19]
---

Pues creo que esto me está afectando, todos hablamos de aprovechar el tiempo y eso. No es problema. Se trata más a nivel emocional, me encuentro como en un estado de alerta permanente. He pensando en ponerle un límite a la información, a los grupos, a poder ser molestado a cualquier hora por quien sea. A las horas que paso monitoreando los datos y avances a nivel mundial. Hoy se alarga el estado de alarma 15 días más. España 28,5k infectados. Ifema se abre como hospital de cuarentena. 1,7k muertos. Italia 53k. USA 26,9k, Alemania 25,8k, UK 5k. 

  

Me he propuesto mirarlo conscientemente como con las comidas. Leerlo y desconectar. Teniéndome obsesionado, creo que es lo mejor. Y no es porque me preocupen las cifras, es como "si vamos a ser llevados al matadero" quiero saberlo. Me parece algo absorbente. Así dicho, me pongo con el curso de Swift, a hacer vida casera.