---
title: 'Adele – Chasing  Pavements (Grammys)'
date: 2009-02-14T14:49:00.000+01:00
draft: false
tags : [Grammy, Adele, Coldplay, Musica, Alison Krauss, Voces]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SZbMzoJc5MI/AAAAAAAAAZU/QE6BAoAojHM/s320/Grammy%2520Logo%2520Gold.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/SZbMzoJc5MI/AAAAAAAAAZU/QE6BAoAojHM/s1600-h/Grammy%20Logo%20Gold.jpg)  

Este artículo es sobre los premios **Grammy** pero quiero hacer mención a esta cantante que ganó un Grammy por esta canción y por actriz revelación, me gusta mucho la voz que tiene. La letra es buena es un pequeño romance pero está bien, aunque yo me fijaría más en la canción, el ritmo, lento pero soberbio. Aun así os pongo la letra la traducción:

.

"Ya me he decidido, no necesito pensarlo más, si estuviese equivocada tendría razón, no es necesario mirar hacia adelante, esto no es una lujuria, ya lo sé… esto es amor, pero… Si le dijese a todo el mundo,nunca sería suficiente,porque no te lo he dicho a tiy eso es exactamente lo que necesito hacersi estoy enamorada de ti. Debería rendirme? o simplemente continuar prosiguiendo el camino incluso si este no condujera a ningún lado? …sería una pérdida de tiempo? Incluso si supiese que tendría que abandonar mi espacio? Debería rendirme? o simplemente continuar prosiguiendo el camino incluso si este no condujera a ningún lado? Yo me desarrollaría y volaría en círculos, esperando hasta que mi corazón se desplome y mi espalda se comience a estremecer… finalmente… podría ser así?Debería rendirme? o simplemente continuar prosiguiendo el camino… incluso si este no condujera a ningún lado? …sería una pérdida de tiempo? Incluso si supiese que tendría que abandonar mi espacio? Debería rendirme? o simplemente continuar prosiguiendo el camino incluso si este no condujera a ningún lado? "

.

Respecto a los Grammy hubo muchos premios pero salieron vencedores: Robert Plant y **Alison Krauss** con 5 Grammys a su cargo y en segundo puesto los ganadores de tres Grammys **Coldplay**, con su disco “Viva la vida…”.Podéis leer todos los premios en este link: http://www.grammy.com/grammy\_awards/51st\_show/list.aspx