---
title: 'El imperio del sol (1987)'
date: 2012-04-15T18:30:00.000+02:00
draft: false
tags : [John Williams, John Malkovich, Christian Bale, J. G. Ballard, Cine, Allen Daviau, Steven Spielberg, Tom Stoppard, 1980]
---

[![](http://2.bp.blogspot.com/-QHogpdRIWgc/T4r3cjZzAII/AAAAAAAABYA/LGd7xj40eQY/s400/rostard%5D.jpg)](http://2.bp.blogspot.com/-QHogpdRIWgc/T4r3cjZzAII/AAAAAAAABYA/LGd7xj40eQY/s1600/rostard%5D.jpg)**Steven Spielberg** también conocido como '_el rey Midas de Hollywood_' hizo popular el cine de aventuras, y conquisto los espíritus soñadores de la juventud y la vejez a lo largo de las cuatro décadas que lleva rodando, no sin enriquecer a sus participantes. Le tengo una especial admiración a este excelente director por la creatividad y la vida que reflejan sus películas, que tienen muchos elementos en común.

  

En '_**El imperio del sol**_' **Christian Bale** fue seleccionado en una casting de unos 4000 participantes.  Se depositó mucha responsabilidad sobre los hombros de este joven actor que no defraudó en absoluto sino que sorprendió con su talento, expresividad, viveza y esfuerzo. Su poder interpretativo es equiparable al del adulto más expresivo y convincente. Lo cierto es que después de esta película cualquiera apostaría por el joven Bale. Se entrega a este papel en el que no hay plano que no aparezca su pasión por los aviones, la imaginación lo emociona en cada instante, la lista de palabras cultas que son escogidas en sus curiosos diálogos y su espíritu aventurero, independiente, simpático, ocurrente.

  

Se hace una proyección de la infancia con todas sus características que emociona al espectador, interesa cualquier gesto o palabra del inquieto Jim, que se abre paso en cada uno de los infortunios que va provocando la guerra. También se refleja -a pesar de lo espabilado y despierto del personaje- su crecimiento y descubrimiento de las cosas,  así como su actitud frente a la tristeza y desesperanza que lo rodea. Una vida intensa que le hace crecer antes de tiempo, debido al poderoso espíritu de supervivencia que despierta en Jim.

  

De hecho hay momentos de especial dramatismo, como cuando vuelve a su casa y se sigue comportando como un niño en la bicicleta y cuando no le queda más remedio que ir apurando ansiosamente la escasa comida que queda en cada lata. Ahí se produce un cambio, aunque en realidad se sigue mostrando una visión ingenua y simple de lo que está pasando, no acaba de salir de la inocencia que lo caracteriza, pero sale airoso de cada trance.

  

También es sugestiva su relación con Basie (**John Malkovich**), pues  no conoce a gente de su edad y se apega con facilidad a un personaje al que el público detesta porque conoce sus verdaderas intenciones, dejando a un lado la simpatía que inspira la excelente interpretación de Malkovich, que se ve envuelto en una personalidad tan fuerte y cautivadora como la de Jim. Basie es un sujeto muy real, parece rastrero y bueno a la vez, una creación ingeniosa.

  

Un guión de **Tom Stoppard** basado en la novela de 1984 de **J. G. Ballard**, te deja sencillamente sin aliento, consigue absorber al público en cada uno de los 'retos' que se presentan frente al protagonista. Sigue la historia asombrado por el carácter de Jim que es una versión de Tom Sawyer en situación desesperada, y recuerda a otros múltiples filmes sobre campos de concentración. A pesar de su extensa duración, este brillante argumento te mantiene atado al asiento, midiendo los tiempos con una exactitud admirable. Emociona su percepción tan esmerada de la historia.

  

El alto presupuesto del largometraje permite mostrar una recreación de los años 40 en China casi auténticos, donde todo esta estudiado y medido. La banda sonora es épica y extraordinaria en este drama bélico, realizada por **John Williams**. También la fotografía de **Allen Daviau** tiene su lugar en la lista de halagos.

  

Una película de Spielberg olvidada y rechazada. Una de mis favoritas del director y de la historia, que sin duda me parece completa, intensa y fantástica. Muy llevadera en cuanto a la velocidad y hace gala de la imaginación e ingenio del eterno **Steven Spielberg**. La recomiendo a todos los que disfrutan con el cine.