---
title: '#8 COVID-19'
date: 2020-03-18T19:24:00.001+01:00
draft: false
tags : [pandemia, coronavirus, diario coronavirus, covid-19]
---

Estoy siguiendo como si se tratara de la tele, un monitor que actualiza los casos de coronavirus en todo el mundo en tiempo real. Y es fascinante ver cómo van sumándose como una especie de competición, como una contemplación del misterio del contagio y la muerte. La gente está concienciada con lo de no salir. En el supermercado observas el nerviosismo, la prisa. El gobierno sigue sacando medidas para una crisis sin precedentes.

  

[![](https://1.bp.blogspot.com/-bVaSzkKEq-k/XnJlsjrSB9I/AAAAAAAAfDw/EDDQat-xZIMuzzJ47hInInaFJ93cBQ--ACNcBGAsYHQ/s320/1111.PNG)](https://1.bp.blogspot.com/-bVaSzkKEq-k/XnJlsjrSB9I/AAAAAAAAfDw/EDDQat-xZIMuzzJ47hInInaFJ93cBQ--ACNcBGAsYHQ/s1600/1111.PNG)

  

  

Y aquí hay algo curioso. Alemania (rojo) está a punto de alcanzarnos en casos probados de contagio. Sorprendentemente, en sus búsquedas no parecen tan histéricos como aquí (azul). No sé qué conclusión sacar de esto, pero digo que si no están tan histéricos, no están tan concienciados, serán más imprudentes.

[![](https://1.bp.blogspot.com/-ifwAZb89JC0/XnJkludCcDI/AAAAAAAAfDY/0c9XH70DQMoMgMcs4BqTpVQjMQ7dxlvKQCNcBGAsYHQ/s320/germ.PNG)](https://1.bp.blogspot.com/-ifwAZb89JC0/XnJkludCcDI/AAAAAAAAfDY/0c9XH70DQMoMgMcs4BqTpVQjMQ7dxlvKQCNcBGAsYHQ/s1600/germ.PNG)