---
title: '#6 COVID-19'
date: 2020-03-16T21:26:00.000+01:00
draft: false
tags : [pandemia, coronavirus, diario coronavirus, covid-19]
---

Paso gran parte del día empanado viendo los datos mundiales del coronavirus. En la plaza de Rutilio Gacís se empieza a jugar tímidamente al veo veo y suena habitualmente Mónica Naranjo después de nuestras peticiones. Brillan las linternas. El aspecto de las calles es desértico, con goteo de gente. El presidente avisa de que va a movilizar 200.000 millones para hacer frente a la crisis. Habrá multas para los que incumplan las normas del confinamiento. C., es recogida todas las mañanas por un taxi para ir al trabajo.  Esto empieza a tomar un cariz de rutina con algunos puntos álgidos de emoción. En Italia dejan de atender a los mayores de 80 años. 

  

[![](https://1.bp.blogspot.com/-w3MfCzFWD4A/XnExPuJN9ZI/AAAAAAAAfC4/rWw0V-ArvnMfZQgn5OVIJHB2FPF17dCNwCNcBGAsYHQ/s320/trends.PNG)](https://1.bp.blogspot.com/-w3MfCzFWD4A/XnExPuJN9ZI/AAAAAAAAfC4/rWw0V-ArvnMfZQgn5OVIJHB2FPF17dCNwCNcBGAsYHQ/s1600/trends.PNG)

  

En este gráfico están, a través de Google Trends, comparadas, las búsquedas de coronavirus en Italia (amarillo), España (azul) y Reino Unido (rojo). Creo que el alarmismo, se puede ver bien reflejado. Italia anda cerca de los 30k casos, España se acerca a los 10k y Reino Unido 2k. Obviamente es interesante cuántas pruebas hacen y cómo diagnostican y clasifican los fallecimientos, aquí tiene que haber maquillaje también.

  

Mientras tanto, la vida sigue. No se puede estar catatónico indefinidamente. "Ya cruzaremos ese puente cuando venga".

  

[![](https://1.bp.blogspot.com/-_lwKxyz30-s/XnEyZTpA4MI/AAAAAAAAfDE/6N80vqB9qVE-Gu5bdOTqTHUiGpyyLPYDwCNcBGAsYHQ/s320/_R072882-2.jpg)](https://1.bp.blogspot.com/-_lwKxyz30-s/XnEyZTpA4MI/AAAAAAAAfDE/6N80vqB9qVE-Gu5bdOTqTHUiGpyyLPYDwCNcBGAsYHQ/s1600/_R072882-2.jpg)

Los del segundo piso, bajo la ventana que alegra las tardes a las 20 horas.