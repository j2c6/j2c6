---
title: 'VIII'
date: 2020-04-08T19:53:00.002+02:00
draft: false
tags : [poesía cotidiana, poesía contemporánea]
---

Te miro  
mientras te estiras.  
me miras y parezco concentrado.  
te curvas sobre ti,  
como lo hacen todos  
mis deseos.  
  
ya no cruza el atardecer  
el suelo del salon  
y se acerca la noche,  
que es nuestra.  
  
¿hay una piel  
más específicamente  
diseñada que la mía  
para estar pegada  
a la tuya?