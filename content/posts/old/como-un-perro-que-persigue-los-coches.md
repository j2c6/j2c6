---
title: 'Como un perro que persigue a los coches'
date: 2012-10-03T08:56:00.000+02:00
draft: false
tags : [Frank Bernard Dicksee, Ana Karenina, Felicidad, Dorian Gray, Tolstoi, Joker, Uncategorized, Verdad, Sueño, Búsqueda, Charles Chaplin]
---

  

"_—¿Hay algo nuevo sobre eso? —preguntó Levin.__  
—Hay, hay... ¿Conoces ese tipo de mujer de los cuadros de Osián? Esos tipos que se ven en sueños... Pues mujeres así existen en la vida. Y son terribles. La mujer, amigo mío, es un ser que por más que lo estudies te resulta siempre nuevo.  
—Entonces vale más no estudiarlo.  
—¡No! Un matemático ha dicho que el placer no está en descubrir la verdad, sino en el esfuerzo de buscarla._

_     Levin escuchaba en silencio, y a pesar de todos sus esfuerzos, no podía comprender el espíritu de su amigo. Le era imposible entender sus sentimientos y el placer que experimentaba estudiando a aquella especie de mujeres_."

  

_**Ana Karenina**_, León Tólstoi. Madrid, Austral 2007. 

  

[![](http://4.bp.blogspot.com/-FLF-L16Twes/UGvhmBmpbyI/AAAAAAAABjM/jwDobOYmdXE/s400/rostard.jpg)](http://4.bp.blogspot.com/-FLF-L16Twes/UGvhmBmpbyI/AAAAAAAABjM/jwDobOYmdXE/s1600/rostard.jpg)Perdóname querido lector, llevo tiempo queriendo escribir y no es que me falte, es otra cosa. Leo '_**Ana Karenina**_', todavía es pronto pero estoy disfrutando muchísimo con **Tólstoi**, la alta sociedad rusa, su psicología y la elegancia; circunstancias a las que es imposible no rendirse. Me recuerda en cierto sentido al imprescindible **Dorian Gray** por sus encantadoras pero breves y sutiles descripciones y sus numerosos diálogos cargados de ideas estimulantes e ingeniosas. Me resulta fácil sonreír mientras leo,  síntoma que no sufría desde hacía tiempo con la lectura.

  

He citado este frasco de la novela porque es una especie de resumen concentrado de sensaciones. No es lo que dice en sí, sino todo lo que me sugiere y despierta este fragmento, cuando pulsa una tecla especial. Ahora me doy cuenta de que no tiene nada que ver con lo que voy a decir. La vida no deja de parecerme compleja. **Chaplin** decía que de cerca es una tragedia y de lejos una comedia.

  

En realidad no sé ni por dónde empezar. Cuentan por ahí que estamos hechos para ser felices y que por eso la humanidad entera busca la felicidad a toda costa, unos a corto plazo, a largo plazo, otros creen que no la buscan pero sí lo hacen, no hay remedio: inexorablemente olfateamos un plato jugoso, prometedor y nos deslizamos rápidamente tras el aroma para probarlo. Por el camino muchos otros perfumes y pestes se confunden con este primero, y vamos a parar a vertederos y desagües, pero después de sacar nuestra nariz -si es posible- de la basura, seguimos al acecho.

  

Unas veces serán -y han sido- agradables sorpresas que tal vez nos hagan olvidar temporalmente el olor enigmático e irrepetible que sigue en el fondo de nuestra cabeza -del que estamos enamorados sin saber qué es-, pero luego te acostumbras y quedas insatisfecho y sigues buscando la esencia sublime. Y te das cuenta de que es terriblemente entretenido  ir velozmente tras algo y no llegar realmente a ningún sitio, leer placenteramente una novela genial sin fin, desconociendo cuando acabará.

  

Los sueños -me cuesta admitirlo- son inalcanzables, no son reales y ahí está su atractivo. Perseguir algo eternamente con el autoconvencimiento de alcanzarlo -falso, por supuesto- es la llave de la felicidad.  Las cosas no son lo que parecen, afortunadamente son mucho más, nunca llegaremos a entenderlas. Pero entretanto uno estudia, aprecia los infinitos detalles de la belleza de este mundo, y tiene para rato desengañándose y descubriendo nuevos encantos de los que tarde o temprano se hartará. Es muy importante que esté entretenido en ello.

  
Pero siempre insaciable seguirá buscando la 'verdad'. Por supuesto que en la ignoracia no está la felicidad, cabe disfrutar muy poco en la necedad, probablemente la 'verdad' te haga sufrir muchas veces, pero tantas otras te hará disfrutar. Eso sí, nunca la encontrarás, y nunca tendrás la certeza de lo absolutamente importante, pues así son las reglas del juego. ¿Para qué estar seguro? es infinitamente más placentera y deleitosa la duda, la seguridad no tiene la menor fascinación al contrario que la hechizante y misteriosa duda.  
  

Incluso ya habrás empezado a mentirte para seguir buscando. Es más diría que es fundamental para recorrer el camino hacia la felicidad engañarse, inventar para sobrevivir. No se puede sobrellevar toda la carga 'real' de la existencia sin hundirse, se necesita fingir y pulir la realidad con algunas mentiras que te ayuden a seguir buscando ese perfume del que eres cautivo, o simplemente evadirse de toda la presión existencial.

  

Lo que intento decir es que estoy muy contento con 'las reglas del juego'. Realmente todavía no me he expresado con claridad, pero este intento me deja satisfecho en algún punto. Como dice el Joker:   

>   

> "S_oy como un perro que persigue a los coches, no sabría qué hacer si alcanzara uno_".