---
title: 'El intercambio'
date: 2009-02-14T13:28:00.000+01:00
draft: false
tags : [El Intercambio, John Malkovich, Clint Eastwood, Cine, Entretenidas, Angelina Jolie]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/SZa5VJMhIAI/AAAAAAAAAY8/P2JA56xqrvk/s320/el-intercambio-6.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/SZa5VJMhIAI/AAAAAAAAAY8/P2JA56xqrvk/s1600-h/el-intercambio-6.jpg)

Fotograma de la película

Este sí que es un buen peliculón, lo vimos hace poco, y te coge es una película que te coge, te agarra y no te suelta, te pones totalmente de parte de Cristine Collins **(Angelina Jolie**) y es una película dura. Está basada en hechos reales, lo que hace que te guste mucho más, porque es como más cercana o auténtica.

.

El tema del vestuario y el escenario del suceso, están muy trabajados, y luego está el tema principal, los niños, que siempre hacen que una catástrofe sea más dura de lo normal. Es muy interesante como te metes en la película y sientes lo mismo que Cristine, te da mucha rabia, son muy buenas las conversaciones de la película, los monólogos con flash back como el del niño que encuentran, que tiene que ir a Canadá y le cuenta la verdad al agente de policía, o el de **John Malkovich** (Cura), con Cristine .

.

Es una película histórica que refleja el modo de hacer las cosas de la ley hace 80 años más o menos, en Los Ángeles. Está nominada a varios Oscar y **Globos de Oro**. Creo que es una buena película para ver de **Clint Eastwood**, sus películas me gustan, como otro peliculón, **Million Dollar Baby**, ganadora de **4 Oscar**, impresionante, aunque habría alguna cosa que comentar, o discutir sobre la película. Bueno en definitiva verla es una buena “peli” si sois de esos que os gustan (como a mi) los planes de “foton” con palomitas.