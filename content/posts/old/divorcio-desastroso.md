---
title: 'Divorcio desastroso'
date: 2020-01-15T10:15:00.000+01:00
draft: false
tags : [metro, divorcio, conversaciones, teléfono]
---

Vuelvo en metro por la línea 9 de clase de batería. Uno de unos 38 años, habla acaloradamente por teléfono, mientras deambula por el estrecho vagón, a veces se para y mira la oscuridad del túnel entre paradas:

  

> Yo también tengo mis días, hasta mi padre tiene sus días. Todos tenemos nuestros días. Pero vamos, que es el divorcio más desastroso que he visto. Que ya ha pasado un año, que puedes estar con quien quieras, salir, bailar. Pero tienes todo el rollo de la tradición y lo que debería ser.