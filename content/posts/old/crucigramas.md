---
title: 'Crucigramas'
date: 2020-03-05T13:32:00.000+01:00
draft: false
tags : [donuts, poesía cotidiana]
---

Quedamos en la esquina  
cuando sales de trabajar.  
Me llamas y me escribes  
y miro tus ojos azules.  
En el chino no tienen  
lo que buscamos.  
Pedimos un par de donuts  
veganos de los seis de la semana  
y los tomamos bromeando  
al sol.  
Con todo, y devorándonos  
en el sofá verde,  
no sé dónde se  
quedaron los crucigramas.