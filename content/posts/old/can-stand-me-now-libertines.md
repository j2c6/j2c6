---
title: 'Can''t Stand Me Now - The Libertines'
date: 2010-01-31T17:30:00.000+01:00
draft: false
tags : [Indie, Rock, Garage Rock, Pete Doherty, The Libertines, Musica]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/S2Wv0_demOI/AAAAAAAAAoo/LYTUQappems/s320/the_libertines.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/S2Wv0_demOI/AAAAAAAAAoo/LYTUQappems/s1600-h/the_libertines.jpg)

Esta semana vengo, con uno de los grupos mas influyentes de esta ultima década, que promovieron el movimiento conocido como _**Garage Rock**_. La banda londinense empezó a conocerse a principios del nuevo milenio, tenían clase, y mucho ritmo. una mezcla de **_punk_** muy melódico. En su segundo disco **_The Libertines_** (2004), movieron el mundo y llegaron a los Nº1  de ciertos países, claro que aquí no llegaron tanto, aunque si nos llegaron noticias de su problemático cantante _Pete Doherty_, junto con noticias de _Carl BarÂt_, lideres del grupo. Estos han influido en muchos otros grupos, quizás ahora mas populares pero que valen algo menos como: _Blink 182, Sum 41, Yellowcard,_ y también en la historia de _**Garage Rock**_, y el _**Indie**_. Trágica la historia de Pete, pero buenas letras, y canciones con mucha fuerza, como esta que os traigo, del disco _The Libertines_ del 2004, la primera de todas, una representación de su estilo, todos compenetrados, melodia y ritmo bien marcados. Con mucho volumen, gracias.