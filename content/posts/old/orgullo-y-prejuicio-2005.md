---
title: 'Orgullo y Prejuicio (2005)'
date: 2012-04-11T20:22:00.000+02:00
draft: false
tags : [Dario Marianelli, Joe Wright, Emma Thompson, Roman Osin, Cine, Donald Sutherland, 2000, Carey Mulligan, Rosamund Pike, Debborah Moggach, Jane Austen, Keira Knightley, Matthew Macfadyen]
---

Te aseguro, querido lector, que me encantaría poder decir todo lo que pienso después de ver una película o leerme un libro, sin embargo, debido a las circunstancias o a mi incapacidad momentánea de expresarme, tengo esa horrible sensación de no resultar sincero cuando pretendo serlo. '_**Orgullo y prejuicio**_' es una de las eternas recomendaciones, así que espero hacerle justicia.  
  

[![](http://1.bp.blogspot.com/-9q6xUtCix6A/T4XLPFRxQGI/AAAAAAAABXI/vcHlb3V31hc/s640/kk_eb_still_pandp2005_6.jpg)](http://1.bp.blogspot.com/-9q6xUtCix6A/T4XLPFRxQGI/AAAAAAAABXI/vcHlb3V31hc/s1600/kk_eb_still_pandp2005_6.jpg)

  

El clásico más famoso de la escritora **Jane Austen**, se lleva a la gran pantalla con una excelente realización de **Joe Wright**. Pocas películas se han realizado en la última década con tanta delicadeza acompañada de buen gusto, como esta opera prima del entonces desconocido Joe Wright. Para dejarla a un lado, el argumento en sí mismo es uno de los más románticos de la historia y precursor de la novela de este género, a partir de ahí sale un interesante guión hecho a medida por **Deborah Moggach**, que realiza una generosa adaptación, que esculpe y resucita el genio de la inmortal escritora inglesa. Así como la mención de **Emma Thompson** como autora de los diálogos adicionales, una experta en la literatura clásica.

  

Dentro de la adaptación de esta obra, está la sinfonía poética que posee este filme tan especial. La dirección artística que se ve reflejada en la ambientación, decoración, la recreación de la época de Austen, el vestuario (**Jacqueline Durran**)... es exquisita y refleja la elegancia y detalle de finales del siglo XVIII. Se supone que reflejan una época, pero es difícil expresar el placer del espectador al verse envuelto en semejante atmósfera de trajes y salones decorados grandiosamente, así como los más austeros calificables como sencillamente idílicos.

  

Esto es mostrado a través de románticos planos y tomas de una fotografía (**Roman Osin**) sublime. Desde las casas señoriales y las mansiones en sus fiestas más engalanadas, deleitando con cada vela y candelabro, hasta un paseo por el jardín, el campo y la naturaleza que inspiraría a **William Blake** a componer aquellos versos tan magníficos. La fotografía es importante, pero lo es más todavía en la novela clásica, donde la magia de las descripciones quizá se perdería en la gran pantalla, cosa que no ha sucedido en este largometraje tan evocador. Podría enumerar varios planos que me han cautivado sobremanera sin alejarlos de la historia, además citando que se han tomado la libertad de rodar escenas en lugares más apasionados que los de la propia novela.

  

La música de **Dario Marianelli** con un precioso tema central, acompañado por estas espléndidas imágenes citadas con anterioridad, cierra un círculo de exquisito y admirable  carácter cinematográfico. Sin embargo estos no son todos los ingredientes de tan grandiosa cinta.

  

El reparto. En primer lugar la soberbia y memorable actuación de **Keira Knightley**, que dejó a todos boquiabiertos en una sincera y natural interpretación del encantador personaje de **Elizabeth Bennet**. La hemos visto en otros ejemplares pero ningún trabajo tan memorable como este, exento de esa pose artificial que exhibe desgraciadamente en otros largometrajes.  Un papel deslumbrante y excepcional, que eclipsa a los demás. Pero no por esta estrella incandescente despreciaremos otras brillantes interpretaciones. **Rosamund Pike**, en la piel de la dulce **Jane Bennet** quizá secunde a la hermana número dos, creo que se trata de una actuación muy especial. Esta actriz no ha encontrado todavía otro papel que apoye su prometedora carrera, pero esperamos entusiasmados que lo encuentre.

  

Las otras hermanas Bennet: **Talulah Riley, Jena Malone** y la observada **Carey Mulligan**, debutantes en este inestimable film, firman a su vez portentosos futuros, como ya se comprueba en el caso de esta última. **Matthew Macfadyen**, así como **Donald Sutherland** rodeados de belleza femenina, no salen tan bien parados, en papeles menos sugerentes pero aceptables.  
  
  

En fin,  es la alegría desbordante que despierta un largometraje tan joven y primaveral, con una acertadísima versión de la novela de Jane Austen. Aún así resulta perfecta y original sin defraudar las más ambiciosas expectativas, resultando una eminencia a principios del nuevo siglo del cine, destacando por su belleza artística y delicadeza visual.