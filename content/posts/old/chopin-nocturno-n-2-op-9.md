---
title: 'Chopin - Nocturno nº 2 Op. 9'
date: 2010-06-30T13:53:00.000+02:00
draft: false
tags : [Claro de Luna, Beethoven, Nocturnos, Debussy, Dorian Gray, Beauty of Tragedies, Musica, Chopin, Oscar Wilde, Musica Clásica]
---

Supongo que este año, he redescubierto la música clásica. La gran variedad de obras que se incluyen en este campo me aturden, no puedes esperar conocerlas todas, simplemente disfrutarlas a su paso, como los segundos, y los minutos. El poder del piano destaca entre todas ellas, tiene un sonido que bien puede ser como las olas del mar bañando la orilla, o los truenos de una cruel tormenta, es el señor de todos ellos (los instrumentos). Lo cierto es que me encanta, y me dice lo que nadie puede decirme.

.

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TCsy5mioR7I/AAAAAAAAAvA/SHtv3a0RfF0/s200/Copia+de+chopin_y_piano.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TCsy5mioR7I/AAAAAAAAAvA/SHtv3a0RfF0/s1600/Copia+de+chopin_y_piano.jpg)  

No sé si es muy conocida ya que no tiene un apodo como "**Claro de Luna**" (**Beethoven, Debussy**), es simplemente el segundo Nocturno de Chopin.  Los nocturnos, como su nombre indica son piezas inspiradas en la noche (algo que adoro), tranquilas, melancólicas a veces, como las lágrimas. Aunque tengo mis teorías, puesto que **Dorian Gray** (el mismo) dice: "la belleza, la simple belleza, podía llenarte los ojos de lágrimas" **("El Retrato de Dorian Gray" Oscar Wilde**). Es decir quizás sea la belleza de la pieza, su sencillez... La pieza tiene en la mano izquierda un silencioso y grave acompañamiento, a una delicada derecha y más aguda y cristalina melodía. Esta melodía no suena en los oídos, sino directamente en otro lugar más hondo. 

  

.

La obra de cuatro minutos y medio, empieza narrando una tragedia (**Beauty of Tragedies**, hablaremos), algo que empezó como una joven primavera, y que tal vez fue marchitada por un algo inevitable, lo que ocurrió es que los efectos permanecieron para siempre, convirtiéndola en inolvidable. Hay algo de amor, pasión, incomprensión y un ápice de locura. Habla en un idioma desconocido, pero que misteriosamente  se comprende como el llanto de la desilusión. La música te pide que te dejes llevar, y tú la sigues obnubilado,  quieres saber que ocurre con la historia, la tragedia; sin embargo no se altera ante lo inexorable y sigue tranquila su curso como un riachuelo de tierras lejanas, que llega hasta el infinito...

.

Simplemente me encanta, me dice demasiado como para despreciarla: