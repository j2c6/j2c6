---
title: 'Lo imposible (2012)'
date: 2012-11-20T07:30:00.000+01:00
draft: false
tags : [Ewan McGregor, Óscar Faura, Fernando Velázquez, Sergio G. Sánchez, Cine Español, Cine, 2010, Juan Antonio Bayona, Naomi Watts, Tom Holland]
---

Ante la crisis -casi eterna- que lleva sufriendo el cine español, es una gran noticia que haya una excepcional recaudación en una película íntegramente producida por capital nacional. Y esto hace que me 'enorgullezca' de alguna manera. Realmente se trata de una película muy bien hecha, hecha para sufrir. No me gustan los largometrajes sobre catástrofes, comprendo la emoción que despierta la incógnita de sí una familia que sufre uno de los tsunamis más devastadores -como es el que sucedió en diciembre de 2004 en el sudeste asiático- sobrevive o no. No es el cine ideal para mí, pero las verdades sean dichas: llega a ser muy emocionante.

  

  

[![](http://1.bp.blogspot.com/-eib2210v5c8/UKqcXN0fepI/AAAAAAAABls/H9pOspAMiXQ/s1600/rostard-imposible.jpg)](http://1.bp.blogspot.com/-eib2210v5c8/UKqcXN0fepI/AAAAAAAABls/H9pOspAMiXQ/s1600/rostard-imposible.jpg)

Supongo que de momento le da otro caché el que, a pesar de que la dirección (**Juan Antonio Bayona**) y producción sean españolas, el reparto principal sea americano. Las caras conocidas de **Naomi Watts** y **Ewan McGregor** dan cierta tranquilidad al espectador. De algún modo se te olvida que es española, algo curioso donde la película nacional más taquillera sencillamente no lo parece, esto dice bastante del cine español. Sí tiene algo reconocible el guión (**Sergio G. Sánchez**) y familiar como aquello de "_cierra los ojos y piensa en algo bonito_".

  

Pero sobretodo por una buena fotografía (**Óscar Faura**) y música (**Fernando Velázquez**) que acompañan los trabajados e impresionantes efectos especiales de las olas, el dolor debajo del agua, la situación y entorno tras el desastre. Y además de todo lo que se sufre en la piel de Watts especialmente -sobretodo con su visual herida en la pierna y hasta que llegan al hospital-, y quiero detenerme porque hasta los lloros del niño rubio suenan especialmente dolorosos y sensibles hasta el mareo; hay una luz especial en el filme, una luz preciosa o esperanzadora que me gustó. Así como la tierna relación madre e hijo (**Tom Holland**), poderosa y más que convincente. Pero definitivamente, aunque aplaudo tan buen trabajo técnico no es mi tipo de pieza.