---
title: 'The Overstory'
date: 2019-06-24T11:38:00.002+02:00
draft: false
tags : [ficción, naturaleza, Richard Powers, Pulitzer, árboles, Literatura, The Overstory]
---

El Overstory, título exportado como '**_El clamor de los bosques_**' -no me mola en absoluto la dosis de azúcar del término [clamor](http://etimologias.dechile.net/?clamor)\-, de Richard Powers está bien. Dice que vive al pie de las Grandes Montañas Humeantes. A falta de 75 páginas para terminarlo.

  

Especialmente las primeras doscientas, cuando narra los comienzos de las historias, sin alardear, sólo narrando lo que le apetece, me gusta mucho. Luego el libro se vuelve muy descriptivo en la etapa activista, un poco melindrosa y adoctrinada. Prefiero que me den los ingredientes y ya los meto yo en la Thermomix.

  

La sensación de que los árboles son, socialmente, invisibles, te la llevas. Tienen algo.