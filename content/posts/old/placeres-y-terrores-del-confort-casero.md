---
title: 'Pleasures and Terrors of Domestic Comfort'
date: 2019-06-06T22:49:00.002+02:00
draft: false
tags : [Peter Galassi, MOMA, fotografía]
---

[![](https://1.bp.blogspot.com/-V2crON5SxAM/XPl8HvvunoI/AAAAAAAAdLg/ZrMLjcC76K0Y60AzF1Gp2wUfQWUaorw_QCLcBGAs/s320/71X0u0Td-VL.jpg)](https://1.bp.blogspot.com/-V2crON5SxAM/XPl8HvvunoI/AAAAAAAAdLg/ZrMLjcC76K0Y60AzF1Gp2wUfQWUaorw_QCLcBGAs/s1600/71X0u0Td-VL.jpg)