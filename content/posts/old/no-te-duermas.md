---
title: 'No te duermas'
date: 2020-07-06T12:23:00.001+02:00
draft: false
tags : [Microrrelatos, relato]
---

No podía dormir. Había algo que le pedía que se levantara. Un susurro: 'no te duermas'. Al mismo tiempo, estar despierto era sentir el calor del verano, las chicharras sobre ti. Pensó, mañana compraré un aire acondicionado. Y dijo en voz alta:

—Mañana compraré un aire acondicionado.

Pero ella no dijo nada. Apenas se oía su respiración. Una planta le miraba impasible, quieta como si nada.