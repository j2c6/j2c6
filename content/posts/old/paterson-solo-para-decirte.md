---
title: 'Paterson, sólo para decirte'
date: 2017-06-01T11:05:00.001+02:00
draft: false
tags : [Jim Jarmusch, Adam Driver, ciruelas, paterson, William Carlos Williams, Cine, Poesía]
---

Descubriendo al poeta de lo cotidiano, William Carlos Williams, en la película Paterson, me quedo con este poema que funciona como una nota en la mesa de la cocina.  
  
Esto es sólo para decirte  
que me comí  
las ciruelas  
que estaban  
en la nevera  
y que  
probablemente  
estuvieras guardando  
para el desayuno  
perdóname,  
estaban deliciosas  
tan dulces  
y tan frías  
Por algún motivo, el aire despreocupado de unos versos así, conquista por su simplicidad.