---
title: 'El producto eres tú.'
date: 2018-05-01T11:20:00.003+02:00
draft: false
tags : [privacidad, datos, futuro, software, control, google, whatsapp, mal, empresas, facebook]
---

Google, Facebook, WhatsApp.

  

¿Hay alguna alternativa? Si en un futuro cercano y maléfico --así es como los sapiens imaginamos el futuro, los zombies nos van mucho-- estas empresas decidieran, no necesariamente informándonos, aprovecharse definitivamente de nosotros, no dispondríamos de grandes alternativas sin trastocar nuestras comunicaciones y modos de vida.

  

¿Es mejor no tener e-mail? ¿No disponer de ninguna de esas plataformas de comunicación? Poco a poco, vamos basando nuestra vida en ellas, nuestra manera de relacionarnos, pasar el tiempo y obtener información.

  

Por otro lado nos hemos acostumbrado a disfrutar de estos servicios 'gratis', ¿es la hora de planteárselo? **Cuando un servicio es 'gratis', el producto eres tú.**

  

Ahí lo dejo, tal vez más adelante vea esta nota con ingenuidad o la mire asombrado como una señal que no supe escuchar.