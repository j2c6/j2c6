---
title: 'Foo Fighters - Everlong'
date: 2009-08-19T20:06:00.000+02:00
draft: false
tags : [Nirvana, Grunge, Musica, The Colour and the shape, David Grohl, Kurt Cobain]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SoxC6TXd5ZI/AAAAAAAAAjk/s_T7sd6Pjcg/s320/Foo_Fighters-The_Colour_And_The_Sha.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SoxC6TXd5ZI/AAAAAAAAAjk/s_T7sd6Pjcg/s1600-h/Foo_Fighters-The_Colour_And_The_Sha.jpg)  

Bueno continuamos, con el verano. Hoy os traigo a un grupo, que me gusta bastante, que fundo el baterista de **Nirvana**: _**David Grohl**_. Es una gran canción  donde Grohl hace la guitarra y voz, algo sorprendente, ya que le daba enérgicamente, a la baquetas del, grupo, mas famoso de _grunge._ La batería, es de lo mas conseguido, y tiene, un estilo, tétrico tipico de _grunge_. Este tema, se hizo uno de los himnos del grupo americano, ya que lo tocan, después de cada concierto. Tanto tiempo; "**_Everlong"_**. Es un romance, de una larga espera con premio: "Te espere tanto tiempo". La canción a la vez es preciosa, y un tan potente, es un grito de aleluya... Bueno es impresionante, solo tenéis que escucharla, es el tema mas potente de su álbum  "**_The Colour and the shape_**", a disfrutar: