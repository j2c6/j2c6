---
title: 'Noche y Día'
date: 2010-07-27T06:10:00.000+02:00
draft: false
tags : [Cameron Díaz, Cine, Entretenidas, Tom Cruise]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/TEV5Oe9hR7I/AAAAAAAAAwc/Mo6kvxs5Zv8/s320/cinemaniablog_noche_y_dia.gif)](http://2.bp.blogspot.com/_VT_cUiG4gXs/TEV5Oe9hR7I/AAAAAAAAAwc/Mo6kvxs5Zv8/s1600/cinemaniablog_noche_y_dia.gif)

No estamos hablando de nueve Oscars, sino de una película de acción. Con acción no me refiero a ACCIÓN (acción agresiva tipo Ciudadano Ejemplar...), sino a acción divertida, en plan comedia con coches y pistolas. Una línea argumental casi nula, deja destacar un buen guión -parece incompatible- de diálogos entretenidos, y situaciones que solo aparecen en películas de **Tom Cruise**: soy un tipo duro que maneja a los malos con la mano izquierda mientras juego al póquer con la derecha. Además aparece **Cameron Díaz** que no aparecía en el mismo reparto que Tom desde **Vanilla Sky**... hace el papel de mujer ingenua, y "adolescente" que se ve envuelta sin buscarlo entre explosiones, y alguna que otra persecución. Un rato divertido, con dinero "Hollywoodiano" y agentes secretos al más puro estilo **Bond** (elegancia y agresividad). Una manera fugaz de pasar 111 minutos.