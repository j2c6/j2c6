---
title: 'Venta de Atención'
date: 2020-03-05T12:45:00.000+01:00
draft: false
tags : [Amazon, Apple, HBO, google, Disney, facebook, Netflix, tiempo]
---

Así, en bruto, la cantidad de tiempo que puedes utilizar, viendo algo que no te interesa. Explícitamente, dándote cuenta de la escasa calidad de contenido, pero te quedas hipnotizado pensando "¿es esto posible?". Y ahí sigues, sin poder resistirte a la tentación de mirar qué más cosas estará haciendo la gente, discutiendo y poniendo mucho empeño en llamar la atención, metidos en un ajo, subidos a una noria, que no va a ningún sitio. Mientras, tu vida, en un suspenso, una cámara lenta que hace la imagen cuasiestática, se para, deja de respirar. Dejas de respirar, de ser tú, para dejarte absorber por algo que te aleja del sentido de la vida, que ni siquiera te entretiene, que es un parásito.

  

Los loables motivos por los que se crea ese contenido, para etiquetar, para dar voz a las ideas, la rabia, para venderse. Y tengo esa lista de cosas pendientes, que en teoría me acercan a lo que quiero ser y hacer, que se queda en barbecho por ese mecanismo de captura de atención.

  

Todas las grandes empresas (Netflix, Apple, HBO, Google, Amazon, Disney, Facebook) se centran en la producción de contenido. Después de conseguir en apenas diez años que los llevemos todo el día encima y no podamos prescindir de ellos, ahora quieren nuestro tiempo.