---
title: 'El cielo puede esperar (1978)'
date: 2012-02-25T16:25:00.000+01:00
draft: false
tags : [Warren Beatty, Julie Christie, Charles Grodin, Duan Cannon, Cine, 1970, Harry Segall, Buck Henry]
---

[![](http://4.bp.blogspot.com/--gxc17kOEPc/T0j85MG86EI/AAAAAAAABPo/SrZh_sYNJBI/s400/rostard.jpg)](http://4.bp.blogspot.com/--gxc17kOEPc/T0j85MG86EI/AAAAAAAABPo/SrZh_sYNJBI/s1600/rostard.jpg)El portentoso jugador de rugby Joe Pendleton (**Warren Beatty**) es llevado al cielo por error justo antes de su posible muerte. Cuando se dan cuenta de que su cuerpo ha sido incinerado tras el accidente, deciden introducirlo en el cuerpo de un multimillonario como es el Sr. Farnsworth. Desde ahí Joe tendrá que arreglárselas para conseguir su sueño: jugar en la final de fútbol americano.

  

Basada en la obra de teatro de **Harry Segall**, el remake de '**_El difunto protesta_**' (1941, **Alexander Hall**) '_**El cielo puede esperar**_' se ha convertido en una comedia memorable a lo largo de los años. La película sin grandes pretensiones que obtuvo **9 nominaciones** a los **Oscar** y una estatuilla a la mejor dirección artística, presenta a un personaje muy sencillo y campechano, bueno por naturaleza. La popularidad de **W. Beatty** se incrementó considerablemente por su actuación y dirección con la colaboración de **Buck Henry**. El éxito se esconde en el romance con **Julie Christie**, exento de superficialidad con el tópico "la belleza está en el interior", así como el divertido dúo de asesinos **Charles Grodin** y **Dyan Cannon**.  
  
  

La historia genialmente fantasiosa atrae la atención desde el minuto uno. Hay algo curioso en  lo que va ocurriendo, ya que es impredecible. Las peculiaridades de Farnsworth y la afabilidad de Pendleton llevan las riendas. El resultado es entrañablemente interesante. Sin duda un ejemplar de comedia que corresponde y satisface al espectador, llevándolo --como es propio del género-- de la risa a las lágrimas. Muy recomendable, dentro de la simplicidad del filme.