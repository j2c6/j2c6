---
title: 'Cocina en tupper'
date: 2018-10-01T12:48:00.000+02:00
draft: false
tags : [MiAlcampo, comida. tupper, fiambrera, fría, lentejas, legumbres]
---

Entre los grandes éxitos de la cocina comprimida en fiambreras, mirando mi lista, sorprendentemente se encuentra la ensalada de lentejas, que no va más allá de lentejas cocidas con pepino, tomate y pimiento fresco y frío, aliñados abundantemente. Es una comida que aguanta muy bien esa noche en la nevera hasta la comida.

  

Nos hemos hartado del pollo con pimientos asados, pero volveremos sobre él, con moderación. También está en el top, la tortilla preparada de las neveras de MiAlcampo.