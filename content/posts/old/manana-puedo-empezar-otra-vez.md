---
title: 'Mañana puedo empezar otra vez'
date: 2020-04-20T11:40:00.002+02:00
draft: false
tags : [ficcion, relato]
---

  

Cómete el último, el de la vergüenza, dijo. Y no me sentí digno de tocarlo. Iba a alargar la mano y llevármelo a la boca, pero esa orden me puso en una situación incómoda. Él llevaba algo más de media hora contándome el drama sin solución. Con todos los detalles, notas al pie, posibles elucubraciones de distintos autores. Tenía el brazo estirado sobre la cresta del respaldo del banco. La música era suave y la luz tibia, me sentía cómodo. La película es mala, pero te encantan las palomitas. Me gusta el olor de la ropa nueva y sentirme recién duchado. Y así permanecí, hasta que dijo eso, cómete el último, el de la vergüenza. No me considero alguien agresivo, procuro tener una psicomotricidad acorde con los mamíferos más lentos y sedentarios. No tengo prisa.

  

Él estaba esperando que lo cogiera, como regalándome algo. Le miré a los ojos, y pensé abiertamente en lo cansado que estaba de lo unidireccional de lo nuestro, de las historias de todas las semanas. En realidad, todo es mucho más sencillo. Me levanté y me fui. ¿Pague? ¿Le di un saludo? No. Lo dejé ahí, plantado. Me senté en el coche, esperé a que empezara a salir calor del salpicadero.

  

Di una vuelta por a la autovía. Paré en un puente desde donde se veía de cerca cómo aterrizan los aviones y estuve contemplándolos largo rato. Empezó a no haber luz. Los faros de los coches hacían sombras fantasmales con las farolas. Tuve la sensación de que, en ese momento, nadie estaba pensando en mí, y me sentí bien. Una ficha del tablero desprotegida. Mañana iré al supermercado, y me haré con nuevos estropajos. Ordenaré el salón y dejaré de beber compulsivamente. Mañana puedo empezar otra vez.