---
title: '«Es un goal que no entra en nuestro roadmap»'
date: 2020-07-25T11:49:00.002+02:00
draft: false
tags : [bigdata]
---

«Pero para ello se tiene que cumplir nuestro definition of done» Azúcar Sintáctico.  

  

Datacentric, elimina silos entre clusters, gestionando frameworks, patrones SAGA. Cassandra. Kafka (no el escritor, desde luego),  HDFS, Elasticsearch, Postgres, con el desarrollo de una infraestructura agnóstica, Iaas, cloud, AWS, Azure, Mesos 2013, virtual o baremetal, docker, containers, Kubernettes, Crossdata, Spark SQL, Jupyter (R, Python, Scala), MLib, TensorFlow, Sparkit-Learn, Sparta, Paradigma es un SpinOff, aquí somos hiperagile, git fetch upstream.

  
«Eres nuevo, y cuando hablas con alguien, no sabes con quién estás hablando. Esto no es la Universidad»

  

Command Center, Governance, Intelligence, Andrómeda (sí, ahora sí, la constelación).

  

Keanu dice algo de Vault, EOS,  migración back-end de Vault, back-up and restore. XD pruebas no funcionales. QA Post.  

  

«¿Por qué los clientes no vienen a la daily?»

  

En capítulos anteriores de Agile:  

  
«El jefe no te dice qué tienes que hace pero te facilita una dirección»  
«Entrega incremental en un proceso iterativo»  
«Pero no hablemos de los stakeholders»

  

KPI, John Willis, Peter Drucker,  
«Nunca vais a poder cumplir un planning, ni en el trabajo ni en la vida»  

"Getting Things Done", Backlog

  
«No se respeta a quien no tiene un conocimiento técnico»

TTL, Time To Line.  

*   Limpieza de NEXUS a Glacier
*   Modelo de documentación Openstack.
*   Hemos desplegado Sparta y Kafka.
*   Ticket de JIRA; que quiten los parámetros  (4) que hacen que no haya logs.
*   XXX.SNAPSHOT está desplegado con otra versión desde local, túnel desde máquina del desarrollador.
*   Brocker, Scheduler, Orquestador.  
    
*   Boro, Sodio, Níquel.
*   Marathon, Exhibitor.
*   SSO han metido un secreto más, se accede a él al hacer teardown.
*   No se deberían hacer ENDPOINTS para cambiar los roles.
*   Orion compatible con Andrómeda.
*   Lunch & Learn, Framework QA  
    

Cucumber, Gherkin, Jenkins, Hooks.

  

ADN 12:30.

  

«Pensar a lo grande, los límites no te los pongas tú. Uso respondable de la libertad que tenemos aquí. Somos CAPACES DE PEDIR AYUDA. Es muy importante dejar claro en qué he estado trabajando.»

  
«Planning de QA y review en el PIT»  

  

DCOS-cli, el descriptor con autodescubrimiento de servicios. Particularidad basada en kafka. Healthchecks.