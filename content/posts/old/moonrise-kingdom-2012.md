---
title: 'Moonrise Kingdom (2012)'
date: 2012-07-02T12:47:00.000+02:00
draft: false
tags : [Bruce Willis, Bill Murray, Wes Anderson, Alexander Desplat, Cine, Edward Norton, 2010, Benjamin Britten, Frances McDormand, Roman Coppola, Robert D. Yeoman]
---

[![](http://4.bp.blogspot.com/-sI5Ap_row6A/T_F5xcGlKmI/AAAAAAAABfY/eBZGsZlUbxI/s400/rostard.jpg)](http://4.bp.blogspot.com/-sI5Ap_row6A/T_F5xcGlKmI/AAAAAAAABfY/eBZGsZlUbxI/s1600/rostard.jpg)Solo la historia resulta encantadora. Verano de 1965, una isla de Nueva Inglaterra, Sam (miembro de la brigada 55 de los Scouts) decide fugarse con Suzy una chica de la que se enamora poco antes de la representación de '**_El diluvio de Noé_**'. Parece algo corriente, pero es la forma de contarlo lo que sin duda es original.   
  

El director de fotografía (**Robert D. Yeoman**) habitual colaborador del director hace un trabajo singular, trabajada belleza frente a la cámara. Minuto uno. Muestra la sección de una casa como de muñecas, partida por la mitad y la cámara se va deslizando por ese plano vertical mostrando lo que ocurre en cada habitación con ese agradable corte sesentero y colores _polaroid_. Es mágico este sistema que luego se utiliza en muchas escenas a lo largo del largometraje, es como si vieras por el ojo de la cerradura lo que acontece, como si estuvieras fuera, pero te inundas de cada detalle.

  

Solo este modo de empezar acompañado del programa de **Benjamin Britten**, y la caracterización del escenario y de los personajes en esa época parece sensacional. Después de que un extraño de rojo resuma brevemente las características meteorológicas de la isla como una especie -y no me lo pregunte, querido lector- de robot costumbrista, se presente el campamento de un aparentemente joven Randy (**Edward Norton**), que supervisa las actividades de los scouts. Todo sucede de forma irónicamente seria, postiza e irreal. Una mezcla diferente.

  

No dejas de reírte y disfrutar con la ingenua historia, envuelta por una intensa y soñadora banda sonora de **Alexander Desplat** que ya vamos escuchando con más frecuencia. Porque el guión es impecable, lleno de detalles cargados de énfasis que el espectador recibe con desconcierto y no sabe cómo tomarse. '_Un año antes_' con unas elegantes letras amarillas, cuenta el lacónico flechazo de los dos jóvenes protagonistas y actores prometedores que no debemos perder de vista (**Jared Gilman, Kara Hayward**). Suzy era un cuervo y Sam un scout experto en supervivencia. Un misterioso acuerdo mutuo mantenido tras la correspondencia, los une y van conociéndose y tranquilamente siguen una antigua ruta migratoria de la isla.

  

Mientras los otros jóvenes scouts peinan armados la isla, buscando al impopular ejemplar de su brigada, también el capitán Sharp (un reconocido **Bruce Willis**) y un curioso matrimonio formado por **Frances McDormand** y **Bill Murray**, que son loas padres de Suzy, con el '_protesto señoría_' propio de los juicios. Momento de Suzy en la bañera: su madre la mira mientras se subraya la dramática música sublimando el momento y dice suplicante la pequeña: '_nos hemos enamorado, solo queremos estar juntos_'. En fin ¿porqué seguir narrando?, entretenidos giros de guión e innovadoras formas  de contar las cosas. 

  

Nada sofisticado, con el peculiar y surrealista modo de hacer las cosas en el que ha estampado su firma **Wes Anderson** también autor del libreto (y **Roman Coppola**), extremadamente entretenido y de forma muy enigmática, hasta enriquecedor diría. No sé qué tiene este largometraje que hace que me quede boquiabierto por su creatividad, pero que he podido disfrutar considerablemente. Cuenta un romance casi adulto, una historia donde los ingredientes son conjurados de forma inesperada pero obteniendo una receta perfecta. Todo un pasatiempo, claro que es fundamental el anzuelo inicial de los grandes actores que después dejan espacio a los jóvenes intérpretes.