---
title: 'Notas y recetas'
date: 2020-06-03T09:03:00.002+02:00
draft: false
tags : [poemas, poema, poesía cotidiana, poesía contemporánea]
---

Nosotros no planchamos  
pero así aparece él,  
el día, preparado,  
planchado y listo para  
ti, sí, que eres mía  
y para mí, que soy tu  
mitad, que comparto los  
genes entrelazados,  
espirituales y  
nuestros en las palabras.  
Buceando por dentro  
no puedo verte entera  
me paro y contemplo las  
notas y las recetas.  
Pasas la vista sobre  
esa cosa sin brillo,  
sucia, imperdible, y  
lleva el nombre con que me  
llamas y me besas tú.p { margin-bottom: 0.1in; line-height: 115%; background: transparent }