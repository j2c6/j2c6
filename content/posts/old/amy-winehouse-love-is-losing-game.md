---
title: 'Amy Winehouse - Love is a losing game'
date: 2008-11-06T21:02:00.000+01:00
draft: false
tags : [R'and'B, Musica, Jazz, Amy Winehouse]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/SRRTs5pqKLI/AAAAAAAAAU4/8nzg4a1kJSQ/s320/portada.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/SRRTs5pqKLI/AAAAAAAAAU4/8nzg4a1kJSQ/s1600-h/portada.jpg)  

Es una de mis favoritas, la voz de **Winehouse** no es de este mundo, una voz con todo.La letra de esta canción propia de **Amy** ha sido usada para comentarios de texto en Reino Unido, habla de amor ahí va un pequeño trozo (lo bueno en frascos pequeños):

  
  

"... sé que apuestas,

el amor es una partida perdida.

el amor es una partida perdida,

más de lo que podría soportar"

  
  

De todas maneras es mucho mejor en su inglés original, siempre suena mucho mejor : son letras para lengua inglesa. Ahi va el comienzo os dejo de **Goear** para que la escuchéis:

  

  
  
  

"For you I was a flame

Love is a losing game

Five story fire as you came

Love is a losing game"