---
title: 'Beneficios de una Pandemia Global'
date: 2020-04-23T17:47:00.001+02:00
draft: false
tags : [pandemia, conspiración, coronavirus, pandemia global, nuevo orden mundial, diario coronavirus, orden mundial, elite, predicciones]
---

En Agosto de 2014, La Gazzeta, un canal de YouTube creado en 2012, [subió un vídeo](https://www.youtube.com/watch?v=8uZMQEiD1mM&feature=youtu.be) hablando de las ventajas que tendría la 'Élite' de una Pandemia Global. El vídeo parece anticiparse a todo lo que está pasando en la actualidad con el Coronavirus. Casualmente, el último vídeo de este canal es de Abril del año pasado, desde entonces, el canal parece abandonado, así como los dos enlaces a los blogs que adjuntan en sus vídeos. Copio un resumen del vídeo:

  

**Beneficios de una Pandemia Global:**

1.- Sumisión a la autoridad.

2.- Abortar una revolución/rebelión.

Se prohiben las manifestaciones.

3.- Atomización de la sociedad.

4.- Eliminación de Disidentes.

5.- Gran Hermano Tecnológico.

6.- Vigilancia Masiva Ciudadana.

7.- Eliminación del Dinero Físico.

8.- Eliminación de los Medios Alternativos.

Propagación de bulos.

9.- Gobiernos Tecnocráticos: personas datos.

10.- Imperio de las Transnacionales: Compañías.

11.- Cambio de Modelo Económico.

12.- Guerra Biológica Encubierta.

13.- Control de la Inmigración Ilegal.

14.- Negocio para las Corporaciones.

  

**Características de la Pandemia:**

A.- Fácilmente controlable.

B.- Elevado nivel de pánico.