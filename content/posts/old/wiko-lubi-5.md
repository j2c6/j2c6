---
title: 'Wiko Lubi 5'
date: 2019-03-18T11:02:00.001+01:00
draft: false
tags : [moviles, Nokia, Wiko Lubi 5, simple]
---

Después de Nokia [105](https://precuneo.blogspot.com/2018/12/nokia-105.html), de repente un día, dejó de recibir señal, apenas 60 días después de su compra, estaba encantado con él. Por 12,99€, llegó Wiko Lubi 5, que tiene hasta Bluetooth. Ya veremos, he hecho todas las entrevistas con él, malos tonos de llamada, buen sonido, y radio sin auriculares. Además se le puede introducir una sd.  
  

[![](https://4.bp.blogspot.com/-92R9D8Zpr8U/XI9si08gAFI/AAAAAAAAdDo/MF6YQa3puigp7_cPPfcemDN8_sO7Jw0VACLcBGAs/s320/photo_2019-03-18_10-58-34.jpg)](https://4.bp.blogspot.com/-92R9D8Zpr8U/XI9si08gAFI/AAAAAAAAdDo/MF6YQa3puigp7_cPPfcemDN8_sO7Jw0VACLcBGAs/s1600/photo_2019-03-18_10-58-34.jpg)

Ahí está, pequeña y poderosa herramienta.