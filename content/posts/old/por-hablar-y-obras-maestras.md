---
title: '''Hablar por hablar'' y obras maestras'
date: 2013-01-08T20:54:00.000+01:00
draft: false
tags : [Oscars, Criticos, Stravinski, Blade Runner, Charles Chaplin, Mozart, Arte, Obras Maestras, Oscar Wilde, Magnum Opus, Uncategorized, Casablanca]
---

[![](http://1.bp.blogspot.com/-J7xp5D4uTfk/UOx4f7NhJ4I/AAAAAAAABt4/AbpxRloOV3U/s1600/rostard.jpg)](http://1.bp.blogspot.com/-J7xp5D4uTfk/UOx4f7NhJ4I/AAAAAAAABt4/AbpxRloOV3U/s1600/rostard.jpg)Uno de los grandes problemas del arte reside en que el 'artista' o el autor de la obra -siendo más concreto, ya que este es un título para ilustres- habla o crea cuando no tiene nada que decir, habla por hablar. Del mismo modo que esta conducta se ha arraigado en el mundo de la sensibilidad artística, se ha introducido en el espectador y en el ciudadano de a pie. '_**El arte por el arte**_' era un buen lema pero 'hablar por hablar' da lugar a odiosos resultados. Es evidente que para hablar y no caer en esta tendencia, hay que pensar lo que se dice, algo costoso y no siempre fácil de aplicar.

  

Uno de estos odiosos resultados de los que hablábamos es la prostitución de la palabra. Hay ciertos vocablos que, aunque atractivos y poderosos, pierden valor con su repetición abusiva, es cierto que expresan una idea concreta, pero es tan delicada y tan poco común que me extraña que el ciudadano que las pronuncia sepa adecuadamente qué significan, del mismo modo que no sea corriente su continua aparición. Resultado: devaluación de la palabra.

  

El término 'obra maestra' (magnum opus) en el cine -arte y entretenimiento al que cualquiera puede acceder- es un buen ejemplo. La mayoría de los críticos, pedantes a su vez, se empeñan en sentar cátedra acerca de qué es una obra maestra y qué no lo es, y realmente no tienen ni la más ligera idea de lo que están hablando. 'Obra maestra' ya es una referencia considerablemente compleja como para que aparezca cada treinta segundos en las páginas sobre cine. Podríamos admitir que todos hemos abusado de ella, de igual forma podríamos admitir que no es justo.

  

[![](http://2.bp.blogspot.com/-iuh8ZKK58WE/UOx5LB1rSMI/AAAAAAAABuI/qz87NmqkfJY/s1600/Midnight+in+Paris.png)](http://2.bp.blogspot.com/-iuh8ZKK58WE/UOx5LB1rSMI/AAAAAAAABuI/qz87NmqkfJY/s1600/Midnight+in+Paris.png)Por un lado hay que saber reconocer lo valioso. No necesariamente tu película favorita es una obra maestra, es muy probable que mis favoritismos se centren en el _pan y circo_, y no tiene ninguna importancia. Si mi obra predilecta es totalmente banal, predecible y necia, aunque divertida y capaz de despertar en mí sentimientos únicos, pues genial. Pero es necesario abstraerse para contemplar lo verdaderamente valioso, tampoco totalmente alejado de los gustos personales. Reconocer detalles trascendentales, una forma excelente de ver una idea que no es nueva, una banda sonora singular, una actuación superior a toda la obra...  
  
De modo que lo que entretiene no es necesariamente valioso, ni lo  que se eleva y trasciende es necesariamente  una  broma  pesada a mi reloj. Lo que ocurre es que la hipocresía aprovecha las dificultades para estafar al espectador. Por norma general en este país, lo intelectual un poco más original de lo común es basura; y me temo, querido lector, que no es así. Muchos pseudopensadores y pseudoartistas, hacen creer que su idea es genial cuando en realidad no va más allá de ser simplemente marketing patético.  Sin embargo otros más honestos te ofrecen algo nuevo,  digno de  cierto análisis y esfuerzo para su comprensión y es  entonces,  neciamente rechazado. No todo es blanco  o negro, por supuesto. Es posible que ante un guión mediocre, brille una fotografía única  y una banda  sonora innovadora y única; así como ante  una visión profunda, una carencia de ritmo narrativa, o incluso algo completamente sin precedentes incomparable a todo lo demás. Hemos se saber valorar lo sublime, ser  ladrones  de la belleza y no consumidores  insatisfechos. El arte no está para satisfacer nuestras expectativas.  
  
Además para aplicar el título de 'obra maestra' -inaplicable por lo visto- es necesaria la  supervisión de un factor clave, el tiempo. El tiempo hace justicia. Reconoció la  obra de **Mozart**, elevó la obra de **Wilde,** honró '_**La consagración de la primavera**_' abucheada y pataleada de **Stravinski**,  hizo justicia con ** Chaplin** y la farsa de los Oscar y  así  con otros escándalos y  muertes sociales. Por tanto, desprecio las habladurías que para alabar tal o cual aspecto de una obra se empeñan en bautizarla como 'una obra maestra'. De algún modo, una obra maestra además de ser la pieza por excelencia de la producción de un artista, es algo con lo que la humanidad entera se identifica, no es la idea loca de un 'crítico' de turno, sino algo grande. Es importante sentirnos identificados o ver qué hay de humano en ella.  
  
  

[![](http://2.bp.blogspot.com/-4TbmaWERQjw/UOx4goDwdrI/AAAAAAAABt8/md9NT_4hhpU/s1600/rostard2.png)](http://2.bp.blogspot.com/-4TbmaWERQjw/UOx4goDwdrI/AAAAAAAABt8/md9NT_4hhpU/s1600/rostard2.png)

Pero es difícil descubrirlas, pues fácilmente estamos condicionados por los que la juzgaron primero, los que dicen que '**_Casablanca_**' es una obra maestra cuando tan solo es una obra mítica y legendaria pero ni de lejos una obra maestra, y esto se demostrará. Aunque también existen las obras que revolucionaron su tiempo como '_**Blade Runner**_', pero que no por ello son una _magnum opus_, como sucede en este caso. Hay que intentar ver lo valioso de la obra, tal vez contrastar después con el resto del mundo, pero ante todo tener pensamiento crítico, algo personal y propio que no sea influido por las falsas corrientes de 'sensibilidad artística'. Ser un espectador de primera.

  

En definitiva, saber descubrir cada obra, reconocer lo que me gusta, aquello que consigue despertar ideas y transmitir sensaciones, lo valioso, y estudiarlo. Tal vez todavía no sepamos que estamos ante algo grande, o por el contrario ante un fantasma, pero en cualquier caso ser honesto con tu justa visión de espectador, fiel a sus sentidos e ideas, siempre con mucho por aprender y recordando que _solo los tontos no cambian de opinión_.