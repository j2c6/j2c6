---
title: 'Martes. Vas por la calle de camino al trabajo.'
date: 2019-09-19T09:43:00.000+02:00
draft: false
tags : [velada literaria, texto, ficcion]
---

Martes. Vas por la calle de camino al trabajo. Te encantan los martes, si cierras los ojos recuerdas el olor del esmalte cerámico de cuando ibas a clases de pintura. La profesora te puso un apodo incómodo, pero lo pronunciaba con cariño. Te gusta cómo te queda el pelo al día siguiente de lavártelo y cómo chirría la puerta desencajada del armario de la despensa. Cuando eras pequeña, te levantabas en plena noche a beber agua de la nevera y cuando te saciabas, mirabas alrededor, sentías el frío de la corriente a través del camisón, la luz roja del horno, los tubos fluorescentes de la pecera y pensabas que algún día tendrías tu propia casa donde beberías tu vaso de agua de madrugada. Este año no te has leído ningún libro que te haya gustado.

  

Tus zapatos pisan sin hacer ruido. El hombre árabe de la frutería ya está levantando la persiana y le esperan varias pilas de cajas de plástico y sobresalen puerros. No, son apios, que siempre me lío. No sabes si volverás a llevar tacones porque te confunde que te identifiquen con ese ruido que hacen al tocar el suelo. En el corcho de tu habitación cuelgas fotos con tus amigas, con tu padre, esa de tus abuelos recién casados en blanco y negro y las postales de Lisboa y Karlovy Vary.

  

Un día te levantaste por la mañana y ya no tenías claro si serías como siempre habías pensado que serías. Si sabrías desencajar el cajón de los cubiertos, enviar guasaps sin emoticonos que aceleren el pulso cardíaco, si te quitarían el pizco del pelo porque estamos en esa época de la dispersión de las semillas de chopo. Tienes miedo de que no te quieran. De no ser suficiente. Has perdido el bus 171 y quedan 7 minutos para el 153. Una señora te pregunta si esta es la cola para el que va a Alcobendas. La señora lleva gafas de sol a las 8 de la mañana. Le dices que sí. Te da las gracias.

  

No tiene sentido hacer voluntariado cuando no echas una mano en casa, eso es lo que piensas cuando tu tarjeta de transporte pita al pasar por la máquina. Te sientas en un sitio junto a la puerta de atrás de autobús y te colocas pegada a la ventana empañada. En realidad quieres que llegue la Navidad, notar la marca que deja la goma de los calcetines cuando te los quitas, arrugar el envoltorio de los polvorones de almendras. El conductor se queja de un coche parado en doble fila. Estás obsesionada con los cactus, las personas con familiares autistas y sigues escuchando la banda sonora de La Llegada. Llevas una mancha en el puño de la blusa. Se te ha olvidado el táper en casa.