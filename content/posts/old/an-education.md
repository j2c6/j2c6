---
title: 'An Education'
date: 2011-03-30T12:21:00.000+02:00
draft: false
tags : [Lone Scherfig, Reino Unido, Beth Rowley, Dame Victorienne, An Education, Juliette Gréco, Nick Hornby, Cine, Emma Thompson, Buenas, Carey Mulligan]
---

[![](http://1.bp.blogspot.com/-ltMFjAoWgX4/TZMCiQjjSII/AAAAAAAAA8s/JoRTcG2L4oo/s320/An+Education.jpg)](http://1.bp.blogspot.com/-ltMFjAoWgX4/TZMCiQjjSII/AAAAAAAAA8s/JoRTcG2L4oo/s1600/An+Education.jpg)

Jenny (Carey Mulligan) en "_An Education_"

En los años sesenta presenta la película a Jenny (**Carey Mulligan**) una adolescente inteligente y guapa que lleva una vida modesta, totalmente enfocada por sus padres a acceder a la universidad de Oxford. Un día de lluvia conoce a un treintañero -David (**Peter Sarsgaard**)- que se ofrece a llevarle el violonchelo. Los sueños de Jenny vuelan por encima de ir a una universidad: habla francés, ama la literatura, disfruta con la música, y es una apasionada de la vida aunque con limites. Cuando aparece David, parece que su "limitada y aburrida" agenda se convierte en un día a día interesante y animado: va a conciertos, restaurantes de lujo, subastas de arte, se deja encantar por París... en fin lleva un ritmo de  **dame victorienne**: Jenny disfruta, le brillan los ojos y quizás pierde los estribos.  
  

"**An Education**" es un largometraje cuidadosamente elaborado por la danesa **Lone Scherfig**, que acaba siendo mucho más profundo que una simple película superficial. Con un guión impecable **Nick Hornby** adapta las memorias de Lynn Barber. La banda sonora tiene mucha clase, parece propiamente elegida por la protagonista con temas de: **Juliette Gréco**, **Beth Rowley** \-canciones lentas en plan _sixties-_ y algunos otros como Brenda Lee, Ray Charles o Duffy representando esta década.  El vestuario y la fotografía se adecuan perfectamente a los 60 dándole además un aire de recuerdo a la época victoriana. Supongo que la idea es que la **educación** supera a la apariencia, y que detrás de una vida que parece monótona y fracasada puede haber más contenido que en una existencia intensa y frívola donde reina la hipocresía como loción diaria. La adolescencia queda representada fielmente: "El estudio de una niña al borde de la feminidad" he leído. La educación, la inocencia, los intereses, la clase social y la figura de los padres son algunos de los temas que trata la película.  
  

Carey Mulligan deslumbra al espectador con su brillante actuación en el papel \-nominada al Oscar- de una chica de dieciséis años  que recuerda a **Hepburn** , asegurando una carrera espectacular en el mundo del cine, sin duda excelente. Incluyendo a otros de renombre en el proyecto como **Emma Thompson**, **Alfred Molina** u **Olivia Williams** que completan una obra que habla al público, y se hace comprender. Esta película me ha llamado considerablemente la atención, transmite un mensaje. En la industria del cine escasean estos ejemplares tan cuidados... y necesitaba uno. _Recomendada por MB&N_