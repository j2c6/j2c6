---
title: 'Oscar Wilde - ''La duquesa de Padua'''
date: 2012-05-08T00:56:00.000+02:00
draft: false
tags : [Frank Bernard Dicksee, Shakespeare, Teatro, Oscar Wilde, Literatura, XIX]
---

[![](http://4.bp.blogspot.com/-6n9Nas8XTmk/T6hTif4CxjI/AAAAAAAABdY/PhxuQHNlBQ8/s400/rostards.jpg)](http://4.bp.blogspot.com/-6n9Nas8XTmk/T6hTif4CxjI/AAAAAAAABdY/PhxuQHNlBQ8/s1600/rostards.jpg)

Para empezar creo que es muy relevante decir que me he sentido un privilegiado, volviendo a leer a **Oscar Wilde** a quién admiro profundamente. Todavía me es casi desconocido por su lado dramático. Tras '**_Vera o los nihilistas_**' leo la siguiente cronológicamente: '_**La duquesa de Padua**_'. No creo que haga esto de leer toda la obra teatral de un autor de forma ordenada, pero Wilde merece esta excepción y dedicación. La de los nihilistas fue una toma de contacto, no me entusiasmó en exceso, pero tuvo un gran valor significativo al ser la primera obra del irlandés. No sé, pensaba que es toda una sensación; quizás Wilde esté observando a cada desprevenido lector que lo lee y espera su reacción.

  

'_**La duquesa de Padua**_' es una obra mucho más clásica y se acerca visiblemente a la medida de **Shakespeare** con numerosas referencias a _**'**_**_Macbeth'_** y a **_'_**_**Romeo y Julieta'**_. Sus metáforas e imágenes recuerdan al eterno inglés, quizás en exceso, sin embargo la obra es suya. He podido saborearla mucho más que el problema social de Vera, tiene esos elementos misteriosos y siempre invitados a esta fiestas líricas en verso: la muerte, el amor, la venganza, los remordimientos. Es una obra hecha para el tiempo y no tanto para expresarse como para interesarse, es completa y más convencional.

  

La historia es muy interesante de cómo en una venganza se entromete un romance y el pecado y el crimen. Wilde da una visión del amor muy concreta que resucita a las almas corrompidas, pero que luego es asesinado por el crimen. Tiene giros inesperados basados en las reglas morales que rigen el pensamiento de la obra, bien expresado en diálogos inspiradores, donde se recalca el peso de la moral y la religión no sin poner en duda su validez, que utiliza conforme a sus propósitos. Diría que no me han hecho mucha gracia los comentarios cómicos del comienzo del Acto IV, pero eso es insignificante.

  

Wilde, no hace alardes de su personalidad con sus aforismos característicos pero sí empieza a introducir frases por el estilo como: '_**fracaso, el único crimen que nunca he cometido**_'. Por aquel entonces en 1883 estaba en la escalada hacia su consagración como esteta y autor dramático, y no dejan de aparecer moderadamente comentarios ocurrentes. Se pone muy interesante con el silencio del reo y el giro argumental del Justicia Mayor. Entre algunas de las frases que se dicen los enamorados hay cosaspatéticas y sublimes.

  

La traducción se ha empeñado en cambiar absolutamente el orden de las palabras. Me ha gustado la belleza de la tragedia romántica al estilo Shakespeare, poética y extremadamente sobrecogedora. Finalmente con la lectura de la obra uno queda muy satisfecho, sabiendo que así se tejían los grandes dramas y aunque no es de los más halagados del autor, no carece por ello de importancia y encanto. Además se han dado a conocer algunos aspectos de la vida y su sentido que resultan muy estimulantes, como siempre suscita y tienta este paradójico literato.  
  

> "_Ahora sé bien que nunca habéis amado. Es el Amor el sacramento de la vida; pone virtud allí donde faltaba; al hombre lava de todas las viles corrupciones de este mundo; es el fuego que de sus escorias oro purifica, es la criba que separa el grano de la paja, es la primavera que, en una tierra helada, hace como una rosa florecer la inocencia. Pasaron los días en que Dios andaba con los hombres , pero Amor que es Su imagen, ocupa Su lugar. Cuando una mujer a un hombre ama, conoce entonces el secreto de Dios y el secreto del mundo. No hay casa tan pobre ni tan miserable casa donde, si es puro el corazón de quien la habita no pueda Amor entrar; más si el sangriento crimen a las puertas del palacio llama y se le deja entrar, entonces el Amor, cual criatura herida, escapa sigiloso y muere. Ése el castigo es que al pecado impone Dios. Los malvados no pueden amar._"

'_**La duquesa de Padua**_', **Oscar Wilde**. Valdemar, Mayo de 2008.  
Ilustración de  **Frank Bernard Dicksee**, '_**Romeo and Juliet**_'.