---
title: 'ser-crecer != productivo-activo'
date: 2020-07-20T17:47:00.000+02:00
draft: false
tags : [productividad]
---

No podemos dedicar tanto tiempo a no-ser. Tenemos que dedicar el mayor tiempo posible a ser y crecer. Ser-crecer != productivo-activo. No. Ser es dedicar tiempo a lo real, a alimentar el alma, a estar presente. A conectar con los demás. A empatizar. A profundizar en el misterio de la vida. Las mañanas podrían ser muy sencillas, llenas de vida.