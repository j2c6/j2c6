---
title: 'Crimen y Castigo  - Fiodor Dostoievski'
date: 2010-04-15T16:51:00.000+02:00
draft: false
tags : [Rusia, Fíodor Dostoievski, Un Clásico, S.XIX, Literatura, Raskólnikov, Novela]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/S8cnqG8IdvI/AAAAAAAAAqc/rly8LL05c-Y/s400/raskolnikov.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/S8cnqG8IdvI/AAAAAAAAAqc/rly8LL05c-Y/s1600/raskolnikov.jpg)

Ciertamente creo que la **literatura,** que tanto ha enriquecido al mundo y hecho de  él un lugar maravilloso, debe  _quemar_ más líneas de este pequeño blog. Nunca es tarde para leer clásicos, que se hacen universales. Después de todo aunque se traduzcan, son tan humanos que hablan una misteriosa lengua internacional, que hacen que andemos con el corazón en la mano, hacia donde en autor en cuestión nos quiera llevar. Por eso son elementos imprescindibles en la vida de cualquier admirador de la vida, y de la humanidad.

.

_**Crimen y Castigo**._ No soy un gran lector, por desgracia. Es la segunda vez que empiezo el libro, en esta ocasión en invierno, y hubo momentos duros en los que era difícil continuar, sin embargo fue genial . No es que os vaya a hacer un resumen del libro, pues pienso que es mejor leérselo. **Raskólnikov** es un ex-estudiante del que se hace un intenso análisis mental y psicológico a lo largo de toda la obra, en tal manera que puedes llegar a comprender, su situación. El libro es tremendamente sincero en sus sentencias extraídas de la atormentada conciencia de **Rodia**. A través de las  setecientas páginas, sientes una profunda depresión con origen en la oscuridad del fascinante protagonista, de modo que esta escrito de tal manera que es auténtico y fiel a la realidad, que da motivos para asustarse, y reflexionar sobre: la vida, la muerte, el crimen, el amor, el bien, el mal, la existencia... Una infinitud de cuestiones que empapan la vida cotidiana.  Realmente no tengo experiencia en esta clase de comentarios, pero la verdad es que no encuentro palabras para describir cómo me ha maravillado está **novela del siglo XIX**. Sencillamente al leerla he comprendido porqué es un clásico  este trozo de realidad: porque es algo que atañe a toda la humanidad, la vida de este Raskólnikov. El amor de Sonia, de Dunia; geniales los personajes de **Razumijin, Marmeladov, Luzin**... y sobretodo la magia de Dostoievski, que hace que la tinta haga fuegos artificiales.