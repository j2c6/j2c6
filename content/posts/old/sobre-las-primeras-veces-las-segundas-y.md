---
title: 'Sobre las primeras veces, las segundas y acostumbrarse'
date: 2012-11-27T13:52:00.000+01:00
draft: false
tags : [detalles, segundas veces, Shakespeare, experiencia, primeras veces, Uncategorized, El Columpio, novedad, acostumbrarse, Fragonard]
---

[![](http://4.bp.blogspot.com/-0bbJlCTTdf0/ULS3Pn1QYhI/AAAAAAAABoM/jN14KZkaqm0/s1600/rost.jpeg)](http://4.bp.blogspot.com/-0bbJlCTTdf0/ULS3Pn1QYhI/AAAAAAAABoM/jN14KZkaqm0/s1600/rost.jpeg)

Hay algo en el hombre que resulta ser odioso, pero que está en nuestra naturaleza: el hombre se acostumbra a todo. Sí, por muy novedosa que llegue a ser la circunstancia, al final, uno se adapta, pierde toda la sensación de estar en una situación diferente, se acostumbra. Podríamos ver consecuencias positivas de esta característica, como la capacidad de supervivencia, que puede ajustarse a condiciones físicas o emocionales extremas, lo que no lo excluye de ser destrozado de por vida aunque consiga soportarlas. Desde desayunar presa ibérica todos los días, hasta asesinar y convertirse en un sádico asesino en serie que se habitúa al cruel derramamiento de sangre. Sin embargo no deja de ser una cosa curiosa, el hombre es capaz de cualquier cosa y luego acostumbrarse a ella.

  

Es una pérdida de la sensibilidad, y esta se conserva considerando cada acción -aunque repetitiva- singular. Apreciando cada detalle, cada tecla que despierta una actitud. Cuando se pierde esta verdadera consciencia de lo que se está haciendo creo que en la mayoría de los casos -quizá restando los de supervivencia, pues habrá de soportarlos- el ser humano sale perdiendo. Si se trata de brutalidades, fácilmente se convierte en una bestia, e incluso fácilmente considerará leve el asunto amparándose en la cantidad de veces que lo  ha perpetrado. Si se trata de algo trascendente y valioso, algo sublime e importante, tal vez corra el riesgo de tratarlo con irreverencia, de no entonar la veneración que merece y lo banaliza; a pesar de que posiblemente lo idolatrara en sus manos la primera vez como algo majestuoso.  Pero afortunadamente el hombre es capaz de cualquier cosa, de lo mejor y de lo peor, de pasar de un extremo a otro, y esto es  grandioso y magnífico.

  

Pero no es mi intención reparar en esta capacidad infinita de lo mejor y de lo peor -muy observado en las obras de Shakespeare- . Por un lado uno desea saber y conocer el mundo que lo rodea, para ser un entendido, para satisfacer esas ansias de llenar el alma de conocimiento. Pero por otro lado está la perspectiva del todavía ignorante e inexperto en infinidad de cosas -que aquí es dónde nos encontramos todos realmente, pues ¿qué sabemos en verdad?- que es sencillamente muy prometedora. Pues -por ejemplo- las obras maestras del cine que desconoce, las nuevas voces de la música que no han deleitado sus oídos todavía, las almas de las personas a las que aún no conoce profundamente, están por descubrir, esperan una 'intensa primera vez'.

  

Y es que hay pocas cosas tan intensas como las primeras veces. Lo novedoso atropella al novato y no sabe cómo responder, solo se ve envuelto en una terrible situación que ahoga a todos sus sentidos y no sabe muy bien qué está pasando, pero en la sorpresa está la ventaja. Experimenta algo nuevo y sufre incluso una fiebre extraña, un hormigueo esencial, un poderoso placer en el descubrimiento y no es capaz de hacerse cargo de la situación, es esclavo de una sensación nueva. Realmente fascinante.

  

Muchas veces podríamos pensar en las infinitas posibilidades que desconocemos y que están por deleitarnos. Sin embargo ahí no acaba todo. A partir de la primera vez, uno es más dueño de sí mismo, es capaz de analizar y deducir que es lo que le ha provocado tanto placer, qué es lo que lo ha seducido y sorprendido tanto y es capaz de disfrutar y maravillarse más si cabe. Ya no es esclavo del momento, pero conoce mejor el misterio -no al completo por supuesto-, uno no es tan víctima como juez en las segundas y siguientes veces hasta el desgraciado acostumbramiento.  
  
En definitiva, sabiendo esto sabemos que aún nos esperan grandes secretos por descubrir que es la especialidad del ignorante sediento de saber y experiencia, y por otro lado hay una infinitud de fantásticos detalles que despreciamos cada día y vemos tan ordinarios como la lluvia, pero que en realidad son increíbles, como lo es que caiga agua del cielo. Los pequeños detalles y matices salpican nuestra 'monótona' vida, y ciegos somos que no los vemos.