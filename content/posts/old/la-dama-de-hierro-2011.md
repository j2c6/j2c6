---
title: 'La dama de hierro (2011)'
date: 2012-01-08T14:33:00.000+01:00
draft: false
tags : [Phyllida Lloyd, Abi Morgan, Meryl Streep, Cine, 2010, Biopic, Margarel Thatcher]
---

[![](http://1.bp.blogspot.com/-o2yeeYx2Ggk/TwmaG2FzmUI/AAAAAAAABF4/5jScxLzJEqE/s400/rostards.jpg)](http://1.bp.blogspot.com/-o2yeeYx2Ggk/TwmaG2FzmUI/AAAAAAAABF4/5jScxLzJEqE/s1600/rostards.jpg)Las expectativas siempre han supuesto un problema a la hora de apreciar una película, especialmente si lo que se espera es algo increíble. Comparar es útil, juzgar antes de tiempo es práctico, pero la parcialidad resultante puede acabar en el asesinato de la película.

  

**Meryl Streep** es el punto de mira de todos los espectadores en este '_biopic_' sobre **Margaret Thatcher**, ya que además de ser una de las actrices más representativas de la historia del cine --que es todo un título--, la caracterización y parecido de Streep y Thatcher es muy llamativo. Otro elemento muy interesante es la visión de una ex-primera ministra demente ya en los últimos años. El recurso de rememorar los momentos más relevantes de su vida era una buena idea, pero el guión (**Abi Morgan**) se vuelve desesperante --la sincronización resulta caótica--, pues a pesar del interés que tiene este personaje, los pasajes son fugaces y no tienen fuerza suficiente. No transmite y por ello es olvidable.  
  

Creo que si se hubiera centrado más en una de las épocas, la película hubiera tenido más agarre. No existe un tiempo de reflexión, no se muestra a la verdadera M. Thatcher, pues no llegas a comprender a la protagonista a pesar del impresionante trabajo de Meryl Streep que tras numerosas nominaciones desde su segundo y último Oscar hasta la fecha, optará sin duda a conseguir el tercero. El fallo está en el desastroso guión, y en la desafortunada dirección de **Phyllida Lloyd** (_Mamma mia!_, 2008) pues esta historia tenía una gran proyección, pero se ha quedado en un decepcionante intento.