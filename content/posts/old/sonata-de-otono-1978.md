---
title: 'Sonata de otoño (1978)'
date: 2013-02-09T19:20:00.000+01:00
draft: false
tags : [Ingrid Bergman, Liv Ullmann, Sonata de otoño, Ingmar Bergman]
---

En la película de **Bergman**, se describe como en una novela psicológica y con exquisita sensibilidad los sentimientos y reacciones en la vida de las protagonistas. La relación madre e hija, asfixiada y tensa, sedienta de reproches del pasado es el centro del filme. El despliegue de palabras del guión es delicado y perfeccionista, con una soberbia honestidad a la hora de expresar qué ocurre en el interior del ser humano.

  

[![](http://3.bp.blogspot.com/-rfhqiQNddX0/URaSvlhCy-I/AAAAAAAAByw/MPFFPGGxZjk/s1600/sonata-de-otono-rostard.jpg)](http://3.bp.blogspot.com/-rfhqiQNddX0/URaSvlhCy-I/AAAAAAAAByw/MPFFPGGxZjk/s1600/sonata-de-otono-rostard.jpg)Sin un gran despliegue de calidez visual, pero sin resultar fría y muerta, la historia resulta progresivamente intrigante; los personajes parecen muy amables y, conforme empiezan a sincerarse ante el espectador, uno redescubre al ser humano, con sus sentimientos (sin resultar empalagosos), con sus miedos (sin abusar de ellos) y situaciones propias de vidas corrientes, duras e increíbles pero al mismo tiempo reales, que se muestran con una especial naturalidad, tratadas con notable maestría. No son confesiones artificiales, algo que resulta complicado de obtener. Aunque no me llamó la atención el trabajo de la actriz -sin ningún parentesco con el director- en la vieja '_**Casablanca**_' (1940, Michael Curtiz), me ha parecido muy poderosa su presencia en pantalla, en parte justificada por su papel de diva y pianista mundial y en parte por esa grandeza frente a la cámara que solo las actrices más excepcionales poseen. **Liv Ullmann** también resulta muy a la altura de esta sencilla pero detallada película. Es una obra que exige algo del público, sobretodo una compresión muy interesante que enriquece. Todavía no he estudiado al fascinante Bergman, pero estamos en ello, es el cómo y no el qué.

  

"_Cuando no puedo dormir por la noche, me pregunto si he vivido de verdad. También me pregunto si será lo mismo para todo el mundo o si hay personas que tienen más talento que otras para vivir, o es que hay personas que no viven nunca y solo existen._"