---
title: 'Napalitano'
date: 2017-12-20T16:34:00.000+01:00
draft: false
tags : [poemas, vocales, poema, nepal, nápoles]
---

Realmente esto de Nepal está en el tintero. Pero espera, antes o copio el poema Napalitano y luego os digo:  
  

> Ella allana el pan:  
> le lee, llena, una nana.  
> Él pelea la paella,  
> apela a la penal enea.  
> Ella, él, planean Nepal en papel.

  
Y es que nos hemos dado cuenta de que Nepal y Nápoles tienen muchas letras en común: a e l p n. Letras con las que se pueden hacer palabras:  
  

> el apenan leal apenan alelan pe al allanan nepa len pelee palee ana elena a alea apeen llapa ella ala leen enea e le enea leen apelan ene elle apalean pelean lee alelan allanen nena nea panel llana llanea enea nepa papal plepa plena nene ene apaleen allanen lea planean aplana pe anea ana ana le plan apea apeen nea lee en le llena apela apea en apaleen ana pe lea len enea ene planean peal en pepena allana pepa lela apena paleen ene nepa pela el ene plena allanen a ella apela ene ene pella ene apenan lana lapa apalean ele pe al nea allane lea apena panela palla peal pana ella peal ana lee anal el pelea planean pe lean penal plan pelan el el pan le apena apaleen llenen al a lee alee allanan len enana pelen ala peal apalea planean palee anal elena lene aleen pepa anea e papel nana len enea apela alele allanan en en apea la leal apenan la lean len nea pe apaleen pan planea apee len llena len pea palean penal allana pepa al apela lea el allanan nana el pene nea planean allanan pana e palea allanen nena pea palee apelen pea plan nea allanan pan alean la ana en nepal paleen apelan pele apee lapa paella nea panel