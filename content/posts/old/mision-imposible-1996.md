---
title: 'Misión imposible (1996)'
date: 2012-01-29T10:31:00.000+01:00
draft: false
tags : [Emmanuelle Béart, Jean Reno, Lalo Schifrin, 1990, Brian De Palma, Cine, Tom Cruise, Bruce Geller, Vanessa Redgrave, Danny Elfman, John Voight]
---

[![](http://1.bp.blogspot.com/-IElNVYEDzho/TyURJCe_j8I/AAAAAAAABK8/ZP0pRX__C8Y/s400/rostards.jpg)](http://1.bp.blogspot.com/-IElNVYEDzho/TyURJCe_j8I/AAAAAAAABK8/ZP0pRX__C8Y/s1600/rostards.jpg)Hollywood  y América en estado puro. Creo que el argumento no importa tanto como la experiencia visual:  emociones fuertes, pero en el cine. El "pan y circo" es imprescindible en el el séptimo arte, además de un buen negocio. Películas de este género habrá siempre pero solo unas pocas hicieron historia, '**Misión Imposible**' es una de ellas.

  

La clave es que estas superproducciones tengan algo propio, un sello de la casa. Es este caso podríamos nombrar el tema de la banda sonora de **Lalo Schifrin**, una de las canciones más imitadas y parodiadas de la década para espías y agentes secretos. Por no hablar de la escena en la que **Tom Cruise** queda pendiente de un hilo y permanece horizontal frente a los sensores de seguridad. El film de **Brian De Palma** autor también de '**El precio del poder**', '**Atrapado por su pasado**' o '**Los intocables de Eliot Ness**', impresionó al mundo basándose en la serie de **Bruce Geller**, un trabajo calificable con adjetivos como: espectacular o "potens".  
  

El optimismo de la **Paramount** es increíble, nada es insípido todo es a lo grande y me encanta, ¿quién no disfrutó con una ejemplar así? **Danny Elfman** puso el resto de la música a **Tom Cruise**, **Jean Reno**, **John Voight**, **Emmanuelle Béart**, **Vanessa Redgrave**... prefiero que hagan ellos el trabajo "sucio", a que lo intenten unos desconocidos.