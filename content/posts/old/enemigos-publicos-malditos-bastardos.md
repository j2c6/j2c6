---
title: 'Enemigos Públicos, Malditos Bastardos, Asalto al tren Pelham 123'
date: 2009-10-20T19:37:00.000+02:00
draft: false
tags : [Denzel Washington, Buenas, Enemigos Públicos, Tarantino, Mafia, Johnny Depp, Asalto al tren Pelham 123, Malditos Bastardos, Cine, Entretenidas, John Travolta, Brad Pitt]
---

En los últimos meses han salido a la gran pantalla, grandes superproducciones que pintaban ser diferentes e innovadoras… algo nuevo en resumen. En concreto voy a nombrar a tres, que he visto. Son el prototipo de películas que dices. Esta tenemos que verla.

  

**[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/St314tbQaWI/AAAAAAAAAj8/G_iu0ZjGFRQ/s320/enemigos_publicos.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/St314tbQaWI/AAAAAAAAAj8/G_iu0ZjGFRQ/s1600-h/enemigos_publicos.jpg)****Enemigos públicos.**  

Todo el mundo esperaba el peliculón, “incluso mejor que el Padrino” oí decir. Exactamente, tanta publicidad con **Johnny Depp** a la cabeza, no podía fallar a nadie, pensó **Michael Mann** director de la película, y lo ideo muy bien, pero no triunfo. Quiso colocar en su último filme los elementos más característicos de una película de gangsters: dinero, disparos, chica, tensión… y no consiguió nada. Una película técnicamente mala, en varias ocasiones intento conmovernos y no consiguió nada, como siempre digo no es nada de lo que debamos acordarnos. Un argumento mediocre, sin consistencia entre los personajes, con las típicas frases, sin resultado favorable, originalidad cero. Creían que el público se iba a conformar con los de siempre y mal hecho, para llenarse los bolsillos de pasta, y no fue bien.

  

  

  

**Malditos Bastardos. **

**[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/St32B6181_I/AAAAAAAAAkE/5uOYN2wC0v8/s320/malditos-bastardos-poster.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/St32B6181_I/AAAAAAAAAkE/5uOYN2wC0v8/s1600-h/malditos-bastardos-poster.jpg)

  


**

La última película de **Quentin Tarantino: Brad Pitt**, Nazis, y buenas críticas americanas. La verdad es que a mí me encanto, no he visto todos los largometrajes de Tarantino pero: Espectacular: buenos diálogos, efectos espectaculares, buenos actores, gran guion, momentos de todo tipo: tensión, reír, llorar, esta película lo tiene todo. La situó en un marco histórico de la segunda guerra mundial, aunque no le importo cambiar el curso de la historia… Impresionante, es de las mejores películas que he visto este año, si os gustan los peliculones: “**Malditos Bastardos**”.

  

  

  

**[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/St32SkwQ3CI/AAAAAAAAAkM/qFdNNxJNyZQ/s320/asalto-al-tren-pelham.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/St32SkwQ3CI/AAAAAAAAAkM/qFdNNxJNyZQ/s1600-h/asalto-al-tren-pelham.jpg)****Asalto al Tren Pelham 123.**  

Estaba esperando desde hacía tiempo una película de **Denzel Washington**. No obtuvo muy buenas críticas, es mas paso desapercibida, pero me gustó. Especialmente los dialogo **John Travolta**, Denzel. John hace un papelón, en cambio Denzel no destaca tanto, pero el argumento recae sobre él. Quizás es una película de acción mas pero es muy buena, entretenida. Con escenas divertidas, y de movimiento. La película no tiene más, pero lo pasaras genial.