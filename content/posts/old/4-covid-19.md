---
title: '#4 COVID-19'
date: 2020-03-14T12:52:00.000+01:00
draft: false
tags : [pandemia, coronavirus, diario coronavirus, covid-19]
---

6,000 contagiados, 190 muertos y 500 altas médicas. Oficialmente entramos en estado de alarma. El Presidente sale en directo a comunicar las medidas. Mayoritariamente se aparenta la cuarentena que se va a guardar en los próximos 15 días. Instagram se llena de gente proactiva que enseña lo a gusto que están en casa, lo ideal que es todo haciendo yoga, desayunos o malabares con rollos de papel higiénico y otras aficiones fotogénicas y bien consideradas. Una buena parte del día es alimentarnos de los últimos memes, audios y vídeos informativos de gente yendo al supermercado, himnos que suenan en el barrio a todo volumen, bingos entre edificios con todas las ventanas activas. Se hace un aplauso de apoyo al personal sanitario a las nueve de la noche para el jolgorio de todos. 

  

Mientras tanto en Reino Unido, Boris Johnson, opta por una medida a la contra de toda la escasa experiencia en la gestión del coronavirus: dejar que la población se contagie para alcanzar una supuesta inmunidad, arguendo que no tienen medios sanitarios para hacerle frente y, supuestamente, salvando su economía, ya que afirman que la medida del parón sería necesario aplicarla durante tres meses para su efectividad con el consecuente desastre financiero. Si bien es cierto, que las consecuencias de las medidas de contención del contagio en la UE, son mayoritarias, todos, como miembros de esa unión, tienen el apoyo mutuo, que para la crisis futura se encontrarían juntos. Reino Unido, con el reciente Brexit, no tiene esa posibilidad y una vez más, dan un paso de distancia con el resto del mundo. Tenemos todo el tiempo por delante para verlo. 

  

Ayer bajamos a comprar, vino y encurtidos de celebración (qué importante es conservar el espíritu festivo) y encontramos el supermercado abastecido con normalidad. Cancelamos el viaje de Mayo a Berlín. Y aprovechamos para ponernos al día con familia y amigos respectivamente recluidos en sus casas. Pese al buen humor de las redes sociales que nos permiten estar constantemente en estado de shock y FOMO, es preocupante el resultado de las consecuencias que van a tener estas medidas en nuestro estilo de vida, más allá de la pandemia. Por supuesto, como hablaba con C. ayer, mientras leía tumbada en la ducha sobre su alfombra de yoga, lo más importante es la vida, y después del precio a pagar por ella, entonces y solo entonces nos preocuparemos de las consecuencias que va a tener. 

  

No puedo decir que no esté interesante esta incertidumbre. ¿Conseguirán una vacuna efectiva a tiempo? ¿Cuánto durará el estado de alarma? ¿Qué medidas contrarestarán o paliarán el estado de la economía para no sumirnos en la hecatombe económica? Todos somos novatos ante esto, y va a cambiar el curso de la vida moderna y el modelo de capitalismo que seguimos en la actualidad. Por primera vez algo pone en jaque, como comentábamos ayer, a Occidente. Esto es como una película, pero es la vida real.  
  

[![](https://1.bp.blogspot.com/-qqq8dhRtA4g/Xm5aflzkWAI/AAAAAAAAfCg/7_DfnxkANwMtBJzleTWmsabRfMZJDhbEQCNcBGAsYHQ/s320/_R072837.jpg)](https://1.bp.blogspot.com/-qqq8dhRtA4g/Xm5aflzkWAI/AAAAAAAAfCg/7_DfnxkANwMtBJzleTWmsabRfMZJDhbEQCNcBGAsYHQ/s1600/_R072837.jpg)