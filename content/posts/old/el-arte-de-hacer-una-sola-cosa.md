---
title: 'El arte de hacer una sola cosa'
date: 2012-11-19T17:08:00.000+01:00
draft: false
tags : [atención, Arte, Sorolla, Música, Uncategorized, concentración, el arte de no hacer nada]
---

[![](http://4.bp.blogspot.com/-sCl0apiaRrQ/UKpXXK8EM7I/AAAAAAAABkw/h-xG6fgWyNo/s400/rostard-sorolla.jpg)](http://4.bp.blogspot.com/-sCl0apiaRrQ/UKpXXK8EM7I/AAAAAAAABkw/h-xG6fgWyNo/s1600/rostard-sorolla.jpg)

Dejando a un lado la obligatoriedad de que el cuerpo por sí solo y despreocupadamente sea capaz de hacer varias funciones al mismo tiempo, pocas cosas tienen la suerte de concentrar toda  nuestra atención. Como si no existieran actividades que lo merezcan, como si no fueran tan importantes como para dejar los demás asuntos y empeñarnos de lleno en este. Realmente de forma inconsciente pensamos que ahorramos tiempo, que tampoco es necesario dejar el resto a un lado. Es una desgracia: mutilamos la riqueza de ciertas acciones, y no nos hacen todo el bien -o mal- que deberían, no son efectivas (en un mundo donde la eficacia y la efectividad son casi virtudes).  Pero parece que no perdemos nada, que avanzamos y en el fondo despreciamos placeres sublimes que degradamos a mero entretenimiento, cuando deberían ser ardientes pasiones. Uno de los principales asesinos es el cerebro.

  

El cerebro tiene un poderío asombroso. Con él, sencillamente, puedes estar en cualquier parte, pero esto tiene sentido cuando estás encerrado en el ascensor, o evidentemente cuando lo estás utilizando, en otras ocasiones estorba. Estorba porque se pierde la capacidad de contemplar, de admirar, una de las actividades más placenteras en peligro de extinción. En muchas ocasiones el cerebro impide el ingenuo 'dejarse llevar', viendo una película, leyendo un libro, o simplemente observando una escena. La mente llena de preocupaciones, prejuicios e inmediateces absurdas, siempre analizando no deja disfrutar al estresado espectador. No digo que nos convirtamos en animales irracionales, sino que por un momento dejemos que las cosas ocurran como si se tratara de la primera vez, como sino supiéramos qué va a pasar, como si no hubiera otra cosa en el mundo, sin juzgar, sin predecir, deleitándonos con cada detalle ahora novedoso para nuestro cerebro apagado.

  

Quizá este primer enemigo de la concentración sea más difícil de gobernar, tal vez indomable, pero eso se lo perdono querido lector, porque sí hay algo evitable y que sí es imperdonable. Cuando se trata de arte, hacer dos cosas a la vez es pecado. ¿Cuánto tiempo hace que no escucha música de verdad, sin hacer otra cosa, sin ir a ningún sitio, sin leer...? La música ha sido una de las grandes damnificadas de entre las artes: ya nadie tiene a la música por un plato fuerte, sino solo como acompañamiento, como guarnición. Entiendo que hay música que no merece toda nuestra atención, y también que muchas actividades se convierten en excelentes, acompañadas de música, pero ahora estamos en el punto de despreciarla. Hay que dejarse llevar, cerrar los ojos y notar cada vibración, cada nota, y sentir todo el poder y significado. Así no se aborrece la música. Y además la esclavizamos abusando tantas veces de la misma pieza, como de una bailarina a la que hacemos bailar sin descanso hasta la muerte y a la que no le dedicamos toda la atención que merece.

  

Pero no me refiero únicamente a actividades tan sofisticadas y refinadas como el arte de escuchar música. Podríamos ir a un plano más insignificante y también despreciado como el arte de no hacer nada, de simplemente dejar nuestros sentidos encendidos y apreciar un instante de la existencia con todo su valor y expresión. O hablar con alguien sin que la televisión me robe un ápice de atención. O de ver cine y no tocar el teléfono, o parar cien veces la película por múltiples motivos. ¿Cuántas personas notan que concentramos toda nuestra atención en ellas como si no hubiera otra cosa en el universo? Hay mil cosas, que infravaloramos, la magia está ahí y no la vemos porque tenemos 'tantos' quehaceres, que no disfrutamos de uno solo. Vamos con una extraña prisa que no nos permite contemplar o asombrarnos, y en realidad no es tan fructífera la vida que llevamos, ni tan importantes esos menesteres. Estamos ante algo grande y no lo reconocemos porque vibra el móvil en el bolsillo, hago otras cosas al mismo tiempo o sencillamente he perdido la capacidad de dedicar mi tiempo con toda su plenitud.

  

  

No sé como explicar este asunto pero cuando me pregunto porqué hago las cosas, me doy cuenta de que muchas no merecen mi atención, y tantas otras más preciosas se ven privadas de ella. Solo unos pocos privilegiados disfrutan del arte de hacer una sola cosa.