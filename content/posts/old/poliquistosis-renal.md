---
title: 'Poliquistosis renal'
date: 2020-01-30T13:19:00.000+01:00
draft: false
tags : [velada literaria, ficcion, relato]
---

  

La primera vez que oriné sangre echaban el último capítulo. En el trabajo no se hablaba de otra cosa. Martín, que todavía no sé por qué es nuestro jefe, dijo que la serie era una Obra Maestra. Se llenaba la boca con esas dos palabras: _Obra Maestra_. Su sobrino se pasó por la oficina con dos bolsas enormes de palomitas y la cara llena de granos. Las trajo del cine en el que trabajaba.

  

Me traía de cabeza la planificación del tercer trimestre. Me levantaba y pensaba en ello hasta acostarme. En casa no me entendían y empecé a no enterarme de quiénes eran los nuevos vecinos o dónde había comprado mi madre el ambientador de la sala de estar. Fumaba sin parar y quería que llegara el verano para no tener frío cuando fumaba fuera, para notar el calor del cigarro por encima de mi propio calor. A veces alguien se paraba y me preguntaba _¿estás bien?_ y yo, que me sentía raro e incómodo, respondía: _estoy bien_.

  

La de Administración estaba de baja y por eso íbamos a tomar la mediamañana a su despacho. Nos sentíamos seguros si cerrábamos la puerta, creíamos que olía a relaciones extramatrimoniales. En la pared de detrás de la mesa había un cuadro abstracto y absurdamente rojo que a veces era motivo de discusiones sobre su valor y significado. Nuestra risa se oía por todos los despachos del pasillo. Dejé de reírme cuando me di cuenta de que toda esa sangre de la pintura, podría ser mía.