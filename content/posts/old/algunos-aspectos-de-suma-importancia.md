---
title: 'Algunos aspectos de suma importancia desde donde veo el Sabrett '
date: 2019-09-03T09:37:00.000+02:00
draft: false
tags : [propuesta, ficcion, textos]
---

He conseguido vender el diccionario por Wallapop. Diccionario María Moliner en tres tomos. Sin usar. Bueno, lo usé en primaria, creo, para esas actividades de lengua de hacer listas de palabras con definiciones y poco más. Hay algunas que ni fú ni fa. Palabras, quiero decir. No me gustan fórceps, subrayador y espalda. Me atraen heliotropo, tulipa y filtrum. Me gusta la pelusilla que se queda atrapada en el suelo del carrito e inundarme un poco con el vapor aceitoso de la freidora, pero no sé si hay una palabra para eso.

  

He pensado que en el fondo no quiero ser abogada, no sé en qué estaba pensando, aquí, pagando facturas con salchichas llenas de ‘E’s, y todavía no sé qué significan los numeritos que las acompañan. Aquí vienen muchos trajeados a por sus hot-dog, con aires de estoy-pidiendo-un-hot-dog-vestido-con-un-traje-de-mil-dólares-pero-mis-zapatos-valen-más-que-eso. Todos, estoy segura, han visto Suits, pero odian con toda su alma los perritos calientes, pero aquí están, defendiendo a Oscar Mayer, siguiendo las tendencias, con tanta personalidad como una hoja de hierba en el césped. Eso me ha quedado bien.

  

Y encima me dice mi madre que a dónde voy con ese vestido. Y a ti qué te importa. El sábado pasado quedé con un chico. Salió de esas presentaciones estúpidas de Sophia, y luego el chico te sigue en Instagram y escribe por WhatsApp. Era un ejemplar babeante y yo me sentía como el babero de Bruce, el niño ese que se come la tarta de chocolate en Matilda. ¡Vamos Bruce! Pero sabes, me sentía muy bien, echaba de menos eso. Con el anterior me sentía como el primer smartphone que le compramos al abuelo. Pero vamos, no pasó nada, no me interesa profundizar.

  

Se ha perdido un gato en el barrio. Debe de estar mejor alimentado que yo por la foto que hay pegada a la farola justo enfrente del puesto. Missing Golden. La última vez que lo vieron, el martes pasado. En este banco a la sombra está bien y me gusta cómo se ve el puesto desde aquí. Me gusta ver pasar a la gente y pensar en sus vidas. Si tendrán gato, si conocerán a chicos babeantes y cuáles serán sus palabras favoritas y las que no les gustan. Hay un montón trozos de servilleta o servilletas completas por el suelo, y eso que tenemos dos bidones para echar la basura. Uff, creo que voy a volver, llevo mucho rato aquí sentada.