---
title: 'What it takes - Aerosmith'
date: 2009-05-24T08:04:00.000+02:00
draft: false
tags : [Rock, Gun N' Roses, Aerosmith, Musica, Bon Jovi, Steven Tyler]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/Shjv-GxjZNI/AAAAAAAAAcQ/4_RDLhicrqw/s320/800px-Aerosmith_B.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/Shjv-GxjZNI/AAAAAAAAAcQ/4_RDLhicrqw/s1600-h/800px-Aerosmith_B.jpg)  

Fue en 1973 cuando los primeros oídos de los amantes del rock, escucharon a un grupillo de Boston, tocar en un local musical, no había mucha gente, pero era suficiente, eran **Aerosmith**. Este grupo se formo tras un coctel, de otras bandas: "The Strangers", o "Jam Band". Pero a quien le importa, han dejado una huella en la historia del rock, similar a la de un _Tiranosaurius Rex_, estamos hablando de los grandes . Conocidos ya en su primer LP con aclamaciones del publico por "Dream on", la banda de Boston fue sacando éxitos que llenaban los corazones de los fans, aquello era arte: "Nos encanta subirnos al escenario y tocar" afirma Joey Kramer, baterista del grupo. Tuvieron mucha influencia, de ellos salieron grupos como "**Bon Jovi**", "**Gun N' Roses**"y "**Motley CrÜe**", los escenarios de rock se plagaron de grupos similares, que con sus voces y gritos, conquistaron el mundo. Solo dos álbumes: "_**Permanet Vacation**_", y "_**Pump**_". De este ultimo es de donde sale este temazo. Lo tiene todo guitarras ecoicas y ondeantes con riffs, y punteos inolvidables; el bajo siempre presente ¿que serian sin él?; batería: redobles, y ritmos limpios , piano para completar la orquesta que nos dé unas notas de marfil, y un vozarrón, que resuena en todo el escenario. ¿Quien no se impresiona ante un espectáculo, así?. Ahí la tenéis:

  
.