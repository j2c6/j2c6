---
title: 'Mientras leo los cuentos de Amy Hempel, rondan en mi cabeza'
date: 2019-12-04T13:35:00.000+01:00
draft: false
tags : [palabras clave]
---

Ray Carver, Paterson, Miguel D'Ors, William Carlos Williams, poesía cotidiana

Carver, Amy Hempel, minimalismo, realismo, Cheever, Gordon Lish

Jorge de Cascantes, Foster Wallace, Vollmann,

Noah Baumbach y Marriage Stories

Tomate rosa, tomate del mar azul

Fotografía errónea, Saul Leiter, Sergey Neamoscu, ruído

Collages, poemas sencillos, elementos handmade

Sobriedad, silencio, ayuno, humanidad, personas, calma, reflexión, meditación, anonimidad, vida interior, placeres sencillos, cocina lenta.

Complejidad de los traumas personales.