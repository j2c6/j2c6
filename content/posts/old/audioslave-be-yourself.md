---
title: 'Audioslave – Be Yourself'
date: 2009-01-18T20:03:00.000+01:00
draft: false
tags : [Indie, Rock, Musica, Audioslave]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SXN9c2xeZEI/AAAAAAAAAXs/pTwXbsoaziM/s320/theguitarsa5.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/SXN9c2xeZEI/AAAAAAAAAXs/pTwXbsoaziM/s1600-h/theguitarsa5.jpg) **Audioslave** tiene buenas canciones, no tienen mucha dificultad, pero tienen alas, potencia, creo que la palabra es: poder, y es que aunque no sepamos ingles (para eso tenemos las traducciones), tienen letras con gracia, y les sale espectacular. En “Be Yourself”, se mezcla todo, empieza tranquila, pero potente, la guitarra, el bajo… la batería limpia…la voz es clara, y va ascendiendo una montaña, hasta el estribillo, ahí lo comprendes todo, comprendes que “ser tu mismo es lo único que puedes hacer”, luego si quieres juegas con la distorsión, y haces una canción espectacular como **“Be Yourself”**.Esta es de las mías. Ahí tenéis la letra:  
  
  
_“Alguien se hace pedazos, Durmiendo solo, Alguien mata el dolor, Girando en silencio, Para finalmente distanciarse, Alguien se emociona, En el jardín de una capilla, Y coge el ramo, Otro deja una docena, De rosas blancas en una tumba._  
_Ser tú mismo es lo único que puedes hacer…_  
  
_Alguien encuentra la salvación en todos, Y otro solo encuentra dolor, Alguien intenta ocultarse, Dentro de sí mismo reza, Alguien jura su amor verdadero, Hasta el final de los tiempos, Otro huye, ¿Separados o unidos?, ¿Cuerdos o locos?_  
  
_Ser tu mismo es lo único que puedes hacer…_  
  
_Aun cuando hayas pagado lo suficiente, Hayas sido destrozado o retenido, Con cada recuerdo, De la buena o mala cara de la suerte, No pierdas el sueño por ello, Estoy seguro de que todo acabará bien, Puedes ganar o perder.Pero ser tu mismo es lo único que puedes hacer. Ser tu mismo es todo lo que puedes hacer…”_