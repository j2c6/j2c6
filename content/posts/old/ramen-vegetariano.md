---
title: 'Ramen Vegetariano'
date: 2020-05-29T09:40:00.001+02:00
draft: false
tags : [ramen, receta, fideos, caldo, china, oriental, shiitake]
---

  
**Los Ingredientes**  

*   Dos litros de caldo de verduras.
*   Shiitakes secos.
*   Salsa de soja.
*   Cebolla.
*   Dos dientes de ajo.
*   Un centrímetro de jengibre picado.
*   Una cucharada de margarina.
*   Miso.
*   Mirin o vino blanco.
*   Espinacas baby.
*   Huevos.
*   Setas ostra o shiitakes frescos.
*   Tofu.
*   Bambú.
*   Cebolleta china.

**  
El Caldo**  

1.  Hierve caldo de verduras con shiitakes secos, apagar el fuego y dejarlo reposar menos de veinticuatro horas. Luego trituras los shiitakes con algo de caldo y salsa de soja y añadir a la olla.
2.  Salteado de cebollas, ajo y jengibre con aceite y añadir al caldo.
3.  Antes de servir, batir margarina, miso y mirin (o vino blanco).

  

[![](https://1.bp.blogspot.com/-OwrOeBrGQfw/XtC8C-SQxhI/AAAAAAAAfbo/DcdW4gw3s00jwKjoAY6dI0pBqC4SdoRIwCK4BGAsYHg/w300-h400/photo_2020-05-29_09-38-17.jpg)](https://1.bp.blogspot.com/-OwrOeBrGQfw/XtC8C-SQxhI/AAAAAAAAfbo/DcdW4gw3s00jwKjoAY6dI0pBqC4SdoRIwCK4BGAsYHg/photo_2020-05-29_09-38-17.jpg)

  

**Toppings**  

*   Hervir/saltear espinacas.
*   Hervir/saltear bambú.
*   Saltear setas y shiitake.
*   Tofu a la plancha.  
    
*   Hervir huevos de 5,5 minutos para que queden líquidos por dentro.
*   Hervir fideos ramen.
*   Rodajas finas de cebolletas chinas.

  

  

Esta receta está basada en la de [Umamigirl](https://umamigirl.com/easy-vegetarian-ramen/)