---
title: 'Timecode'
date: 2020-09-12T12:11:00.002+02:00
draft: false
tags : [cortometraje]
---

he visto timecode de juanjo giménez peña, es un cortometraje de 16 minutos. una mujer que trabaja en la vigilancia de un parking y un juego con otro compañero en el que bailan frente a las cámaras. así en un lugar prosáico como un párking, y en un puesto tan prosáico como uno de vigilancia, donde sale la expresión corporal.  me gusta ese poder de ver todo lo que ha pasado en el parking. el sonido del teclado cuando ponen el timecode. la impasividad de la protagonista cuando no está bailando. el saludo ordinario del cambio de turno pese a la magia de su juego. como que no se pueden expresar en la vida real y tienen que hacerlo en ese formato. las notas adhesivas con el corte temporal, son notas de amor.

juanjo giménez. 2016. 15 min. españa.