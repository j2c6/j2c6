---
title: 'Notas, ''El arte y la ciencia de no hacer nada'''
date: 2017-05-27T13:33:00.001+02:00
draft: false
tags : [libro, contemplar, neurociencia, andrew j. smart, educación, modernidad, tiempo, conciencia]
---

Me ha gustado mucho el libro de Andrew J. Smart, tiene ideas que me llevo a la lavadora. Sí que es verdad que parece que entorno a las últimas páginas, se motiva y empieza a hacer su tesis política sin solucionar gran cosa y para mí, me quedo colgado y con ganas de saber más ideas para activar mi Red Neuronal por Defecto (RND).

  

\-Se ha descubierto que el cerebro goza de gran actividad cuando carece de estímulos externos. Se refuerzan conceptos y se establece relación entre elementos que antes no la tenían. Herramienta importante de creatividad. Todo esto es gracias a la RND. \[De ahí parte de verdad tal vez en las religiones o la meditación, tiene sentido\]  
  
\-Muy interesante el funcionamiento de las redes no lineales, el parecido de la red neuronal con una colonia de hormigas y cómo sistemas sencillos con elementos que obedecen a ciertas reglas básicas son capaces de crear una mente compleja y colectiva. ¿Seremos así los humanos a MACROescala?

\_\_\_\_\_\_\_\_\_\_\_\_

  

[![](https://2.bp.blogspot.com/-UPHZ9uM-IjU/WSli3U3r0jI/AAAAAAAAOu4/Fg00cNcFhbcUJSodzFB2sc4p3IMi5AP7wCLcB/s320/9788494207310.jpg)](https://2.bp.blogspot.com/-UPHZ9uM-IjU/WSli3U3r0jI/AAAAAAAAOu4/Fg00cNcFhbcUJSodzFB2sc4p3IMi5AP7wCLcB/s1600/9788494207310.jpg)

  

'_**Slacker**_' de Richard Linklater. \[He encontrado una copia de The Criterion Collection\]

'_**The Art of Doing Nothing**_' de Veronique Vienne

Saciedad semántica: cuando dices muchas veces una palabra y deja de tener sentido.  

_Karoshi_, palabra japonesa de muerte por exceso de trabajo.

Algo del neurólogo Marcus Raichle.

¿Teoría de Grafos?

A ver a dónde nos lleva el precúneo.

Metacognición. Ese vértigo al mirarte al espejo y empezar a asombrarte de que ese eres tú.

La hormiga de Lenton.

[Waldsterbergen](https://es.wikipedia.org/wiki/Extinci%C3%B3n_paulatina_del_bosque): muerte del bosque.

Criticidad autoorganizada,

(Wer, wenn ich schriee, hörte mich denn aus der Engel Ordnungen?) Rilke.

Mecanismo de resonancia estocástica, cierto ruido es necesario para la amplificación de la señal.

"_Lo que es malo para la administración del tiempo es bueno para el arte_" 

Efectos del ruido ambiental sobre la cognición creativa. Lawrence Ward.

Ruido Blanco.

"_El problema radica en que en muchas grandes empresas, el método pasa a ser sustituto del pensamiento_" Elon Musk.

Cuanto más creemos en la imposibilidad del trabajo, más probable es que haya. \[Lo que me recuerda cuánto hay que estudiar y comprender los errores humanos para no caer en ellos. El creerse libre de caer ahí está más indefenso frente a ello.\]

Reforzar el poder de la utopía, el cómo deberían de ser las cosas.

Christian Huygens, en el siglo xvii, se dió cuenta, mientras estaba enfermo en su cama, como dos relojes de péndulo en la misma pared, a desfase, acababan sincronizándose y eso se debía a las vibraciones que se transmitían entre ellos.