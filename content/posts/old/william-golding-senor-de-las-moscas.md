---
title: 'William Golding - ''El señor de las moscas'''
date: 2012-09-19T10:12:00.000+02:00
draft: false
tags : [Inglaterra, S.XX, Perdidos, William Golding, Literatura]
---

La novela conocida por todos quizá gracias a la película de los noventa, a pesar de obtener pocas ventas iniciales, se convirtió en una lectura imprescindible para los escolares ingleses. A mí me ha cogido distraído y reconozco que no le he prestado demasiada atención, pero en realidad tampoco la merece, pues la simbología que hay detrás de esta sencilla historia se puede encontrar bien explicada en Internet, y la literatura del autor es sencilla y muy llevadera, pero cuidado: las apariencias engañan.

  

[![](http://4.bp.blogspot.com/-yg9HmrSeb38/UFl94J77ABI/AAAAAAAABiM/veQoYRUEm-M/s400/flies.jpg)](http://4.bp.blogspot.com/-yg9HmrSeb38/UFl94J77ABI/AAAAAAAABiM/veQoYRUEm-M/s1600/flies.jpg)El señor Golding, trata en esta novela distintos temas como la inocencia, la educación, el caos o la civilización  y la naturaleza del hombre mala o buena por definición. Es una cuestión muy interesante sobre la que aún no tengo una postura firme al respecto. Además en este caso es fascinante pues se trata de una situación límite de treinta niños, inocentes que de repente se ven despojados de todo lo que les protegía y se encuentran con lo puesto en una isla desierta en medio del océano. Se centra realmente en la infancia y en la necesidad del adulto en ella, ya que se empieza por asambleas y se acaba en cazas de muerte al jefe.

  

Todo el mundo hace su propia lectura de la historia. Por un lado la educación que precede a los niños antes de la isla es determinante para su comportamiento en ella, por ejemplo, si su único modo de comportarse es movido por el miedo al castigo, en la isla no hay 'mayores'. Pero de todos modos, la teoría de **Golding** parece decir lo contrario que el niño fuera de la civilización tiene un comportamiento de bestia en ausencia del adulto y pierde la inocencia, según las personalidades de los portagonistas. No se trata de hacer un diccionario pero resume un poco las ideas: _Piggy_ (inteligencia ignorada por su aspecto físico), _Ralph_ (jefe del grupo es la civilización: las normas), _Jack_ (agresividad, fin de la responsabilidad), _Simon_ (la verdad), _Roger_ (bestialidad), etc, etc.

  

La simbología básica se encuentra en la caracola (que es la autoridad), el fuego (la esperanza de salir de la isla), la fiera (el miedo). Algunos otros han encontrado similitudes entre los personajes y el diablo, el nazismo, e incluso el parecido entre _Simon_ y Jesucristo. A mí todo esto me aturde, pienso realmente que se esconde una poderosa mente detrás de la historia que tampoco pretende demostrar nada concreto, sino estimular las cabezas de los afortunados lectores sobre el comportamiento humano.

  

Sobre el enigmático título he leído algunas cosas, **el señor de las moscas**, la cabeza del jabalí es el espejo del miedo, la muerte, la diversión y la ferocidad. Su escasa aparición lo hacen todavía más misterioso.