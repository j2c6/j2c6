---
title: 'Fannie Flagg - ''Bienvenida a este mundo, pequeña'''
date: 2012-05-04T10:10:00.000+02:00
draft: false
tags : [Fannie Flagg, XX, Literatura, Estados Unidos]
---

[![](http://1.bp.blogspot.com/-M21_wocBrcU/T6OOr_vK3SI/AAAAAAAABcw/TFt6eI9oO-4/s400/rostard.jpg)](http://1.bp.blogspot.com/-M21_wocBrcU/T6OOr_vK3SI/AAAAAAAABcw/TFt6eI9oO-4/s1600/rostard.jpg)

**Fannie Flagg** famosa por escribir la novela y guión en que se basó la película '**Tomates verdes fritos**', ha conseguido que una buena parte de sus libros lideren la lista de los más vendidos. No es que se trate de la panacea, pero sí es una novela sencilla sin grandes pretensiones para pasar el rato. La prosa no es demasiado reveladora, simplemente se centra en contarte una historia. Y desde el punto de vista de la protagonista, está descrito de forma muy veraz.  
  
  
El libro es muy dinámico y no se detiene demasiado en cosas que no te interesan como '**_el programa de la vecina Dorothy_**'. Además de seguir a Dena, la protagonista, hace un buen retrato del mundo de la información, en concreto el periodismo y la televisión, sus entresijos, trampas, mentiras y verdades, así como el poder de los medios de comunicación y la fama. Dena es una triunfadora nata y todos se rinden a sus pies, pero tiene un pasado mudo, pues no sabe qué fue de su madre, y que la tiene fuera de lugar y desequilibrada.  
  
  
Luego introduce un problema social relacionado con la cuestión racial, que sinceramente no me ha interesado demasiado, quizá Flagg trataba de darle más profundidad y proyección a la novela, pero creo que era mejor el ritmo de vida del principio. No transmite nada especial, es para pasar un rato como he dicho, con principio y final. Me da la sensación de que la protagonista cambia con mucha facilidad al final, cosa que no me acaba, y es un poco Walt Disney  versión 'y vivieron felices...' en ese sentido. En conclusión, fácil de leer, fácil de disfrutar, fácil de acabar, fácil de olvidar.  

  

"_Todo había empezado igual que cada vez que aceptaba tomar una copa con J. C. Después de los cócteles habían ido a cenar al Copenhagen, en la calle 58, donde bebieron Dios sabe cuántos vasos de aguardiente muy frío y de cerveza helada para acompañar la tabla de quesos. Como en una nebulosa, Dena recordaba haber insultado a un francés y haber ido a pie al Brasserie a tomar un café irlandés. Se acordaba muy bien de que ya había salido el sol cuando llegó a casa, pero al menos ahora estaba en su cama y sola: J.C. se había ido gracias a Dios. Entonces se dio cuenta. ¿Qué le habría dicho a J.C.? A ver si se había comprometido a casarse con él otra vez... En ese caso, tendría que inventar una forma de romper el compromiso de nuevo. Siempre lo mismo_."

  

 '_**Bienvenida a este mundo, pequeña**_', **Fannie Flagg**.

Ediciones Salamandra, febrero de 2001