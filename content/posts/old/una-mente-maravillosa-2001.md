---
title: 'Una mente maravillosa (2001)'
date: 2012-02-02T18:12:00.000+01:00
draft: false
tags : [James Horner, Jennifer Connelly, John Nash, Akiva Goldsman, Russell Crowe, Ron Howard, Sylvia Nasar, Paul Bettany, Cine, Ed Harris, 2000, Christopher Plummer]
---

[![](http://2.bp.blogspot.com/-9_3Su-vLc5g/TyrDS0PyKII/AAAAAAAABMU/jU45S3_iYSo/s400/rostards.jpg)](http://2.bp.blogspot.com/-9_3Su-vLc5g/TyrDS0PyKII/AAAAAAAABMU/jU45S3_iYSo/s1600/rostards.jpg)El brillante matemático John Nash **(Russell Crowe**) es un hombre de éxito a pesar de no ser muy sociable y poseer características poco comunes. Tiempo después de casarse con Alicia (**Jennifer Connelly**) se da cuenta de que tiene un problema pero esta vez de otro tipo: es esquizofrénico. La lucha por hacer frente a sus alucinaciones y salir adelante lo harán un personaje reconocido.

  

Una vida sorprendente la del premio **Nobel John Nash**, novelada por **Sylvia Nasar** y posteriormente transformada en guión (**Akiva Goldsman**) por el mismo que lo hizo en '**Soy Leyenda**' (2007) o '**Cinderella Man**' (2005). Un drama conmovedor sobre el afán de superación, la capacidad y esfuerzo que supone vivir con una enfermedad mental tan influyente  en el día a día como la esquizofrenia. **Russell Crowe** tiene un don especial para tocar la fibra sensible del espectador haciendo de héroe consigo mismo, con causas muy personales cargado de carácter y principios. Una visión muy real sobre el caso, halagadoramente realizada. **Ron Howard** consigue cuatro estatuillas, en uno de sus mayores triunfos.  
  

La excentricidad del genio, está rodeada por otras grandes estrellas como  **Ed Harris, Christopher Plummer, Paul Bettany** o la ganadora del **Oscar** aquel año **Jennifer Connelly**. A pesar de que esperaba más del film y en su totalidad es más que correcto: la ambientación, la banda sonora de **James Horner,** la lacrimosa emotividad final de la que he disfrutado considerablemente... diría que le falta algo que no le hace llegar a la cumbre, no sabría decir tal detalle del guión, que aleja un poco de la historia por su parsimonia o falta de intensidad en la primera mitad del largometraje. Pero no olvidemos que está repleta de interés y atención.