---
title: 'Escuela de jóvenes asesinos (1989)'
date: 2012-03-10T17:16:00.000+01:00
draft: false
tags : [Christian Slater, Bonnie and Clyde, Winona Ryder, Cine, Moby Dick, Jack Nicholson, Michael Lehmann, J.D. Salinger, 1980]
---

[![](http://2.bp.blogspot.com/-wnq4QyjP628/T1t-VHCSpII/AAAAAAAABQo/MLAE8tf1N-8/s400/rostard.jpg)](http://2.bp.blogspot.com/-wnq4QyjP628/T1t-VHCSpII/AAAAAAAABQo/MLAE8tf1N-8/s1600/rostard.jpg)

Voy a dejar de hacer las sinopsis de las películas o simplemente eso creo por ahora. La clave está en ir al cine sin saber nada, sin expectativas. Así es como se disfruta el séptimo arte, sin prejuicios. Claro que siempre encontraremos el problema de qué película escoger. Una mala elección puede significar algo más que tiempo perdido o lo que es peor: nada. Porque lo bueno enriquece, lo malo hace daño, y lo ambiguo mata.

  

Escogí '**_Escuela de jóvenes asesinos_**' por el título y porque sale **Winona Ryder**, que parece ser que tuvo una época dorada actualmente irrecuperable. Se trata de cine independiente. Y habla de una adolescencia considerablemente interesante e incluso morbosa. El guión se hace cargo de las muertes y obscenidades acontecidas en un instituto, que empiezan a ser dirigidas por un joven psicópata interpretado por **Christian Slater**, que no sé si tenía pasado pero apuesta muy fuerte por un futuro. Su interpretación no es especial, pero vale la pena disfrutar su propia versión de **Jack Nicholson**.

  

Las frases del guión que simulan ser parte de la feria adolescente americana hacen un eco  mucho más profundo. Tienen un significado trascendental, desde el punto de vista de los protagonistas que plantean sus dilemas y cuestiones a su modo. Los temas como el suicidio y la violencia "social" por llamar de alguna manera a las despóticas relaciones en función de la popularidad de los estudiantes, son tratadas con una admirable irreverencia, quizá ahí reside en encanto del filme, es una tragedia contada con un humor negro, surrealista y cruel. 

  

Tras la interminable lista de historias adolescentes y manidas ocurridas en los institutos americanos, llegó '_**Heathers**_' de **Michael Lehmann**, que pretende reírse de una manera original de estos melodramas y de todos sus tópicos con una carcajada irónica,cínica y puede que hasta ingeniosa. No es una obra maestra pero sí una caricatura atractiva y tóxica al estilo de **J.D. Salinger** en su reconocida obra maestra: '**_El guardián entre el centeno_**'.  
  

Vale la pena perder un poco el tiempo con esta obra, aunque sea para sacar el lado idealista y soñador en su vertiente loca y psiquiátrica que en fondo todo el mundo posee en pequeñas o grandes dosis, pasando por alto algunos escabrosos pasajes que se olvidan tras esta incitante  pareja del tipo _**Bonnie and Clyde**_.