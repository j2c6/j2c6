---
title: 'Bill en el espejo'
date: 2020-03-24T11:24:00.003+01:00
draft: false
tags : [Microrrelatos, ficcion, relato, escribir]
---

Bill mide uno setenta, pesa 82 kilos, sus estudios son especializados en la segunda mitad de la literatura del romanticismo. Su bio de Instagram es: ‘Nunca me salto el desayuno’. En una entrevista diría que sus hobbies son: comer pizza, ver series y quedar con sus amigos. Después de una hora sigue mirándose al espejo. Se ve feo y rudo, dos adjetivos que le encantan. Un pequeño corte en el labio, le ayuda a sentirse bien. Su madre le grita que ponga la mesa. A través de la ventana, hay un cielo azul hecho por ordenador y las nubes tienen forma de infografías de un periódico internacional. Puka, la tarántula moteada que le mira desde el terrario, muda y solemne, pone una de sus patas sobre el cristal, un gesto indudable de apoyo y comprensión. Mirando el labio a través de su reflejo, ve su sabor, la acidez y el regusto dulce de la sangre todavía por secar. Es la primera vez que se mira al espejo de verdad, no para ver su nariz o la distancia de separación de sus ojos, sino para saber quién es él en realidad. Su madre vuelve a gritar, pero le cuesta reconocer que ese chillido vaya dirigido a ese chico con el labio partido. Sin embargo: es Bill.