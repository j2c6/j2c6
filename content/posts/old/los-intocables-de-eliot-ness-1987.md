---
title: 'Los intocables de Eliot Ness (1987)'
date: 2012-03-14T12:27:00.000+01:00
draft: false
tags : [Sean Connery, Charles Martin Smith, Brian De Palma, Robert De Niro, Kevin Costner, Cine, Ennio Morricone, Andy García, David Mamet, 1980, Al Capones]
---

[![](http://2.bp.blogspot.com/-JPgCiiXxlYo/T2CAbqp8LtI/AAAAAAAABRI/BVrf2x5_fcU/s400/rostards.png)](http://2.bp.blogspot.com/-JPgCiiXxlYo/T2CAbqp8LtI/AAAAAAAABRI/BVrf2x5_fcU/s1600/rostards.png)Hay muchas películas acerca de la mafia, ejemplares de máximo renombre como '_**El Padrino**_', etc. Sin embargo ninguna es tan espectacular como '**_Los intocables de Eliot Ness_**' y con esto me refiero a su puesta en escena, banda sonora y fuegos artificiales.

  

No creo que el objetivo del filme sea una lección histórica o de moral, si no bastará informarse de la historia que ocurrió 55 años antes del rodaje de **Brian De Palma**. Es sin lugar a dudas el disfrute ajustado al buen cine. La recreación de los años 30 en **Chicago**: los locales, bares, coches, vestuario, el hotel... documentados y expuestos con total perfección. La elegancia de este largometraje es uno de los motivos de su trascendencia. Una puesta en escena extraordinaria. En este sentido vale la pena decir que ciertos objetos de **Al Capone** fueron usados para la película, así como el interés que puso **Robert De Niro** en usa la ropa interior de seda similar a la de Al, aunque no apareciera en el filme.

  

Otro ingrediente inolvidable y más que fundamental no es otro que la espectacular banda sonora de **Ennio Morricone**, gran maestre de las sinfonías del séptimo arte, que hace que cualquier momento --por si fuera poco la presencia de grandes actores y puesta en escena-- sea especial y único --y quizá desmesurado-- para el desarrollo del largometraje. Evidentemente la experiencia de Ennio M. en la música para el cine es inigualable que data desde los años sesenta y continúa haciéndolo actualmente. Además de la obra con la que se abren los créditos iniciales, debo decir que el festivo tema que identifica a Capone es soberanamente sensacional.

  

Los actores también tienen su responsabilidad en la grandiosidad del filme. Para el papel de Ness quizá -- a pesar de estar en uno de sus mejores momentos-- fue **Kevin Costner** uno de los últimos considerados para el papel tras: **Gibson, Douglas, Nicholson, Don Johnson**, etc. Sin embargo se demuestra brillante delante de la cámara. **De Niro** además de engordar y tener ensayada la pose napolitana, estuvo encantado con el papel y con él todos los espectadores. Es sin duda una de las mejores caracterizaciones junto con su extraordinario talento para la actuación, así como el reflejo de la actitud poderosa y despótica de Capone. Añadimos también al veterano **Sean Connery, Charles Martin Smith** y al joven **Andy García**.  
  

El guión (**David Mamet**) a pesar de ser especialmente agradable, no es brillante. La historia está desdibujada, y no están plenamente justificadas las causas de los acontecimientos. Hay escena antológicas como la escena del tren (homenaje a '_**El acorazado de Potemkim**_' 1925, **Eisenstein**) y algunas otras. Se luce especialmente en las remarcadas frases y escenas de Capone, con gracia e ingenio. Tal vez es este asunto el que deja ligeramente coja a la película, pero queda olvidado y totalmente camuflado tras el descomunal despliegue de las fuerzas de **Brian De Palma**. Altamente recomendable.