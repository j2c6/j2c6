---
title: 'Fumarse un puro o perder la dignidad'
date: 2013-02-07T13:14:00.000+01:00
draft: false
tags : [Groucho Marx, dignidad, Oscar Wilde, Uncategorized, fumarse un puro, Jack Nicholson]
---

[![](http://3.bp.blogspot.com/-Dvf9T3pbs18/UROahSeUZDI/AAAAAAAAByI/vG3I6vf7Jbo/s400/rotarddd.jpg)](http://3.bp.blogspot.com/-Dvf9T3pbs18/UROahSeUZDI/AAAAAAAAByI/vG3I6vf7Jbo/s1600/rotarddd.jpg)

Cuando uno se da cuenta de que lo realmente importante para él son menudencias y detalles nimios, cambian muchas cosas. Una humilde ilustración es coger el autobús: ves como a lo lejos, el autobús, el tuyo, el que te lleva a casa, se acerca a la parada en la que no estás tú. Entonces un repentino ataque de prisa te sacude por entero: el mero pensamiento de perder el autobús. Y la primera vez -y tal vez tampoco las doscientas siguientes- no te planteas las tres opciones a las que te expone esa situación: primero y la más habitual, perder la dignidad corriendo como alma que lleva el diablo a por el bus, muriendo en el esfuerzo desesperado por no retrasarse unos minutos esperando en la parada; segundo: dejar escapar el autobús y así conservar orgullosamente la dignidad; o el último y desafortunado caso de perder las dos cosas: que es cuando tu sudorosa cara, ve como, tras el esfuerzo, se marcha el autobús con tu dignidad.

  

No tiene demasiado que ver, pero tal vez cansado de esas innumerables pérdidas, te preguntes _¿porqué hago las cosas?_, y es realmente una de las preguntas más fascinantes que un ciudadano de este mundo, de nuestra especie, se puede hacer. Además, lo más relevante no es que hagas las cosas bien o mal,  o que merezcan la pena, o que dejes de hacerlas, sino que seas consciente -y por tanto aceptes- el motivo de tus acciones; es muy posible que lo que andes buscando con esos 'esfuerzos heróicos' sea algo que no merezca la pena, o que tus motivos sean nefastos o ridículos, pero ahora ya lo sabes, aunque te equivoques, que al menos no estás en paradero desconocido. Es un gran momento: ahora sabes que eres un idiota pero por eso una pequeña parte de ti ha dejado de serlo.

  

En el caso del autobús, -puede que no te lo hubieras planteado hasta ahora, querido lector- perder la dignidad por algo tan pequeño como un autobús es penoso, así como por otros minúsculos resultados que te agobian, convierten tu existencia en un reloj frenético y agotador con un tic-tac sonoro y molesto que no te deja apreciar las hermosas dimensiones que tiene la vida cuando en lugar de, presa del pánico, 'perder la dignidad', _te fumas un puro_ a su salud. Sí, _te fumas un puro_ y ves como se larga cargado de pasajeros que se lo pierden. Siempre hay gente que tiene preocupaciones más intensas, mayores responsabilidades y sufrimientos más poderosos que las tonterías que acaban con tu poesía existencial. No eres alguien importante, asúmelo.

  
Los mil millones de temas de siempre que te hartan -tampoco tienen mucho que ver-, ese nuevo obstáculo para la tranquilidad absoluta, el  impedimento del gozo... fúmate un puro y disfruta con ello: "_la vida es demasiado importante como para tomársela en serio_" (Wilde) o si te gusta más "_la vida es demasiado corta para tomársela en serio_" (Groucho Marx) los grandes nos avalan, ¿vas a ser destrozado por mosquitos? Ahoga tu ambición, calma tu ansiedad, sé inteligente: confórmate, disfruta con lo que tienes, hazte el tonto.   
  

Las prisas que asesinan a tantas personas cada año se pueden evitar. Tu ocupada vida y tu apretada agenda son una mentira. Uno tiene tiempo para lo que quiere y esto se demuestra con un sencillo análisis que no voy a realizar. Por eso no soporto a los insignificantes que se dan importancia diciendo que no tienen tiempo: tienen todo el tiempo del mundo. La gente se empeña en ahorrar mucho tiempo, en 'optimizar' las acciones, en llegar antes, en liquidar el asunto... y se equivocan. Al final llegan a una avanzada edad en la que se dan cuenta de que han pedido el tiempo intentando ahorrarlo, porque como alguien dijo: '_la vida es aquello que pasa mientras estás ocupado haciendo planes_'.  
  

Finalmente uno se da cuenta de que lo que parecía significativo no lo es, y lo que se presentaba como insignificante es lo solemnemente importante. La poesía es lo importante, la actitud, los deseos, el pensamiento. Los resultados, las estadístisticas, la conducta pragmática, el utilitarismo es lo que nos ocupa y vulgarmente mata nuestro tiempo. Dedica tiempo a lo 'verdaderamente importante' que tal vez parece más inútil, más humilde, pero en realidad sublime y que  es lo auténtico de la existencia. Sino aprovechas una oportunidad, disfruta mientras la dejas pasar y te brinda otras diferentes. Así que cuando a lo lejos vuelvas a ver el autobús, piénsalo una vez más: fumarse un puro o perder la dignidad.