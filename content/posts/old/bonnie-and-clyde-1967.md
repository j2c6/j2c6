---
title: 'Bonnie and Clyde (1967)'
date: 2012-05-15T00:35:00.000+02:00
draft: false
tags : [Charles Strouse, Warren Beatty, Arthur Penn, Gene Hackman, 1960, Bonnie y Clyde, Robert Benton, Cine, David Newman, Estelle Parsons, Faye Dunaway]
---

[![](http://1.bp.blogspot.com/-Vn-d9Uw4Xr4/T7GGv9qOcqI/AAAAAAAABds/_KEF9VtfQBs/s400/rostards.jpg)](http://1.bp.blogspot.com/-Vn-d9Uw4Xr4/T7GGv9qOcqI/AAAAAAAABds/_KEF9VtfQBs/s1600/rostards.jpg)Vamos, realmente hay pocas historias tan vendibles como la vida de **'Bonnie y Clyde'**, una pareja de atracadores de bancos que van por el mundo románticamente, mientas roban y sobreviven en su fuga. Es de todos conocida esta increíble historia, tiene el tono épico y el encanto que deberían tener los protagonistas forajidos. A mí la primera vez que la oí, me cautivó sobremanera este caso que acaba en tragedia y que ha sido retratado por canciones y referencias ejemplares. A todo el mundo le encantan los estos criminales.

  

Supongo que esta visión de **Arthur Penn**, no tiene nada que ver con lo que pasó en realidad. En primer lugar no era una pareja tan glamorosa ni tan elegante, sino más sencilla y tosca. El 'honesto' **Warren Beatty** hace una versión de un apuesto ex-presidiario con principios y todo que no creo que se corresponda con el descerebrado **Clyde**, es como un mafioso de Hollywood más. Produjo la película que le supuso un enriquecimiento importante, Beatty ya llevaba algunos años en la gran pantalla y comenzaba a hacer historia.

  

La que resalta especialmente es la 'estilosa' y atractiva **Faye Dunaway**, que resucita una versión parisina de **Bonnie,** con mucho refinamiento. Es una actriz que parecía emerger con  mucha fuerza pero no triunfó tanto como prometía en este papel, que sin duda la esculpió con  revólver en mano para la eternidad. En efecto, no creo que Bonnie se pareciera en nada a esta mujer, pero es la estrella que más brilla en esta modesta versión, y se disfruta como tal. Se observa también a un joven **Gene Hackman**, y a una ridícula **Estelle Parsons** que me arrancó alguna patética carcajada, un papel pequeño pero de mérito -quizá no tanto- y con _Oscar_.

  

Sin duda esta obra está sobrevalorada. Es cierto que fue algo novedoso y cruel en cuanto a la violencia por aquel entonces y el hecho de despenalizar la vida criminal de esta pareja, que conquistó al público. La escena del tiroteo final es histórica. Sin embargo hablamos de un guión sin fuerza y casual de **David Newman** y **Robert Benton**, que no muestra ninguna evolución de los personajes, no dota de sentido ni consistencia a cada uno de los dramáticos hechos que se producen ni justifica los acontecimientos que se quedan sin garra en el hilo argumental. No se muestra lo incomprendidos que están, ni los supuestos sentimientos y el resultado es pobre. La banda sonora es de risa (**Charles Strouse**), y hay escenas incomprensibles.

  

Está considerada por los cinéfilos como uno de los grandes clásicos, y no sé de dónde sacan tantas alabanzas para este filme mediocre. Pero no seamos tan negativos aunque parezcan una broma estas diez nominaciones y dos estatuillas, que obtuvo la idolatrada cinta. Esta película marcó un antes y un después, claro que vista con posterioridad no resulta tan provocativa, aunque sí consigue mantenerte pegado al asiento y desbordante interés hacia tan grandioso drama que inmortaliza una peculiar imagen del dúo, que se ha emulado con posterioridad  en multitud de ocasiones. La historia habla por sí sola.