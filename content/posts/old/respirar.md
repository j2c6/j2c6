---
title: 'respirar'
date: 2020-03-21T11:53:00.002+01:00
draft: false
tags : [presente, desesperanza, ser, respirar, ser humano, agobio, pánico, pensamientos]
---

ante la desesperanza, el agobio, el pánico, la incertidumbre. el presente.  
  
tengo la sensación de que los seres vivos, estamos hechos para respirar. lo que nos hace _ser_, es precisamente eso, respirar. así es cuando más nos parecemos a nosotros mismos.