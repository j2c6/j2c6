---
title: 'La Pasión de Cristo (2004)'
date: 2012-04-05T22:07:00.000+02:00
draft: false
tags : [Mel Gibson, Benedict Fitzgerald, John Debney, Caleb Deschanel, Cine, La Pasión, 2000, Jim Caviezel]
---

[![](http://1.bp.blogspot.com/-xMRcG4-zeCE/T337fc8cWBI/AAAAAAAABVo/VfFs5MPXtDg/s400/rostard.jpg)](http://1.bp.blogspot.com/-xMRcG4-zeCE/T337fc8cWBI/AAAAAAAABVo/VfFs5MPXtDg/s1600/rostard.jpg)Considerada por **Entertainment Weekly**, como la #1 en la lista de las películas más controvertidas de todos los tiempos, '_**La pasión de Cristo**_' fue todo un fenomeno cinematográfico y social. Tampoco es cuestión de relatar aquí las consecuencias de tan influyente largometraje, pero se pueden hablar de las peculiaridades iniciales que poseía y sus efectos en la taquilla y el público.

  

Para empezar es una película sobre la pasión de Cristo, que en un principio cabe pensar que atañe a un público más reducido. Por el contrario **Mel Gibson**, director y escritor (también **Benedict Fitzgerald**) de la película, a pesar de todos los detalles y símbolos del film, apuesta también por un mensaje claro y abierto, en el que escribe a fuego que un personaje histórico y revolucionario como Jesús de Nazaret entregó su vida por la humanidad, sufriendo ampliamente y respondiendo a ese 'infierno' con perdón, amor y comprensión. Sin duda un hecho así da en qué pensar.

  

Uno de los aspectos más llamativos de la obra es sin duda su cruda visión de la violencia, derramamiento de sangre, odio y sufrimientos reflejados casi en la totalidad de las escenas. Actualmente no muchos ejemplares pueden presumir de tener una buena razón para mostrar tanta crudeza que no sea ella misma su objetivo. La intencionalidad del director reside en hacer hincapié en cuánto y cómo pues eso de alguna manera mide la calidad de un personaje como es Jesucristo. Efectivamente es chocante, pero quizá necesario para acercarse un poco al sentido de la película.

  

Por otro lado visualmente es excelente. El subrayado que se le ha dado a ciertos elementos, para marcar su relevancia, así como la simbología que hay detrás, llena de riqueza el largometraje. Quizá abusando de la cámara lenta para así poder captar mejor ciertos gestos que tienen un valor especial en esta historia y mucha importancia para el cristianismo, recordados una y otra vez, leídos en los evangelios, **Mel Gibson** consigue darle un carácter muy apropiado a esta versión, se observan detalles muy cuidados y expuestos en realidad entre tanta sangre con mucha delicadeza, de modo casi poético. La fotografía corre a cargo de   **Caleb Deschanel**.

  

El idioma ha sido otra de las cuestiones interesantes. La idea de rodar la película en lenguas originales como el arameo, latín o hebreo es algo muy serio, es una apuesta fuerte por la fidelidad a la 'pasión'. Primero se pensó en no colocar los subtítulos para que la actuación de los personajes hablara por sí sola, esto demuestra la soberbia interpretación y exposición de los hechos. Luego se consideraron convenientes para apreciar mejor el filme. La recreación de la Jerusalén del siglo I es espectacular, viendo el vestuario y la ambientación da gusto comprobar la insistencia de Mel Gibson evocar las situaciones que rodean las escenas, todo especialmente documentado.

  

Los personajes -la mayoría italianos- se adaptan con realismo al lenguaje, y aportan la solemnidad que merecen los acontecimientos.  **Jim Caviezel**, escogido para el papel de Cristo, se inmola en su piel -literalmente hablando- además de sufrir algún latigazo durante el rodaje. La labor de soportar el maquillaje es casi heroica, además de que resulta mucho más que convincente su 'divina' presencia en la pantalla. **Rosalinda Celentano** hace un papel de Satanás espeluznante y bien trabajado a pesar de sus escasas apariciones. Nombrar las transformaciones que sufrió el reparto tras las cámaras, por estar en contacto con una obra tan profunda y ambiciosa.

  

Por último -aunque se podrían mencionar infinidad de detalles- me gustaría elogiar la música, imprescindible la partitura de **John Debney** que dota de significación a toda la historia, un aspecto vital para el recuerdo.   
  

A pesar del interés que tiene esta obra en particular para el cristianismo, nos encontramos ante una pieza única en la historia del cine. No es difícil comprender el mensaje, aunque sí es más complicado su aceptación, no obstante se trata de un trabajo excepcional que posee todo el interés necesario para soportar y cargar con la dureza y magnificencia del largometraje,   y esto lo avalan las portentosas cifras de taquilla. No te dejará indiferente.