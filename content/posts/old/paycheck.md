---
title: 'Paycheck'
date: 2010-08-05T16:40:00.000+02:00
draft: false
tags : [Uma Thurman, John Woo, Cine, Ben Affleckt, Paycheck, Buenas Ideas, Ciencia]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/TFrNTtizMZI/AAAAAAAAAxU/4YD6IbS18Wo/s320/Paycheck-ben-affleck-256170_1024_768.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/TFrNTtizMZI/AAAAAAAAAxU/4YD6IbS18Wo/s1600/Paycheck-ben-affleck-256170_1024_768.jpg)

2003.  Creo que tras ver esta película, me estoy planteando la posibilidad, de incluir la etiqueta de "Ciencia" en nuestro archivo. B**en Affleck** no es un actor que me haga mucha gracia, sin embargo debo reconocer su triunfo (aunque no artístico) en este largometraje junto a **Uma Thurman**, que queda en un segundo plano. Jennings (**Ben Affleck**) es un ingeniero muy cotizado en empresas de alta tecnología, en las que habitualmente después de trabajar y como lo exige el contrato de confidencialidad se le borra la memoria sobre lo desarrollado y recibe un cheque de seis cifras. En su último proyecto se le presentan ocho cifras. Al acabar el contrato se encuentra en lugar del cheque habitual, con un sobre con veinte objetos aparentemente inútiles... ¿Qué ha pasado? Ciertamente, tengo una debilidad por los largometrajes que muestran pequeños esbozos de la tecnología del futuro, especialmente si se le dan explicaciones científicas. En este ejemplar en concreto, una idea muy interesante: **John Woo** el director chino marca la diferencia conforme a sus películas anteriores. Una película de ciencia-ficción y acción muy elaborada.