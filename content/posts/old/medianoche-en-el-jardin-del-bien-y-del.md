---
title: 'Medianoche en el jardín del bien y del mal (1997)'
date: 2012-05-01T17:01:00.000+02:00
draft: false
tags : [1990, John Cusack, Clint Eastwood, John Berendt, Kevin Spacey, Cine, Jude Law, John Lee Hancock]
---

[![](http://2.bp.blogspot.com/-9YaogeEKdZI/T5_6DoXcpgI/AAAAAAAABcE/pekMixlrusM/s400/midnight-in-the-garden.jpg)](http://2.bp.blogspot.com/-9YaogeEKdZI/T5_6DoXcpgI/AAAAAAAABcE/pekMixlrusM/s1600/midnight-in-the-garden.jpg)John Kelso (**John Cusack**) es escritor y va a Savannah a cubrir un artículo sobre una fiesta organizada por Jim Williams (**Kevin Spacey**). Me parece una forma muy interesante de empezar esta fascinante historia, en la que un visitante llega a una ciudad donde las cosas no funcionan igual que en la suya. Recordando al estilo inglés de la época victoriana, se presenta la peculiar mansión del señor Williams. El personaje de Cusack es muy adecuado, es más o menos elegante y educado, las anormalidades se encuentran con él y sabe adaptarse muy bien a los distintos personajes con los que se va encontrando, no sin dejar de sorprenderse.

  

Está realizado de modo que el espectador es John Cusack, exactamente eso y va viendo qué pasa. El corte de la película es el típico de lo que yo recuerdo como de los noventa. Me gusta porque no va al grano, hay muchos elementos que no son imprescindibles y que dan a conocer esta particular población, sin tener especial repercusión sobre el relato. No creo que en la actualidad un nuevo visitante pueda relacionarse tanto con desconocidos y es algo que envidio, sin pasarse pero una relación hospitalaria y formal que tiene su valor. 

  

Savannah es presentado con cordialidad y misterio. Detrás de su fachada el público cree ver otra realidad que será revelada con posterioridad. La consensuada amabilidad parece esconder discretamente otra cosa, al principio no se puede ni sospechar. Lamento decir que por desgracia, esto ocurre pero que detrás de este misticismo no hay nada. Sí es verdad que tiene algo de diferente, como cuando un grupo de viudas hablan en tono casi macabro de los suicidios de sus difuntos esposos; también el hombre que pasea a un perro invisible o el que quiere envenenar a todos los ciudadanos, o la misteriosa Minerva. Estos elementos han sido fundamentales para captar mi atención así como la reserva de Jim Williams y la moderada curiosidad con que Cusack indaga.

  

La primera mitad del largometraje es muy elegante y prometedora. Y lo cierto es que si solo hubiera visto esa hora y cuarto que es larga y placentera hubiera acusado al filme de espléndido y notable. Pero poco a poco se va desinflando, condensándose de repente en un drama judicial y sin fuerza, mucho menos cautivador que lo anterior. Se muestra un poco las pasiones sexuales reprimidas y ocultas de algunos de los personajes, y su posible influencia en un jurado del juicio. También se hace mención sobre la verdad de lo que pasó la noche del asesinato, y augura una confesión relevante o llamativa.

  

Estabas esperando que acabara la película porque sí te interesa conocer el final, y termina pero te percatas de que la última hora y media no tiene ningún atractivo. Ya has sido introducido en un mundo peculiar y no quieres salir de él, pero eres consciente de que no es ni la mitad de apasionante que el supuesto señuelo inicial.  
  

**Clint Eastwood** se vuelca con esta novedosa estética extravagante en formas y diálogos, realmente persuasiva, pero lo que es el guión del que se resposabiliza **John Lee Hancock** queda flojo camino hacia la muerte  (basado en la novela de **John Berendt**). Respeto y admiro a Eastwood, y me ha hecho disfrutar demasiado como para perdonarle esta pérdida de consistencia. Los dos principales, Cusack y Spacey convencen y me gusta considerablemente ver cómo trabajan. Esperaba un ejemplar del tipo '_**El talento de Mr. Ripley**_' que sin duda es intrigante, un portento y también cuenta con la presencia de **Jude Law**, pero me ha defraudado. Si tú querido lector conoces algo parecido a Ripley con el corte de este filme, tus recomendaciones serán bien recibidas.