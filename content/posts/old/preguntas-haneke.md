---
title: 'Preguntas Haneke'
date: 2020-01-07T12:13:00.000+01:00
draft: false
tags : [preguntas, Cine, Michael Haneke]
---

Están Derribos Mateo y ahora Preguntas Haneke. Leo en una entrevista suya que, con sus películas,  hace preguntas cuya respuesta no sabe. Y sus pelis tienen eso flotando, incómodo e inquietante.