---
title: 'Leer con tranquilidad'
date: 2020-05-11T18:12:00.000+02:00
draft: false
tags : [jose luis amores, palido fuego]
---

En la fascinante historia de Jose Luis Amores en sus peripecias desde que se convirtió en el hombre orquesta de Pálido Fuego. Gracias a él he podido leer a Vollmann (a.k.a Bill), Evan Dara, Danielewski, y pronto, en cuanto me llegue, a Sergio de la Pava. Decía hace tiempo en una reseña de 'Contraluz' de Pynchon:  

> Para hablar de Pynchon antes hay que haberlo leído, con tranquilidad y sin el acoso absurdo de una torre de otros libros esperando a ser comentados.