---
title: 'Cinco Minutos de Gloria'
date: 2011-01-19T13:21:00.000+01:00
draft: false
tags : [Irlanda, James Nesbitt, Liam Neeson, Ruairi O'Brien, En el nombre del padre, Cine, IRA, Buenas, Oliver Hirschbiegel]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TTbWZ0-VqJI/AAAAAAAAA3s/4PJwkU6QY5k/s320/fiveminutesofheaven-f1.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TTbWZ0-VqJI/AAAAAAAAA3s/4PJwkU6QY5k/s1600/fiveminutesofheaven-f1.jpg)

En 1975 un adolescente revolucionario (**Liam Neeson**) con ganas de luchar por una causa e influir en la vorágine social, decide con otros tres amigos del UVF irlandés, asesinar a un obrero y así contribuir a su reputación.  Empieza y te sitúa con la música, los planos, el movimiento y con todos los músculos en tensión en el pellejo de un aspirante al triunfo de un asesinato. No cuenta con que a la puerta del pobre desgraciado -que caerá al mismo tiempo que los casquillos del arma-, está el hermano pequeño de la víctima (**James Nesbitt**) sobre el que pesarán las culpas y lo hundirán en una profunda depresión: su madre tras el crimen lo bautizará como el único responsable de la muerte de su hijo, por el que no hizo nada. La culpabilidad convertirá a ese niño de nueve años en un saco de nervios. Lustros más tarde un programa sensacionalista prepara la reconciliación entre el asesino y el machacado hermano de la víctima. Monólogos, recuerdos y futuros encuentros rondan por las mentes de nuestros protagonistas.

[](http://1.bp.blogspot.com/_VT_cUiG4gXs/TTbWZ0-VqJI/AAAAAAAAA3s/4PJwkU6QY5k/s1600/fiveminutesofheaven-f1.jpg)  

  

Una película trabajada que es muy capaz de introducirte en esas vidas y momentos difíciles, conteniendo la respiración aún sin motivo aparente, y no esperando lo inesperado con astutos cambios de guión. La posición psicológica de los protagonistas examinada por el espectador le lleva a plantearse qué haría él en su lugar, convirtiéndose por una hora y media en un asesino y su "víctima" podríamos decir.   
  

La cuestión sobre el **IRA**, los católicos, etc. la hemos visto en otras películas (_**En el Nombre del Padre**_, 1993) y esta lo retrata brevemente con imágenes de disturbios, y peleas. Sin embargo el corte de la época, en cuanto a la fotografía y el color... no sé pastel y frío que aparece _Cinco Minutos de Gloria_ es admirable, y queda perfectamente representado, esto se lo debemos a **Ruairi O'Brien** (Fotografía), y al director del film **Oliver Hirschbiegel** (_El Hundimiento_, 2004).