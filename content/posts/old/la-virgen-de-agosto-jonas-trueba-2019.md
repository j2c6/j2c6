---
title: 'La virgen de agosto (Jonás Trueba, 2019)'
date: 2020-04-26T18:49:00.000+02:00
draft: false
tags : [madrid, Cine Español, verano, jonas trueba]
---

Pues la sensación del verano en Madrid. Las calles tranquilas, el sol a través de las ventanas. Conversaciones. Cine. Nada de horas de Internet perdidas. Tiene la sensación nostálgica del verano. Sobria y sencilla. Podemos incluirla en la lista de películas de gente que busca. Hay historias sin terminar, contacto humano.