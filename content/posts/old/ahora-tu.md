---
title: 'Ahora tú'
date: 2019-11-20T15:45:00.000+01:00
draft: false
tags : [cas, poesía cotidiana]
---

hoy mientras llegas,  
ni riego las plantas ni  
pongo la lavadora:  
mimo la casa  
la vacío de cosas mirándola,  
quito los libros, la alfombra,  
las pelusas, el olor a comino.  
  
respiro hondo y siento la ligereza  
el como te sienta el azul marino,  
la electricidad cuando te toco  
y mis oídos vibran con tus  
palabras, las que escoge tu  
espíritu, que creo, puedo tocar.  
  
cierro los ojos en paz y  
te digo _esté donde esté_  
_ahora tú_  
tal vez no pueda mandarte esta  
información pero _ahora tú_  
cada día, cada momento  
_ahora tú_  
y en algún lugar, algo está a punto  
de suceder