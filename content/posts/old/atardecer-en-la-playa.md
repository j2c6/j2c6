---
title: 'Atardecer en la playa'
date: 2020-04-08T19:56:00.001+02:00
draft: false
tags : [ordenador, asus, diario coronavirus, confinamiento]
---

Desde el confinamiento he hecho un atardecer a partir del borde del ordenador.  
  

[![](https://1.bp.blogspot.com/-TIG9yT8pgcc/Xo4QCTbozlI/AAAAAAAAfHU/NjmCQN2r2OMCs580eHNosqQCfngbhVRewCNcBGAsYHQ/s320/photo_2020-04-08_19-54-29.jpg)](https://1.bp.blogspot.com/-TIG9yT8pgcc/Xo4QCTbozlI/AAAAAAAAfHU/NjmCQN2r2OMCs580eHNosqQCfngbhVRewCNcBGAsYHQ/s1600/photo_2020-04-08_19-54-29.jpg)

  

  

[![](https://1.bp.blogspot.com/-1xYwjHJeEyk/Xo4QCfA4epI/AAAAAAAAfHQ/p41Y0dvHmUkEeNFH-RyN8Cf-7QSHcoC_gCNcBGAsYHQ/s320/photo_2020-04-08_19-54-32.jpg)](https://1.bp.blogspot.com/-1xYwjHJeEyk/Xo4QCfA4epI/AAAAAAAAfHQ/p41Y0dvHmUkEeNFH-RyN8Cf-7QSHcoC_gCNcBGAsYHQ/s1600/photo_2020-04-08_19-54-32.jpg)