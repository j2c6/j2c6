---
title: '4D'
date: 2020-02-01T09:36:00.000+01:00
draft: false
tags : [poesía cotidiana]
---

En el sofá  
con la manta y un cigarro  
veo otra película de Yorgos Lathimos  
veo el cesto de las  
siemprevivas secas algo  
iluminadas por las persianas entre-  
cerradas.  
Mis espermatozoides se movían lejos de mí  
a los ojos de un microscopio  
te los has llevado contigo esta mañana.  
En los labios se queda  
la forma de los tuyos que  
encajan perfectamente y  
esta tarde estás de vuelta de un cumpleaños  
y nos miramos y nunca se hace de noche del todo.