---
title: 'Evan Dara respondió un e-mail'
date: 2020-06-04T15:53:00.002+02:00
draft: false
tags : [correspondencia, palido fuego, Literatura, el cuaderno perdido, Evan Dara, la cadena facil]
---

Querido Javier,  

  

Muchisima gracia por tu mensaje y por tus palabras muy amables. (Y porfa perdona mi Castellano escrito horroroso!)

  

Hay tantos libros a suggerir, pero no puedes hacer un error con la epoca milagrosa de Faulkner (1929-1938) o con las obras maestras de Garcia Marquez. Mas circa de nuestro tiempo, ambos Angels y Jesus' Son de Denis Johnson estan super bien hecho, y Richard Powers es siempre de valor. Es decir, no grandes sorpresas.  

  

De nuevo, Javier, mi agradecimiento.

  

Cuidate! Lee bien! (Los dos mas o menos la misma cosa, non?)  

  

ED  

  

\--------------------------------------------------------  

  

Query: hi i'm javier,

i'm from spain, i've read tls and i'm going for the easy chain. so i love evan's writing, i would ask, as a favour, 3 or a few recommendations of best novels he likes or considers special ones, i would apreciate that, of course, without wanting to disturb its anonimity. i hope you're safe and sound in this strange times.

  

thank you for your work.

  

best regards,

maleej

  

ps. sorry for my orthopedic english, i'm still learning it.