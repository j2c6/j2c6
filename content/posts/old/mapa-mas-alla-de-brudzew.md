---
title: 'Mapa más allá de Brudzew'
date: 2020-05-26T09:16:00.001+02:00
draft: false
tags : [ficcion, fotorelato]
---

Queda lejos el frío cerrado. Pero no lo recuerdo como frío. Me quitaba el suéter frente a la mesa camilla con el brasero y mi madre me decía, tienes que comer más, estás muy delgado. Me agobiaba la ropa en la Iglesia, repleta e invadida como un sandwich labrado por una colonia de hormigas. La nochebuena. Mis tíos preguntándome sobre el colegio. Yo quería salir de ahí. Había mapa más allá de Brudzew. Los diecisiete años arrogantes con que me marché me parecen tiernos y me emocionan casi tanto como el sonido al chocar las botellas de vidrio en mi bolsa de plástico. Nadie me habló de la moderación. Veía como había que coger todo lo que estaba a tu alcance y gritar si fuera necesario.  
  
Muchas vidas en una, y mis compañeros siempre han sido las primeras luces del alba, los pájaros y esos líquidos presos en las botellas, esperando a ser liberados y besados por mi. Todavía me sorprende cómo puedo plantarme el lunes frente a toda la clase, coquetear con la maestra de historia y sentirme presentable en las cenas de etiqueta de la asociación cultural. Cuando estoy en la calle, con las puntas de los dedos heladas, esas mismas puntas que han osado posarse sobre todo lo sucio y desigual, los pequeños accidentes en la simetría natural, esa es mi salsa. Y después de las noches oscuras esperar el canto de los grajos como suaves trompetas al comienzo del Apocalipsis. Ver a la gente pasar y repasar los encuentros más fascinantes de la semana.