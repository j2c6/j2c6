---
title: 'Valkyrie/Valkiria'
date: 2009-02-14T13:56:00.000+01:00
draft: false
tags : [Bryan Singer, S.XX, Sospechosos Habituales, Cine, Entretenidas, Tom Cruise]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/SZbAGRHOJoI/AAAAAAAAAZM/WgD67ZgH9vc/s320/valkiriacartel.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/SZbAGRHOJoI/AAAAAAAAAZM/WgD67ZgH9vc/s1600-h/valkiriacartel.jpg)  

Fuimos a ver un peliculón, pero nos equivocamos, no fue para tanto, faltaban muchas cosas, el guion fue un poco pobre, faltaban “emocionadas” más diálogos interesantes, discursos, y faltaba que te pusieras mas de parte del protagonista, porque el coronel Stauffenberg ( **Tom Cruise**), hace un papel un poco pobre en mi opinión,y no lo ensalzan tanto como deberían (yo tampoco es que tenga criterio), faltan muchas cosas, la película no se hace pesada es más bien entretenida, pero cuando acabas los 120 minutos de Valkiria te das cuenta de que no ha sido todo lo que podría… Luego faltan mas desenlace final, y los actores están un poco desaprovechados, se centran en Cruise, y el resto… No obstante la película es una superproducción de Hollywood, donde todo está muy preparado y montado, no hay razón por la que no te haga pasar un buen rato. **Bryan Singer** (Director) no ha hecho nada de lo que debamos acordarnos,no como aquello de “_**Sospechosos Habituales**_” que también es suya eso sí que es una buena película que esta recordada, entre resto. Si queréis ver el trailer: [http://www.peliculas.info/23-01-2009/peliculas/drama/trailer-valkiria](http://www.peliculas.info/23-01-2009/peliculas/drama/trailer-valkiria)