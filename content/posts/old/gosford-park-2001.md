---
title: 'Gosford Park (2001)'
date: 2012-04-29T17:33:00.000+02:00
draft: false
tags : [Emily Watson, Helen Mirren, Robert Altman, Derek Jacobi, Michael Gambon, Kristin Scott Thomas, Richard E. Grant, Stephen Fry, Maggie Smith, Kelly Macdonald, Cine, Julian Fellowes, 2000, Ryan Phillippe, Clive Owen]
---

[![](http://3.bp.blogspot.com/-O1uVcUI8qvo/T51enUemG4I/AAAAAAAABbc/Rw1BR389NYQ/s640/rostard.jpg)](http://3.bp.blogspot.com/-O1uVcUI8qvo/T51enUemG4I/AAAAAAAABbc/Rw1BR389NYQ/s1600/rostard.jpg)

Al principio la película comienza -sin anestesia- a presentarte la infinitud de personajes incluidos en este reparto: todo el desglose de sirvientes, mayordomos, nobles, viudas y matrimonios con sus respectivos criados y sus particulares historias, entrelazadas con multitud de nombres desconocidos y confusos. Hasta la primera mitad todo es exactamente así, los nobles se visten, cenan y en las cocinas los sirvientes hablan y luego sirven. 

  

No es la primera vez que me pasa, pero me da la sensación de que los largometrajes ingleses que transcurren en grandes mansiones y se sitúan en la etapa de la servidumbre, se atiborra al espectador con datos y cuestiones de la vida de los personajes y del sistema social de los caserones -por llamarlo de alguna manera- y se ha convertido en una especie de debilidad: lo muestran como si fuera una parte sofisticada  y a la vez interesantísima del guión, pero lo cierto es que desorienta y aburre.

  

En ese sentido muchos filmes son iguales y solo constituyen rumores y chismorreos de alta sociedad que confieso que estoy empezando a despreciar conscientemente. No consiguen centrar tu atención demasiado tiempo en un personaje que es lo que me suele interesar. Los temas de conversación no sé si están cargados de humor inglés incomprensible para un español o es que no tienen fuerza ni encanto.

  

Me parece totalmente desaprovechado este lujoso reparto y a pesar de las maravillas que dice la crítica sobre este descafeinado guión, creo resulta totalmente ordinario y poco sugerente. Y es que por eso escogí la película, por el portentoso reparto: **Michael Gambon, Maggie Smith, Kristin Scott Thomas, Jeremy Northam, Bob Balaban, Ryan Phillippe, Stephen Fry, Kelly Macdonald, Clive Owen, Helen Mirren, Eileen Atkins, Emily Watson, Camilla Rutherford, Tom Hollander, Alan Bates, Derek Jacobi, Richard E. Grant**... la lista es interminable y poderosa.  Pues no ha conseguido fascinarme ni llamarme la atención como es debido, aislando el asesinato dos veces pronunciado y la causa del mismo, que podría haber atraído un poco más a su vez; así como algunos comentarios de Maggie Smith, la _reina madre_, como yo la llamo.

  

No es que sea un _fan_ de primera fila de la serie '_**Downton Abbey**_', pero me he dado cuenta de que es una telenovela de guante blanco, y '_**Gosford Park**_' -titulo tabú en los diálogos de la película- otro tanto. No entiendo bien como es el humor de este **Robert Altman**, prolífico director americano que se vuelca ahora con el costumbrismo inglés. No comprendo como confían el guión a **Julian Fellowes**, creador de la serie citada y  ganador en esta ocasión al **_Oscar por el mejor guión_**, pero creo que es apatía lo que despiertan estos dramatismos sin importancia. Tendré que observar más trabajos suyos, pero este no me ha entusiasmado, y no es que este ambiente carezca de interés sino que el acabado es monótono. Os copio una de las carátulas que me ha gustado.