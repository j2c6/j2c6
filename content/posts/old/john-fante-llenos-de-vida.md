---
title: 'John Fante - Llenos de Vida'
date: 2011-06-03T20:29:00.000+02:00
draft: false
tags : [John Fante, Bueno, Literatura, Llenos de Vida, Novela]
---

[![](https://lh3.googleusercontent.com/-Q0crzdR29Kg/TXnzhyAi3LI/AAAAAAAAA60/APKA5pLhzkk/s1600/john-fante-llenos-de-vida.jpg)](https://lh3.googleusercontent.com/-Q0crzdR29Kg/TXnzhyAi3LI/AAAAAAAAA60/APKA5pLhzkk/s1600/john-fante-llenos-de-vida.jpg)

El guionista de Hollywood, después de hacer sus guiones hizo sus novelas. En primera persona John Fante relata el tiempo de embarazo de su mujer, los buenos momentos y los malos, la convivencia, los problemas... de una manera bastante sincera y con estilo. Su literatura es exactamente de película, y con esto me refiero por ejemplo a que no van de pic-nic con bolsa  de plástico al monte, sino con el mantel a cuadros rojos y blancos, la cesta y el lago de fondo tras la verde colina y los rayos de sol de un cielo con nubes claras y monumentales, pero al mismo tiempo despejado. Tiene un afán por describir lo vivido de forma atractiva, donde el lector opta por sonreír. Y están también esas situaciones que nos han pasado a todos que yacen retratadas con perfección por el autor. Si debes ponerte nervioso, nervioso pasas a la siguiente página. Totalmente humano, y lo que parece agrio y frío, se convierte en sensible y agradable: así son a veces las cosas. Muy disfrutable...  
  
  
  
  
"_Se desplomó en un sillón, con el pelo en desorden otra vez, el dedo en carne viva, ya medalla a la nobleza, con un brillo peligroso en sus agradables ojos grises, con el trapo colgando de la mano y una sonrisa de nostalgia en los labios, una expresión de añoranza que indicaba que estaba pensando en tiempos más felices, probablemente en el verano de 1940, cuando era una joven delgada, cuando no tenía que hacer tareas que le destrozaban la espalda, cuando estaba soltera y sin compromiso y subía la cuesta de Telegraph Hill con el caballete y las pinturas, y escribía sonetos de amor trágico mientras contemplaba el Golden Gate_."  
  
  

"Llenos de Vida" John Fante. Anagrama.