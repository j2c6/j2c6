---
title: 'Los esquemas del otro'
date: 2017-11-30T16:38:00.000+01:00
draft: false
tags : [ideas, submundo, don delillo, otro, esquemas]
---

>   
> 
> > Con Brian era ella misma, independientemente de lo que tal cosa pudiera significar. Sabía qué significaba. Menos inmersa en los esquemas del otro, en su inconsciente modelado de otra vida.
> > 
> > Don Delillo, _Submundo_, 1997

  

Leyendo esto me ha dado la sensación de porqué rompen muchos matrimonios o partes de ellos son infieles. Supongo que a medida que el tiempo va avanzando, ambos se van configurando en torno a un código y de alguna manera se moldean adaptándose a la figura o el papel que deben ser en ese organismo. 

  

Las personas que eran antes, o que son ahora o la evolución que no entra dentro de esos esquemas rompe con la lógica del código. No son admisibles conductas no esperables o da miedo introducirlas y la persona que sufre los cambios o la irresistible fuerza de vivirlos, siente la necesidad --creyendo que en ese matrimonio no hay espacio para ello-- de expandirse fuera, de jugar a ser esa persona, tal vez quien en realidad es, o cansado de su papel trata de escapar a otro lugar.

  

Es una idea sencilla y tal vez misteriosa a la vez. Pero desastrosamente, la mayoría de la gente posa sus largos tentáculos sobre la vida del otro, de modo egoísta, estudiando sus respuestas, analizando su comportamiento o engañándose con respecto a él o como debería ser y los encierran en sí mismos. Y queda tan poco espacio espontáneo, es tan escasa la posibilidad de amar los cambios sin sentir que estás traicionando a la imagen que tienen de ti que inevitablemente hay un escape en ese depósito a presión.

  

En qué momento comprimimos tanto al prójimo que no podemos quererlo como es y seguirle en sus cambios, apoyando sin deformar. 

  

Bueno, estas son las caóticas ideas sin filtro, en bruto. Tal vez más adelante sepa desentrañarlas mejor.