---
title: ' Jobs - Joshua Michael Stern, 2013'
date: 2013-12-24T00:01:00.000+01:00
draft: false
tags : [steve jobs, Cine, ashton kutcher]
---

  

  

23.xii.2013// Las críticas de antemano no eran muy buenas, sin embargo se presentaba como una gran ocasión para conocer un poco la vida de [Steve Jobs](http://es.wikipedia.org/wiki/Steve_Jobs). Al principio -se trata de la primera media hora-, todo acompaña: [Ashton Kutcher](http://www.imdb.com/name/nm0005110/?ref_=tt_ov_st) lo hace decentemente y es buena la caracterización; la **[música aplaude](http://www.youtube.com/watch?v=wDlrRQ6Yzis&noredirect=1)** los momentos clave, y tiene el tono desenfadado y sin miramientos de una película en la que simplemente suceden cosas, hasta cierto punto interesantes. A partir de ahí pierde toda la fuerza al no ofrecer justificadamente la explicación oportuna de lo que acontece y sin el mayor detenimiento. Varias escenas de la visión fanática, casi representando a Jesucristo (JC), lío en la biografía, etc. Total 122 largos minutos, de los cuales sobran noventa muy mal expresados. Horror.  
**D**