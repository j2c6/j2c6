---
title: 'Un horizonte muy lejano (1992)'
date: 2012-01-04T20:01:00.000+01:00
draft: false
tags : [John Williams, Ron Howard, 1990, Nicole Kidman, Cine, Tom Cruise, Robert Prosky]
---

[![](http://4.bp.blogspot.com/-llnc_EfJlsQ/Txwoo7p9wOI/AAAAAAAABIE/pfPkBaMQpMs/s400/uhml%252C+rostards.jpg)](http://4.bp.blogspot.com/-llnc_EfJlsQ/Txwoo7p9wOI/AAAAAAAABIE/pfPkBaMQpMs/s1600/uhml%252C+rostards.jpg)Tras el incendio de su casa y la muerte de su padre, Joseph Donelly (**Tom Cruise**) va en busca del señor Christie (**Robert Prosky**) para vengarse por suinjusta desgracia. Las cosas surgen de modo inesperado, y acaba huyendo con Shannon (**Nicole Kidman**) --la hija del señor Christie-- en busca de fortuna a Boston. Hablamos de finales del siglo XIX.  
  
  
Un divertido guión del director y **Bob Dolman**, situaciónes irónicas y diálogos entretenidos. Quizá al ser una película de época donde la ambientación está muy conseguida, se esperaba más seriedad en estos aspectos del guión, pero ¿qué importa?. Parecía una historia de **Dickens** aquella decoración donde se detallan perfectamente las esferas sociales de aquel tiempo. Los jovencísimos Cruise y Kidman, hacen un dúo genial en pantalla, dos personajes muy frescos que dejan ver el proceso de desarrollo de Joseph y Shannon, muy inocentes y con muchas ganas de vivir. Van evolucionando tras los desengaños, especialmente los de Shannon. Esto se ve en cómo emprenden juntos el viaje como quien no quiere la cosa, a base de ilusión y arrastrados por una sociedad que les ha educado de forma diferente.  
  

Sinceramente he disfrutado muchísimo con este trabajo de **Ron Howard**, hay quien dice que es excesivamente fácil y blando por lo que sus películas nunca han alcanzado el objetivo de ser historias que no se olvidan, pero me temo que se equivocan, a pesar de no ser muy sofisticado y además predecible, el largometraje es excelente, cosa imposible sin la química de los dos protagonistas de la aventura y posterior romance. Por cierto no tan llamativa, pero presente, la banda sonora de **John Williams**.