---
title: 'Lo que queda del día (1993)'
date: 2012-01-06T21:44:00.000+01:00
draft: false
tags : [James Ivory, 1990, Kazuo Ishiguro, Cine, Emma Thompson, Ruth Prawer Jhabvala]
---

[![](http://4.bp.blogspot.com/-qCizD63ROnk/TwijaV_dv_I/AAAAAAAABFw/8CLlUzGbauo/s400/rostards.jpg)](http://4.bp.blogspot.com/-qCizD63ROnk/TwijaV_dv_I/AAAAAAAABFw/8CLlUzGbauo/s1600/rostards.jpg)  

Darlington Hall es una mansión que corre a cargo del correctísimo mayordomo Stevens (**Anthony Hopkins**), él lleva la casa rectamente sin inmiscuirse en los asuntos que tratan sus importantes dueños. Al incorporar al servicio al ama de llaves, Miss Kenton (**Emma Thompson**), todo parece seguir igual, sin embargo se enamoran mutuamente. 

  
  

Desde el principio y sin saber porqué, al espectador parece interesarle considerablemente lo que va presentando el guión (**Ruth Prawer Jhabvala**). Debe ser porque realmente no pasa nada mientras que los actores misteriosamente sugieren que va a pasar algo, pero poco a poco van comprendiendo. Todo el largometraje a la espera de aquellas palabras, y así es como de modo impecable **James Ivory** (_Una habitación con vistas_, 1985) te mantiene absorto en unas interpretaciones memorables. El autor de la historia y novelista **Kazuo Ishiguro** --libro que he de leer-- hace una perfecta descripción de la situación en el servicio, el personaje de Stevens retratado por A. Hopkins, es complejo pero está realizado a la perfección. Se acaba sintiendo la relación "invisible" de los dos protagonistas y la fuerza de sus caracteres.  
  

La sensibilidad de esta tragedia romántica y frustrante, trata de forma muy inteligente temas como el orgullo, el deber, el amor no correspondido, la dignidad, etc. Sin duda esta historia de amor brilla por su belleza y singularidad,  y ha sido bautizada por el tiempo como un clásico, más que disfrutable.