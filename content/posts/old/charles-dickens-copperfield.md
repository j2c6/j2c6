---
title: 'Charles Dickens - ''David Copperfield'''
date: 2012-09-04T13:23:00.000+02:00
draft: false
tags : [Swinburne, Charles Dickens, S.XIX, Oscar Wilde, David Copperfield, Frank Capra, Literatura, Sigmund Freud, Alejandro Dumas, Dostoievski, Novela]
---

El nombre de **David Copperfield**, no puede -como mínimo- resultar desconocido en la vida de un lector. O eso dicen las largas lenguas de los ávidos lectores. Una novela famosísima, la favorita de entre las suyas del propio autor, una "obra maestra suprema" para **Swinburne**, leída por **Dostoievski** en Siberia, también la predilecta de **Freud**... y así podríamos seguir con los grandes que dedicaron su tiempo -entre otras cosas- a leer esta novela por entregas de tan vasta extensión.

  

Siempre, un poco antes de que me presenten a una supuesta obra maestra, el afán por permanecer en el grupo del buen gusto despierta mis mejores deseos para disfrutar la obra. Sino es así, sino disfrutas tanto como describen los halagos, te sientes incómodo, excluido, dejas de tener algo en común, tal vez con la humanidad y eso no es agradable. Hay cierto peligro en no sentirse identificado y es la tendencia a mentir, para evitar socialmente esa falta de coincidencia desagradable.

  

[![](http://1.bp.blogspot.com/-J0fhaRmQDjs/UEXkgbD8NgI/AAAAAAAABhw/R7tPdEhch90/s400/copperfied,+rostard.jpg)](http://1.bp.blogspot.com/-J0fhaRmQDjs/UEXkgbD8NgI/AAAAAAAABhw/R7tPdEhch90/s1600/copperfied,+rostard.jpg)

'**David Copperfield**' no me ha gustado demasiado. No me he aburrido pero, no es ni de lejos mi novela favorita. Es una obra sencilla, tranquila, pausada, que narra la vida del joven David. Las primeras 300 páginas, fui incapaz de disfrutar los primeros años de autocompasión y tristeza de Copperfield. Su literatura es descriptiva hasta en los detalles tal vez menos interesantes o más evidentes, sorprende a menudo con versos entre su prosa, pero no demasiado. Cuando llega a los 17 me pareció más interesante, ya que deja a un lado la tormenta para abrirse paso en la vida. La personalidad de Steerforth, la candidez de Agnes, la belleza de Emily... Me quedaría con una buena lista no obstante. Hay instentes mejores y momentos peores como en cualquier sitio, pero la globalidad me ha decepcionado.

  

Me dí cuenta de que Dickens, se dedica exclusivamente a contar una historia para entretener, no es como **Wilde** o Dostoievski que asedian al lector, sino como **Frank Capra** en el cine. Tampoco debería olvidar la crítica a la sociedad inglesa que premian los literatos, que se esconde bajo el manto de su sencilla literatura. Tal vez ahí reside su grandeza, en la sencillez y el encanto de lo personajes. No he sido inmune a ciertos encantos y he copiado bastantes flores que se encuentran entre sus letras a lo largo de mil páginas, en una elegante, gruesa y pesada edición con la que cargar a todas partes, pero no ha sido especialmente especial.

  

Sin embargo me ha servido para conocer al famoso **Charles Dickens**, desde mi plebeyo púlpito. Me ha parecido que tiene una gran inventiva, imaginación, etc y etc, pero creo que más que las historias, son los personajes los que acaban en el corazón del lector, dejando a un lado el sentimentalismo de algunas escenas, la clave está en seguir un millar de páginas a Copperfield para hacerlo tuyo. Porque para historias por entregas ya está **Dumas** y sus sorpresas.