---
title: 'Lista o mapa ZEN'
date: 2018-01-28T13:10:00.001+01:00
draft: false
tags : [zen, autenticidad, interior, mapa, silencio, simplicidad]
---

Por lo visto ahora me interesa mucho la manera de profundizar en la Vida Interior y el modo en que podar las ramas sobrantes del árbol y alejar el ruido.  De momento aquí dejo algunas referencias del camino. 

  

Tomate Frito

'_**[El arte y la ciencia de no hacer nada](https://pirulaselementales.blogspot.com.es/2017/05/notas-el-arte-y-la-ciencia-de-no-hacer.html)**_'

_**[Paterson](https://pirulaselementales.blogspot.com.es/2017/06/paterson-solo-para-decirte.html)**_ (Jim Jarmusch, 2016)

_**Biografía del Silencio**_, Pablo D'Ors: la meditación, el silencio, la simplicidad, el desierto.

La vida de Thoreau

Slow Movement

Budismo ZEN

'_**[Con el tiempo](https://pirulaselementales.blogspot.com.es/2011/04/enrique-garcia-maiquez-variacion-sobre.html)**_', Enrique García-Maiquez.

Poema de '_**[Sobre tu risa](https://pirulaselementales.blogspot.com.es/2010/11/miguel-d-sobre-tu-risa.html)**_' de Miguel D'Ors.

Documental Tashi y el Monje (Pilgrim Films, 2014)

Los Hermanitos del Cordero que nos encontramos.

Camino de Santiago / Casas rurales

La vida sencilla, sin brillo. Poesía cotidiana. Autenticidad humana. Vivir el presente. Estar concentrado. La simplicidad. Minimalismo. Lo infinito. Paz. Contemplación, asombro. Exploradores que salen del _sistema_ y buscan.

  

Comprensión. Tolerancia. Maneras de entender el mundo.

  

NOTA: Comprendo el ambiente caótico de este post. Se trata de un apunte rápido, una pista.