---
title: 'Ocean''s Thirteen (2007)'
date: 2012-01-29T10:58:00.000+01:00
draft: false
tags : [Casey Affleck, Elliott Gould, George Clooney, Ellion Gould, Al Pacino, Don Cheadle, Andy Garcia, David Holmes, Bernie Mac, Carl Reiner, Steven Soderbergh, Scott Caan, Cine, Matt Damon, 2000, Brad Pitt, Eddie]
---

[![](http://3.bp.blogspot.com/-s0ZpOi76UHg/TyUX9renrMI/AAAAAAAABLE/exz_vWq-VOI/s400/rostards.jpg)](http://3.bp.blogspot.com/-s0ZpOi76UHg/TyUX9renrMI/AAAAAAAABLE/exz_vWq-VOI/s1600/rostards.jpg)  

Willie Bank (**Al Pacino**) se la ha jugado al socio de su hotel, Reuben Tishkoff (**Ellion Gould**) dejándolo en la estacada. Pero Danny Ocean (**George Clooney**) y los suyos han vuelto y también los grandes "trabajos" y robos. 

  

  

El producto de rizar el rizo de '**Ocean's eleven**' (2001) salió mal la última vez con la desastrosa y patética _twelve_, aunque la recuperación en esta parte de la trilogía es de agradecer, debe ser por la incorporación de **Al Pacino** al elenco de estrellas más extenso de la cine: **George Clooney, Brad Pitt, Matt Damon, Andy Garcia, Don Cheadle, Bernie Mac, Ellen Barkin, Casey Affleck , ****Vincent Cassel, ****Scott Caan, Eddie Jemison, Shaobo Qin, Carl Reiner, Elliott Gould**...  
  

El producto está claro: 'glamour', dinero, robos, lujo, elegancia y una acertada banda sonora de **David Holmes**. Algo muy entretenido en cada detalle y de lo que se ha ocupado  **Steven Soderbergh**, la fotografía de corte "sesentero" y la dirección de estrellas. Sin duda no es necesario dejar la moral a un lado para disfrutar del crimen.