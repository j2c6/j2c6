---
title: '#9 COVID-19'
date: 2020-03-21T10:31:00.000+01:00
draft: false
tags : [pandemia, coronavirus, diario coronavirus, covid-19]
---

Si algo ha demostrado este virus es que estamos rodeados de sabios, gente que sabe un montón, mentes brillantes, a la que escuchar, a abrirnos a sus consejos a perder otros poco minutos sin coste para aprender. NINGUNO de ellos ha evitado ni evitará una pandemia como estas. Gente con ganas de 5 minutos de fama. Un día recopilamos todos los vídeo consejos que hemos recibido y nos reiremos. Lo único que saco en claro ahora que todos estamos tan disponibles para escuchar consejos, es que hace un mes esto era una "cosa china" y no era 'nuestro problema'. Para la próxima vez a lo mejor podemos pensar que sí que es problema nuestro.

  

Declaraciones de Fernando Simón, Director del Centro de Coordinación de Alertas y Emergencias Sanitarias del Ministerio de Sanidad: 

  

> *   24-ene: "La población ahora mismo yo creo que tiene que tener un nivel de percepción de riesgo incluso muy bajo."
> *   31-ene: "Nosotros creemos que España no va a tener, como mucho, más allá de algún caso diagnosticado."
> *   01-feb: "Obviamente tenemos que tener un seguimiento mucho más cuidadoso de este evento."
> *   09-feb: "Lo que sí que está claro es que esto no implica ahora mismo un incremento de riego para nuestro país, puesto que no ha habido transmisión en España."
> *   23-feb: "En España ni hay virus, ni se está transmitiendo la enfermedad ni tenemos ningún caso actualmente."
> *   26-feb: "La situación en España ahora mismo no ha cambiado sustancialmente como digo, pero es cierto que tenemos que tener cuidado. Es una enfermedad nueva."
> *   27-feb: "Obviamente es posible que algún caso fallezca en nuestro país, pero entraría dentro de lo esperable. No de lo deseable, pero sí de lo esperable."
> *   01-mar: "Nosotros nos seguimos manteniendo por ahora en una situación de contención, no es necesario ahora mismo en España elevar el nivel de alerta."
> *   03-mar: "En España en concreto tenemos ahora mismo 150 casos."
> *   06-mar: "Un contacto estrecho tiene que vigilarse los 14 días salga o no negativo."