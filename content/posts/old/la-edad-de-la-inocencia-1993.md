---
title: 'La edad de la inocencia (1993)'
date: 2012-01-28T18:08:00.000+01:00
draft: false
tags : [Beethoven, Daniel Day-Lewis, Martin Scorsese, Michelle Pfeiffer, Strauss, 1990, Elmer Bernstein, Edith Wharton, Winona Ryder, Cine, Florian Ballhaus, Dante Ferreti, Mendelssohn]
---

[![](http://1.bp.blogspot.com/-MKIE84nGXJ0/TyQrXYE2bII/AAAAAAAABKk/MSDTF9D8z5g/s400/the+rostardss.jpg)](http://1.bp.blogspot.com/-MKIE84nGXJ0/TyQrXYE2bII/AAAAAAAABKk/MSDTF9D8z5g/s1600/the+rostardss.jpg)**Nueva York**, finales del siglo XIX. Newland Archer (**Daniel Day-Lewis**)  está prometido con May Welland (**Winona Ryder**), cuando conoce a la duquesa Olenska (**Michelle Pfeiffer**) una mujer europea,  divorciada, con mucha personalidad y nada convencional. Lo resumiré diciendo que nadie permanece indiferente tras estas presentaciones.

  
  

**Martin Scorsese** había dirigido ya obras de referencia como '**Taxi Driver'** (1976), '**Uno de los nuestros'** (1990), '**El  Cabo del miedo'** (1991), '**Toro Salvaje'** (1980) y ahora presentaba una prometedora adaptación del premio **Pulitzer** de **Edith Wharton**: '**La edad de la inocencia**'. Quizá abusivo por la explícita prosa de la novela, Scorsese se ajusta todo lo posible a la atmósfera creada por la poética narrativa del libro, materializando la belleza y el buen gusto en escenas encantadoras. Es desbordante el placer del espectador al seguir el hilo argumental mientras convive con la aristocracia americana en espléndidos salones, comparte sabrosos banquetes y se deleita con la elegancia de los personajes de la película. Los agradecimientos deberían ser también para **Dante Ferreti** (dirección artística), **Florian Ballhaus** (fotografía) y la música de **Elmer Bernstein,** además de **Strauss, Beethoven y Mendelssohn**.

  

Los que interpretan aquella sociedad diluida en la hipocresía y las apariencias, hacen posible este trabajo, **Day-Lewis**, **Pfeiffer** y **W. Ryder** --un papel hecho para ella--. Nos vamos acostumbrando al buen cine, pero no dejo de ser consciente del lujo que resulta ver en pantalla a estos grandes actores. A pesar del eterno fraude que supone ver la adaptación de una novela, la esencia de **Wharton** es testada por el público, permaneciendo envuelto en un romance sin ausencia de polémica, en sí la historia es embriagadora.