---
title: 'Origen'
date: 2010-08-30T18:25:00.000+02:00
draft: false
tags : [Matrix, Joseph Gordon-Levitt, Marion Cotillard, Wachowski, Muy Buenas, Origen, Christopher Nolan, Ellen Page, Leonardo DiCaprio, Cine, Hans Zimmer, Sueño, Buenas Ideas, El Caballero Oscuro, Michael Caine]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/THvbYMuBXiI/AAAAAAAAAyE/ddTztJsox1o/s320/PHoHKrtxbOrQsr_3_l.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/THvbYMuBXiI/AAAAAAAAAyE/ddTztJsox1o/s1600/PHoHKrtxbOrQsr_3_l.jpg)  

Tras algunas de sus películas: psicológicas como  "**Memento**"(2000), "**Insomnio**"(2002) y  "**Truco Final**"(2006) junto al triunfo más taquillero de "E**l Caballero Oscuro**"(2008), **Christopher Nolan** enseña al mundo su nuevo proyecto: "**Origen**" (**Inception**). De la mano de **DiCaprio** de trayectoria ascendente (**Titanic, El Aviador, Shutter Island, Infiltrados**) presenta un nuevo drama mental en el mundo de los sueños. La idea  de vender lo irreal como verdadero, es atractiva a todo buscador de felicidad acomodada en los sentidos, cuando inicias este camino es difícil "distinguir la vigilia del sueño" (**Descartes**). La parte racional más independiente y caprichosa, -el subconsciente-  rabiosamente cotizada por el mundo, es alcanzable en este largometraje. Si pudiéramos elegir nuestros sueños: ¿Soñaríamos eternamente? ¿Está la felicidad en una "felicidad" indiferente e ignorante? ¿Se puede elegir un mundo virtual similar al  nuestro en el que lo que parece que sientes no lo estás sintiendo de verdad?.

  

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/THvbVaaROJI/AAAAAAAAAx8/Yl-t8zEiY4Q/s320/pageydicaprioorigen.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/THvbVaaROJI/AAAAAAAAAx8/Yl-t8zEiY4Q/s1600/pageydicaprioorigen.jpg)"¿Cuál es el parásito más resistente? Una idea"  Como anuncian los carteles  "Tu mente es el escenario del crimen". Hablando cinematográficamente encontramos un excelente guión del puño y letra del mismísimo director inglés. En ocasiones vemos reflejados en él parte del filme **Wachowski**: "**Matrix**", la idea de un mundo virtual alternativo, la linea circular de una historia . Además el argumento aparentemente nublado y complejo, es delicadamente digerido a lo largo de la película.Con un gran presupuesto de 200 millones de dólares, el filme está dotado de las mejores sensaciones y calidad image-sonido. **Hans Zimmer** reconocido por sus bandas sonoras (**Piratas del Caribe, El Caballero Oscuro, Sherlock Holmes**), pone el tiempo y ritmo a "Origen", haciendo cada momento el más álgido y tenso a base de instrumentos de viento y violonchelos, hace de un maremoto un asombroso tsunami. Dirección técnica y puesta en escena muy cuidadas y cómodas, en ellas queda destacada la minuciosa y atareada mano de obra que construye esta obra maestra. **Ellen Page** (**Juno**) vuelve a demostrar sus artísticas dotes para la actuación, prometiendo una carrera brillante: una actriz sorprendente. Aparecen también otras estrellas como el veterano **Michael Caine** (**Un Plan Brillante, Las Normas de la Casa de la Sidra y el Caballero Oscuro**) que adopta un papel secundario, **Joseph Gordon-Levitt** (**500 Dias Juntos**), **Marion Cotillard** (**Big Fish**).

  

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/THvbTbs7nOI/AAAAAAAAAx0/odrSkF-qmKc/s320/inception2.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/THvbTbs7nOI/AAAAAAAAAx0/odrSkF-qmKc/s1600/inception2.jpg)

[](http://2.bp.blogspot.com/_VT_cUiG4gXs/THvbTbs7nOI/AAAAAAAAAx0/odrSkF-qmKc/s1600/inception2.jpg)La película acelera el corazón: creamos un sueño el sujeto lo llena de su subconsciente que lo llena de recuerdos.. de sus secretos, nuestro objetivo es averiguarlo; el problema entra cuando no nos conformamos con esto y queremos introducir una idea en el sujeto: esto es origen. Todo esto se complica más aún por una tragedia: una vida soñada y no superada que recorre la conciencia y la inconsciencia del protagonista haciéndole retorcerse entre la supuesta veracidad de los sueños y el pasado. No nos dejará indiferente y reaparecerá en nuestros comentarios, convirtiéndose así en una referencia cinematográfica, espero volver a verla.

  

Un ejemplar revolucionario e impecable, emocionante de principio a fin, muy atractivo.