---
title: 'f / 64 & Nikolay Paustovskiy'
date: 2020-04-03T13:33:00.002+02:00
draft: false
tags : [vk, Rusia, 64mag, f64, Paustovsky]
---

[f / 64](https://vk.com/64mag) ó [64mag](https://www.instagram.com/64mag/) Visual culture

Photography / Cinema / Fashion

La comunidad más grande dedicada a la cultura visual. Fotografía, moda, cine, arte contemporáneo, música y arquitectura.

  

Kolya Paustovsky / Nikolay Paustovskiy (February 1, 1990, Moscow)

[Documentary and fashion photographer](https://www.instagram.com/merde/) / visual artist based in Russia, Moscow.