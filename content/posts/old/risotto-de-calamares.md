---
title: 'Risotto de Calamares'
date: 2020-05-29T10:22:00.001+02:00
draft: false
tags : [receta, arroz, calamares]
---

**Ingredientes**  

*   300gr de calamares
*   Arroz
*   Un litro de caldo de verduras
*   Ajo
*   Cebolla
*   Pimentón de la Vera
*   Sal Pimienta
*   Tomillo
*   Hoja de laurel  
    

**  
Preparación**  

1.  Sofreimos cebolla con tomillo y posteriormente el ajo.
2.  Añadimos una cucharada de pimentón de la vera.
3.  Doramos y los calamares. Añadimos una hoja de laurel.  
    
4.  Añadimos arroz para que empiece a tostarse.
5.  Cubrimos con caldo, dejamos hervir y vamos añadiendo hasta que el arroz esté hecho.

  

[![](https://1.bp.blogspot.com/-O6gvtWFW-p0/XtDF_BF8uGI/AAAAAAAAfcg/yiZvePT8YGwLx1I4ALlNl-qwsIIEa83ZgCK4BGAsYHg/s320/photo_2020-05-29_10-20-53.jpg)](https://1.bp.blogspot.com/-O6gvtWFW-p0/XtDF_BF8uGI/AAAAAAAAfcg/yiZvePT8YGwLx1I4ALlNl-qwsIIEa83ZgCK4BGAsYHg/photo_2020-05-29_10-20-53.jpg)