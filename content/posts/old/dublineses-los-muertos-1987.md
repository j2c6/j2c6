---
title: 'Dublineses (Los muertos) (1987)'
date: 2012-04-16T12:01:00.000+02:00
draft: false
tags : [Tony Huston, Fred Murphy, Cine, John Huston, James Joyce, Anjelica Huston, Donal McCann, 1980]
---

[![](http://3.bp.blogspot.com/-0kBwdoNdDfQ/T4vu7BJDiXI/AAAAAAAABYg/ZssAb8NgwuI/s400/rostard.jpg)](http://3.bp.blogspot.com/-0kBwdoNdDfQ/T4vu7BJDiXI/AAAAAAAABYg/ZssAb8NgwuI/s1600/rostard.jpg)

O no era ni el momento ni el lugar, o no he entendido la película, o sencillamente no es gran cosa. El testamento de **John Huston** fue dirigido en silla de ruedas y con máscara de oxígeno. Huston en las últimas, rueda -tras una infinita lista- otra adaptación literaria, en este caso del relato de **James Joyce**.

  

En primer lugar me gustaría pensar que es su última película pero no tiene mucho en común con las demás. Podríais decir que está al borde de la muerte, y que la reflexión del personaje de Gabriel Conroy (**Donal McCann**) hace una brillante reflexión sobre que la vida no tiene sentido, poco después de cuando su mujer Gretta (**Anjelica Huston**) le cuenta como la canción que ha escuchado en la cena le ha recordado a un viejo amor de su juventud. Pero sinceramente me parece que la película está muerta.

  

La música de **Fred Murphy** del principio, con los créditos, parece prometedora. E incluso a medida que van apareciendo los personajes con su educados y comedidos diálogos de humor irlandés se espera una película de costumbres, sensible pero profunda. Sin embargo vuelves a mirar el reloj y no ha pasado nada, ni siquiera te ha sorprendido. Los invitados parecen tan a gusto en la fiesta de la Epifanía -y no cesan de repetirlo- pero el espectador parece contemplar un banquete al que no ha sido invitado.

  

La lectura del poema, el canto de la anciana parece ser de otro mundo. Tal vez fuera demasiado profundo para el momento del visionado, pero me temo que la receta se ha quedado sosa. Los diálogos hablan de cosas casuales, no nos cuentan nada intrínseco de los personajes, sino tangente y arbitrario, por lo que no sabes a quién prestarle atención. El guión de **Tony Huston** -aquí está toda la familia- resulta poco convincente, a pesar de que deja ver un intento para una disertación trascendente y existencial, no atrae al público en absoluto por no prepararlo adecuadamente a pesar de los medios con los que cuenta.

  

Finalmente llega la escena sustancial del filme donde Gretta se emociona y cuenta la muerte de un amor de su adolescencia y no acaba sin derramar algunas lágrimas. El espectador se ve atropellado por algo tan personal tras todas las trivialidades que lo precedían, y no sabe que hacer. Este relato lo conmueve porque no esta sordo, pero no todo lo que debería, ya está desesperado, aunque no carece de cierto interés. Y como colofón la profunda, triste y melancólica divagación  de manos de Gabriel -el personaje diferente- acerca del sentido de la vida y su inexorable final: la muerte. En fuera de juego.  
  

No creo que este largometraje a pesar de su supuesta sensibilidad y ambición artística, sea el legado de este director por su proximidad con la fecha de su muerte, el sabio espectador sabrá encontrar cualquier otro ejemplar de este prolífico y memorable director donde anclar el recuerdo de un gran maestro del séptimo arte como **John Huston**. Un libreto inerte al final de su carrera no podrá borrar lo que creó durante su vida: excelente cine.