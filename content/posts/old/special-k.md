---
title: 'Special K'
date: 2018-07-07T20:50:00.001+02:00
draft: false
tags : [vida, cotidiano, Poesía, cereales]
---

[![](https://2.bp.blogspot.com/-uGQf6LqRGmo/W0ELjZRpu-I/AAAAAAAAXws/QhpUKMJExvY5TsAi9rKHRpxdm5eQg3rDACK4BGAYYCw/s320/IMG_20180707_204515-752938.jpg)](http://2.bp.blogspot.com/-uGQf6LqRGmo/W0ELjZRpu-I/AAAAAAAAXws/QhpUKMJExvY5TsAi9rKHRpxdm5eQg3rDACK4BGAYYCw/s1600/IMG_20180707_204515-752938.jpg)

Deliciosos,

no me distraen de la luz

que entra por el cortinaje

mudos de sabor,

cereales insípidos,

el peso plegado de mis

piernas sucias de verano

que reciben vibraciones

de la nevera de cristal

cualquier mañana de Julio