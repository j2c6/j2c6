---
title: 'Alejandro Casona'
date: 2009-02-07T16:56:00.000+01:00
draft: false
tags : [Alejandro Casona, S.XX, Prohibido suicidarse en primavera, La dama de alba, Los árboles mueren de pie, Muy Bueno, Teatro, La casa de los siete balcones, La sirena varada, Literatura, Nuestra Natacha]
---

[**![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SY2wTxgieKI/AAAAAAAAAYs/SQQdSZ79IVk/s320/20080227120307-libreria1.jpg)**](http://1.bp.blogspot.com/_VT_cUiG4gXs/SY2wTxgieKI/AAAAAAAAAYs/SQQdSZ79IVk/s1600-h/20080227120307-libreria1.jpg) Bueno creo que es el primer artículo sobre lectura que escribo pero creo que este autor se lo merece, creo --otra vez-- que he leído bastantes de sus obras, me gustan porque son historias, bonitas, ingeniosas, y es de hace nada, entonces entiendes todo, tiene buenas ideas, lo bueno es que no cuestan nada de leer, se leen en una hora más o menos, y te llenan, no sé qué decir… Me he leído:  
  
  
_**\-Los árboles mueren de pies.**_  
_**\-Prohibido suicidarse en primavera.**_  
_**\-La casa de los siete balcones.**_  
_**\-La dama de alba.**_  
_**\-Nuestra Natacha.**_  
_**\-La sirena varada.**_  
  
De verdad que son muy buenas, se merece mención, pues eso que lo sepáis y que he disfrutado mucho leyéndolas. Si os habéis leído alguna decid que pensáis, me gustaría compartir mi opinión con alguien. Espero que si os las leéis os gusten.