---
title: 'Fracasar intensamente'
date: 2012-08-31T09:59:00.000+02:00
draft: false
tags : [Oscar Wilde, James Stewart, Uncategorized, vivir intensamente]
---

[![](http://1.bp.blogspot.com/-RV4UjaIZPoY/UEBuml-1_2I/AAAAAAAABgQ/QmDu5I3huyI/s400/original.jpg)](http://1.bp.blogspot.com/-RV4UjaIZPoY/UEBuml-1_2I/AAAAAAAABgQ/QmDu5I3huyI/s1600/original.jpg)

Abrí este blog para triunfar, ¡qué idiotez!, se trataba de conseguir dejar reflejadas mis piradas y teorías, mi visión. Supongo que no podré nunca dejar de avergonzarme por ciertas cosas que he escrito, y me gustaría decir que lo he hecho con la mejor intención, asentándome en la autenticidad y honestidad como principios básicos, pero me temo que no es así. De vez en cuando se enciende una chispa en el cerebro que amenaza  con dejar todas las letras ardiendo, ¿qué has hecho?¿qué has aportado?. Y cuando me doy cuenta no soy sino otra maldita marioneta de las que tanto me irrita ver retorcerse  y rendirse cada día a la mediocridad y a la vulgaridad. Sorprendentemente adolecemos de los defectos que denuncian nuestras críticas.

  

Me resulta más complicado de lo que pensaba ser fiel a las propias convicciones y pensamiento. Es fácil que secuestren tus ideas y las cambien por otras, es sencillo no ser auténtico, es complicado 'evolucionar' y no perder la esencia, es horroroso ser un necio más con aires de superioridad y olvidar el monstruo ignorante que nos domina. Y es exquisitamente superior no contradecirse. Así son las cosas. Las personas mutan y no son inmunes al bombardeo de estímulos que cada día nos hacen avanzar o retroceder. Controlar las teclas interiores que despiertan los mecanismos de pensamiento es imposible, tan solo podemos escuchar la melodía, el resultado, pero de nosotros depende saber quiénes son esas manos que se deslizan por el poderoso y sofisticado piano de nuestras mentes abandonadas.

  

Seguiré escribiendo, y seguiré con el ideal de elaborar y crear ideas que hagan pensar (esto queda mejor decirlo cuando has hecho algún regalo a la humanidad, alguna aportación inmensa o mísera, pero una aportación después de todo) aunque no lo vea realizado; también seré mentiroso y falso, predecible y necio, perezoso y mezquino, pero de algo estoy seguro  y es que solo sabiendo esto estoy convencido de que en algún momento brillará algo propio y único aunque tal vez insignificante, por lo que valga la pena haber estado haciendo el imbécil todo ese tiempo.

  

Al final o estás en medio de ningún sitio o te has empotrado contra algo, pero lo significativo será eso último: que has fracasado INTENSAMENTE, eso es lo importante. Triunfar exitosamente o naufragar estrepitosamente, ambas me parecen poderosas. Lo interesante será saber que estás en algún sitio y que no estás en medio de la nada desperdiciando la existencia. Escribir será tan solo una minúscula parte de vivir intensamente, porque como dice el ilustre **Oscar Wilde**, '_la mayoría de la gente solo existe, eso es todo_'.