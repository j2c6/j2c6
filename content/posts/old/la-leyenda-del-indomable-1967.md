---
title: 'La Leyenda Del Indomable (1967)'
date: 2010-04-19T07:38:00.000+02:00
draft: false
tags : [60', George Kennedy, Cine, Paul Newman, Muy Buenas]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/S8vr3bwB3II/AAAAAAAAArM/5J4aWRdIuZM/s400/90562-050-87ACAC6E.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/S8vr3bwB3II/AAAAAAAAArM/5J4aWRdIuZM/s1600/90562-050-87ACAC6E.jpg)

Fotograma de la película. Paul y George

  

**Stuart Rosenberg** pensó en el "indomable" de la novela _**Cool Hand Luke**_ de **Donn Pearce**, más con **Paul Newman**, y **George Kennedy** (ganador del Oscar al año siguiente 1968), le salió algo más que una película sobre el drama carcelario. Ciertamente este largometraje, tiene que ser visto en su contexto, ya que por desgracia hoy en día no se tiene el conocimiento, de lo "indomable" en el sentido de la libertad, el esfuerzo, que en el caso de Luke (Newman), es muy superior al de la media. Aunque también con sus excepciones, Luke es enviado a la cárcel de los años 60, por destrozar los cabezales de los parquímetros bajo los efectos del alcohol. Allí  aguanta  los castigos de los guardias y lo que le venga encima, que junto con su personalidad, hace que llegue a ser considerado un líder por los otros presos. Luke es un incomprendido, que tiene sus problemas y dudas que intenta superar, no hay ley que se le imponga y por ello muere luchando por su libertad.

  

.

Es una película de las buenas, en la que la calidad del filme está basada, en los diálogos y gestos de los actores. Da una gran sensación: una película tan sencilla, con la trama de la cárcel, sin cosas raras, la vida tal cual es. En  "**La Leyenda Del Indomable**"  podrán disfrutar del verdadero cine, con buenos actores y alguna que otra enseñanza.