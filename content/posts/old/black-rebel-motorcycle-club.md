---
title: 'Black Rebel Motorcycle Club'
date: 2009-12-11T07:58:00.000+01:00
draft: false
tags : [Rock, Garage Rock, Musica, Black Rebel Motorcyle Club, Marlon Brando]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SyHse7gOvyI/AAAAAAAAAno/exdhz7sKoD0/s320/brmccover.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SyHse7gOvyI/AAAAAAAAAno/exdhz7sKoD0/s1600-h/brmccover.jpg)

  

Bueno el grupo que voy a presentar esta semana es un gran grupo aunque aparentemente no lo  parezca. Han sacado ya cuatro discos y sacaran el quinto (como ya se anunció en el apartado de noticias) en mayo del 2010.Su nombre viene de una famosa película de **Marlon Brando** de los años  50: "_**Salvaje**_", la protagonizan una serie de motoristas: **Black Rebel Motorcycle Club (_B.R.M.C_).** Comenzaron en San Francisco, en lo que viene a ser el _**Garage Rock**_, que es un estilo caracterizado  principalmente por la inexperiencia de los miembros, y por el entusiasmo y expresión de las canciones: en resumen que no son profesionales pero que tienen talento, ilusión, afán, y poder. Esto es lo que se nota aquí precisamente , sus temas tienen mucho ritmo, _riffs_ sencillos, siempre sin bajar el nivel, siempre al máximo. Este estilo me encanta: es lo que estaba buscando, porque te da lo que necesitas: gritos, marcha, sentimiento, libertad, desahogue... todo, es como un bidón de gasolina, o un coche a 180. Es importante que cuando lo escuches estés de camino a algún sitio, o con algo que vayas  a hacer, de modo que cambie tu perspectiva. Un apunte importante es que ocurre un terremoto en su estilo musical, y es que en 2005 en _**Howl**_ se tiran al country, pero el resto es como un tsunami. Bueno os pongo dos temas, de mis favoritos, y de sus comienzos: _B.R.M.C_, su primer _EP_,  mucho espíritu...y como siempre en **MB&N**. A un **_White Stripe_.** 

  

Por cierto si os gusta, ver todo lo relacionado con el _garage rock_: habrá mas publicaciones.