---
title: 'Stefan Zweig - ''La impaciencia del corazón'''
date: 2012-06-29T18:32:00.000+02:00
draft: false
tags : [S.XX, Stefan Zweig, XX, Literatura]
---

Ya me había leído una novela del excelente **Stefan Zweig**, una muy corta: '_**Novela de ajedrez**_'. Se lee en un instante. Me quedé con cómo describe este autor el estado del corazón humano. Pero la idea es que no pude apreciarlo en tan escaso tiempo de lectura, un suspiro es un suspiro.

  

[![](http://2.bp.blogspot.com/-ZjxOPUs1Xo4/T-3YcYVviRI/AAAAAAAABfI/ZyLduVFlyhk/s640/Johanna+Harmon,+Rostard.jpg)](http://2.bp.blogspot.com/-ZjxOPUs1Xo4/T-3YcYVviRI/AAAAAAAABfI/ZyLduVFlyhk/s1600/Johanna+Harmon,+Rostard.jpg)

  

'_**La impaciencia del corazón**_' también conocido como '_**La piedad peligrosa**_', es una novela muy fácil de leer, desde el principio vuelan las páginas de una sentada. Me he dedicado a leerlo en el autobús, puedo presumir de haberme centrado en los exámenes, pero al mismo tiempo no he vendido la lectura. No se sabe cuánto influye el sitio de lectura de un libro en el placer que experimenta el lector y la capacidad de apreciación del mismo.

  

La historia aparentemente sencilla, va evolucionando de forma sutil. Zweig tiene una sensibilidad especial, consigue dotar de cierta importancia a los matices, y de ese modo se hace hincapié en las sensaciones y gestos en ocasiones apenas imperceptibles. El lector se enriquece con estos detalles y por supuesto lo involucran mucho más en la historia.

  

La narra en primera persona. La acción es muy dinámica por las rápidas reflexiones de sus propias palabras y gestos. Tiene un especial talento como observador, desvelando el significado quizá oculto de los ademanes de los personajes. Usa un tono irónico a veces, pero siempre sincero y personal. No solo describe lo que ve, sino también lo que ocurre en las mentes de los protagonistas; sus pareceres sobre los personajes van y vienen enriqueciéndolos con múltiples pinceladas.

  

También ha habido situaciones en las que se detiene que me han aburrido un poco y me da la sensación de que les da excesiva importancia. Sin embargo el tema de la piedad y la compasión es expuesto con especial profundidad. Comentarios delicados y agudos de las circunstancias hacen muy significativos los distintos instantes del relato. Y a pesar de que no se detiene demasiado en descripciones físicas, de vez en cuando añade adjetivos muy artísticos a sus narraciones.

  

En definitiva 'La impaciencia del corazón' demuestra la maestría del popular autor austríaco. Tengo que leer más cosas suyas, escucho recomendaciones y alabanzas suyas desde todos lados. La cavilaciones de Zweig son sin duda muy sugerentes e interesantes. Ha habido momentos de desconexión con el libro y acuso sin duda a los exámenes; pero una vez finalizada la novela y en frío reconozco algo grande entre estas páginas.

  

> "_Ver a alguien a tu lado consumiéndose en el fuego de su deseo y quedarse quieto e impotente, sin encontrar la fuerza ni el poder ni la capacidad para arrancarlo de estas llamas. Quien ama sin ser correspondido puede a veces dominar su pasión, porque no es solo criatura, sino también creador, de su aflicción; si un amante no puede dominar su pasión, por lo menos sufre por su propia culpa. En cambio, está completamente indefenso y desvalido el que es amado sin corresponder, pues la medida y los límites de esta pasión ya no están en sus manos, sino más allá de sus fuerzas, y si otro lo quiere su voluntad se anula. Quizá solo el hombre es capaz de ver claramente que esa atadura no tiene escapatoria, que esa necesidad de resistencia que le es impuesta sólo a él se convierte a la vez en martirio y culpa, pues cuando una mujer se defiende contra una pasión no deseada, en el fondo obedece a la ley de su sexo; a toda mujer es innato, primitivo por decirlo así, el gesto de la negativa inicial, y aún cuando se niega a sí misma el deseo más ardiente no se la puede llamar inhumana. ¡Pero qué fatalidad cuando el destino invierte la balanza, cuando una mujer ha vencido su pudor hasta el punto de revelar su pasión a un hombre y le ofrece su amor sin la certeza de ser correspondida, y él, pretendido la rechaza con frialdad! Enredo irresoluble siempre, pues no corresponder a ese deseo de una mujer significa aniquilar también su orgullo, destruir su pudor; quien se niega a una mujer que lo desea, por fuerza tiene que herirla en lo más noble. Resulta inútil entonces toda forma de eludirla -por delicada que sea-, absurdas todas las evasivas corteses, ofensiva toda oferta de simple amistad; una vez que la mujer ha dejado al descubierto su debilidad, toda resistencia del hombre se convierte irremisiblemente en crueldad; siempre que no acepta el amor, se convierte sin culpa en culpable._"

  
  

Stefan Zweig. '**_La impaciencia del corazón_**', Acantilado 2006.

Imagen: **Johanna Harmon**.