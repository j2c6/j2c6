---
title: 'Intocable (2011)'
date: 2012-04-22T23:12:00.000+02:00
draft: false
tags : [Omar Sy, Cine, 2010, Olivier Nakache, Eric Toledano, François Cluzet]
---

[![](http://3.bp.blogspot.com/-T13Nk3TNZsA/T5R0HZ3Yg4I/AAAAAAAABa0/RgcxXB2H2l8/s400/rostard.jpg)](http://3.bp.blogspot.com/-T13Nk3TNZsA/T5R0HZ3Yg4I/AAAAAAAABa0/RgcxXB2H2l8/s1600/rostard.jpg)

Otra película francesa que arrasa en las taquillas mundiales. Lo cierto es que a primera vista me parecía el típico drama alegre y melancólico al mismo tiempo, que intenta enseñar algo arrancando exclamaciones al compadecido público que se encariña con la desgracia ajena con facilidad.

  

Hacía mucho tiempo que no me reía tanto en el cine. Presentan una historia inusual de un millonario tetrapléjico (**François Cluzet**) que por algunas razones contrata a un peculiar y marchoso personaje (**Omar Sy**) de los suburbios de París, para que le atienda en sus necesidades diarias. El filme suaviza las posibles incomodidades que puedan surgir para que al espectador se le haga más que llevadera esta fructífera relación. El estilo cómico es realmente gracioso y gustoso salvo por los vulgares y repetitivos comentarios obscenos.

  

El guión consigue ser extremadamente divertido y sorprendentemente logra crear también  atmósferas más serias y dramáticas sin abusar de la tendencia lacrimosa del público. La relación se convierte en un dúo inseparable y afable. Centrándose en el personaje de _Philippe_ se dan a conocer algunas de las caras y cargas que tiene que sobrellevar una persona con este tipo de discapacidad como su día a día con este impedimento físico, la distante relación con su hija, la condescendencia que derrochan con él los que lo rodean e incluso la relación epistolar con una mujer. Desde ahí hasta sus ideales vitales, el soporte y aceptación del dolor que resultan temas muy interesantes tratados ligeramente y de forma muy amistosa.

  

El guión de manos de los directores (**Olivier Nakache, Eric Toledano**) es muy dinámico y muestra en parte lo que quiere mostrar. Una banda sonora muy animada y rica que presenta casi todos los episodios alegrando las caras, impresionadas por las increíbles situaciones que van apareciendo. En verdad me parece que lo que se muestra, aunque es posible, no es real, a pesar de la consensuada irreverencia hacia las personas con discapacidad, la relación resulta utópica pero aún así no falsa, además de que solo muestra una de las caras. Sin embargo otro de los grandes méritos es su capacidad -olvidando los tópicos- para no caer en lo fácil. Aunque se trate de un tema serio y difícil tratado como un chiste, creo que su propósito no es enseñar nada, sino contar una historia con cierto tacto y divertir al espectador. 

  

_Driss_ llega a convertirse en imprescindible, porque aunque parezca mentira, _Philippe_ necesita a alguien que diga lo que piensa sin morderse la lengua que no tenga la pegajosa piedad que ahoga su vida como tetrapléjico, ese es el valor que tiene este juerguista y poco a poco empieza a desarrollar cierta sensibilidad, la necesaria para llegar a una sólida relación de amistad. Sin duda congenian por su complicidad y complementariedad se trata de almas completamente distintas, el filme juega hábilmente con ese contraste. La química demostrada en la gran pantalla por estos dos actores se convierte en la evidente clave para un filme tan llano y eficaz.  
  

Basada en una historia real, no avalada por los verdaderos protagonistas. Una de las películas más divertidas del año, donde las dos horas de duración se pasan volando. Trasmite un ligero mensaje pero sin duda lo que más consigue es que pases un rato divertido y agradable de forma muy sutil y desacomplejada.