---
title: 'La Casa del Propósito Especial, La Apuesta - John Boyne'
date: 2010-07-21T06:17:00.000+02:00
draft: false
tags : [Rusia, Palacio de Invierno, Apuesta, Raskólnikov, Zar, San Petersburgo, Novela, Bueno, John Boyne, Literatura, El Niño Con El Pijama de Rayas, La Casa del Propósito Especial]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TEV6f1w6FFI/AAAAAAAAAwk/I9E6uDt9SZI/s320/2006020500280501.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TEV6f1w6FFI/AAAAAAAAAwk/I9E6uDt9SZI/s1600/2006020500280501.jpg)

Tras leer hace unos años _"**El Niño Con El Pijama de Rayas**"_ que disfruté tanto, me animé a leerme la _La Casa del Propósito Especial._ Georgi tras haber salvado al primo del **_Zar_**, es enviado al **_Palacio de Invierno_** (San Petersburgo) como guardaespaldas del _zarevich_. Así es como conoce a la bella Anastasia y se enamora de ella. Esta historia se une con la de _Georgi_ cuando ya es mayor en Londres casado con _Zoya_, y padre de una hija muerta en un accidente de coche. Las historias se van uniendo y acercándose describiendo la vida de _Georgi_, y prometiendo un final inesperado. Me sorprende lo bien que escribe este hombre, sinceramente sus descripciones sencillas y concretas... y su forma de hacer las cosas atractivas hace de la novela un ejemplar muy entretenido. Lo cierto es que tras varios rusos, estoy empezando a querer **San Petersburgo**, especialmente por mi querido **_Raskólnikov_**. Esta historia se convierte en una de las inolvidables. No es un clásico pero si una gran obra.  

Tras haberme leído esta gran obra, llegó a mis ojos "**_La Apuesta_**", esperaba un efecto similar, y más gratuito (cien paginas: corto). Lo ojeé en la libreria, y me lo llevé a casa. La historia es entretenida, aunque empieza perder fuerza progresivamente. Sin embargo no deja de estar bien y ser curiosa simplemente un buen rato, aunque recomiendo mucho más la de "**_La Casa del Propósito Especial_**" sin duda muy buena.