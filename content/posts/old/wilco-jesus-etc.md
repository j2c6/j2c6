---
title: 'Wilco - Jesus, etc.'
date: 2009-03-01T11:09:00.000+01:00
draft: false
tags : [Rock, Wilco, Musica, Rock Alternativo]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/SaphDcjTnCI/AAAAAAAAAaI/c_UPM-E_GCY/s320/wilco_yankee_hotel_foxtrot.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/SaphDcjTnCI/AAAAAAAAAaI/c_UPM-E_GCY/s1600-h/wilco_yankee_hotel_foxtrot.jpg)  

Bueno, traigo una canción completamente distinta a lo que hemos visto antes, es pulso cero, muy tranquila, y una voz, suave, pero increíble. La cancion en si parece que es un poco "evangélica" lo vereis ahora con la letra. Tenemos también violines (se de alguien a quien le encantan los "diálogos" con violines). Yo no puedo decir nada mas solo que la disfrutéis.

  
.  
La traducción esta mas o menos: "_Jesús no llores, puedes contar conmigo, puedes combinar lo que quieras, estaré por aquí. Tenías razón sobre las estrellas, cada una es una puesta de sol. Edificios altos que se tambalean, voces que escapan cantando tristes canciones al son de notas encordadas en tus mejillas, amargas melodías dando vueltas alrededor de tu órbita. No llores, puedes contar conmigo, puedes pasarte por aquí cuando quieras, estaré por aquí..._  
_._  
_Tenías razón con lo de las estrellas, cada una de ellas es una puesta de sol. Edificios altos que se tambalean, voces que escapan cantando tristes canciones al son de notas encordadas en tus mejillas, amargas melodías dando vueltas sobre tu órbita._  
_._  
  
  
  
_._  
_Voces que lloriquean, los rascacielos rascan a coro, tu voz está fumando los últimos cigarrillos, que es lo único que puedes tener dando vueltas sobre tu órbita. Nuestro amor, nuestro amor, nuestro amor es lo único que tenemos. Nuestro amor, nuestro amor es el único dinero de Dios, cada uno es un sol ardiente._  
_._  
_Edificios altos que se tambalean, voces que escapan cantando tristes canciones al son de notas encordadas en tus mejillas. Amargas melodías dando vueltas alrededor de tu órbita. Voces que lloriquean, los rascacielos rascan a coro, tu voz está fumando los últimos cigarrillos, que es lo único que puedes tener dando vueltas sobre tu órbita."_