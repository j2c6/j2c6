---
title: 'Edison, Tesla y el elefante'
date: 2012-04-03T13:47:00.000+02:00
draft: false
tags : [Elefante, Corriente Alterna, Corriente continua, Nikola Tesla, Ciencia, Thomas Alva Edison, Electricidad]
---

[![](http://1.bp.blogspot.com/-aDKMDll9bjA/T3rjDiwStlI/AAAAAAAABVI/DVQF6iFMYkI/s400/Nikola-Tesla3.jpg)](http://1.bp.blogspot.com/-aDKMDll9bjA/T3rjDiwStlI/AAAAAAAABVI/DVQF6iFMYkI/s1600/Nikola-Tesla3.jpg)

Nikola Tesla

En efecto **Edison** era el no va más.  Muy popular y amigo de la aristocracia, el genio que estadísticamente hizo un invento cada quince días, era inalcanzable. Empezó con un sencillo mecanismo para el conteo de votos en 1868. Algunos años después vino el revolucionario fonógrafo, la lámpara incandescente (en realidad únicamente perfeccionada por él)... 1000 patentes. Durante 1880 funda la **General Electric**, todavía vigente en la actualidad. Incluso hizo una '_guerra de patentes_' con los intocables **hermanos Lumière**, generando numerosas aportaciones al cine. En fin que cuando murió en 1931 varias ciudades apagaron las luces en honor de este indiscutible e inspirador científico, empresario e inventor. Y esto es tan solo un detalle.

  

**Nikola Tesla** nació en el Imperio austro-húngaro. Con una trayectoria mucho más discreta que la de Edison estudio ingeniería eléctrica en la universidad de Graz. Con sus primeros trabajos pasaba el tiempo leyendo infinitud de libros aprovechando su portentosa memoria fotográfica. Tras algunos empleos se trasladó a París, y se implicó en una de las empresas de Edison, diseñando mejoras eléctricas de las ideas de Edison provenientes de América. Tras desarrollar el motor de inducción, se trasladó a Nueva York más cercano a Edison donde ya se hicieron notorias sus resoluciones para complicados problemas. Se le ofreció rediseñar los generadores de corriente continua de la compañía Edison, pero le pagaron miserablemente contradiciendo burlonamente el salario ajustado.

  

Tesla dejó a Edison y fundó la '_**Tesla Electric Light & Manufacturing**_'. Pasados algunos años además de desarrollar los rayos X o su motor de inducción, inició la **transmisión inalámbrica de energía**, algo que a muchos sorprenderá todavía en la actualidad. Pero después llegó la corriente alterna.

  

  

Aquí se inició la '_**guerra de corrientes**_' donde Tesla pretendía demostrar la superioridad de 'su' corriente alterna, frente a la continua de Edison. Se hicieron numerosas exhibiciones. La ventajas de la corriente alterna se hacían poco a poco más evidentes al público, como por ejemplo que no era necesario un cable de retorno para transportar esa energía a largas distancias. El mismo Edison harto de envidia, inventó la silla eléctrica que después se usó para practicar la pena de muerte, para darle mala fama al invento de Tesla. Pero eso solo fue la conclusión de la exhibición.

  

  

[![](http://4.bp.blogspot.com/-2iyi1iqiysA/T3rjEWPql-I/AAAAAAAABVM/hzqeCRxdl0c/s400/rostard.jpg)](http://4.bp.blogspot.com/-2iyi1iqiysA/T3rjEWPql-I/AAAAAAAABVM/hzqeCRxdl0c/s1600/rostard.jpg)En 1903 Edisón convocó a cientos de periodistas para demostrar la ferocidad y peligro de la incontrolable corriente alterna. Y para hacerlo patente **electrocutó hasta la muerte a un elefante**. Se le ajustaron unos electrodos de cobre a las patas y los técnicos esperaron la señal de Edison para la matanza. Bien es cierto que hasta lo envenenaron con zanahorias bañadas en cianuro por si no funcionaba el espectáculo. Pero no fue necesario, tras la señal, en unos segundos cayó muerto. _Topsy_ era un elefante que había aplastado a tres cuidadores en el zoo de Coney Island y estaba condenado a muerte. Mil quinientas personas presenciaron la misma.

  

Y aquí termino esta anécdota, recordando cuanto de humano e intangible hay en la ciencia, una historia llena de pasiones, envidias, triunfos, progresos... y matanzas. No es difícil reconocer el mérito de la vida dedicada al progreso de Edison, es más, a poca gente le sonará este macabro episodio, sin embargo el gigante de la ciencia, **Nikola Tesla** ha sido ninguneado y olvidado por la cultura popular (excepto por la unidad del campo magnético medido en **Teslas**, **T**), cuando actualmente la humanidad entera se beneficia de sus descubrimientos, haciendo gala -sin darse cuenta de ello-  de un ilustre personaje como él lo era.