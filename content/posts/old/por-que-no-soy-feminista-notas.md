---
title: 'Por qué no soy feminista, notas'
date: 2018-07-21T13:13:00.000+02:00
draft: false
tags : [etiqueta, feminismo, Jessa Crispin, cultura]
---

\[No me puedo dedicar a copiar citas, tengo que extraer mis conclusiones de esto, sacar 'nuevas' ideas\]

  

Esta mujer, Jessa, reniega de la etiqueta, porque eso es.

  

Los que querían hacer del feminismo una camiseta universal "olvidaron que para que algo sea universalmente aceptado ha de resultar lo más banal, inocuo e inoperante posible. De ahí la pose. \[...\] No entraña ninguna reflexión, ninguna incomodidad, ningún cambio real."

  

Hacen camisetas y las venden con mensajes motivadores. RADICAL FEMINIST vendido en una tienda. Mainstream. "Han convertido el feminismo contemporáneo en un producto más."

  

Un feminismo auténtico "creará su propio sistema religioso, su propio gobierno y economía. El mío no es un feminismo de cambios graduales que se acaba revelando como más de lo mismo. Es un fuego purificador."

  

"Si queremos que una persona o una sociedad haga cambios drásticos, tiene que haber un cataclismo mental o emocional. Una ha de sentir poderosamente la necesidad del cambio antes de llevarlo a cabo por propia decisión. Aunque el feminismo se ha puesto de moda, la auténtica labor feminista de crear una sociedad más justa sigue estando tan poco de moda como siempre lo ha estado."

  

"Si estás rodeada de gente que comulga contigo, no tienes que pensar demasiado. Si estás rodeada de gente que se ve a sí misma igual que tú, no tienes que esforzarte en construir una identidad única. Si estás rodeada de gente que se comporta del mismo modo que tú, no tienes que poner en tela de juicio tus propias decisiones."

  

"La meta aquí es el empoderamiento: la capacidad para vivir la vida que hemos escogido sin centrarnos en lo que podría o debería ser esa vida"

  

"Ahora una mujer puede adoptar la etiqueta feminista sin llevar a cabo una verdadera adaptación política, personal o relacional. Es una chapa en la chaqueta, una pegatina más en el parachoques. El contenido interno permanece intacto. Si el feminismo hiciera realmente más felices a las mujeres y les proporcionase mejores orgasmos, matrimonios más sólidos y más dinero, no haría falta proselitismo."

  

"Siempre serás más fácil compadecernos de alguien por tomar una decisión distinta a la nuestra que tratar de comprender qué la ha llevado a tomar esa decisión. Uno predica para deshacerse de sus propias dudas, no para propagar la buena nueva."

  

[![](https://2.bp.blogspot.com/-Y4UmW2bxa20/W1MT-GJ8v4I/AAAAAAAAX-U/twMTyRWPArIUldXQmD7WlRTG64criKwZgCLcBGAs/s320/Captura.PNG)](https://2.bp.blogspot.com/-Y4UmW2bxa20/W1MT-GJ8v4I/AAAAAAAAX-U/twMTyRWPArIUldXQmD7WlRTG64criKwZgCLcBGAs/s1600/Captura.PNG)

  

"Usamos la lástima como un mecanismo de autodefensa. Sentimos lástima de alguien para no tener que otorgar ningún valor a nada de lo que diga, haga o crea. Para no tener que escuchar sus críticas a nuestras creencias."

  

[![](https://4.bp.blogspot.com/-J29-fwRIktg/W1MUoHXjzQI/AAAAAAAAX-c/G6izxrzCXTAOpx7cED29ZKcnrakIMc5GwCLcBGAs/s320/Captura2.PNG)](https://4.bp.blogspot.com/-J29-fwRIktg/W1MUoHXjzQI/AAAAAAAAX-c/G6izxrzCXTAOpx7cED29ZKcnrakIMc5GwCLcBGAs/s1600/Captura2.PNG)

  

  

"Si entendemos que estamos desperdiciando nuestras vidas en trabajos que contribuyen al mal en el mundo no vamos a tener más remedio que hacer algo al respecto"

  

  

\[Se espera actualización\]