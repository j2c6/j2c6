---
title: 'El Invitado (2012)'
date: 2012-02-25T12:53:00.000+01:00
draft: false
tags : [Daniel Espinosa, Denzel Washington, Cine, 2010, Ryan Reynolds]
---

[![](http://1.bp.blogspot.com/-YZbUjVr0mnc/T0jL4WxSqtI/AAAAAAAABPg/nIV7N_it19Q/s400/rostard.jpg)](http://1.bp.blogspot.com/-YZbUjVr0mnc/T0jL4WxSqtI/AAAAAAAABPg/nIV7N_it19Q/s1600/rostard.jpg)

Mi afán por ir a un buen cine y disfrutar del séptimo arte se me hace cada vez más difícil. Puede ser porque las exigencias cada vez son mayores o porque las elecciones son malas. Confío y espero que solo se trate del segundo caso. No voy a contaros el argumento de '**Safe House**' porque el tráiler ya te cuenta magníficamente la película. Y es que otra de las desesperaciones de las productoras (**Universal** en este caso) y distribuidoras es que tienen que contarlo todo en clips que parecen cortometrajes: hay hasta punto de inflexión y final. El romance, el monstruo, la batalla final, todo aparece en el desgraciado tráiler.

  

El _"pan y circo_" tiene su gracia por el entretenimiento y calidad, pero ya no nos conformamos con tan poco. Aún así reconozco mi satisfacción por volver a ver a la personalidad de **Denzel Washington** en escena, ya que tiene algo que inspira respeto y confianza solo en su porte exterior, confirma que es un gran actor por millonésima vez, pero pienso que se debería dedicarse a películas con grandes aspiraciones y con ello no me refiero a altos presupuestos. Tras '**_Imparable_**' (2010, **Tony Scott**) en la que hacía un papel sencillamente penoso para el nivel de este gran actor, ha entrado en el reparto de **Daniel Espinosa**. La sorpresa a mi parecer ha sido **Ryan Reynolds** (**'Buried'** 2010, **Rodrigo Cortés**), con un modo de interpretar a Matt Weston admirable, algo prometedor, claro que dentro de una película predecible y típica aunque cruda en la violencia que es la especia que le han echado a la sosa receta.  
  

No sé qué le pasa a los _thrillers_ últimamente pero creo que deberíamos dejar de llamarlos así para no mancillar la emblemática etiqueta de _thriller_. Lamento haber sido un poco duro con este ejemplar, ya que tan solo ha sido la inocente gota que colmó el vaso. Si no has visto ninguna película en tu vida, ve a verla, te gustará. Si ya has saboreado el cine alguna vez, no pagues la entrada. Me temo que voy a quedarme un poco atrás con el francotirador, en vez de ir caóticamente a la primera línea de guerra, para así escoger mejor las joyas que todavía sigue ofreciendo el cine y no los resultados de la taquilla.