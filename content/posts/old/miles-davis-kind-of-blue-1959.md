---
title: 'Miles Davis - Kind Of Blue (1959)'
date: 2010-12-23T09:57:00.000+01:00
draft: false
tags : [Kind Of Blue, Miles Davis, Bill Evans, John Coltrane, Lluvia, Musica, Jazz, Trompeta, So What]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TRN2g4F-cAI/AAAAAAAAA2k/BeEh__S63Ao/s1600/miles_davis.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TRN2g4F-cAI/AAAAAAAAA2k/BeEh__S63Ao/s1600/miles_davis.jpg)

_**Miles Davis - Kind Of Blue**_

"So What"

"Freddie Freeloader"

"Blue in Green"

"All Blues"

_"Flamenco Sketches"_  
  

_Jazz..._ no le he dedicado mucho tiempo, pero _**Kind Of Blue**_ es algo superior. Es como si todavía estás con papillas, y te dan un solomillo a la pimienta: ¿Sabrás apreciarlo?. Con motivo del 50 aniversario (2009) del lanzamiento del _LP_ más famoso y reconocido del dueño de la trompeta y concepto del _jazz: **Miles Davis**_. Elogiado por la crítica hasta el punto de ser inigualable, pero qué es la crítica sino la voz de la cultura del tiempo, pues tantas veces ha despreciado la crítica algo que luego se valora como imprescindible... He descubierto que el jazz es como reflexión sin palabras. Escuché _Kinf Of Blue_ un día lluvioso, caían unas lágrimas del cielo, todo nublado un teatro, era casualmente perfecto, el sol creo que no acompaña mucho, era algo más bien nocturno, o lluvioso. Supongo que no escuchas nada importante cuando vas rápido, decidí que era música estática, para escuchar desde un sitio cómodo y lento, o incluso entre los radios de la bici, entre giro y giro lentamente, como una travesía en barca, arrastrando el agua suavemente. La expresividad de las notas... habla en otro idioma, sin traducciones, directamente al cerebro y al subconsciente, todo cambia y ondea levemente. El aire romántico se lo da el saxofón del mismísimo _**John Coltrane**_, ausente en el apellido del autor del disco _Davis_, que lo baña todo en azul desde su reflexiva trompeta con sordina con su carácter tan personal, Miles la hacía hablar. En mi opinión el piano golpeado por _**Bill Evans**_ -otro grande del Jazz-  sea quizás junto con el contrabajo el único acompañamiento, siendo discursos entre una trompeta azul, y un saxo.Entre los discos más atractivos del **_Jazz_**, _Kind Of Blue_ es muy asequible, suele ser la puerta de entrada a esta dimensión nocturna y moderna, puesto que es casi la definición del género, relajada, tranquila -salvo por la tormenta de notas de cierto momento de "_**So What**_"- para aprender...  Debo reconocer que es una música un poco individualista, no está hecha para una conversación y para su oída simultánea, aunque sí en el silencio de la noche, una navidad blanca, frío entre luces artificiales que llenan parte de la oscuridad de color, algunas velas y dos hielos flotando en una copa ancha.