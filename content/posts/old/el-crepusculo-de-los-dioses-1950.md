---
title: 'El crepúsculo de los dioses (1950)'
date: 2012-04-02T21:24:00.000+02:00
draft: false
tags : [1950, Billy Wilder, Anna Q. Nilsson, Sunset Bulevard, Buster Keaton, Ray Evans, Bach, HB Warner, Gloria Swanson, Hedda Hopper, Strauss, Cecil B. DeMille, Cine, Jay Livingston, William Holden, Erich von Stroheim]
---

[![](http://1.bp.blogspot.com/-X-V2kqLfNtw/T3n8yrROe5I/AAAAAAAABVA/oimStFfP3X8/s400/rostard.jpg)](http://1.bp.blogspot.com/-X-V2kqLfNtw/T3n8yrROe5I/AAAAAAAABVA/oimStFfP3X8/s1600/rostard.jpg)Siempre es bueno repasar la historia del cine y ver quiénes inspiraron a los que ahora intentan no caer en el olvido. **Billy Wilder** -aunque no ser lo crea, mi querido lector- era un desconocido para mí, hasta que hace no mucho para mi vergonzosa indiferencia vi '_**Testigo de Cargo**_' un filme lleno de maestría, humor y entretenimiento. Sencillamente eso de Wilder empezó a sonarme y me decidí a ir al grano con '**_El crepúsculo de los dioses_**' que no de 'los ídolos' a lo **Nietzsche**, pero esto es un problema que viene por el cambio de título ya que los americanos sabrán que me refiero a '**Sunset Boulevard**'.

  

Dije que no haría sinopsis pero me parece tan interesante esta curiosa historia que no puedo evitar sumergirme unas líneas en ella. Un guionista (**William Holden**) acabado empieza conociendo a una antigua estrella del cine mudo (**Gloria Swanson**) que delira millonariamente con su viejo poderío y sueña con volver a la gran pantalla por el reclamo del público. La personalidad de la figura del cine -ahora recordada por todos- **Norma Desmond** es demoledora. Su vanidad y narcisismo mezclados con su debilidad y locura aportan una portentosa fórmula para este innovador filme de cine negro.

  

Era uno de los primeros largometrajes que retrataban exactamente eso: el ocaso de las estrellas de cine, de los que triunfaban y luego se veían sumidos en un sueño convertido en pesadilla. De esta cuestión hay mucho de cierto. **Gloria Swanson** fue la actriz encontrada con ese pasado real de triunfo en el cine mudo, aunque antes se consideraron **Mary Pickford, Pola Negri** así como **Mae West**, esto le da un carácter más real al personaje ya que era como interpretarse a sí misma. Además los numerosos cameos contruyen la fachada curiosa de la película: **Hedda Hopper, HB Warner, Buster Keaton, Anna Q. Nilsson, Jay Livingston, Ray Evans** (y la música del dúo) **y Cecil B. DeMille** que aceptó por 10.000 dólares y un Cadillac.

  

Hay un mensaje en este ejemplar relacionado con el cine, su metodología, el lujo, la fama y sobretodo el poder. Muchas almas hay implicadas hay en este mundo y solo hay una triste verdad que olvida el sufrimiento y por otro lado la satisfacción y disfrute que conllevan sus ajetreadas vidas. Esta es una película que trata -seriamente en realidad- esta cuestión, y que hace reflexionar con profundidad al respecto.

  

Sin duda el guión es fantástico. Desde la ironía y la crueldad, hasta la lástima y la comedia. El inicio inaugurado por la narración de un muerto es todo un detalle de **Billy Wilder**. Numerosas frases quedaron grabadas en las cabezas de los espectadores, casi todas provenientes de la boca de Desmond: '_Yo soy grande es el cine el que se hizo pequeño_', '_No hay nada trágico en tener cincuenta años, a no ser que se intente tener veinticinco_' o ' _Señor **DeMille,** cuando quiera estoy lista_'. Un papel memorable que no se acaba de comprender su alcance hasta el solemne y trágico desenlace, su expresividad y gestos y diálogo lo hacen inolvidable. 

  

No quiero dar detalles que destrocen la magia de la película que en realidad me parece espeluznante, empezando por el misterioso personaje de el mayordomo Max (**Erich von Stroheim**). Se podrá comprobar que el secreto está en el detallismo de este director y en el ingenio del guión.  Muchos sentimientos y sensaciones humanas encarnadas en la desgraciada Norma D como el rechazo al fracaso tras el triunfo de la juventud, abocado a la locura; la devoción observada en Max; la decisión entre el amor o el triunfo del dinero y poder en William y algunas cuestiones más.  
  

Un largometraje para disfrutar y observar con lupa. Me temo que no lo he exprimido lo suficiente esta vez, por lo que volveré con más apreciaciones tras masticarlo y meditarlo pausadamente, aún así el primer visionado ha sido solemne e increíble. Nombrar las apariciones sonoras de **Bach** y **Strauss**.