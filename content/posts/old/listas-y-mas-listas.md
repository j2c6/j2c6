---
title: 'Listas y más listas'
date: 2020-04-25T15:04:00.000+02:00
draft: false
tags : [reddit, Cine, Listas]
---

Pedí una lista de recomendaciones:  
  
1\. _**Jean Cocteau Addresses the Year 2000**_ (1962) dir. Jean Cocteau  
2\. _**Battle Royale**_ (2000) dir. Kinji Fukasaku  
3\. _**Cinema Paradiso**_ (1988) dir. Giuseppe Tornatore  
4. _**Submarine**_ (2010) dir. Richard Ayoade  
5\. _**Close-Up**_ (1990) dir. Abbas Kiarostami  
6. _**Jane Eyre**_ (2011) dir. Cary Joji Fukunaga  
7\. _**Three Colors Trilogy**_ (1993-1994) dir. Krzysztof Kiéslowski  
8\. _**La Jetee**_ (1962) dir. Chris Marker  
  
Dije que me gustaban: 'Columbus', 'Her, 'Magnolia', 'Arrival', 'Paterson', 'Dogtooth', 'Take Shelter', 'An Elephant Sitting Still', 'Cold War', 'The Hunt'. Repasando algunas que molaron del siglo 21:  
  
**2000**  
In the Mood for Love  
Gladiator  
Requiem for a Dream  
Code Unknown  
  
**2001**  
Mulholland Dr.  
Royal Tenenbaums, The  

Donnie Darko

La vida de los otros.  
  
  
  
**2002**  
Lejos del Cielo  
Adaptation  
Catch Me if You Can  
Minority Report  
About Schmidt  
  
**2003**  
Lost In Translation  
Kill Bill  
School of Rock  
Good Bye Lenin!  
  
**2004**  
Eternal Sunshine of the Spotless Mind  

El Aviador

Collateral

Las consecuencias del amor

Bourne

  

**2005**

Cache

Match Point

Paradise Now

Broken Flowers

  

**2006**

Children of Men

Volver

Little Miss Sunshine

Apocalypto

  

**2007**

There Will Be Blood

No es pais para viejos

Zodiac

Promesas del Este

Juno  
Sweeney Todd  
Control  
  
**2008**  
Wall-E  
Dark Knight  
Synecdoche New York  
Two Lovers  
Slumdog Millionaire  
The Class  
Gran Torino  
An Education  
Revolutionary Road  
Frozen River  
  
**2009**  
Malditos Bastardos  
Up  
Avatar  
Fantastic Mr. Fox  
Dogtooth  
Distrito 9  
Coraline  
500 días juntos  
Shutter Island  
  
**2010**  
La red social  
Copia Certificada  
Toy Story 3  
Origen  
El discurso del rey  
Valor de Ley  
Scott Pilgrim Contra El Mundo  
Inside Job  
El ilusionista  
Somewehere  
Let me in  
  
**2011**  
El árbol de la vida  
Una separación  
Melancholia  
Hugo  
Drive  
The Artist  
Los descendientes  
Take Shelter  
Oslo, 31 de Agosto  
Midnight in Paris  
Holy Motors  
  
**2012**  
The Master  
Tabu  
Zero Dark Thirty  
Moonrise Kingdom  
Frances Ha  
Lincoln  
Django  
Argo  
Wadja  
  
**2013**  
Under The Skin  
Inside Llewyn Davis  
Her  
12 Years Slave  
Lobo de Wall Street  
Ida  
Gravity  
Blue is the warmest colour  
La Grande Bellezza  
Antes del Atardecer  
Only Lovers Left Alive  
Locke  
Like Father, Like Son  
Blue Jasmine  
Short Therm 12  
  
**2014**  
Boyhood  
Gran Hotel Budapest  
The Babadook  
Inherent Vice  
It Follows  
Birdman  
Whiplash  
Phoenix  
Gone Girl  
Mommy  
Interstellar  
Mr. Turner  
Nightcrawler  
Foxcatcher  
  
**2015**  
Mad Max  
Carol  
Inside Out  
Ex-Machina  
Spotlight  
The Lobster  
Brooklyn  
45 Years  
Room  
Mustang  
Sicario  
Moonlight  
**2016**  
La la land  
Manchester By The Sea  
Elle  
The Handmaiden  
Cameraperson  
Paterson  
Arrival  
Personal Shooper  
OJ Made in América  
Sing Street  
Everybody Wants Some!  
Your Name  
  
**2017**  
Zama  
Phantom Thread  
Lady Bird  
Call Me By Your Name  
First Reformed  
You Were Really Here  
The Florida Project  
The Rider  
Blade Runner 2049  
Coco  
Okja  
  
**2018**  
Roma  
Burning  
La Favorita  
Lazzaro Feliz  
Cold War  
Hereditary  
BlacKKKlansman  
An Elephant Sitting Still  
Private Life  
Shirkers  
  
**2019**  
Parasite  
Once Upon A Time in Hollywood  
Portrait of a Lady on Fire  
The Souvenir  
Marriage Story  
Uncut Gems  
Pain and Glory  
Us  
Knives Out  
Booksmart  
Joker  
  
Es curioso como, a medida que voy creciendo tengo más vistos los años más recientes. Estas películas están sacadas de las listas del siglo XXI de [TSPDT - The 1,000 Greatest Films (Introduction)](http://www.theyshootpictures.com/gf1000.htm)