---
title: 'Consumo frío'
date: 2020-05-15T12:29:00.002+02:00
draft: false
tags : [consumo, twitter, Listas]
---

Y veo Twitter que no es el mundo real, pero sí parte de lo que está pasando en el mundo. Y veo la gente consumiendo cultura, diciendo sus noes y síes en voz alta, justificándolos. Algo que yo he hecho muchas veces por escrito y charlando con mis amigos. Charlando me parece natural, siempre que no des una conferencia que se llame 'Mis películas favoritas del 2019'. Puede que lo que me llama la atención sea una mentalidad anticuada. Creo que en un contexto, criticar una obra, a raíz de algo, aportando algo nuevo, tiene su sentido, pero un montón de críticos por afición diciendo qué ven y luego un sí y un no, es algo que me empieza a poner nervioso. Lo cual coincide con ver los 'defectos' de los demás en uno mismo. Es frío el consumo compulsivo y la crítica rápida.