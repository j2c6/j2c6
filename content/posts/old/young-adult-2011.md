---
title: 'Young Adult (2011)'
date: 2012-07-02T11:33:00.000+02:00
draft: false
tags : [Charlize Theron, Jason Reitman, Cine, Diablo Cody, 2010, Patton Oswalt]
---

Sin duda, el dúo triunfante de '_**Juno**_' -sin tener en cuenta a **Ellen Page**, por supuesto- de **Diablo Cody** y **Jason Reitman** es muy prometedor. La película del 2007 a me gustó muchísimo, las actuaciones eran notables y el guión brillante e ingenioso. '_**Young Adult**_' cómo no, prometía por las expectativas y por una central **Charlize Theron** que ha hecho trabajos dignos de mención. En este caso la protagonista es la versión inversa a la de Juno, alguien muy mayor para lo inmadura que es, muy interesante.

  

La historia está bien enmarcada. El guión no es tan ingenioso como su famoso predecesor pero es más que llevadero, visual y exacto. El personaje de Theron, Mavis, es muy divertido porque odia a todo el mundo y todo le viene mal; pero algo especialmente llamativo de su personaje es su nostalgia por el pasado y el empeño y estrategia por recuperar a su ex-novio. También hace el trabajo sucio de escribir novelas para adolescentes y es divertido ver cómo se basa en muchos comentarios que oye por la calle. Todo hace un visible contraste con la protagonista, especialmente cuando llega a su ciudad natal. Se ve patética su actitud y quieres esperar a ver qué ocurre. Las escenas te dejan ver todo, y cada toma es un detalle que construye al personaje, en eso es excepcional.

  

Otro personaje que acaba congeniando a la perfección con Mavis es Matt Freehauf (**Patton Oswalt**), que también tiene ese poder de decir en todo momento lo que piensa porque está en la curiosa situación en que no tiene nada que perder. Este encuentro casual es un buen recurso del guión, le da un toque de humor y sirve para ir más allá en la vida de los personajes.  Los diálogos son originales y alcanzan momentos de verdadera tensión a lo largo del filme. También se ven las expectativas que se tenían sobre la 'chica popular' y cómo los simples la siguen adorando.

  

Al cabo de una hora te das cuenta de que el relato es estrictamente lineal y en realidad evoluciona poco, en sí atrae pero esperas algo más concluyente. La película acaba y terminas confuso, te preguntas ¿dónde está el cambio? y es insatisfactorio. No sé si cabe esperar siempre un cambio, en fin. La idea es buena, pero tiene poco desarrollo y además está cargada de tópicos, las famosas '_reunions_' estadounidenses y el rollo de la popularidad del instituto. Y a pesar de que la guionista es una experta, esta vez no resulta tan contundente y tampoco trasciende en exceso, desapareciendo el efecto de profundidad entre aparente superficialidad, sin ignorar el intento de conseguirlo. No es el mejor resultado, aunque he entrado de lleno en el largometraje. Esculpe la encarnación de un personaje memorable por su inmadurez, amargura, atractivo y crueldad.