---
title: 'Patatas fritas.'
date: 2017-10-24T10:30:00.002+02:00
draft: false
tags : [zen, multitarea, monoactividad, Música, patatas fritas, melodía, el arte de no hacer nada]
---

¿Cuánta gente conocéis que se pare a oír música (no de camino a ningún sitio, ni ordenando su preciosa casa, tampoco yendo al trabajo, haciendo deporte, cocinando o esperando a que ocurra tal cosa), así, sin multitarea, monoactividad, _únicamente_ escuchando música, quieto, en algún sitio?

  

Porque hay personas que intentan valorar esas cosas, no como esa gente que sólo se acuerda de las patatas fritas cuando tiene delante una hamburguesa, las consideran _un acompañamiento_ y nunca una melodía principal. Patatas fritas con huevos rotos y jamón. NO. Patatas fritas.