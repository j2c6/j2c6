---
title: 'American Gangster (2007)'
date: 2013-02-10T10:44:00.000+01:00
draft: false
tags : [Michael Corleone, Denzel Washington, Ridley Scott, Frank Lucas, Richie Roberts, Steven Zaillian, American Gangster, Training Day, Cine, Eliot Ness, Russell Crowe]
---

[![](http://2.bp.blogspot.com/-nwQnw_YjQtY/URdr-ctIWiI/AAAAAAAABzY/qGjJaEqLPDA/s1600/american_gangster_rostard.jpg)](http://2.bp.blogspot.com/-nwQnw_YjQtY/URdr-ctIWiI/AAAAAAAABzY/qGjJaEqLPDA/s1600/american_gangster_rostard.jpg)

**Ridley Scott** es un  tipo que ha sabido venderse, y de vez en cuando sabe lo que el público necesita y se lo da sin reservas. Dejando a un lado las habladurías fanáticas de que es uno de los mejores directores, cosa que no tengo demasiado clara, sí soy un gran admirador de su cine de entretenimiento. '_**American Gangster**_' tiene sencillamente un reparto de lujo, aunque ya empiezo a darme cuenta de algunos de los puntos débiles de **Russell Crowe**, que tampoco es tan polifacético, casualmente en esta película me parece fantástico. Si una de mis debilidades no fuera admirar a **Denzel Washington** -aunque hace algunos años que no participa en una película decente- también diría que en esta obra y teniendo en cuenta '**Training Day**' (2001, Antoine Fuqua) como una de sus actuaciones culmen, está sensacional, y le sienta muy bien ese traje de gangster con un código férreo de conducta. Tal vez el único problema es que la historia es un poco lo de siempre, pero ya que la realización del guión por una de las garantías como es **Steven Zaillian** es excelente, se nos olvida (podríamos mirar a **Frank Lucas** como **Michael Corleone**, o la brigada brutal al estilo **Eliot Ness**).

  

Además Scott nos introduce en el mundo del tráfico y el crimen con imágenes muy visuales e impresionantes, como las distintas tomas de consumidores incondicionales, la escena inicial de la quema de algún deudor suplicante, la enorme plantación, etc. También sabe crear escenas de acción con mucha tensión como cuando entran en el edificio donde se corta el 'material' y entran por sorpresa, el niño jugando con la pelota y esos elementos. Por lo demás son las fuertes personalidades de los dos protagonistas las que te mantienen despierto, sino lo hacían ya las espectaculares imágenes del ambiente o la esforzada ambientación de los sesenta-setenta. Ambos tienen un _modus operandi_  muy característico, no admiten la doblez: tanto en el caso de **Richie Roberts** devolviendo el famoso millón de dólares, o los distintos terribles y autoritarios enfados de F. Lucas. 

  

Es una gozada de película, sorprende e impresiona aunque no puedas evitar acordarte de otras tantas míticas. Un trabajo excelente con mucho ritmo aunque extensa, una gran obra de entretenimiento absorbente.