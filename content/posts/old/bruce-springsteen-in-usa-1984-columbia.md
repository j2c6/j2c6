---
title: 'Bruce Springsteen - ''Born in the U.S.A'' (1984, Columbia)'
date: 2012-02-01T12:34:00.000+01:00
draft: false
tags : [Música, Bruce Springsteen, 1980]
---

[![](http://2.bp.blogspot.com/--9Qf--oxYFg/TykjN5bBnEI/AAAAAAAABME/CtOTr8thAus/s400/born+springsteen+rostar.jpg)](http://2.bp.blogspot.com/--9Qf--oxYFg/TykjN5bBnEI/AAAAAAAABME/CtOTr8thAus/s1600/born+springsteen+rostar.jpg)Tras el disco quizá más oscuro y profundo de '**Nebraska**', **Bruce Springsteen** decidió escoger de entre más de cien temas de su composición, doce canciones para su nuevo disco, ahí nació '**Born in the U.S.A**'. Este disco fue importante para consolidar definitivamente a Bruce en el mundo musical, compitiendo  y compartiendo cabezas de lista ese mismo año con **Madonna**, '_**Like A Virgin**_' y el rey del pop **Michael Jackson** '_**Thriller**_' dos gigantes de las ventas. **15 millones** de copias solo en Estados Unidos.

  

En su pegadizo _rock'n'roll_ ochentero (donde comienza a usar sintetizadores) aparentemente comercial hay mucho más que acordes básicos y canciones que enloquecieron a la juventud durante las décadas posteriores, él no fue un producto del reclamo de las discográficas, tenía actitud y eso es lo más importante del rock. Sin actitud, sin tener nada que decir y sentirlo de verdad las canciones "nacen" muertas.

  

La icónica carátula del disco con la foto de **Leibowitz** es tan solo la presentación de un hito. El LP del '**Boss**' ha confundido ciertamente, puede parecer un disco patriótico de ideas felices, pero detrás de la desgarradora voz de Springsteen se esconden verdades que llevan en el alma incontables coetáneos suyos. Por eso es una pieza que decenas de millones de personas no olvidarán nunca, y se convertirá en la esencia de una etapa de su vidas.   
  

  
  

'**_Born in the U.S.A_**' 

Abre el disco y lo primero que suena es una enérgica batería, y el teclado: tan tantantitan... con el ritmo lineal y potente tarareado cien veces. Fue canción promocional en las elecciones para representar a cierto partido, pero la realidad es que se trata de una crítica irónica al sueño americano, uno de los himnos más recordados de la historia. Sufrimiento y desolación en la guerra e ideales rotos. 

  

'**_Cover Me_**'

Habla de la inquietud que siente al ver el mundo devastado y cruel que le rodea. Este fue uno de los grandes éxitos del momento posicionándose en el nº7 de **Billboard**. 

  

'**_Darlington County_**'

Uno de los aires más festivos del disco. Aquí suena el sonido clásico de la **E Street Band** algo desenfadado y amistoso, sino escuchad los coros que rodean la voz de Bruce. Personalmente una de mis favoritas. "_En un descapotable con rock'n'roll sonando a tope, cantando: sha la la..._" 

  

'**_Working on the Highway_**'

Del representativo espíritu luchador de la clase obrera. Ellos son los verdaderos héroes americanos que se matan a trabajar y sueñan sencillamente con algo mejor. "_Todo el día sostengo una bandera roja y miro el tráfico al pasar por mi lado, guardo la imagen de una chica preciosa en mi cabeza._"

  

'**_Downbound Train_**'

Algo relativamente más tranquilo, Bruce habla desde la experiencia. Es una versión de 'Working on the Highway'. También es una historia de la clase obrera, pero no tan animada. La tristeza y la amargura de uno que se siente un perdedor y solo, como "_un tren a la deriva_", la vida le parece un trabajo sucio.

  

'**_I'm on Fire_**'

Íntima de Bruce habla de los instintos y la pasión, todo en un susurro. Una guitarra punteada  casi invisible y una percusión casi inexistente donde todo se concentra en la voz de Springsteen.

  

'**_No Surrender_**'

Otro de los grandes temas. Vuelve la alegría, fuerza y optimismo de Bruce: "_sin rendición_". Una orquesta a un ritmo muy definido y casi frenético. De mis preferidas. La voz parece especialmente sincera y es como si entendieras lo que te esta gritando: "_Porque hicimos una promesa y juramos que siempre la recordaríamos: sin retirada, sin rendición._"

  

'**_Bobby Jean_**'

Una balada diferente. La preciosa '**_Bobby Jean_**' habla de amistad y buenos sentimientos hacia un amigo perdido. Se piensa que se refiere a **Steve Van Zandt** y su marcha de la **E Street Band.**"_Fui a buscarte a tu casa, tu madre me dijo que te habías ido \[...\]  quizá estés en algún lugar de la carretera y me oigas cantar esta canción, si es así sabrás que estoy pensando en ti y en todas las millas que nos separan_" a esto se añade el inconfundible _solo_ de saxo de **Clarence Clemons**.

  

'**_I'm Goin' Down_**'

La melancolía otra vez, una historia de amor en lo que hay algo que no funciona. Es clara y evidente la depresión: "_I'm goin' down_". Los teclados han sido fundamentales en estas tres últimas canciones tan emotivas.

  

'_**Glory Days**_'

Vuelve el sonido legendario de viejas historias, buenos recuerdos: "_Glory Days_". El rasgueo de las guitarras, los redobles, el teclado, las voces quieren revivir los mejores momentos. No sé cómo pero es exactamente lo que me pasa con esta canción. El tono alegre de Bruce que canta casi hasta reventar.

  

'**Dancing in the Dark**'

Este temazo llegó al nº 1 en los Estados Unidos. Una de las favoritas de muchos seguidores, es de lo más "comercial" del disco, se sumó a las listas de baile de los ochenta, con el coro de sintetizadores y efectos representativos de la década. Algo que también se ve en el videoclip, se adaptan a nuevos tiempos.

  

'**_My Hometown_**'

Cierra el Lp, con suavidad en esta canción de historias perdidas en las poblaciones de su país. ¿Cómo olvidar tu ciudad natal?. En este tono de "se acabó la fiesta" se despide reflexivo, y todo lo demás.

  

Sin duda uno de los grandes ejemplares de todos los tiempos, también reflejado en el índice de ventas, **Bruce Springsteen** se encuentra entre los más grandes. Cambió el mundo, como la música cambiará el mundo.

  

En recuerdo del fallecido 

el 18 de junio de 2011: 

**Clarence Clemons**

saxofonista de la **E Street Band**