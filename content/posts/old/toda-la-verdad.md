---
title: 'Toda la verdad'
date: 2020-04-01T09:24:00.001+02:00
draft: false
tags : [vitrocerámica, poesía cotidiana]
---

A los pies de la  
vitrocerámica  
después de tu sesión de  
hatha yoga  
donde te estiras, te extiendes  
y crezco a tu sombra  
seguro y feliz,  
nos decimos toda la verdad.