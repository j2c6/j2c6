---
title: 'El gran dictador (1940)'
date: 2012-06-09T19:22:00.000+02:00
draft: false
tags : [Crítica, Nacionalsocialismo, Cine, 1940, Sátira, Charles Chaplin]
---

[![](http://1.bp.blogspot.com/-bXPfMRmzRuc/T9OGA7V8-XI/AAAAAAAABes/J1dSvEPtxRs/s400/rostard.jpg)](http://1.bp.blogspot.com/-bXPfMRmzRuc/T9OGA7V8-XI/AAAAAAAABes/J1dSvEPtxRs/s1600/rostard.jpg)**Charles Chaplin**. ¿Qué puedo decir? Todavía no soy un experto en su filmografía, ya que para ello no solo basta con una espontánea reacción tras un primer visionado, sino un profundo análisis de su trayectoria y la contemplación de su trascendental reflexión acerca de la vida y su belleza que sin duda deslumbra en su obra. Solo recordaré dos de mis favoritas como son: la muda y sublime '**_Luces de la ciudad_**' que no detallaré hoy y la melancólica '_**[Candilejas](http://rostard.blogspot.com.es/2012/03/candilejas-1952.html)**_' de la que ya tenéis un modesto comentario.

  

Si el portentoso genio fue olvidado por la academia, recibiendo simplemente un Oscar casi honorífico no es casualidad. Su influencia en el mundo del espectáculo es completa. '**_El gran dictado_**r' resulta ser una sutil crítica directa al régimen dictatorial de Hitler en Alemania, no es necesario rememorar detalles, sin embargo quizá el público deba saber que Chaplin no hubiera filmado esta obra de saber realmente qué ocurría en los campos de concentración. Tal vez esto sea una tímida explicación de porqué trata la crueldad nazi con tanta suavidad, no se sabía lo que se supo después.

  

La película comienza con unas escasas escenas bélicas, que lo primero que me hicieron pensar fue en que me había equivocado de largometraje. A mí no me hace ninguna gracia el típico número cómico de equivocaciones y torpezas la versión patética del género humano. Así pues durante los cinco primeros minutos me pareció trivial, hasta que están en el avión y en un instante el oficial Schultz hace una referencia a cómo debe de estar su casa y su madre regando los narcisos y no sé qué, donde me dí cuenta de que había empezado el filme.

  

Su papel de Hynkel y su idioma inventado que emula el afilado alemán de Hitler es sensacional, su expresividad da mucho juego y casi hace comprender lo que esta diciendo con su voz mediante gestos duros y afilados. Hynkel resulta ser -dejando a un lado toda la doctrina antisemita- un desastre más, desmitificándolo poco a poco. La escena con la bola del mundo es un buen ejemplo.  Posteriormente Chaplin da una pincelada inocente a la situación del gueto judío. Allí crea la historia del barbero donde se ven todavía retazos del viejo **Charlot**. 

  

El alma de Chaplin está en los diálogos, pero se centra más en contar una historia y en ironizar con el régimen de las dos cruces. Hay algún momento dramático con Hannah (**Paulette Godddard**) que era en ese momento la esposa del director, pero no se hace especial hincapié en ella, simplemente le da vida al guión. El cóctel es humor y drama, el contraste no es tan poderoso como en otras obras, pero la escena del pudin y las monedas es un clásico.

  

Tras algunas casualidades el barbero acaba en el sitio de Hynkel y se pronuncia el famoso discurso. En estas palabras está toda la intención de '_**El gran dictador**_' que es una excusa de casi dos horas. De manera muy emotiva Chaplin se pronuncia y habla de la ambición y de cómo los gobiernos y el poder corrompen, así como de la libertad inscrita en cada el en corazón del hombre y de muchas otras cosas. Sin duda merece un análisis por parte del espectador. Hay muchas ideas de este discurso que evidentemente tienen total efecto y vigencia sobre la actualidad:el problema que narra el filme lleva causando estragos desde la eternidad.

  

Personalmente no me parece el plato fuerte de este cineasta tan prolífico y magnánimo. Sí me resulta muy interesante su reflexión política, y su visión poética de la vida. La música es otro de los elementos que me llevaría conmigo ya que posee ese atractivo encandilador que hace lo reluciente aún más precioso. El propio Hitler exigió el visionado del largometraje en dos ocasiones y el mismo Charles C. esperó su opinión -que nunca llegó-  con especial interés.