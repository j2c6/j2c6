---
title: 'John H. Newman - El saber como fin en sí mismo'
date: 2011-03-29T14:10:00.000+02:00
draft: false
tags : [Universidad, Irlanda, La Red Social, John H. Newman, Aristóteles, Cicerón, Discursos]
---

He aquí otro de los grandes del siglo XIX. Los _**discursos**_ _**pronunciados en Dublín**_ _**en 1852**_ hablan de un ideal de la educación universitaria. El **Cardenal** **Newman** además de ser un gran teólogo es considerado un magnífico humanista que además estoy descubriendo. En este discurso habla sobre **la importancia del saber, _el fin de la universidad_** y otros aspectos relacionados.

  

[![](http://2.bp.blogspot.com/-XV0pB42sCDA/TZGhJVMP2dI/AAAAAAAAA8k/MTrP0nPfVs0/s400/socialnetwork_window.jpg)](http://2.bp.blogspot.com/-XV0pB42sCDA/TZGhJVMP2dI/AAAAAAAAA8k/MTrP0nPfVs0/s1600/socialnetwork_window.jpg)

Fotograma de "_La Red Social _"

Al principio dice la importancia de la consideración de todas las ciencias, "_ya que conceder prominencia indebida a una ciencia supone injusticia con otras, y descuidar o preferir a unas es apartar a otras de su propio objeto._ \[...\]_Toda ciencia habla de modo diferente cuando se la toma como parte de un todo, a como habla cuando se la toma en si misma, sin la protección, por así decirlo de las demás._" Esto también atañe a los del famoso dilema de "ciencias o letras" y todas las bromas y discusiones que existen al respecto. La pregunta al fin y al cabo podría ser: "_¿Cuál es el fin de la educación universitaria y del saber liberal o filosófico que pienso que se debe impartir?_" a lo que Newman se responde: "_El saber es capaz de ser su propio fin_", el fin en sí mismo como indica el título. Claro que a partir de ahí surgen otras cuestiones. El saber es atrayente, se "_considera óptimo, y equivocarnos o errar, nos parece una desgracia_" según Cicerón (otro). Consideremos también que "_entre las posesiones son útiles las que producen una ganancia y son liberales las que tienden a ser disfrutadas. Por lucrativas entiendo las que rinden unos ingresos; por disfrutables, las que nada proporcionan  excepto el uso mismo que se hace de ellas_" añade Aristóteles. El saber es una de estas últimas. 

  

"_Resulta más correcto  hablar de una universidad  como un lugar de educación más que de instrucción._" Actualmente las universidades mediocres \-independientemente del puesto que ocupen en el _ranking_\-  solo se preocupan de la parte instructiva, convirtiéndola en un medio de aprendizaje puramente profesional, pero las personas no somos solo trabajadores, somo más que eso, y por ello estoy de acuerdo con que una universidad así me parece insuficiente. Claro que el mundo  se ha vuelto, (o continua siendo) completamente pragmático, por lo que como lo más importante es lo que sirve de modo tangible, el resto se desprecia por distintas razones: "no entra en el programa", "falta tiempo"... Así el hombre se convierte en un código penal, en un compás, o en un bisturí. 

  

"_Una educación es una palabra más elevada. Implica una acci__ón que afecta a nuestra naturaleza intelectual y a la formación del carácter._" Claro que a la hora de la verdad podríamos decir que muchos de los "defensores del saber liberal" y por ello quizás "no utilitaristas", que conocían sus consecuencias luego se han visto criticados por sus propias palabras, han fracasado en sus pretensiones, se han vuelto contrarios a la virtud y a la humanidad. Entonces ¿para qué?¿Ha cumplido su tarea el "saber liberal"?. "_La filosofía por ilustrada y profunda que sea, no proporciona dominio sobre las pasiones, ni motivos influyentes, ni principios vivificadores. La educación liberal no hace al cristiano, ni al católico, sino al caballero. Es bueno ser un caballero, como es bueno también poseer un intelecto cultivado, un gusto exquisito, una mente sencilla, equilibrada y desapasionada, y un comportamiento noble y cortés en los asuntos de la vida. Son, todas ellas, cualidades de una saber hondo, son el fin de una universidad._\[...\] _no constituyen garantía de santidad ni de recta conciencia, y pueden asociarse a gente mundana, libertina y sin corazón, que envuelta en esas cualidades, puede resultal agradable y atractiva._\[...\]_La educación liberal, considerada en sí misma, es sencillamente el cultivo del intelecto como tal, y que su objeto es, ni más ni menos, la excelencia intelectual._ \[...\] _Hay una belleza física y una belleza moral, hay una belleza de persona y una belleza de nuestro ser moral, que es la virtud natural, y de igual modo hay una belleza y una perfección del intelecto."_ 

  

Me ha interesado mucho este discurso, -vendrán más- pero es muy interesante la idea del saber en sí mismo, el verdadero aprendizaje y el dilema de cómo la educación en la universidad Newmaniana de las personas no contempla en realidad la de las personas rectas, leales, etc.   
  
  

_Discursos sobre y el fin y la naturaleza de la educación universitaria._  
John H. Newman. EUNSA 1996