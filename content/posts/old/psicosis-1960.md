---
title: 'Psicosis (1960)'
date: 2012-02-04T19:25:00.000+01:00
draft: false
tags : [1960, Alfred Hitchcock, Janet Leigh, Cine, Joseph Stefano, Anthony Perkins, Bernard Herrmann, Robert Bloch]
---

[![](http://1.bp.blogspot.com/-hBr9iAQaYjY/Ty14AdaQusI/AAAAAAAABNA/Z8EfuLl2en0/s400/psycho+rostards.jpg)](http://1.bp.blogspot.com/-hBr9iAQaYjY/Ty14AdaQusI/AAAAAAAABNA/Z8EfuLl2en0/s1600/psycho+rostards.jpg)Marion Crane (**Janet Leigh**) decide fugarse con 40.000$ en efectivo de su oficina. En su nerviosa fuga para a pasar la noche en un motel, cuyo servicio solitario regenta Norman Bates (**Anthony Perkins**). Todo intriga.

  

**Alfred Hichtcock** el dueño del cine de suspense, cuando se enteró de novela semejante (**Robert Bloch**) adquirió los derechos por 9.000 dólares, y a continuación compró todos los ejemplares que pudo para que desaparecieran del mercado, y así encerrar un secreto misterio en el guión de **Joseph Stefano**. Posteriormente con su equipo de producción rodó una barata pero portentosa cinta en blanco y negro titulada '**_Psycho_**', de presupuesto 800.000. Finalmente el terrorífico largometraje recaudó cuarenta millones en Estados Unidos.

  

Además del icono del cine **A.H**, la banda sonora de estridentes y paranoicos violines de **Bernard Herrmann** se llevó la fama y atemorizó los oídos de los espectadores del mundo a lo largo de las décadas que siguieron al filme. No cabe ninguna duda de cómo controla **Hitchcock** al público. Un guión lleno de misterio, personajes interesantes y también misteriosos en grandes interpretaciones de **Leigh** y **Perkins**. Los planos de suspense, la trabajada puesta en escena, las miradas escrutadoras que no están seguras de lo que puede ocurrir y la memorable escena en la ducha con los citados e infernales violines. Nunca pensé que un instrumento tan romántico y pasional pudiera esculpir el terror.  
  

Considerada una obra maestra del cine por su diligencia y sincronización para conservar la atención y tensión del afortunado o desafortunado que observa atónito cada escena, '**_Psicosis_**' ha sido modelo para el cine negro y los posteriores _thrillers_ que seguirán entreteniendo a las butacas de los cinco continentes. Intocable en la historia de cine, toda una referencia. Uno de los filmes más sonoros del maestro del séptimo arte. Alto entretenimiento, aunque algo importante es no conocer el final.