---
title: 'Radiohead - No Suprises'
date: 2009-03-14T17:39:00.000+01:00
draft: false
tags : [Rock, Musica, Rock Alternativo, Radiohead]
---

  

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/SbvghxmjhmI/AAAAAAAAAaw/d_qHsDICWbg/s320/RadioheadNoSurprise.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/SbvghxmjhmI/AAAAAAAAAaw/d_qHsDICWbg/s1600-h/RadioheadNoSurprise.jpg)

[](http://4.bp.blogspot.com/_VT_cUiG4gXs/SbvghxmjhmI/AAAAAAAAAaw/d_qHsDICWbg/s1600-h/RadioheadNoSurprise.jpg)Llevo oyendo a Radiohead bastante tiempo, pero ahora es cuando estoy empezando a escucharlo. Lo que más me gusta del grupo, es la guitarra que tiene: es como si cada nota fuera un caramelo. En esta canción se ve claro, en general las canciones de **Radiohead** son tristes y melancólicas, pero hacen boom... Esta canción "_**No Suprises**_" solo se puede definir con una palabra y es : magia. En seguida os daréis cuenta...