---
title: 'Huawei'
date: 2019-06-24T09:12:00.001+02:00
draft: false
tags : [5g, comercio, tecnología, usa, china, google, telefonía, huawei]
---

Tras el veto de Estados Unidos, las ventas de Huawei se desploman. Intel, Qualcomm, Broadcom, Xilinx Inc, Infineon Technologies, Micron Technology, Western Digital y otros además de Google-Android (escribo esto desde una plataforma de Google con un Google Chrome como navegador) le dan la espalda. Huawei se queda solo.

  

Pienso, que aunque esto sea un batacazo, va a ser perfecto para el mercado. Después de un par de años jodidos para Huawei, habrán forzado la competencia real a Google. Una alternativa a Android, seguro. Y es como que lo que siempre ha sido tan difícil, conseguir un terminal en el que Google sea ajeno, parece cada vez más cercano.

  

Creo que los que apuesten por Huawei, los que se arriesguen, se pueden alegrar en un futuro. ¿Apuestas por Huawei?