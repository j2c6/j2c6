---
title: 'Adaptation. (El ladrón de orquídeas) (2002)'
date: 2012-04-30T19:20:00.000+02:00
draft: false
tags : [Susan Orlean, Nicolas Cage, Charlie Kaufman, Spike Jonze, Meryl Streep, Orquídeas, Cine, Chris Cooper, 2000]
---

[![](http://4.bp.blogspot.com/-9csIcTZWP_s/T57JfiYDKdI/AAAAAAAABbs/JzEOKHCx2GE/s400/adaptation.jpg)](http://4.bp.blogspot.com/-9csIcTZWP_s/T57JfiYDKdI/AAAAAAAABbs/JzEOKHCx2GE/s1600/adaptation.jpg)

Es una película bastante extraña, como un ensayo diría yo. Para empezar, es como una historia dentro de la misma historia. Me explico, el guionista se llama igual que el protagonista, que a su vez también se dedica a escribir guiones. A primeras es una idea original. La voz en off que va narrando los problemas existenciales de un guionista frustrado, es el típico papel sincero que utilizaría **John Cusack**, pero que se ha quedado **Nicolas Cage**.

  

Este tipo de papeles de personajes más o menos inteligentes pero que en cualquier caso chocan contra los obstáculos de la vida porque no se sumergen en los famosos métodos de evasión, me gusta. Puede parecer deprimente, pero me temo que es exactamente un buen reflejo de la situación. **Charlie Kaufman**, ha triunfado escribiendo el libreto de '_**Cómo ser John Malkovich**_'. Está repleto de inseguridades y eso es lo que le pasa con su trabajo: ahora no sabe cómo continuar su próxima película que está basada en un libro sobre orquídeas escrito por **Susan Orlean** (**Meryl Streep**). Y así es en realidad -y no me repito más- '_**Adaptation. El ladrón de orquídeas**_' está basada en un libro de la auténtica Susan Orlean y la escribe el omnipresente Kaufman.

  

De ese modo es como presenta la película. Es como que te mandan inventar una historia y tú escribes sobre que no sabías qué escribir y cómo hacerlo, y vas ensayando en directo y cuando acaba tu intento tienes el resultado. Esa es la idea original del largometraje y creo que merece un respeto a pesar de lo surrealista que la encuentro. No es dramática, los personajes son bastante estáticos y lo reconoce. Posteriormente se da cuenta de que tiene que existir alguna clase de evolución o cambio y así ocurre con un semi-emocionante final totalmente desconocido a la marcha reflexiva de esta extraña divagación.

  

'_Una película sobre flores_'. La entrada y presentación de las orquídeas como flores fascinantes, misteriosas e inalcanzables en algunos casos como la '_orquídea fantasma_', ha acabado por interesarme. Resulta que con la excusa se acaba por dar una visión general aesta especie y a su 'enloquecedor' atractivo. Susan Orlean, investiga sobre un tal John Laroche (**Chris Cooper**) que es un coleccionista. En realidad ella busca una cosa que le haga apasionarse tanto por ella como a John Laroche las orquídeas, y está ensimismada con esa búsqueda; es como un apasionamiento por la propia pasión. Escribe sobre una vida fanática y le gustaría vivir una así. Chris Cooper (Oscar por este papel) hace un personaje muy informal y extravagante, es muy suyo y de su vasto conocimiento de orquídeas, que le permite ver la vida con una visión muy particular.

  

El resultado es bastante curioso. Quién lo vea como un intento puede reírse del director **Spike Jonze**, ya que no es su primera originalidad. Es una idea bastante novedosa, pero creo que no ha despertado todo el encanto de una verdadera excentricidad. La película - a pesar de que parece hecho a propósito- no es muy dinámica, tiene algún interés pero le falta consistencia y lo que exhibe como un ensayo programado acaba por convertirse en un  raro intento.