---
title: 'Fargo (1996)'
date: 2012-01-22T14:35:00.000+01:00
draft: false
tags : [Ethan Coen, Steve Buscemi, 1990, Peter Stormare, Harve Presnell, Joel Coen, Cine, William H. Macy, Frances McDormand]
---

[![](http://3.bp.blogspot.com/-lyYJak-wb78/TxwP_Npp3yI/AAAAAAAABH0/unyPM168kY4/s400/fargo%252C+rostards.jpg)](http://3.bp.blogspot.com/-lyYJak-wb78/TxwP_Npp3yI/AAAAAAAABH0/unyPM168kY4/s1600/fargo%252C+rostards.jpg)

La ganadora del Oscar: ** Frances McDormand**

Jerry Lundegaard (**William H. Macy**) decide contratar a Carl Showalter (**Steve Buscemi**) y a Gaear Grimsrud (**Peter Stormare**) para que secuestren a su mujer y así obtener el precio del rescate que corre a cuenta de su millonario suegro (**Harve Presnell**). Un plan fantástico que podría salir bien si no existiera la ley de Murphy. Marge Gunderson (**Frances McDormand**)  lleva este caso.

  

Sin duda la idea de planear el secuestro de su propia mujer llama la atención, concretamente si está basada en hechos reales. El transcurso de la película es "reflexivo",  las escenas acontecen casi sorprendentemente con unas interpretaciones estelares. Los papeles son realmente curiosos ya que a nadie de los que aparecen --a pesar de ser perfiles corrientes-- en esta hora y media de largometraje se le podría calificar de normal. ¿Quién en esta vida es normal? El guión de los brillantes hermanos **Coen** ('**El gran Lebowski**', 1998) es un corrosivo cóctel de humor negro, ironía y crueldad. Es intrigante cómo hacen digerir al espectador la tragedia y la violencia de las escenas como si se tratara de una comedia, todo un género.  
  

Habría que hablar de la trayectoria de los surrealistas **Joel** y **Ethan****Coen**, y de cómo arrancaron los **Oscar**s a la academia, ya que han contado lo mismo de siempre. Su cine (dentro de su peculiaridad) está muy estudiado y medido. Un estilo que tiene muchos seguidores y que he de estudiar detenidamente. Es una completa burla al que está frente a la pantalla, que es absorbido por una conducción magistral y temeraria por una autopista inflamable. Esto es lo que tienen, hacen magia.