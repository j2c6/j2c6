---
title: 'Agua en Marte'
date: 2018-07-27T09:37:00.002+02:00
draft: false
tags : [Marte, planetas, vida extraterrestre, espacio]
---

Sí, ahora que se ve Marte en el horizonte, anuncian que italianos han encontrado un lago de 20 Km en Marte. ¿Habrá microbios dentro?

  

Es como que a la humanidad apenas le afecta según hablo con la gente, pero qué pasada.  
  
Sobre qué hablamos. Qué escribimos. Qué nos importa y qué queda. Pienso en el día a día.  
  
si escribo todo en minúsculas, será como ir en zapatillas de andar por casa. lo que pasa es que tengo la sensación de estar diciendo lo que diría cualquiera acerca de cualquier cosa. echo de menos las pirulas.  
  
la gente trabaja, está en casa, ve series, cotillea lo que sea en redes sociales, se va de vacaciones, vuelve al trabajo, sale, queda, se queja, y otra vez. tengo ganas de quejarme sobre eso, pero ¿qué hago yo?