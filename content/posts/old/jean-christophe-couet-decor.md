---
title: 'Jean-Christophe Couet, DECOR, Росфото'
date: 2019-09-09T15:30:00.000+02:00
draft: false
tags : [photography, Росфото, Rusia, expo, Jean-Christophe Couet]
---

  

  
[![](https://1.bp.blogspot.com/-R4nXws8CvbQ/XXY_g4JmuwI/AAAAAAAAecA/YJHXc5D9Bt8mCKM1qRKsxTD-WVrIDW5VQCLcBGAs/s320/2019-07-06_bc08cc86-a002-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-R4nXws8CvbQ/XXY_g4JmuwI/AAAAAAAAecA/YJHXc5D9Bt8mCKM1qRKsxTD-WVrIDW5VQCLcBGAs/s1600/2019-07-06_bc08cc86-a002-11e9-99c5-c65302bf1052.jpg)

  

  
[![](https://1.bp.blogspot.com/-py6yReLVKV4/XXY_gh8d1XI/AAAAAAAAeb8/hTY5j8omi9kMBE9Z7ZQQsb-4ilIAQmM6gCLcBGAs/s320/2019-07-06_0855b33e-a011-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-py6yReLVKV4/XXY_gh8d1XI/AAAAAAAAeb8/hTY5j8omi9kMBE9Z7ZQQsb-4ilIAQmM6gCLcBGAs/s1600/2019-07-06_0855b33e-a011-11e9-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-qOZqaEkaotE/XXY_eF-dLII/AAAAAAAAebg/Ff5DecAh3es-MAafv55gXhRboE3iyxqagCLcBGAs/s320/2018-05-23_3f988902-5e54-11e8-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-qOZqaEkaotE/XXY_eF-dLII/AAAAAAAAebg/Ff5DecAh3es-MAafv55gXhRboE3iyxqagCLcBGAs/s1600/2018-05-23_3f988902-5e54-11e8-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-B8ZCUL5ePlE/XXY_eKdSR7I/AAAAAAAAebc/ex85BfSYeiMznj3YC3CZNAllp4aqd3eTQCLcBGAs/s320/2018-05-23_917359cc-5e55-11e8-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-B8ZCUL5ePlE/XXY_eKdSR7I/AAAAAAAAebc/ex85BfSYeiMznj3YC3CZNAllp4aqd3eTQCLcBGAs/s1600/2018-05-23_917359cc-5e55-11e8-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-Xh4SZKWqIfE/XXY_eJBma-I/AAAAAAAAebk/mvoqrIc8Z9gh3_TxfixPY7czpgoAI7-bgCLcBGAs/s320/2018-05-23_bee2692c-5e53-11e8-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-Xh4SZKWqIfE/XXY_eJBma-I/AAAAAAAAebk/mvoqrIc8Z9gh3_TxfixPY7czpgoAI7-bgCLcBGAs/s1600/2018-05-23_bee2692c-5e53-11e8-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-868X4lZgLC0/XXY_fBTiVQI/AAAAAAAAebo/1ZrpTrIGaNUhBDtwbZjrJOBTDU4rvGeAQCLcBGAs/s320/2019-04-23_761aec3e-659e-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-868X4lZgLC0/XXY_fBTiVQI/AAAAAAAAebo/1ZrpTrIGaNUhBDtwbZjrJOBTDU4rvGeAQCLcBGAs/s1600/2019-04-23_761aec3e-659e-11e9-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-zkQa3vFNBVM/XXY_ftSRv-I/AAAAAAAAebs/C7RXJSR5SJgn6xYGhQJ4NF-ztv5TshSZgCLcBGAs/s320/2019-04-23_b7970a88-659b-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-zkQa3vFNBVM/XXY_ftSRv-I/AAAAAAAAebs/C7RXJSR5SJgn6xYGhQJ4NF-ztv5TshSZgCLcBGAs/s1600/2019-04-23_b7970a88-659b-11e9-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-4r0XJk8qf6U/XXY_fh_iT6I/AAAAAAAAebw/P2fEkbt_g9QcUxeYEFakJnQAIKG2ixytwCLcBGAs/s320/2019-04-23_bf48ce70-65a4-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-4r0XJk8qf6U/XXY_fh_iT6I/AAAAAAAAebw/P2fEkbt_g9QcUxeYEFakJnQAIKG2ixytwCLcBGAs/s1600/2019-04-23_bf48ce70-65a4-11e9-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-UkpDRv04ouM/XXY_gFirn0I/AAAAAAAAeb0/_fML0yfM10AUhJNDthuX9nE0wFCPv2M6QCLcBGAs/s320/2019-04-23_c5c94b8c-65a7-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-UkpDRv04ouM/XXY_gFirn0I/AAAAAAAAeb0/_fML0yfM10AUhJNDthuX9nE0wFCPv2M6QCLcBGAs/s1600/2019-04-23_c5c94b8c-65a7-11e9-99c5-c65302bf1052.jpg)

  

[![](https://1.bp.blogspot.com/-b2LNTWcxuEM/XXY_gru_RwI/AAAAAAAAeb4/xM9DG3KGCyIp_vChQSMIbqi7lgaNqNm5QCLcBGAs/s320/2019-04-23_cb89deec-659d-11e9-99c5-c65302bf1052.jpg)](https://1.bp.blogspot.com/-b2LNTWcxuEM/XXY_gru_RwI/AAAAAAAAeb4/xM9DG3KGCyIp_vChQSMIbqi7lgaNqNm5QCLcBGAs/s1600/2019-04-23_cb89deec-659d-11e9-99c5-c65302bf1052.jpg)