---
title: '10 Estrategias de Manipulación Mediática'
date: 2013-02-04T20:40:00.000+01:00
draft: false
tags : [manipulación mediática, Noam Chomsky]
---

No creas que, como ciudadano, eres libre, hay muchos intereses en juego que dependen de que _no pienses demasiado_, de que _te diviertas_, de que _te lo pases bien_. En algunos de los blogs _conspiratorios_ a los que estoy suscrito se usa el término '_despertar',_ para referirse a ese empeño por conocer la verdad: ahí fuera no hay buenos propósitos, nadie se preocupa por ti y no es real el cuento que te están contando. Es cierto que estamos siendo controlados y cada vez me doy más cuenta de que detrás de cada resultado hay una mano intencionada detrás, la casualidad no existe. No sea necio querido lector -y ahora me dirijo a usted como a alguien importante que es-, mastique la información que le hacen llegar, retenga lo importante, no se deje engañar, la verdad está al alcance confusa entre los adornos de la representación. He encontrado una imagen que las tiene representadas con distintas tipografías de modo muy atractivo, las diez estrategias para la manipulación mediática (atribuidas por lo visto, falsamente, a Noam Chomsky). No tiene desperdicio.

  

[![](http://3.bp.blogspot.com/-3TVqCWhtet4/URtLzAKBuuI/AAAAAAAAB0A/R5DrQ_c8s38/s640/url.jpeg)](http://3.bp.blogspot.com/-3TVqCWhtet4/URtLzAKBuuI/AAAAAAAAB0A/R5DrQ_c8s38/s1600/url.jpeg)