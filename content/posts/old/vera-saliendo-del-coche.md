---
title: 'Vera saliendo del coche'
date: 2019-10-24T13:34:00.000+02:00
draft: false
tags : [velada literaria, cuento, ficcion, relato]
---

El año ha pasado volando. Dos empastes, las vacaciones de Julio y el Frenadol con el primer suéter. Al salir del trabajo, se le acerca Lorenzo y los invita a cenar.

— ¿Por qué no venís a cenar el jueves? Díselo a Vera, le encantará.

Total, que allá van. Lorenzo y María viven por Chamberí. High class dice Vera en un vestido negro mirándose en el espejo del ascensor. En el reflejo se ven dos botellas de lambrusco y los problemas que van engordando desde la boda del año pasado. Manuel lleva una camisa verde de leñador que le recuerda a Robert de Niro en El Cazador. 

Manuel define estos últimos dos años como una temporada mala. Las cosas con Vera no van bien, no se entienden como antes y discuten por llegar tarde, las manchas en la ropa y la sensación de picor cuando llega el verano.

La puerta se abre y aparece Lorenzo con el delantal y una nebulosa de comida casera. ¿Patatas al horno? ¿Cebolla frita? Mientras echa un vistazo al salón, Manuel oye los dos besos de Vera y piensa que le gustan las casas que huelen a fuego lento. María está a punto de colgar una llamada con las gafas en la punta de la nariz hablando por teléfono. Lleva una camiseta muy grande del Betis y su voz se parece a la que suena en los aeropuertos y estaciones de tren.

Abren una botella. Lorenzo está hablador, ha hecho la cena y está contento de tener invitados en casa. María mira con atención, sirve más lambrusco y pregunta qué tal va la cosa. La cena sabe a mamíferos socializando satisfactoriamente.

Han puesto música de fondo mientras hablan de viajes, películas viejas y sectas. Manuel mira a Vera, que ahora parece relajada, saboreando la comida. Habla distraída, jugando con el tenedor. Durante un momento se parece a la Vera de la risa loca y las tardes cantando temas. El aroma a incienso le recuerda al friegasuelos que utilizaba su tía-abuela en Zamora.

Manuel se ofrece a fregar los platos mientras ellos siguen charlando en la mesa. Abre el grifo y consigue que el agua salga templada. Tiene la imagen de Vera saliendo del coche esa tarde que fueron a la cata de vinos, sus zapatillas, el color de la tierra. Algunos platos resbalan bajo el agua. Ha olvidado por qué no podían parar de reír. Las nubes eran como salidas de una enorme batidora.