---
title: 'Nokia 105'
date: 2018-12-28T11:52:00.001+01:00
draft: false
tags : [tecnología, sencillez, retro, simple, móvil]
---

La simplicidad no depende de la tecnología incorporada a un teléfono, pero puede ayudar. Uno puede preguntarse cuánto aguanta (fuera de casa o el trabajo) sin WhatsApp, por ejemplo. La batería de un Xiaomi 4E, de Diciembre de 2017, no cargaba y nueva, en el barrio chino de Convento Jerusalén, pedían 25€. Por el mismo precio, esta joya de sólo llamadas con linterna y capacidad para 2000 contactos y 30 días sin necesidad de carga. RAM: 4 MB (vs. Xiaomi 4E 3GB RAM). 

  

[![](https://3.bp.blogspot.com/-I0tEGDC1xs0/XCX_b77wZDI/AAAAAAAAcc8/7h87FU76n0kbv-Kd7eOSuOieHmvav-DZgCLcBGAs/s320/1545991237006.jpg)](https://3.bp.blogspot.com/-I0tEGDC1xs0/XCX_b77wZDI/AAAAAAAAcc8/7h87FU76n0kbv-Kd7eOSuOieHmvav-DZgCLcBGAs/s1600/1545991237006.jpg)