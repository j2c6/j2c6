---
title: 'Larga exposición'
date: 2015-12-01T11:46:00.000+01:00
draft: false
tags : [nocturna, casa, patio, comienzos, Noche, fotografía, larga exposición]
---

Esta es la primera larga exposición, le da al patio interior un aire de fantasmas y cielos púrpura.  
  
[![](https://2.bp.blogspot.com/-2UViZ0zMq3s/WS_itS5TqXI/AAAAAAAAOzw/PmM7iVjgiRojhHdpZsnQnUPTt5oEwP3WQCK4B/s400/IMG_0444.JPG)](http://2.bp.blogspot.com/-2UViZ0zMq3s/WS_itS5TqXI/AAAAAAAAOzw/PmM7iVjgiRojhHdpZsnQnUPTt5oEwP3WQCK4B/s1600/IMG_0444.JPG)