---
title: 'Los jardines de Monforte'
date: 2012-10-30T14:02:00.000+01:00
draft: false
tags : [sitios donde leer, Jardines de Monforte, Valencia, jardín]
---

[![](http://1.bp.blogspot.com/-0uZO806EALU/UI_POsXBLlI/AAAAAAAABkA/1lhjdAzSoS0/s320/monforte.jpg)](http://1.bp.blogspot.com/-0uZO806EALU/UI_POsXBLlI/AAAAAAAABkA/1lhjdAzSoS0/s1600/monforte.jpg)

Ya llevaban muchos meses en obras y cerrados, pero ya están abiertos. Y aunque el horario parezca prohibitivo (10.30-18 hrs en invierno) es fantástico pasarse una hora leyendo o simplemente paseando por el jardín. Es un recinto que está como escondido en la ciudad y como está cerca de Viveros, pasa desapercibido. Esto se comprueba porque afortunadamente no está abarrotado de gente y es más silencioso y recóndito, es como si se percibiera una presencia especial. Por supuesto que hay que elegir un día con sol. Ha cambiado la puerta de entrada: ahora se entra por la calle Monforte rodeando un edificio que se encuentra a medias entre lo hortera y lo clásico, pero es pasable. Y nada más entrar vigilan dos magníficos leones de piedra que protegen unos peldaños de acceso al jardín. Frente a la entrada hay un precioso paseo de tinajas y jarrones con flores rojas que lleva hasta casi la fuente. La fuente tiene algunos nenúfares flotantes que recuerdan a los de los cuadros de Monet. El servicio de jardinería hace gala con setos cortados de distintas formas curiosas y vegetación nutrida de flores de muchos colores, el paseo cubierto con las enredaderas, etc. No quiero invitar a nadie porque no quiero que se haga más popular, pero un día pasee por ahí, es encantador. Lo cierto es que todavía no lo he podido explorar con tranquilidad, pero ha sido un primer retorno muy agradable.