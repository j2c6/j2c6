---
title: 'Moulin Rouge (2001)'
date: 2012-07-04T19:44:00.000+02:00
draft: false
tags : [Ewan McGregor, Catherine Martin, Nicole Kidman, Cine, Baz Luhrmann, Moulin Rouge, musical, 2000, Craig Pearce]
---

[![](http://2.bp.blogspot.com/-o5SIy5rMYSU/T_SAJW8_PWI/AAAAAAAABf4/1rtFX8ajptY/s400/rostard.jpg)](http://2.bp.blogspot.com/-o5SIy5rMYSU/T_SAJW8_PWI/AAAAAAAABf4/1rtFX8ajptY/s1600/rostard.jpg)Existen muchas formas de desconcertar al espectador. Si hay algo a la altura de los años 20 neoyorquinos, o a la hipocresía de la época victoriana, eso es el París bohemio de principios del siglo XX: una época cargada de atrayente romanticismo que en principio debería encandilar al público solo estableciéndose allí. No es oro todo lo que reluce y '_Diamonds are a girl's best friend!_'. 

  

Para empezar diría que me ha chocado muchísimo la estética de **Catherine Martin** ganadora del Oscar por la dirección artística de este filme. Personalmente me ha parecido barata y basta, una cómoda excusa para no representar quizá con más realismo el París de principios de siglo. Aunque claro está: solo decir que se trata de ese lugar es fácil que guste. Lo dicho, el colorido y representación del filme me parecen vulgares y chabacanos. Podríamos acusar al vanguardismo o al eufemismo de la libertad artística de tan mal gusto_._

Por otro lado el romance y la historia envueltas por un 'musical' si se puede llamar así a un forzoso cóctel de canciones famosas -y que hicieron historia- para ocultar el vacío real de la trama del guión (**Baz Luhrmann, Craig Pearce**), me parece una elección pobre y que a su vez arruina grandes canciones que a tramos aparecen en la película, conmoviendo irremisiblemente al espectador por la grandeza intrínseca de las mismas. El resultado es aceptable pero el modo es miserable y ordinario.

  

La trama es llevadera e interesante. El argumento supuestamente cargado de romanticismo está totalmente libre de esa fascinación apasionada y delicada del verdadero romanticismo. Por un lado hacen gala de la frivolidad existente,  y posteriormente con la aparición del protagonista se abren las puertas a algo más alto y trascendente, pero son incapaces de llevar al público hasta allí, que es raptado por el caótico espectáculo que sin poder evitarlo acaba en catástrofe por sus ambiciosas pretensiones.

  

El único enamorado podría ser el protagonista **Ewan McGregor** que, convincente y similar al soñador de '_**Big Fish**_' lleva toda la carga poética del film, pero es poco creíble que se encuentre su visión inocente entre la vida de las chillonas del '_**Moulin Rouge**_' un sitio envuelto en misterio y leyenda, totalmente desmitificado por la parafernalia sensual y hortera del film. **Nicole Kidman** no desentona demasiado pero su caracterización es en ocasiones patética. Se juega con los sentimientos del espectador de forma atolondrada y sucia, de modo que no es capaz de darse cuenta que detrás de este montaje no hay nada.

  

En una desgracia, un asesinato. Recoge poderosas conclusiones del romanticismo y de la literatura universal incluso las utiliza egoístamente, abusando del arte y la belleza como si fueran ingredientes de usar y tirar, cuando son mucho más valiosas. Lamento mucho que me hayan engañado. No me ha traído sino confusión este largometraje, he vibrado en algunos momentos, pero sobretodo me ha hecho pensar en qué no funcionaba en este intencionado  y turbio ejemplar, en verdad vacío.