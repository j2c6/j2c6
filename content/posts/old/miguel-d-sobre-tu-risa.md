---
title: 'Miguel D''Ors - Sobre Tu Risa'
date: 2010-11-24T13:25:00.000+01:00
draft: false
tags : [S.XX, Miguel D'Ors, Poesía]
---

Este es el primer post de poesía que pongo, y ciertamente este es un campo casi inexplorado para mi inexperiencia. Creo que leer poesía va bien de vez cuando en cuando, es como ir al campo cada cierto tiempo y respirar el aire puro que mece las espigas, disfrutar de algo que disfruta el mundo. En fin, es un autor (Santiago de Compostela, 1946)  que no conozco mucho, pero me gusta. Es sencillo, y tiene cierta gracia.

  

SOBRE TU RISA

  

_A veces cuando ríes, _

_te olvidas de ti misma_

_y momentáneamente, _

_sin que tú te des cuenta,_

_un brillo campesino_

_se te asoma a los ojos._

  

_Es un instante sólo,_

_pero basta: esa chispa_

_desde tu corazón_

_sube hasta tu mirada,_

_desde el rincón que tienes_

_de muchacha de pueblo,_

_y entonces en tu risa_

_se reconoce toda_

_la noble sencillez_

_que te dejó tu abuela_

_y me vienen impulsos_

_de besar a tu madre,_

_a tu padre, a tu hermana,_

_a los que te conocen,_

_o de escribir un verso,_

_o de escribir el verso_

_más feliz de mi vida_

_para cantar los altos_

_hayedos de tu valle,_

_el río que tú sabes, la frontera,_

_o los verdes que fueron modelo de tus ojos, \]_

_porque viendo ese brillo_

_sé que, a pesar de todos_

_esos libros y esos _

_vestidos y esos gestos_

_fríos que algunas veces_

_te me disfrazan, siempre_

_serás sencilla como la nieve en los caminos,_

_siempre tendrá tu alma la fragancia del heno,_

_siempre, si se me mete_

_un invierno en el pecho,_

_me acercaré a tu voz_

_como a una chimenea_

_y siempre, siempre, niña,_

_siempre diré que tienes_

_un corazón tan puro como el pan de los pueblos._  
  

_26-27, XII, 70_