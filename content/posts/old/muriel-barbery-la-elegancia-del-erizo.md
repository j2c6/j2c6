---
title: 'Muriel Barbery - La Elegancia del Erizo'
date: 2010-11-03T06:33:00.000+01:00
draft: false
tags : [Haiku, Ozu, Francia, Leon Tólstoi, Literatura, Interesante, Suicidio, Muriel Barbery, Novela]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TM26ukfGe1I/AAAAAAAAAz8/pjg3RpTYjj8/s320/Rene.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TM26ukfGe1I/AAAAAAAAAz8/pjg3RpTYjj8/s1600/Rene.jpg)

4.000.000 lectores. Esto me llamó la atención y alguna recomendación. Algo que es leído por tanta gente, merece una explicación y esto fui a buscar. Paloma una niña de 12 años, muy lista (o por lo menos bastante más que las de su clase), planea suicidarse al estar en contra de un mundo que no la entiende y que solo se preocupa de superficialidades de las altas esferas. René la portera de la finca de Paloma, vive sencillamente acomodada en una "moneda de dos caras", es decir: por un lado aparenta ser la portera basta y sin aspiraciones mayores que las de ver la televisión y enterarse de lo que se enteran las porteras, y por el otro su viudez junto con su amor por la literatura rusa y por las películas de **_Ozú_** el director japonés, llenan su vida de todo el entusiasmo que merece. Sorprendería lo que tienen en común estas dos almas gemelas... La escritura de **Muriel**, ha recibido muchos premios, y yo no soy ningún crítico al que se deba reverenciar, sin embargo sus brillantes comentarios, su sarcasmo puesto en boca de las protagonistas para reírse de las apariencias del mundo real, son increíbles. Este libro tiene muchas cosas sorprendentes, como los **_haikus_** de el comienzo de cada capítulo de la "doceañera". Sin embargo a pesar del interés del libro, no lo recomiendo. 

  

Un fragmento del comienzo:

_ "He tomado pues una decisión. Pronto dejaré atrás la infancia y, pese a mi certeza de que la vida es una farsa, no creo que pueda resistir hasta el final. En el fondo, estamos programados para creer en lo que no existe, porque somos seres vivos que no quieren sufrir. Por ello empleamos todas nuestras energías en convencernos de que hay cosas que valen la pena y que por ellas la vida tiene sentido. Por muy inteligente que yo sea, no sé cuánto tiempo aún podré luchar contra esta tendencia biológica. Cuando entre en el mundo de los adultos, ¿seré todavía capaz de hacer frente al sentimiento de lo absurdo? No lo creo. Por eso he tomado una decisión: al final de este curso, el día que cumpla 13 años, el próximo 16 de junio, me suicidaré. Pero cuidado no pienso hacerlo a bombo y a platillo como si fuera un acto de valentía y un desafío. De hecho, más me vale que nadie sospeche nada. Los adultos tienen con la muerte una relación rayana en la histeria, el hecho adopta proporciones enormes, se comportan como si fuera algo importantísimo cuando en realidad es el acontecimiento más banal del mundo. Por otra parte, lo que a mí me importa no es el hecho del suicidio en sí, sino el cómo."_

 Página 20. **La Elegancia del Erizo**. Seix-Barral 2007.

  

**Nota:** Posteriormente hablando con personas no carentes de experiencia y conocimiento... La novela trae un mensaje **Nietzscheano**: el mundo no está al alcance de nuestro intelecto, la razón no puede conocer la verdad, la única salida que se deriva de esta posición es el disfrute con el arte, la belleza: es lo que vale la pena, todo lo demás no tiene sentido. Sabiendo que Muriel es profesora de filosofía en Bayeux, entendida en estas cuestiones.