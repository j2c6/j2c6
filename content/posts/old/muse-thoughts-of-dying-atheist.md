---
title: 'Muse - Thoughts of a Dying Atheist'
date: 2009-02-08T09:39:00.000+01:00
draft: false
tags : [Indie, Garage Rock, Musica, Muse]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/SY6iOcmK_ZI/AAAAAAAAAY0/cFX17z57JOA/s320/MuseAbsolution-300dpi%255B1%255D.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/SY6iOcmK_ZI/AAAAAAAAAY0/cFX17z57JOA/s1600-h/MuseAbsolution-300dpi%5B1%5D.jpg)  

Como puse en la barra una vez aquel tema de **Muse** (**_Plug in baby_**), creo que me ha entrado este grupo de verdad que es un grupo de Rock imprescindible y lo vais a comprobar, es tirando a rock duro pero he pillado algo un poco más suave y sencillo, acostumbran a grades efectos musicales, y solos, especialmente de guitarra, y una voz muy bien trabajada. Esta canción a parte de lo que es el sentido musical en sí de la canción que es una obra maestra, también tiene una letra interesante, va de un hombre moribundo que es ateo, y se está muriendo, y tiene un miedo gigante, y ve el final de su vida y esta muy asustado: os coloco un par de frases de la letra traducida:

  

.

_“Siniestros susurros atrapados bajo mi almohada, No me dejarán dormir, tus recuerdos, Sé que el momento está cerca, Y no hay nada que podamos hacer, Mira a través de un ojo sin fe, ¿Tienes miedo de morir?, Y me da un miedo atroz, Y el final es todo lo que puedo ver “_

.

El titulo de la canción traducido es “Pensamientos de un ateo moribundo” .Yo antes de conocer la letra casi coincidí, pensé que era una carrera en la vida, donde todos van corriendo, y la vida se acababa y los corredores seguían, muy lejos, es potente, pero triste a la vez .Si os ha gustado esta canción de Muse oir otras suyas como: _Bliss, Hysteria, New Born, Time is running out, Supermassive Black Hole_ …Y claro esta Plug in Baby, os echo un acceso directo: [http://www.goear.com/search.php?q=muse](http://www.goear.com/search.php?q=muse), la buscais y las escuchais, a ver si os gustan, ya diréis.Es buena...