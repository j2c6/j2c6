---
title: 'Nirvana, Where did you sleep last night'
date: 2009-03-30T16:37:00.000+02:00
draft: true
tags : [Nirvana, Garage Rock, Grunge, Musica, 90', When did you sleep last night, Kurt Cobain]
---

He tardado en poner este último artículo, no es porque me lo haya preparado más, sino porque, no lo sé. Creo que **Nirvana**, es un grupo que tampoco podía faltar, en nuestras páginas. Nirvana formo parte de la vida de millones de personas, digo formo porque desapareció. Nirvana ha cambiado el rock, ha metido la “N” dentro del Rock. Cientos de guitarras, llevan su nombre, millones de discos vendidos, Bueno yo quería hablar especialmente de una canción mítica del unplugged, “_**When did you sleep last night?**_”. Creo que son los Nº1.

Es una canción de lo más sencilla, y cómo no triunfa: guitarra, y voz. Las dos impresionantes. La canción no tiene más, es una antigua canción de folk, no es de Nirvana, ellos simplemente, hicieron un “cover”. La letra tampoco. Pero eso es lo que mola, tiene algo no se sabe que es una voz desgarrada pero hace que ese”My girl” suene muy profundo, lo dice con paz.Booom.