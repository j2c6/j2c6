---
title: 'Los descendientes (2011)'
date: 2012-02-19T17:17:00.000+01:00
draft: false
tags : [Faxon, Amara Miller, Rash, George Clooney, Cine, 2010, Alexander Payne, Shailene Woodley]
---

[![](http://1.bp.blogspot.com/-vgVDoMruVbo/T0Ega6q-j7I/AAAAAAAABPA/_V7pH4Dm1pI/s400/rostards.jpg)](http://1.bp.blogspot.com/-vgVDoMruVbo/T0Ega6q-j7I/AAAAAAAABPA/_V7pH4Dm1pI/s1600/rostards.jpg)En Hawái, Matt King (**George Clooney**) es abogado inmobiliario, además de poseedor de unas valiosas tierras que se encuentran en el dilema de su venta. Esto es solo un detalle ya que recientemente su mujer ha sufrido un accidente y está en coma en el hospital, lo que fomentará la relación con sus hijas: la pequeña Scottie (**Amara Miller**) y la problemática Alex (**Shailene Woodley**).  

  

Empieza diciendo que Hawái no es lo que todos pensamos, que no están las veinticuatro del día haciendo surf con piña colada esperando bajo la sombra de una palmera inhalando el aroma marino mientras disfruta del radiante sol. También hay lágrimas quiere decir. La realización de **Alexander Payne** deja disfrutar de una drama aliñado con mucha comedia. Una banda sonora discreta pero simpática, excelente elaboración cinematográfica, todo puesto a punto para el disfrute del espectador. Ganadora del **Oscar** al mejor guión adaptado.

  

La condición humana podría ser el tema del largometraje donde salen a relucir o a desagradar los placeres y errores del género humano. La situación no es sencilla, y afortunadamente no se muestra toda su crudeza gracias al sentido del humor. Una historia que recorre la infidelidad, la muerte, la insatisfacción, pasando por la comprensión, el perdón y llegando hasta el amor. Un melodrama mostrado con sorprendente naturalidad. El guión  (**Payne, Rash, Faxon**) sabe deslizarse por instantes tan delicados y tensos para llegar a otros relajados y cómodos.

  

El drama que trae el halagado papel de **Clooney**,  emociona al espectador. El personaje, Matt, es bien representado, hay momentos en los que no sabe exactamente qué hacer y es creíble cómo se desenvuelve añadiendo cierta absurdidad por parte del guión. La desconocida **Shailene Woodley**, se ajusta plenamente al papel de adolescente problemática y complementa a **George C** en los momentos de más emotividad. Sin duda la moraleja de los entresijos familiares y su resolución es más que relevante, no solo cuenta una historia pues **Payne** pretende algo más.  
  
  

 Aunque las ideas son merecedoras de múltiples aplausos no llegan con toda su profundidad quizá por el exceso de patetismo irreal en muchos momentos. A pesar de la mencionada naturalidad con que transcurre el relato, hay puntos que rozan la sensiblería y hacen uso de la tendencia lacrimógena del público americano. Sin embargo el resultado es un filme compacto y divertido pero pasajero.