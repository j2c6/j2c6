---
title: '"Acerca del delito..., y el asesinato y pensamiento de Raskólnikov" I Parte'
date: 2010-09-29T09:57:00.000+02:00
draft: false
tags : [Rusia, Asesinato, Acerca del Delito, Delito, Raskólnikov, La Escalera Que Quemaba Ríos de Tinta]
---

<!-- /\* Style Definitions \*/ p.MsoNormal, li.MsoNormal, div.MsoNormal {mso-style-parent:""; margin:0cm; margin-bottom:.0001pt; mso-pagination:widow-orphan; font-size:12.0pt; font-family:"Times New Roman"; mso-fareast-font-family:"Times New Roman";} @page Section1 {size:612.0pt 792.0pt; margin:70.85pt 3.0cm 70.85pt 3.0cm; mso-header-margin:36.0pt; mso-footer-margin:36.0pt; mso-paper-source:0;} div.Section1 {page:Section1;} -->  

Recientemente he realizado un pequeño estudio, dentro de la maravillosa obra de “**Crimen y Castigo**”. El hecho de que **Dostoievsky** exprimiera la psicología del protagonista, ha producido la fascinación que siento por **Raskólnikov.**

  

En primer lugar, los asesinatos propiamente dichos en la historia son diferentes, al de la novela, pero muy parecidos entre sí: en general todos favorecen a unas “necesidades” de los hombres: dinero, poder, fama, falsedad, envidia, odio, venganza, etc. Sin embargo el motivo de **Raskólnikov** no tiene nada de sencillo, podríamos estar hablando de una revolución, no es un mero acto por una razón primera, sino todo un profundo razonamiento aliñado con cierta locura y trastornos psicológicos… Viendo la complejidad del asunto, y tras haber disfrutado con su lectura, al mismo tiempo decidí excavar en “el asunto”, hasta tocar el lienzo, bañado por la pintura de la obra rusa, y al mismo tiempo universal.

  

Cuando uno emprende un camino, habitualmente es menester tener unos motivos (descontando el caso de elección del camino por puro capricho o azar). Al principio, especialmente en los revolucionarios, existe una teoría, aparentemente descabellada. Los grandes arriesgan su cordura por las grandes ideas.

  

Raskólnikov es un estudiante, inteligente, que aprende y contribuye a la apreciación universal, da clases a otros estudiantes, hace traducciones… en resumen piensa.

  

Las teorías siempre fueron algo muy útil, aunque solo sean para perder el tiempo, en efecto hacen pensar y dan personalidad a las manos que las escriben. “**_Acerca del delito_**” fue un artículo publicado en la “_revista periódica_”, la visión exterior del artículo deja ver una detallada descripción del _estado psicológico del delincuente a lo largo de toda la comisión del delito_, además de que _siempre lleva emparejado una enfermedad_. Sin embargo poco antes de concluir el texto aparece una idea.

  

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/TKLxIJXRopI/AAAAAAAAAys/YnPaxaEfpvI/s320/image004.gif)](http://3.bp.blogspot.com/_VT_cUiG4gXs/TKLxIJXRopI/AAAAAAAAAys/YnPaxaEfpvI/s1600/image004.gif)Muchas personas han dividido el mundo en dos, diciendo algo así como: “en el mundo hay dos tipos de personas: las que están ahí, y las que quieren aparentar estar ahí” este sería un ejemplo. En el caso de Raskólnikov las divide en **_ordinarias, y extraordinarias_**, de las cuales las primeras deben _vivir la obediencia y no tienen derecho a transgredir la ley, pues son ordinarias._ En cambio las extraordinarias tienen _derecho propio de saltar ciertos obstáculos (todo género de delitos y transgresiones de la ley) y aún eso tan solo en el caso de que así lo exige la realización de una idea suya en ocasiones salvadora de toda la humanidad._ Es decir: tienen derecho en el caso de que su misión, o su aportación a la humanidad sea superior a estos medios, a cometer cualquier crimen para alcanzar el fin buscado. Una persona inteligente etiquetaría el caso del tipo “el fin justifica los medios”, pero no creo que la complejidad de _el asunto_ nos remita realmente a esta famosa afirmación, por conclusión: “**el fin no justifica los medios**”.

  

Realmente existen personas, que su aportación es tal, que constituye un gran bien, y que en base a ese gran logro u aportación, _se le olvidan sus faltas_ y oscuridades, por el bien realizado. Esto puede dar lugar a error, puede pensarse que cualquier artista, o personaje revolucionario puede permitirse el lujo de actuar por encima de la moral. Lo primero es que este aspecto se reduce solo a los más grandes, y con ello no me refiero a los más aclamados por un tiempo, sino a los universales, a los que han contribuido al desarrollo y elevación del hombre. Por otro lado una posición respecto a este aspecto es que una persona que compone la 9ª sinfonía, tal es la belleza de la obra, y tal el bien (equiparable a la **belleza**), que no puede ser malvada, es decir que sus faltas no serán de una “excesiva” gravedad, quizás solo su locura, o su estado irascible… el hombre y su naturaleza. Por tanto pienso que una gran aportación va acompañada de una gran humanidad, y bien, y belleza y no del crimen. Aunque esto no quiere decir que desprecie esas aportaciones del momento, de los no tan grandes pero que merecen respeto e incluso reconocimiento, no nos importan las altas montañas nevadas únicamente, sino también las verdes colinas panzudas y floridas.

  

A pesar de todo estas palabras son solo apreciaciones mías, Raskólnikov tiene un pensamiento un tanto más crudo. Antes de continuar debo puntualizar que estas sentencias, estás extraídas en su mayoría de una conversación entre: **Raskólnikov, Razumijin, y Porfiri**. Puesto que nuestro protagonista califica de un modo impreciso al ciudadano _extraordinario,__el talento de decir algo nuevo en su medio_, entendemos que no desarrolló su teoría completamente antes de su famoso asesinato de la vieja usurera y de Aliona Ivanóvna.

  

_Todos los rectores de la humanidad, todos sin excepción fueron delincuentes, aunque solo sea por el hecho de que, al promulgar una ley nueva, violaran la antigua venerada por la sociedad y legada por los padres, \[…\] lo notable es que la mayor parte de estos bienhechores son los que más sangre han hecho correr. Los que son capaces de decir algo medianamente nuevo, han de ser delincuentes por naturaleza._ De todos modos _la masa casi nunca reconoce ese derecho, (de asesinar y cometer crímenes) y por eso los ejecuta y los ahorca. \[…\] Los ordinarios: señores del presente. Los extraordinarios: son los señores del futuro._

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/TKLz3Kqn-UI/AAAAAAAAAy0/RiPkemHNv-0/s320/san_petersburgo__antigua_med.jpg)](http://2.bp.blogspot.com/_VT_cUiG4gXs/TKLz3Kqn-UI/AAAAAAAAAy0/RiPkemHNv-0/s1600/san_petersburgo__antigua_med.jpg)  

En ocasiones algunos de la clase ordinaria se confunden y creen tener el derecho de _eliminar todos los obstáculos. Esto ocurre con frecuencia_ y por tanto aparecen atrocidades sin sentido “justo” que no debieron cometerse. Aún así _son muy pocos los que nacen con un atisbo de aptitud para decir algo nuevo._ Estos son _individuos con un ápice de espíritu de independencia._

  

Razumijin concreta de otro modo el pensamiento de Raskólnikov: _admites la efusión de sangre dictada por la conciencia._ Y dentro de la conciencia reluce otra cuestión, y es que  por el hecho de la grandeza de su misión no desaparecerá la culpa sobre su conciencia: _al que la tenga, le tocará padecer al reconocer su error. Ese será su castigo, además de los trabajos forzados._ Aquí puede verse reflejado el porqué del título del libro, pues cada crimen independientemente de su trascendencia conlleva un castigo. _El sufrimiento y el dolor son siempre obligatorios para una mente amplia y un corazón profundo._ Hablaremos más sobre este tema.

  

Conforme al estado de Raskólnikov no sabemos si se consideraba con seguridad un ser _extraordinario_ pero añade _yo no me tengo por un **Mahoma** o un **Napoleón**._

  

En esta primera parte del artículo hemos considerado la teoría reflejada en el artículo de Raskólnikov: el poder de las personas _extraordinarias_ (que dan a conocer al mundo aspectos nuevos para su desarrollo, o aportaciones), es el derecho a delinquir si su misión se lo exige por el bien de todos. Cabe detallar que en _mi_ opinión no creo que se deba considerar a tanta gente, sino como he nombrado antes “solo a los más grandes”, y además no solo en cuanto a cuestiones políticas (**Napoleón**) o científicas (**Newton, Kepler**) sino también en el arte o las humanidades, dos grandes aportaciones.