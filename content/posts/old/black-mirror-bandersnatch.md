---
title: 'black-mirror-bandersnatch'
date: 2018-12-30T17:30:00.000+01:00
draft: false
tags : [literatura mutante, mapa de decisión, Series, bandersnatch, Cine, black mirror]
---

hemos visto lo nuevo (T5) de black mirror, que te permite configurar el capítulo como una aventura en la que puedes elegir. no sé si es por el mapa de decisión, la ruta que hemos elegido --algo muy relativo eso de elgir-- pero, más allá de la experiencia (y con ella incluida), no me ha gustado nada. el rollo retro años ochenta, el  bacterio absorto en su trabajo, casi loco en su labor de artesano, ok, pero nada más.