---
title: 'Sartén de Setas Ostra con Patatas'
date: 2020-05-29T10:50:00.003+02:00
draft: false
tags : [vegetarianas., receta, sartén, setas ostra]
---

**Ingredientes**  
Setas ostra  
Hierbas aromáticas  
Cebolla picada  
Dos dientes de ajo  
Caldo de verduras  
  
**Las Patatas**  
Precalentamos el horno a 200ºC  
Cortamos las patatas en gajo, sal pimentamos y manchamos de un poco de aceite y al horno.  
  
**La Sartén**  
Freimos cebolla con un poco de tomillo, añadimos los ajos, las hojas de laurel y doramos.  
Añadimos las setas un rato.  
Cubrimos con caldo de verduras 15 min. hasta que se reduzca.  
Añadir las patatas a la sartén y servir.

  

[![](https://1.bp.blogspot.com/-gSUTbuY22hs/XtDMkleWcYI/AAAAAAAAfd0/8_VdYBXdmHEv8Q-MZfJIeqAJh_voHrBKwCK4BGAsYHg/w300-h400/photo_2020-05-29_10-49-04.jpg)](https://1.bp.blogspot.com/-gSUTbuY22hs/XtDMkleWcYI/AAAAAAAAfd0/8_VdYBXdmHEv8Q-MZfJIeqAJh_voHrBKwCK4BGAsYHg/photo_2020-05-29_10-49-04.jpg)