---
title: 'Kahn monumental'
date: 2018-04-24T17:36:00.000+02:00
draft: false
tags : [clásico, taschen, arquitectura, catedrales, Kahn, pirular, monumentalidad]
---

He leído mi primer libro de arquitectura. Es uno de esos con muchas fotos y breves de Taschen. Kahn.

  

Sobrevolando muy ligeramente, \[1\] he entendido, que había como varias pirulas. Por un lado era impensable reproducir las catedrales de antaño con fidelidad, ya que pertenecían a otra época y en una posterior no tendría sentido. Por otro lado, ignorarlas y no aprender de ellas para crear edificios artificiosos y aspirantes a maravillas del mundo era una 'injusticia' \[2\]. Total que Louis dice, "mira, las catedrales están ahí y podemos aprender mucho de sus estructuras clásicas de grandeza y asombro" y ahí nace su concepto de monumentalidad, que sería exportar esa característica de la antigua arquitectura --monumental, impresionante-- a la nueva con sus formas recientes y modernas.

  

La improbabilidad de que esto no tenga nada que ver con la Monumentalidad estudiada en las escuelas de la actualidad es ALTA.

  

\[1\] Se me ocurren esas visitas que hacen los turistas a las ciudades del Mediterráneo cuando van de crucero y que por tanto, tras ese par de horas pasan a formar parte de su lista de ciudades viajadas.

\[2\] Licencia del autor de este texto