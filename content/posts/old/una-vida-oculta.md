---
title: 'Una vida oculta'
date: 2020-05-08T13:40:00.001+02:00
draft: false
tags : [george eliot, Terrence Malick, vida oculta, middlemarch]
---

Es en la película de Terrence Malick donde se citan estas palabras de George Eliot en _**Middlemarch**_:

> Las acciones de su alto espíritu tuvieron hermosas consecuencias que no fueron visibles (…), que no fueron reconocidas en esta tierra. Pero el efecto de su personalidad en quienes le rodearon fue incalculablemente benéfico, porque el aumento del bien en el mundo depende en parte de hechos sin historia; y que las cosas no sean tan malas para ti y para mí como pudieran haberlo sido, se debe en parte a tantos que vivieron fielmente una vida oculta y descansan en tumbas que nadie visita.