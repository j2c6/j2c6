---
title: 'Jet - Are you gonna be my girl'
date: 2009-02-20T14:14:00.000+01:00
draft: false
tags : [Rock, Garage Rock, Musica, Jet]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/SZ__wFXGTMI/AAAAAAAAAZk/5nENkicbBYk/s320/1306841584.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/SZ__wFXGTMI/AAAAAAAAAZk/5nENkicbBYk/s1600-h/1306841584.jpg) Es de lo mas animado que podemos encontrar de Jet, es una canción muy potente (adjetivo archi-utilizado), te levanta el ánimo en una media de 3,5 segundos, guitarras, bateria, voz, y mucho ritmo, así empieza el dia, **Jet**. La verdad es que la canción es de risa, es de mis favoritas, y eso quería ponerla, porque siempre hay alguien que necesita oír una cancion así porque resucita, tiene el pulso acelerado, se despierta, y se rie..."¿Vas a ser mi chica?" dice.  
.