---
title: 'Espai Verd en el día de la convivencia en paz'
date: 2018-05-16T17:05:00.000+02:00
draft: false
tags : [interreligión, naturaleza, brutalismo, benimaclet., espai verd, arquitectura, formas de vida, oratorio, Valencia, convivencia en paz, religión, reunión, antonio cortés, espaci s.l.]
---

Antonió Cortés organizó un encuentro entre religiones en el oratorio interreligioso del Espai Verd del día de la Convivencia en Paz (16-may). Fue el lunes 14 para respetar el Ramadán que empezaría al día siguiente y sobre esa hora no sería posible que asistieran musulmanes. Representantes de distintas religiones hicieron un rato de oración y un coloquio. Se podía contemplar el interior del Espai Verd.

  

[![](https://1.bp.blogspot.com/-FHAr_h-0RQU/WvxEhNwrfyI/AAAAAAAAWIM/x7KkFqJP8Ck1yiyXafXDghfzHJecP-nPgCLcBGAs/s320/_MG_1050.jpg)](https://1.bp.blogspot.com/-FHAr_h-0RQU/WvxEhNwrfyI/AAAAAAAAWIM/x7KkFqJP8Ck1yiyXafXDghfzHJecP-nPgCLcBGAs/s1600/_MG_1050.jpg)

Maximiliano esperando en la puerta. Hizo vídeos de casi todo.

  

[![](https://2.bp.blogspot.com/-CPuuUPLaUjQ/WvxGQOZ27KI/AAAAAAAAWJk/6OXzgazSUWgDNof5i-Ys2R1CgEKFDyghgCLcBGAs/s320/_MG_1064.jpg)](https://2.bp.blogspot.com/-CPuuUPLaUjQ/WvxGQOZ27KI/AAAAAAAAWJk/6OXzgazSUWgDNof5i-Ys2R1CgEKFDyghgCLcBGAs/s1600/_MG_1064.jpg)

  

[![](https://1.bp.blogspot.com/-0KFg6vz-HKs/WvxGP_pJBkI/AAAAAAAAWJg/s1Q6Wt62wW8d-B8MBuTcGcU4WNA2gqXNACLcBGAs/s320/_MG_1048.jpg)](https://1.bp.blogspot.com/-0KFg6vz-HKs/WvxGP_pJBkI/AAAAAAAAWJg/s1Q6Wt62wW8d-B8MBuTcGcU4WNA2gqXNACLcBGAs/s1600/_MG_1048.jpg)

Carmen es de la Asociación Valenciana de Plantas Medicinales

  

[![](https://4.bp.blogspot.com/-3TNcs8aNAdE/WvxEgluJd6I/AAAAAAAAWII/ewgl8PxmBQgq363RYKu0OlpUCAXnI0sewCLcBGAs/s320/_MG_1070.jpg)](https://4.bp.blogspot.com/-3TNcs8aNAdE/WvxEgluJd6I/AAAAAAAAWII/ewgl8PxmBQgq363RYKu0OlpUCAXnI0sewCLcBGAs/s1600/_MG_1070.jpg)

Antonio se saluda con unos compañeros de tuna de toda la vida.

  

[![](https://4.bp.blogspot.com/-0U7ftdpEufM/WvxE1VCSy3I/AAAAAAAAWIg/_ZV05PRCuhQoAOwknaU1VVRZkzWFhE4lwCLcBGAs/s320/_MG_1073.jpg)](https://4.bp.blogspot.com/-0U7ftdpEufM/WvxE1VCSy3I/AAAAAAAAWIg/_ZV05PRCuhQoAOwknaU1VVRZkzWFhE4lwCLcBGAs/s1600/_MG_1073.jpg)

  

[![](https://4.bp.blogspot.com/-5l6aUdlL3R0/WvxE1UDivrI/AAAAAAAAWIc/dnl-phatz6AH0sZLW0oPkzNF6-3m4DBNwCLcBGAs/s320/_MG_1076.jpg)](https://4.bp.blogspot.com/-5l6aUdlL3R0/WvxE1UDivrI/AAAAAAAAWIc/dnl-phatz6AH0sZLW0oPkzNF6-3m4DBNwCLcBGAs/s1600/_MG_1076.jpg)

  

[![](https://4.bp.blogspot.com/-yicBF-e0ChI/WvxFBlvy_tI/AAAAAAAAWIk/3rKiPRxd8-kM0QLwYUwCNcj-t0iU1N1TACLcBGAs/s320/_MG_1079.jpg)](https://4.bp.blogspot.com/-yicBF-e0ChI/WvxFBlvy_tI/AAAAAAAAWIk/3rKiPRxd8-kM0QLwYUwCNcj-t0iU1N1TACLcBGAs/s1600/_MG_1079.jpg)

  

[![](https://1.bp.blogspot.com/-haUci_sRnNk/WvxFgzo8VTI/AAAAAAAAWI4/-6GTWQYUJ9AFK6pALLgMsFv-q9cv5o6fQCLcBGAs/s320/_MG_1108.jpg)](https://1.bp.blogspot.com/-haUci_sRnNk/WvxFgzo8VTI/AAAAAAAAWI4/-6GTWQYUJ9AFK6pALLgMsFv-q9cv5o6fQCLcBGAs/s1600/_MG_1108.jpg)

Escalera de ascenso al oratorio.

  

[![](https://2.bp.blogspot.com/-bPfrcoN5I2I/WvxFlz-3jMI/AAAAAAAAWI8/BWyt4FWaAks7qAIFZ5Qeyohsbo2-nxOmQCLcBGAs/s320/_MG_1115.jpg)](https://2.bp.blogspot.com/-bPfrcoN5I2I/WvxFlz-3jMI/AAAAAAAAWI8/BWyt4FWaAks7qAIFZ5Qeyohsbo2-nxOmQCLcBGAs/s1600/_MG_1115.jpg)

  

[![](https://2.bp.blogspot.com/-oPOlMB8y3MQ/WvxFs8z7xiI/AAAAAAAAWJA/VQLtLqmCB_4Li0WKxR7O2Wvo3j_UnpKgQCLcBGAs/s320/_MG_1120.jpg)](https://2.bp.blogspot.com/-oPOlMB8y3MQ/WvxFs8z7xiI/AAAAAAAAWJA/VQLtLqmCB_4Li0WKxR7O2Wvo3j_UnpKgQCLcBGAs/s1600/_MG_1120.jpg)

Antonio con los strings del teclado. En el letrero: 'El alma necesita silencio para adorar'

  

[![](https://1.bp.blogspot.com/-UoQN5mFKMW8/WvxF7xAPgoI/AAAAAAAAWJQ/fgZqI9nKNUEhUrrGQZht_0ZX0gwk3S5BwCLcBGAs/s320/_MG_1123.jpg)](https://1.bp.blogspot.com/-UoQN5mFKMW8/WvxF7xAPgoI/AAAAAAAAWJQ/fgZqI9nKNUEhUrrGQZht_0ZX0gwk3S5BwCLcBGAs/s1600/_MG_1123.jpg)

Distintos asistentes en el rato de oración.

  

[![](https://4.bp.blogspot.com/-7zc5mHZ93-Q/WvxF7WGqEgI/AAAAAAAAWJI/46bDRAlyTKQpl8qYB74q8RFWSDaaOVtfQCLcBGAs/s320/_MG_1126.jpg)](https://4.bp.blogspot.com/-7zc5mHZ93-Q/WvxF7WGqEgI/AAAAAAAAWJI/46bDRAlyTKQpl8qYB74q8RFWSDaaOVtfQCLcBGAs/s1600/_MG_1126.jpg)

Vela cuya llama ha sido fruto de una cadena desde Tierra Santa, y ahí se mantiene.

  

[![](https://4.bp.blogspot.com/-w0br9qIS0R8/WvxF71cgyrI/AAAAAAAAWJM/1GPA6n_EKhkQj81eAPXDirqbYk4oeyeKACLcBGAs/s320/_MG_1141.jpg)](https://4.bp.blogspot.com/-w0br9qIS0R8/WvxF71cgyrI/AAAAAAAAWJM/1GPA6n_EKhkQj81eAPXDirqbYk4oeyeKACLcBGAs/s1600/_MG_1141.jpg)

  

[![](https://1.bp.blogspot.com/-qhiFTAbHTwY/WvxE0DPmJpI/AAAAAAAAWIY/CCTq2qBkF30q1DspnHVlNbAx1tWuqvSkgCLcBGAs/s320/IMG_1094.jpg)](https://1.bp.blogspot.com/-qhiFTAbHTwY/WvxE0DPmJpI/AAAAAAAAWIY/CCTq2qBkF30q1DspnHVlNbAx1tWuqvSkgCLcBGAs/s1600/IMG_1094.jpg)