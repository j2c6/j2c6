---
title: 'Paterson, Columbus'
date: 2018-03-26T21:35:00.002+02:00
draft: false
tags : [Yasujiro Ozu, arquitectura, señales, Ozu, paterson, Kogonada, Columbus, cotidiano, Poesía]
---

Las señales. Últimamente me hablan las señales, las veo en frases de libros, en apariciones de objetos, en la publicidad, en las palabras. 

  

Me he obsesionado con la no-perfección/perfección cotidiana de '_**Columbus**_' (Kogonada, 2017). Tengo pendiente revisitarla y la he visto por casualidad. Uno posesivo como yo, piensa en dónde están esas otras obras como esta que no han pasado por delante de mis ojos.  
  

[![](https://1.bp.blogspot.com/-r5oTdOm3tt4/WsQEFI3JwQI/AAAAAAAATD0/3brPcWsXxxoB7t798gEkF9VBdi2caaSIACLcBGAs/s320/vlcsnap-2018-03-14-21h05m08s264.png)](https://1.bp.blogspot.com/-r5oTdOm3tt4/WsQEFI3JwQI/AAAAAAAATD0/3brPcWsXxxoB7t798gEkF9VBdi2caaSIACLcBGAs/s1600/vlcsnap-2018-03-14-21h05m08s264.png)

  

_Los_ likes _no tienen ningún impacto real._

_La autenticidad no brilla. No hay premios al más auténtico. No fundan religiones. Se limitan a ser._

  

Frases en negativo. No me acaban de gustar.

  

[  
](https://3.bp.blogspot.com/-9d49X34NKdg/WsQFZWP_deI/AAAAAAAATEQ/Y00OMEzhntEcf_TaErW4V0A8kE-PfqTmwCLcBGAs/s1600/Tashi%2Band%2BThe%2BMonk_cube_Andrew%2BHinton.jpg)[](https://3.bp.blogspot.com/-9d49X34NKdg/WsQFZWP_deI/AAAAAAAATEQ/Y00OMEzhntEcf_TaErW4V0A8kE-PfqTmwCLcBGAs/s1600/Tashi%2Band%2BThe%2BMonk_cube_Andrew%2BHinton.jpg)  
[](https://3.bp.blogspot.com/-9d49X34NKdg/WsQFZWP_deI/AAAAAAAATEQ/Y00OMEzhntEcf_TaErW4V0A8kE-PfqTmwCLcBGAs/s1600/Tashi%2Band%2BThe%2BMonk_cube_Andrew%2BHinton.jpg)En cualquier caso, es una película que hermanaría con '_**[Paterson](https://tomatefrito08.blogspot.com.es/2017/06/paterson-solo-para-decirte.html)**_'. Entre Kogonada y su videoarte de [Vimeo](https://vimeo.com/kogonada) sale el nombre de Yasujiro Ozu, el director japonés.  
  

[![](https://4.bp.blogspot.com/-19tOkpKx5XU/WsQE1NQG_AI/AAAAAAAATEE/IiuJ56SP4FQaHVz4VVGJaYMJoP0ZGjJlgCLcBGAs/s200/Yasujiro_Ozu.png)](https://4.bp.blogspot.com/-19tOkpKx5XU/WsQE1NQG_AI/AAAAAAAATEE/IiuJ56SP4FQaHVz4VVGJaYMJoP0ZGjJlgCLcBGAs/s1600/Yasujiro_Ozu.png)

Yasujiro Ozu. (Es extraño, pero noto parecido con:

  

  
  
[![](https://3.bp.blogspot.com/-9d49X34NKdg/WsQFZWP_deI/AAAAAAAATEQ/Y00OMEzhntEcf_TaErW4V0A8kE-PfqTmwCLcBGAs/s200/Tashi%2Band%2BThe%2BMonk_cube_Andrew%2BHinton.jpg)](https://3.bp.blogspot.com/-9d49X34NKdg/WsQFZWP_deI/AAAAAAAATEQ/Y00OMEzhntEcf_TaErW4V0A8kE-PfqTmwCLcBGAs/s1600/Tashi%2Band%2BThe%2BMonk_cube_Andrew%2BHinton.jpg)

Tashi y el monje)