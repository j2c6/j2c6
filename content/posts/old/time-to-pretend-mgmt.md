---
title: 'Time To Pretend - MGMT'
date: 2010-02-12T17:15:00.000+01:00
draft: false
tags : [Rock, Musica, Oracular Espectacular, Rock Alternativo, MGMT]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/S3V9_jxV1yI/AAAAAAAAApQ/3029SLnjAL4/s200/mgmt.png)](http://3.bp.blogspot.com/_VT_cUiG4gXs/S3V9_jxV1yI/AAAAAAAAApQ/3029SLnjAL4/s1600-h/mgmt.png)

Este grupo triunfó en 2008, canciones con teclados y aires de rockeros. Este grupo lo tenia todo estilo, buenas letras, buenos compositores... La verdad es que su _**Oracular Spectacular**_ ha quedado grabado en esta época, quizás es porque hacia tiempo que no se presentaba algo tan autentico y verdadero en los escenarios _Indies_ de _Nueva York_... Vienen desde _**Brooklyn**_, y han cambiado la historia. La canción con la que los presento (_**Time To Pretend**_) fue su tema mas conocido, gracias a que llego a los nº1, aunque también destacan sus **_Weekend War_, _The Youth, Kids_,** y _Electric Feel_... Todo el disco está lleno de temazos, que no te puedes perder si eres de esos fans de **Indie** por un tubo, te los recomiendo. Por oro lado si estas harto de la música actual hipócrita, y repetitiva, esto es algo nuevo, por si no lo habías oído lo inventaron ellos.