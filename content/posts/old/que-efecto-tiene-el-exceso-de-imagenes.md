---
title: '¿Qué efecto tiene el exceso de imágenes? '
date: 2020-07-06T18:06:00.000+02:00
draft: false
tags : [modernidad]
---

Estás suscrito a cientos de feeds de imágenes, de contenido visual, cuyo visionado provoca reacciones rápidas y embotelladas. Estás abierto a lo que quieran mandarte. Te sientes anonadado con la cuestión. No sabes que pensar de cada una de ellas. Quieres ordenarlas, retenerlas, guardarlas en un cajón. Mientras lo intentas siguen apareciendo. Se te olvida esa de la puesta de sol que te ha dado tan buenas sensaciones. Hay una serie de una fiesta que tiene un tono inquietante, todos parecen cuerpos de revista, altivos, intocables, de plástico. En Instagram funcionan así, entras y empiezan a darte cucharadas, no esperan ni que tragues, deja la tráquea fuera antes de entrar a Instagram. El texto te espera a ser descifrado, no se mete en tu cabeza sin tu consentimiento, puedes parar cuando quieras, te lo pide el cuerpo. Vas a morir como esos peces que mueren empachados de comida para peces como trocitos de papel biblia de colores, de alguna iglesia patrocinada por disney.