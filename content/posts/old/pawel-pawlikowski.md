---
title: 'Pawel Pawlikowski'
date: 2019-01-28T12:37:00.000+01:00
draft: false
tags : [lentitud, Cold War, slow, Cine, Pawel Pawlikowski, Ida]
---

En Noviembre fue 'Cold War', y ahora en Enero 'Ida'. Cuidadas, sobrias y contenidas. Se toma nota de Pawel Pawlikowski. A continuación, ciertas capturas de 'Ida'.

  

  

[![](https://2.bp.blogspot.com/-JiMXJpsEWBU/XE7ozAnbWTI/AAAAAAAAc1I/d3jNqIMKRnAp1usF4X-XI7Kbud9D1C5LQCLcBGAs/s320/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM%2B%25281%2529.jpeg)](https://2.bp.blogspot.com/-JiMXJpsEWBU/XE7ozAnbWTI/AAAAAAAAc1I/d3jNqIMKRnAp1usF4X-XI7Kbud9D1C5LQCLcBGAs/s1600/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM%2B%25281%2529.jpeg)

  

[![](https://1.bp.blogspot.com/-tM3t6npH6J4/XE7ozGsHYxI/AAAAAAAAc1E/puhG_R536nUQmZ47syg0Iws2nKJK6B3uQCLcBGAs/s320/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM%2B%25282%2529.jpeg)](https://1.bp.blogspot.com/-tM3t6npH6J4/XE7ozGsHYxI/AAAAAAAAc1E/puhG_R536nUQmZ47syg0Iws2nKJK6B3uQCLcBGAs/s1600/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM%2B%25282%2529.jpeg)

  

[![](https://2.bp.blogspot.com/-RvQRsB_ijec/XE7ozEYDdEI/AAAAAAAAc1M/Bb1I41q77koUZW60a-iKmn_biuLjhQPJQCLcBGAs/s320/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM%2B%25283%2529.jpeg)](https://2.bp.blogspot.com/-RvQRsB_ijec/XE7ozEYDdEI/AAAAAAAAc1M/Bb1I41q77koUZW60a-iKmn_biuLjhQPJQCLcBGAs/s1600/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM%2B%25283%2529.jpeg)

  

[![](https://4.bp.blogspot.com/-soLhgTdPi8g/XE7o0Jy11rI/AAAAAAAAc1Q/35m6JULCstQ6HsLXXXtHYvontGkhkf5ogCLcBGAs/s320/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM.jpeg)](https://4.bp.blogspot.com/-soLhgTdPi8g/XE7o0Jy11rI/AAAAAAAAc1Q/35m6JULCstQ6HsLXXXtHYvontGkhkf5ogCLcBGAs/s1600/WhatsApp%2BImage%2B2019-01-26%2Bat%2B10.29.54%2BPM.jpeg)