---
title: 'Bípedos de estrechas caderas'
date: 2018-04-24T17:24:00.002+02:00
draft: false
tags : [teorías, antepasados, sapiens, biología, Historia]
---

Por lo visto, una de las consecuencias en nuestros ancestros al comenzar a andar con dos 'patas' fue el estrechamiento de las caderas de las mujeres. Además de las habilidades cada vez más complejas que realizarían nuestras manos al verse liberadas de la tarea del desplazamiento, los cráneos de los nonatos comenzaban a pasar justos por sus caderas al dar a luz, por lo que empezaron a nacer antes. Siempre he pensado aquello de porqué otros animales después del parto salen adelante rápidamente, lo he pensado con fastidio, mirando hacia el cielo. Por lo visto, nosotros los sapiens, nacemos prematuramente.Total que la mujer debía de encargarse de la educación del pequeño durante su primera etapa de crecimiento y alimentación y de entre todas, la selección natural elegía a las que estaban bien capacitadas para buenas relaciones sociales que les permitían seguir adelante con el proyecto de vida de sus retoños y proveerse de protección y alimentación, entre otras cosas. Y de ahí el animal social que somos, la educación en la infancia y esas cosas.

  

Pirulas extraídas y parafraseadas de _Sapiens_, Yuval Noah Harari  (Debate, Barcelona, 2014)