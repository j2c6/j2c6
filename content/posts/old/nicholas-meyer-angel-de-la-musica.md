---
title: 'Nicholas Meyer - ''El Ángel de la Música'''
date: 2012-12-25T19:52:00.000+01:00
draft: false
tags : [Gaston Leroux, Fantasma de la Ópera, Ópera, Sherlock Holmes, Nicholas Meyer, Novela, Sir Arthur Conan Doyle, Robert Downey Jr., Literatura, Degas, París, Garnier, Guy Ritchie]
---

[![](http://1.bp.blogspot.com/-WaghgAHNBp4/UNn1mNomojI/AAAAAAAABs0/jqINK4rgEZo/s1600/garnier.jpg)](http://1.bp.blogspot.com/-WaghgAHNBp4/UNn1mNomojI/AAAAAAAABs0/jqINK4rgEZo/s1600/garnier.jpg)Hay algo que me encantó de este libro antes de leérmelo y fue que aquel que me lo dio me aseguró que me gustaría porque tiene todos los 'elementos imprescindibles' que yo exigiría en una novela. Lo cierto es que, es muy interesante pensar cuáles son esos elementos, qué ingredientes hacen una obra irresistible. Muchas veces he hecho listas de mis libros favoritos hasta la fecha y he comparado cosas comunes y detalles singulares de cada uno. Querido lector, te recomiendo que hagas esta tarea, pues aunque se trate de novelas muy distintas, siempre hay un fondo común una tecla solo tuya que el autor sabe pulsar.

  

'_**El Ángel de la Música**_' tiene un argumento muy atractivo. Yo no soy de los que ya ha leído a **Sir Arthur Conan Doyle** y las aventuras de _**Sherlock Holmes**_, pero desde la anti-versión del cine materializada por **Robert Downey Jr**. (**Guy Ritchie**, 2009) y tras esta novela, estoy reconsiderando su lectura. No tuve un afortunado primer contacto leyéndolo en catalán ('_**El gos dels Baskerville**_'), pero claro: leer por obligación es una de las peores lecturas que hay. El caso es que me había perdido un  personaje tan carismático que ahora es resucitado por el señor Meyer.

  

Holmes se dedica ahora a la apicultura, y retrata un vacío histórico de tres años que desconocía Watson. Después de creerlo muerto pasa un tiempo en Milán y aburrido se traslada a París. Y claro es violinista y es contratado en la orquesta de la Ópera, y un remake de '_**El fantasma de la Ópera**_' con el propio **Leroux** como director de escena: sensacional. Las descripciones del descomunal edificio de Garnier, el fantasma, Christine Daáe y Holmes, un cóctel muy disfrutable.

  

[![](http://1.bp.blogspot.com/-2ZaZV7mkXd4/UNn1kmPFyhI/AAAAAAAABss/DKL3XamGO7g/s1600/Degas.jpg)](http://1.bp.blogspot.com/-2ZaZV7mkXd4/UNn1kmPFyhI/AAAAAAAABss/DKL3XamGO7g/s1600/Degas.jpg)La verdad es que el narrador empieza teniendo un carácter con muy buen gusto, matizando detalles que hacen sonreír de placer, todo mítico y poético en el momento adecuado, como la obra de un artesano y además repleto de cultura fascinante, como la aparición de Holmes en la pintura de **Degas**. Pero a medida que van pasando páginas, aunque la historia se vuelve cada vez más interesante, la novela pierde profundidad y se centra más en los hechos que en dar más fondo y significado a los personajes y a sus acciones, no consigue imprimir una huella honda que lo inmortalice y en ese sentido es una pena, tiene una historia perfecta, pero no llega a calar. Sin embargo lo dicho, resulta un plato delicioso.

  

El tal **Nicholas Meyer** es conocido -además de por ser novelista- por ser un potente director de cine, ha hecho posibles algunas piezas de **Star Trek** y '_**Los pasajeros del tiempo**_'. En fin que no lo conozco de nada, pero que resulta un tipo interesante, ya que ha incluido -y no lo olvido- mis elementos imprescindibles en su novela de un modo que me ha cautivado. En gran parte se lo debe también al brillante **Gaston Leroux** cuya obra queda pendiente. Totalmente agradecido, habrá que investigar a este artesano tan afín.