---
title: 'J.D. Salinger - El Guardián Entre El Centeno'
date: 2011-07-14T13:00:00.000+02:00
draft: false
tags : [S.XX, Muy Bueno, Literatura, J.D. Salinger, Holden Cauldfield, Novela]
---

"—Vamos —le dije—, ¿quieres bailar?  

La enseñé cuando era pequeña y baila estupendamente. De mí no aprendió más que unos cuantos pasos, el resto lo aprendió ella sola. Bailar es una de esas cosas que se lleva en la sangre.  
—Pero llevas zapatos.  
—Me los quitaré. Vamos.  
Bajó de un salto de la cama, esperó a que me descalzara, y luego bailamos un rato. Lo hace maravillosamente. Por lo general me revienta cuando los mayores bailan con niños chicos, por ejemplo cuando va uno a un restaurante y ve a un señor sacar a bailar a una niña. \[...\] Phoebe y yo nunca bailamos en público. Sólo hacemos un poco el indio en casa. Además con ella es distinto porque sí sabe bailar. Te sigue hagas lo que hagas. Si la aprieto bien fuerte, no importa que yo tenga las piernas mucho más largas que ella. Y puedes hacer lo que quieras, dar unos pasos bien difíciles, o inclinarte a un lado de pronto, o saltar como si fuera una polka, lo mismo da, ella te sigue. Hasta puede con el tango.  

  
Bailamos cuatro piezas. En los descansos me hace muchísima gracia. Se queda quieta en posición, esperando sin hablar ni nada. A mí me obliga a hacer lo mismo hasta que la orquesta empieza a tocar otra vez. Está divertidísima, pero no le deja a uno ni reírse ni nada.  
  
Bueno, como les iba diciendo, bailamos cuatro piezas y luego Phoebe quitó la radio. Volvió a subir a la cama de un salto y se metió entre las sábanas.  
  
—Estoy mejorando, ¿verdad? —me preguntó.   
—Muchísimo —le dije.   
Volví a sentarme en la cama a su lado. Estaba jadeando. De tanto fumar no podía ya ni respirar. Ella en cambio seguía como si nada."  

  

J. D . Salinger. _El Guardián Entre El Centeno_