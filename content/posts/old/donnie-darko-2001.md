---
title: 'Donnie Darko (2001)'
date: 2012-03-09T11:40:00.000+01:00
draft: false
tags : [Paranoia, Jake Gyllenhaal, Cine, 2000, Richard Kelly]
---

[![](http://3.bp.blogspot.com/-1NVACtJOu4I/T1nff2L17CI/AAAAAAAABQY/3WlwTB5Q_vs/s400/rostard.jpg)](http://3.bp.blogspot.com/-1NVACtJOu4I/T1nff2L17CI/AAAAAAAABQY/3WlwTB5Q_vs/s1600/rostard.jpg)Donnie Darko (**Jake Gyllenhaal**) sonánbulo y esquizofrénico, tiene visiones de un conejo gigante que le dice que se va a acabar el mundo en un plazo de tiempo. A continuación una serie de sucesos extraños comienzan a ocurrir, empezando por el impacto del motor de un avión en la casa del protagonista.

  

El desconocido **_Richard Kelly_**, se propuso aceptar y realizar esta extraña oferta de '**_Donnie Darko_**'. El estilo surrealista de la película es algo más que llamativo. No se tarda en encontrar situaciones aparentemente absurdas, en las que el espectador entrecierra los ojos y se rasca la cabeza pensando: "¿qué está ocurriendo aquí?". Supongo que la sigues viendo porque compruebas que a excepción del Donnie y '_abuela muerte_' todos parecen asombrarse del interesante comportamiento del protagonista.

  

Rodeada de coincidencias con números y fechas que revelan cierto significado a investigadores paranoicos y que no voy a nombrar aquí, el largometraje de suspense-misterio fue presentado en **Sundance**. No pudo estrenarse en el cine a tiempo por el atentado del **11-S**, donde las autoridades consideraron dejarlo para otro momento. Una apuesta muy fuerte por una rareza esquizofrénica mezclada con ciencia --agujeros de gusano y viajes en el tiempo--, calificada por el público como película de culto o miembro del 'top 10' de ejemplares raros.

  

**Jake Gyllenhaal** a pesar de haber rodado con anterioridad tres o cuatro filmes más, debuta en este drama fantástico, prometiendo una carrera interesante como se comprueba con posterioridad. Me sorprendió la composición del guión ya que a pesar de la alocada idea de **Richard Kelly**, la atención al conejo mutante no cesa, es claramente hipnótica. Sin embargo no puedo dejar de expresar mi confusión, (tras el visionado de algunas películas psicológicas y hasta traumáticas) en esta obra eché de menos una explicación de los hechos que confirmara mis conclusiones.  
  

La realización de la recreación de los años 80, la música y el reparto son aceptables. A pesar de la atracción que soporta este ejemplar, la ambigüedad que no deja entender el curioso largomentraje deja al espectador clavado en su asiento mientras aparecen los créditos finales en un estado frustrante. Por lo que a pesar del interés que tiene la película, que hace pensar y trabajar al público para su conocimiento, su caótico desarrollo sin una aclaración final no le hace destacar especialmente, considerándola un entretenimiento juvenil momentáneo y sin más aspiraciones.