---
title: 'Los vengadores (2012)'
date: 2012-05-23T22:33:00.000+02:00
draft: false
tags : [Gwyneth Paltrow, Jeremy Renner, Chris Hemsworth, Joss Whedon, Chris Evans, Marvel, Scarlett Johansson, Robert Downey Jr., Cobie Smulders, Paul Bettany, Cine, Mark Ruffalo, 2010, Samuel L. Jackson, Zack Penn]
---

Realmente este año me está costando volver contento del cine. El último triunfo fue '**_Intocable_**' sin duda muy divertida y superó las expectativas, que eran reacias a la sensiblería típica. Primero me privé de ver '_**Los vengadores**_' porque no me apetecía, y luego porque había que guardar cierta imagen de estudiante en exámenes, pero ¿porqué no? No soy fan de la galaxia **Marvel**, he visto algunos largometrajes de estos superhéroes y ninguno ha significado demasiado. Quizá ahí está la clave: en no esperar nada.

  

[![](http://4.bp.blogspot.com/-ln2XJGhRZ1I/T71JPQUHJWI/AAAAAAAABeU/O5enMb2CSIo/s640/rostard.jpg)](http://4.bp.blogspot.com/-ln2XJGhRZ1I/T71JPQUHJWI/AAAAAAAABeU/O5enMb2CSIo/s1600/rostard.jpg)

  

Una grata sorpresa. Se condensa un gran elenco de actores famosos y nominados (**Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth, Scarlett Johansson, Jeremy Renner, Tom Hiddleston, Samuel L. Jackson, Cobie Smulders, Clark Gregg, Gwyneth Paltrow, Stellan Skarsgård, Paul Bettany**) muy amable y considerado. Era un supuesto problema para el guión que aparecieran tantos protagonistas, sin darles una historia común y al mismo tiempo que tuvieran su propio trozo de pastel. Los tiempos están bien divididos para cada uno, tal vez acapara cómo el que más **Robert Downey Jr.** con su estilo patente desde '_**Sherlock Holmes**_'. 

  

Habitualmente, este género se centra en hazañas caóticas y un despliegue de medios sin sentido. Sin embargo el guión de **Joss Whedon** y **Zack Penn**, es simplemente notable comparado con todo los demás, es capaz de atarte al asiento durante dos horas y quince minutos sin respirar y de un modo entretenido y potente. Cada cosa es -gracias a Dios- una pieza de un puzzle que se va desglosando coherentemente, un milagro tras ver el desastre de '**_Transformers_**'. Uno tiene que dar gracias porque una película de acción consiga narrarle algo, como consigue la espectacular obra de Joss Whedon.

  

Además de conseguir algo consistente y dejando un lado el rollo del _teseracto_ y los extraterrestres -que sinceramente no me va ni me viene- el guión está repleto de humor, realmente gracioso y no ridículo y primario como se suele observar. Perdonad que compare la obra con sus 'congéneres' pero estamos hablando de una referencia entre las suyas. Como decía, hay bromas entre los protagonistas y un desarrollo notable y claro. Tras este cariz ocurrente y distraído, está toda la atmósfera que hace a estos personajes lo que son: superhéroes con superpoderes y a lo grande, me con esto me refiero a los efectos especiales y la calidad HD del diseño por ordenador, donde cualquier cosa es posible.

  

Un ejemplar que mantiene la esencia de estos emblemáticos personajes y su historia, conservando su interés y estilo, consagrándolo como uno de los grandes por su ingenio y entretenimiento. Respaldado por la taquilla por cifras millonarias en todo el planeta, este _blockbuster_ lo tiene todo como el 'pan y circo' romanos pero de modo elegante y dinámico. Muy disfrutable, de acuerdo con la oferta del género.