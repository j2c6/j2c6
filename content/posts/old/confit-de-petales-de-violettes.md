---
title: 'Confit  de Pétales de Violettes'
date: 2010-09-13T10:40:00.000+02:00
draft: false
tags : [Confit  de Pétales de Violettes, Gastronomía, Violetas, La Escalera Que Quemaba Ríos de Tinta]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/TI3jwMuTevI/AAAAAAAAAyc/MsM012hfz7M/s320/37061.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/TI3jwMuTevI/AAAAAAAAAyc/MsM012hfz7M/s1600/37061.jpg)

No soy un apasionado de la cocina, sin embargo en ocasiones sé apreciar los aromas propios de los jefes culinarios. En cierta ocasión, en un restaurante aterrizó frente a mi, un apetitoso plato de jugosos filetes (entrecot), acompañado con foie, y una especie de "**mermelada de violetas**", similar a los populares caramelos de violetas. Cuál fue mi sorpresa al acompañar al tenedor de los tres elementos... una mezcla delicada a la vista, se deshizo sublime en la boca. Años después (no muchos, dos o tres), llegó un tarro con este titulo a la mesa de mi casa: "**_Confit  de Pétales de Violettes_** ". Al abrirlo recordé aquella terraza, con aquella "mermelada de violetas". Al tener el recipiente en mi mano, tuve tiempo de estudiarlo básicamente, con la vista, el olfato y el gusto. Y he descubierto que esta delicada confitura, es oro entre foie y carne, pero al contrario que el metal precioso, y como las obras maestras, solo se permite gustar en pequeñas porciones, a veces casi inestimables -si el trozo es muy pequeño- pues sinó su exquisito sabor hace que el paladar lo aborrezca en grandes cantidades. Un sabor dulce y delicado, venido desde "**_la France_**" para disfrutarlo con poco.