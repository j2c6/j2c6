---
title: 'El Beso de la Mujer Araña'
date: 2020-04-20T19:05:00.001+02:00
draft: false
tags : [ficcion, relato]
---

  

Anoche volvieron a llamar al fijo. Ya no suena nunca, sólo mi madre y las compañías telefónicas. A las tres de la mañana, todo estaba en penumbra y se puso a sonar el teléfono otra vez. Algo importante para lo que el cuerpo se preparaba mientras salía de la cama. Porqué pondríamos el teléfono en el salón, me pregunté, cuando en las películas suena desde la mesita de noche. Entré en la sala de estar. El teléfono seguía sonando, como cuando la niña era un bebé y lloraba. Encendí la lámpara, y me senté en el sofá mientras descolgaba el teléfono. Y justo, nada. Triple pitido. Ha colgado.

  

Confuso y despierto en plena noche, alargué el brazo y me puse a ojear un magacín del revistero. A los pocos minutos apareció mi mujer en camisón, con la cara arrugada. ¿Qué haces? Me dijo. Ha sonado el teléfono, contesté. Se rascó la cabeza y me dijo, ¿y quién era? No era nadie, ha colgado, añadí yo. Mi mujer se acercó y se dejó caer en el sofá. Últimamente duerme fatal, como ella dice. Poco después se levantó y volvió con un cigarro en la boca y el cenicero, y se puso a fumar.

  

En el magacín se leía sobre economía, muertes por distintas causas, listas de sitios imprescindibles para el verano, las consecuencias de los comentarios de uno de los participantes de un reality show y algo acerca de cómo preparar la minestrone perfecta. Rápidamente se habían hecho las cuatro menos diez. Notaba los músculos perezosos flotando sobre el sofá. ¿Vemos una peli? Ella no contestó. Empecé a dudar de si lo había dicho en voz alta, o si era solo un pensamiento.

  

Mi mujer se encendió otro cigarrillo mientras miraba las fotos colgadas en la pared. Se giró, y me acarició la cabeza con la mano no-fumadora. Me miró dormida y sonriente y dijo: ¿vemos El Beso de la Mujer Araña?