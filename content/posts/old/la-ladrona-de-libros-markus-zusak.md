---
title: 'La Ladrona de Libros - Markus Zusak'
date: 2009-12-02T18:22:00.000+01:00
draft: false
tags : [Liesel Meminger, Markus Zusak, Muy Bueno, La Ladrona de Libros, Literatura, Novela]
---

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/SxajE1TbYkI/AAAAAAAAAmM/3igF8ByyzvM/s200/the-thief.jpg)](http://3.bp.blogspot.com/_VT_cUiG4gXs/SxajE1TbYkI/AAAAAAAAAmM/3igF8ByyzvM/s1600-h/the-thief.jpg)  

A comienzos de noviembre decidí leerme otro libro, estaba harto del que tenía y necesitaba otro. Llegó a mis manos **_"La ladrona de libros"_**, yo ya me veía el típico libro de la segunda guerra mundial, pero no, no fue así, aunque debo reconocer que las sesenta-setenta páginas me costaron lo suyo. La protagonista **Liesel Meminger** me encanto desde el principio y la seguí y me la llevé conmigo a todas partes, lo cierto es que después de sobrepasar las cien devoré el libro pagina a pagina. Hablé con más gente para contrastar opiniones, pero no le dieron tanta importancia como se la di yo. Creo que es una de esas historias en las que se valora lo cotidiano y se aprecia cada detalle de cada cosa, quizás por ello parece lenta la línea argumental... Sin embargo me atrevería a decir que es uno de los mejores libros que me he leído este año, y no digo más que lo que oí decir: "sencillamente espectacular".