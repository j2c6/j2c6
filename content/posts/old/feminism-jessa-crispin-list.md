---
title: 'feminism-jessa-crispin-list'
date: 2018-12-28T12:47:00.001+01:00
draft: false
tags : [autores, libros, notas, feminismo, Jessa Crispin, lista]
---

Jessa Crispin's:  
\----------------------------------------------------  
Sarah Schulman  
Emil Cioran  
Dubravka Ugresic  
Shlomo Sand  
Virginie Despentes  
R.I. Moore  
Franco Bifo Berardi  
Isabell Lorey  
bell hooks  
  
Simone Beauvoir  
Angela McRobbie  
Mattilda Bernstein Sycamore  
Jacqueline Rose  
Diane di Prima  
Michelle Cliff  
Helen Garner  
Laura Kipnis  
Maria Tatar  
Emma Goldman  
Maria Warner  
Eva Illouz  
Bruce Benderson  
Helene Cixous  
Mark Simpson  
Sonia Faleiro  
Simone Weil  
Stephanie Coontz  
Santa Teresa de Jesús  
Julia Kristeva  
Sandra Rodriguez Nieto  
  
Otros de Internet  
\----------------------------------------------------  
Mary Beard: Mujeres y el poder  
Camille Paglia: Feminismo pasado y presente  
Nancy Fraser  
Silvia Federici  
Nuria Varela: Feminismo para principiantes  
Lina Meruane: Contra los hijos  
Roxane Gay: Mala feminista  
Virginia Wolf: Un cuarto propio  
Simone de Beauvoir: El segundo sexo  
Caitil Moran: Cómo ser mujer  
Leticia Dolera: Muerdes la manzana  
Eva Ensler: Monólogos de la vagina  
Carmen G. de la Cueva: Mamá de mayor quiero ser feminista