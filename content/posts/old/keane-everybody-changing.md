---
title: 'Keane - Everybody''s Changing'
date: 2008-11-07T17:46:00.000+01:00
draft: false
tags : [Indie, Rock, Musica, Keane, Sin Guitarras]
---

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/SRRxmeBu5YI/AAAAAAAAAVo/wKv17skeHVw/s320/keane+hopes+and+fears.bmp)](http://2.bp.blogspot.com/_VT_cUiG4gXs/SRRxmeBu5YI/AAAAAAAAAVo/wKv17skeHVw/s1600-h/keane+hopes+and+fears.bmp)  

**Keane** un grupo muy importante que debía estar en nuestro blog.

  

Ha ganado muchos premios como grupo, a pesar de sus diferencias con otros grupos de rock: Mejor Álbum, Premios Onda, Mejor Banda Internacional, Compositor del año, Mejor Álbum, Mejor artista revelación, Banda del año, Canción del Año.

  

Es un grupo **anti-guitarra** eléctrica, la sustituyen por el piano y le acoplan sonidos de sintetizadores y pedales dejando una canción única. De las letras ya hablaremos. ¿Que os parece?