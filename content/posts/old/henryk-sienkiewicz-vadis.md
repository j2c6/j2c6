---
title: 'Henryk Sienkiewicz - ''Quo vadis?'''
date: 2012-04-28T16:50:00.000+02:00
draft: false
tags : [Petronio, Quo vadis, Roma antigua, Imperio Romano, Nerón, Sandro Botticelli, Vinicio, Literatura, Ligia, XIX, Cristiandad, Roma, Henryk Sienkiewicz]
---

¿Quién no recordará la famosa versión cinematográfica de 'Quo vadis'? Pues yo no me acuerdo y me he alegrado mucho en este mes de abril, que lo he dedicado a leer esta célebre aunque quizás olvidada novela del polaco **Henryk Sienkiewicz**, premio nobel del literatura. Lo primero que puedo decir es que me ha encantado, y hacía cierto tiempo que no leía una novela de vastas extensiones que me haya atraído por entero. Suele ocurrir en los libros que sobrepasan el umbral de las quinientas páginas, que aparecen tramos de 50-100 páginas que no llevas tan bien, o no te interesan tanto, o simplemente te aburren. Pero esto no ocurrió con **'Quo vadis?'.**

  

[![](http://3.bp.blogspot.com/-9lUV-lZ9wjs/T5wDNs0fvsI/AAAAAAAABbM/Gjmu92FPZUw/s640/Botticelli+-+La+Primavera.jpg)](http://3.bp.blogspot.com/-9lUV-lZ9wjs/T5wDNs0fvsI/AAAAAAAABbM/Gjmu92FPZUw/s1600/Botticelli+-+La+Primavera.jpg)

  
  
Las novelas históricas que hacen mucho hincapié en el contexto histórico y se empeñan en explicar la genealogía de los personajes y cómo el bravo imperio conquista aquellas tierras, me superan. Aquí importa más esta historia en particular. Los romanos siempre me han fascinado, por su grandeza, su lengua épica y porque en realidad son los inventores y reflejo del mundo que ahora conocemos. Muchas cosas han cambiado, como la esclavitud, pero son las mismas leyes las que mueven el corazón humano. La historia es importante pero creo que lo es por sus personajes. Aquí se centran en ellos, muy variados por cierto y con ricos elementos que fomentan su atractivo e interés. En ellos se revela la reconocida grandeza humana y toda su miseria y más. 

  

La historia de amor a primera vista de Vinicio y Ligia te atrapa desde que se conoce en el minuto uno. La belleza de Ligia parece no haber conocido precedentes y Vinicio pierde la cabeza. Durante toda la novela trata de conquistarla de distintas maneras y las circunstancias juegan hábilmente otro papel muy importante. El primer banquete en la casa del César, tiene una escena descrita de modo inefable. Ligia es sorprendida entre tanto lujo y riqueza, siente pánico ante tanto desenfreno pero al mismo tiempo no es inmune a sus encantos y disfruta muy a su pesar. Sensaciones contradictorias y complicadas como esta son descritas con maestría, así como la evolución y trascendencia del romance.

  

El universo romano es mostrado con todo su esplendor en las locuras de Nerón y la exquisitez y poesía del arbitro de la elegancia, Petronio. Los alardes del poderío del emperador romano me han sorprendido hasta el límite, rompiendo incluso la medida de lo posible. No es despreciable el dominio del mundo y las costumbres de la corte y nobleza romanas. Nerón se da a conocer entre su narcisismo como artista y su bestialidad como déspota y aterrador jefe de estado. Petronio cínico, inteligente e inabarcable, que dentro de su egoísmo y pasión por la belleza terrena consigue nacer la virtud de algún modo, es otro personaje de lo más interesante. Quilón y su fachada aduladora, rastrera e hipócrita. El alma de serpiente de la Augusta Popea, envidiosa, provocativa y peligrosa. La riqueza y retrato de estos personajes hacen que esta extensa novela no me haya cansado en absoluto.

  

Además la historia consigue absorberte como solo lo hacen los grandes clásicos, con delicadeza y al mismo tiempo violencia te atan y esclavizan hasta que conoces el final. He disfrutado muchísimo con los diálogos abundantes en literatura clásica, referencias a los dioses y supersticiones que no dejo de leer con curiosidad. '_¡Roma está ardiendo!_'  es algo sin igual, y que Nerón emulara el incendio de Troya para elaborar versos trágicos y reales reflejando su horroroso intento saliente de su patética alma artística. No solo los romanos han sido protagonistas de estas pesadillas; la traición, la envidia, los intereses, el afán de poder, el miedo, la compasión, el orgullo, la vanidad son temas universales que dotan a este evocador relato de todo realismo e identificación con el género humano.

  

'_Los cristianos a los leones_'. Otro tema que es relatado en profundidad con la presencia de Pablo de Tarso y Pedro, y de como la brutalidad y crueldad de Nerón recaen sobre la inocencia y honestidad cristianas. Es asombroso cómo los cristianos aceptan su muerte y cómo el pueblo en el circo es capaz de disfrutar con semejantes desgracias y orgías de sangre. Sin embargo también se observa la evolución del pensamiento del pueblo, siempre lento a estos razonamientos y por ello mal educado, necio y vulgar. Pero todas las ramas torcidas son taladas o reconducidas de un modo u otro de manera histórica y verosímil.  
  

Es inútil alargar más este comentario: lo dicho no puede equivaler ni siquiera de forma significativa al disfrute que ha generado la lectura de este clásico. He de ensayar más. Solo hay una manera de evaluarlo y es apurando la copa ofrecida hasta el final. Recomendado a todos.

  

_"Al principio no había producido gran impresión en Petronio: le había parecido demasiado endeble. Pero luego en el_ triclinium_, después de haberla mirado más de cerca, pensaba que podía comparársela con la aurora y como buen experto descrubría en ella algo especial. La examinaba por entero y apreciaba todo en ella: su rostro rosa y diáfano, sus labios frescos, que parecían creados para el beso, sus ojos azules como el azul de los mares, la blancura alabastrinada de su frente, los rizos de su abundante y oscura cabellera con reflejos de ámbar y bronce de Corinto, su cuello ágil, la caída <<divina>> de sus hombros, y todo su cuerpo ágil y esbelto, joven, con una juventud de mayo y de flor recién abierta. El artista y adorador de belleza despertaban en él: pensaba que en el zócalo de la estatua de aquella virgen podría escribirse la palabra: <<Primavera>>."_

_'Quo vadis?', __**Henryk Sienkiewicz** .  Valdemar histórica, enero 2001._  
_Ilustración de **Sandro Botticelli**, 'La primavera'._