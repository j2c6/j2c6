---
title: 'Básicos pendientes de Computer Science 2020'
date: 2020-02-19T09:52:00.002+01:00
draft: false
tags : [computer science, programación, ordenadores]
---

  

*   Estructuras de datos y algoritmos
*   Bases de datos y SQL
*   Linux
*   Lenguajes de programación: C/C++, Java, Python, Perl, Scala, Rust, Go, Ruby
*   Básicos de redes de computadoras, networking.
*   Machine Learning,IA