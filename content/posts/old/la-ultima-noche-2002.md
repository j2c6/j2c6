---
title: 'La última noche (2002)'
date: 2012-05-07T21:22:00.000+02:00
draft: false
tags : [Spike Lee, Brian Cox, Barry Pepper, Cine, Edward Norton, 2000, Philip Seymour Hoffman]
---

[![](http://4.bp.blogspot.com/-gzb0D8nxEIk/T6gg7w-1qfI/AAAAAAAABdI/0wyYwAzya4A/s400/The-25th-Hour.jpg)](http://4.bp.blogspot.com/-gzb0D8nxEIk/T6gg7w-1qfI/AAAAAAAABdI/0wyYwAzya4A/s1600/The-25th-Hour.jpg)Buenas críticas. Ese fue el motivo, bueno y **Edward Norton** uno de los grandes, ¡ah! y **Spike Lee**. Sin embargo y a pesar de las promesas, una película desconocida. El público sentencia con la fama y de esta ejemplar no había oído nada. 

  

Las interpretaciones son sólidas y no soberbias. Norton convence mucho, y no está estereotipado. El guión da retazos de la vida de Monty calmadamente sin apresurarse: cómo conoce a su novia y hasta qué punto confía en ella, sus amistades, los supuestos amigos que conserva del instituto y unas escenas con su padre. Desde que salva al perro en la primera escena donde me acomodo para ver lo que viene, hasta la hora y media en la que no pasa nada en realidad, estoy un poco desconcertado. Hay escenas que te llaman la atención, otras en las que simplemente atiendes, y otras más en las que piensas "¿qué está pasando?". Estas últimas son las de la discoteca y la alumna de **P. Seymour Hoffman** y todo eso que no sé a qué viene.

  

Luego te interesas por a ver qué pasa, va a ir a la cárcel y entonces hay dos cosas llamativas. La primera es la escena frente al espejo de Norton en plan '_**American History X**_' rajando el cóctel de razas que reinan en Nueva York y numerosas referencias -durante el film- a los atentados del 11-S. A **Spike Lee** le gustan este tipo de declaraciones, y probablemente su público habitual se impresione y le alabe, no yo. Es en plan un homenaje o una oda a la confluencia de etnias y razas en Nueva York o lo que sea que sinceramente no entiendo.

  

La otra escena es más placentera, -un poco motivada- donde el padre (**Brian Cox**) le narra a su hijo una especie de sueño donde el se escapa de la cárcel y llega a una vida mejor y envejece pensando en el cambio que eso supondría en su vida. El espectador, es decir tú se lo cree y después saborea el amargo final que es inevitable, la cárcel. También es una escena complicada y dura la paliza que le pega Frank (**Barry Pepper**) tras su discurso del 62% y todas sus paridas de seductor y amigo fiel, que le da un puñetazo dramático al largometraje.

  

Algunas ideas sobre la toma de decisiones que forjan la historia de una persona y avalan su posible final. El carácter humano de los errores, las pasiones y las consecuencias se ven reflejados con fuerza, pero la línea es desigual.  Los elementos citados anteriormente distraen del asunto principal, comprendo que hay algunos muy interesantes que reflejan la personalidad del protagonista, pero otros me parecen, no ordinarios -que lo son- sino superficiales.

  

En fin donde la crítica supone que cada elemento y cada palabra son poéticos y esenciales, yo creo que es completamente una exposición subjetiva. No me ha gustado demasiado la película, creo que quizá es por el contexto. La historia es llamativa pero trata temas tangentes o casualidades que hacen que pierda interés, y el resultado lo califico de mediocre a pesar de los elementos fuertes y dramáticos que sí que atañen al público.