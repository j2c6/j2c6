---
title: 'Hipótesis de la Simulación'
date: 2020-05-26T12:11:00.001+02:00
draft: false
tags : [dudas existenciales, Matrix, filosofía, Petri, hipótesis de la simulación, Nick Bostrom]
---

Vamos a suponer por un momento que las neuronas son más complejas de lo que imaginábamos y que en realidad, más allá de hacer operaciones sencillas tienen conciencia de sí mismas. Llevan una vída atareada, llena de detalles y complejidades personales y sociales. Ahora imaginemos que las neuronas viven en un planeta llamado Cerebro. Imaginemos la religión de las neuronas. El comportamiento global de las neuronas da lugar a la conciencia del humano de ese cuerpo. ¿Podríamos ser los seres humanos a su vez neuronas de un cerebro? ¿Y las galaxias del universo? La escalabilidad del problema existencial de los niveles, lo hace más complejo todavía.  

  

Por otro lado, en la hipótesis de la simulación informática --ya hablaremos de Nick Bostrom--, la resolución del problema es parcial si tenemos en cuenta que se puede resolver a varios niveles. Aparentemente, la Simulación no aparenta estar monitorizada, o en cualquier caso no exhaustivamente, sin intervención. Y el motivo de la simulación, el proósito, queda lejos de nosotros. El más básico que se me ocurre es la pura observación, el entretenimiento.

  

El ruido de fondo de pensamiento del cerebro podría ser la ilusión de que en realidad es menos complejo a nivel psicológico de lo que nos imaginamos. Igual que los paneles de control con luces y colores y pitidos de las películas de sci-fi que pretenden mostrar complejidad pero son puro espectáculo.  

  

Habrá formas de vida inimaginables, y si el sistema solar fuera una placa de Petri, el universo es una placa de Petri de placas de Petri, miles de millones de galaxias iterando condiciones para que aparezca la vida. Y la misma materia, lentamente invierte todos sus 'esfuerzos' en conservar la vida, y la propia vida intentando permanecer y perpetuarse. ¿Por qué? ¿Por qué ese ímpetu para seguir existiendo en un universo sin aparente propósito? ¿El la ausencia aparente de proósito o su desconocimiento un motor para seguir?

  

Respecto a la escalabilidad, a nivel temporal, la humanidad en el universo es tan fugaz como unos fuegos artificiales, y aparentemente, si fueran iteraciones, se estaría invirtiendo unos esfuerzos desproporcionados para aparentemente un escaso instante de lucidez en la naturaleza.