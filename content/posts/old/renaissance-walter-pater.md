---
title: 'Renaissance, Walter Pater'
date: 2013-02-02T09:00:00.000+01:00
draft: false
tags : [Renacimiento, Walter Pater, Oscar Wilde, Ensayo, Esteticismo, Decadentismo]
---

[![](http://4.bp.blogspot.com/-x7xGWIeDuEo/UQrf2d1djYI/AAAAAAAABvc/B32asZoWWXY/s1600/url.jpeg)](http://4.bp.blogspot.com/-x7xGWIeDuEo/UQrf2d1djYI/AAAAAAAABvc/B32asZoWWXY/s1600/url.jpeg)

Un ensayo que tanto influyó e inspiró en general a toda una generación esteticista y decadentista, y en concreto al admirado Wilde, es sin duda objeto de estudio. Aunque solo comprendo el 30 % de lo que leo, lo que entiendo está compuesto por unas ideas geniales, que llaman mucho la atención y dan en qué pensar, nuevas ideas al fin y al cabo, consiguen despertar el espíritu crítico y artístico, educando la apreciación de la belleza, y de paso una cascada de cientos de detalles de la cultura universal que derriten las perezosas ansias de saber, tal como Indiana Jones despertó el interés por la arqueología. Aunque no lo estoy saboreando al completo por mi falta de preparación, este primer contacto está siendo muy fructífero, estoy encantado con el señor Pater.

  

  

«¿Qué es para mí esta canción o este cuadro, esta atractiva personalidad que comparece en la vida o en un libro?¿Qué efecto es el que verdaderamente me produce?¿Me da placer? y en ese caso, ¿qué clase o grado de placer?¿Cómo se modifica mi naturaleza por su presencia o bajo su influencia? La respuesta a estas preguntas son los datos originales con los que tiene que operar el crítico de estética; y lo mismo que en el estudio de la luz, de las costumbres, de los números, uno debe verificar por sí mismo tales datos originales o bien no hay nada que hacer. Y quien experimenta estas impresiones con fuerza y se ve directamente impulsado a distinguirlas y analizarlas, no tiene necesidad de preocuparse por el problema abstracto de lo que sea la belleza en sí ni por cuál sea su relación exacta con la verdad y con la experiencia, cuestiones metafísicas tan inútiles como cualesquiera otras cuestiones metafísicas.

  

El crítico de estética, pues, mira todos los objetos con los que tiene que ver, todas las obras de arte y las formas más bellas de la naturaleza y de la vida humana, como poderes o fuerzas que producen sensaciones placenteras, cada una de ellas más o menos peculiar y de un tipo único. Siente esta influencia y desea explicarla mediante el análisis y la reducción de sus componentes. Para él, un cuadro, un paisaje, una personalidad atractiva de la vida o de un libro, La Gioconda, las colinas de Carrara, Pico della Mirandola, son valiosos por sus virtudes, como diríamos refiriéndonos a una hierba, un vino o una gema; por la propiedad que tiene de crear al sujeto una impresión placentera, concreta y única. Nuestra educación se completa a medida que nuestra susceptibilidad a estas impresiones aumentan en profundidad y extensión. Y la función de crítico consiste en distinguir, analizar y separar de sus accesorios la virtud gracias a la cual un cuadro, un paisaje, una personalidad cabal de la vida o de un libro produce esta especial impresión la belleza o el placer, en indicar cuál es la fuente de esa impresión y en qué condiciones se experimenta. \[...\]

  

Lo importante, pues, no es que un crítico disponga de una correcta definición abstracta de lo que es la belleza para el intelecto, sino de un cierto tipo de temperamento, de la capacidad de conmoverse profundamente ante la presencia de objetos bellos. Debe tener siempre presente que la belleza existe en muchas formas.»

_El Renacimiento,_ Walter Pater

Icaria, 1982