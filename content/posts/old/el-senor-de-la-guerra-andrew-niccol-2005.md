---
title: 'El señor de la guerra - Andrew Niccol, 2005'
date: 2013-12-27T23:58:00.000+01:00
draft: false
tags : [2005, Andrew Niccol, _c]
---

27.xii.2013// Más que nada, es el guión. El texto al que pone voz [Nicolas Cage](http://www.imdb.com/name/nm0000115/?ref_=tt_ov_st), narrado con tanta gracia, cinismo e ironía, sin darse mayor importancia, es la clave de la película. Al mismo tiempo te da a conocer al personaje, que es simple y no tiene remordimientos, y sus justificaciones hacen que lo veas como _un hombre_, de negocios y con ambiciones, pero _un hombre_ al fin y al cabo, y por eso conecto. Cómo consigue introducirse en la cúpula de la venta de armas, cómo se toma cada noticia o elabora su estrategia, cómo engaña a la que será su mujer...pero no solo los hechos, sino también los motivos por los que se mueve, y todo visualmente espléndido y con buenos golpes. No da para más, pero está entretenida.