---
title: 'Scoop (2006)'
date: 2012-02-08T17:12:00.000+01:00
draft: false
tags : [Woody Allen, Ian McShane, Hugh Jackman, Londres, Cine, 2000, Scarlett Johansson]
---

[![](http://4.bp.blogspot.com/-A6JdbfE6f38/TzKeU34kjeI/AAAAAAAABNI/nn_dgYXhftY/s400/rostard.jpg)](http://4.bp.blogspot.com/-A6JdbfE6f38/TzKeU34kjeI/AAAAAAAABNI/nn_dgYXhftY/s1600/rostard.jpg)Joe Strombel (**Ian McShane**) el famoso periodista, descubre por un soplo después de su muerte, que el buscado asesino del tarot, no es otro que el multimillonario Peter Lyman (**Hugh Jackman**). Mientras el conocido prestidigitador Sid Waterman (**Woody Allen**) hace desaparecer a Sondra (**Scarlett Johansson**) estudiante de periodismo, se le aparece Strombel y le revela el secreto que la podrá llevar a la fama.

  

**Woody Allen** hace una película al año y esta es la del 2006. Dentro de lo que se puede esperar, entra la clásica comedia. Recordando considerablemente a '**_Misterioso asesinato en Manhattan_**' (1993), '**_Scoop_**' introduce de nuevo la intriga del asesinato, la cómica curiosidad y desenfadado desarrollo de los acontecimientos. Allen escoge por segunda vez a Johannsson, tras '**_Macht Point_**' (2005) pero con otra perspectiva distinta y más luminosa de un nuevo Londres. También revive a su propio y archirreconocido personaje surrealista, dubitativo, extraño y complicado. A pesar de la repetición de los tópicos de Woody Allen el divertidísimo largometraje no deja de asombrar al espectador, riéndose irónicamente de la alta sociedad inglesa y de todo lo que encuentra a su paso.  
  

Lo absurdo de la situación puede dar a entender que es una película sin más, pero a pesar de un presupuesto tan ajustado, la realización es estupenda. No es posible acostumbrarse a las ideas de este interesante y aclamado director neoyorkino, que no deja de sorprender y absorber al público con las mismas inquietudes de siempre. La relación padre-hija de la película es memorable, convirtiéndose en el centro de un filme muy entretenido.