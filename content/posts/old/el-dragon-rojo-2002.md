---
title: 'El Dragón Rojo (2002)'
date: 2012-04-01T17:53:00.000+02:00
draft: false
tags : [Emily Watson, Ted Tally, Brett Ratner, Danny Elfman, William Blake, Dante Spinotti, Anthony Hopkins, Ralph Fiennes, Harvey Keitel, Thomas Harris, Cine, Edward Norton, 2000, Philip Seymour Hoffman]
---

[![](http://3.bp.blogspot.com/-HrKV2rj5JKI/T3h5whP46EI/AAAAAAAABUo/IJZpk9fkzEM/s400/rostard.jpg)](http://3.bp.blogspot.com/-HrKV2rj5JKI/T3h5whP46EI/AAAAAAAABUo/IJZpk9fkzEM/s1600/rostard.jpg)

Si hay un personaje fascinante en la historia de la psicología y los asesinos en serie, ese es el **Dr. Hannibal Lecter**. Hace no mucho tiempo vi la grandiosa, '_**El Silencio de los corderos**_' y la terminé altamente impresionado por la presencia y misterio de Anthony Hopkins en la piel del asesino y psiquiatra.

  

Las precuelas o secuelas suelen ser las víctimas más atacadas por el público, que acostumbra a estallar por las expectativas o por los caprichos a los que se han habituado con las primeras partes. La obra de **Johnathan Demme** es indiscutible así como intocable, sin compararla demasiado con 'las demás', la inquietante situación que divide el cristal de la celda entre la agente Starling (**Jodie Foster**) y el mencionado Lecter, es única.

  

En '**_El dragón rojo_**' se opera milagrosamente en un clímax muy ajustado y conseguido, quizá no tenga esa inefable especia que la hace diferente a '_**El Silencio de los corderos**_', pero ciertamente está a la altura. **Brett Ratner** obtiene una historia bastante similar,  -esto también proviene de la novela  de **Thomas Harris**\- adaptada por **Ted Tally** de nuevo, aunque sigue absorbiendo la mente afectada por la escabrosa pero fascinante personalidad de H. Lecter, ahora menos presente en el eje del thriller. La banda sonora de Danny Elfman siempre es una garantía y seguridad para obtener el suspense necesario para palparlo con los tímpanos en el instante justo.  
  
  
Es un tema a tratar, la cuestión del asesinato y el crimen. En esta obra se da a conocer una vez más -siempre con esencias Hollywoodianas y mitológicas- la visión del asesino, y va desde el horror de la sangre, hasta el placer encontrado en su observación y en el arte de la muerte. También es sugerente en la figura de Lecter, el cómo una personalidad de la medicina y la cultura psiquiátrica, cruza la línea entre el estudio y compresión de la enfermedad mental y la posesión de la misma locura patológica. 

  

El remake de '_**Manhunter**_' (1986, **Michael Mann**) sale airoso de las comparaciones ambientadas en el mismo drama y me refiero a las versiones de J. Demme y **Ridley Scott**.  **Dante Spinotti** el director de fotografía participó también en la primer adaptación de 'Dragón Rojo', 'Manhunter?, podríamos reconocer su interés por este trabajo. El mismo Ratner quiso que Mann hiciera un cameo como conductor de un taxi.

  

Los actores son otro tema muy discutido en estos trances. Will Graham (**Edward Norton**) no es tan sensible como el papel realizado por Foster, es algo que le da otro enfoque a la película a pesar de que en ambas es el **Dr. Lecter** el que con su conocimiento de la psiquiatría y su punto de 'locura' ayuda a encontrar a el asesino, a pesar de que no ocupa un lugar tan central como se dijo anteriormente. Norton no desafina en absoluto, pero Ralph Fiennes se inmola en el papel de psicópata ya retratado en esta piel en otras ocasiones. **Ralph Fiennes** es sin duda un personaje interesante, sin hacer referencias a la enigmática pintura de **William Blake** sobre el '_El gran dragón rojo y la mujer vestida de sol_' que se come en la película.  **Emily Watson** también tiene cierto interés como invidente, aunque me resultan demasiado similares las actuaciones de su filmografía. **Harvey Keitel**, así como **Philip Seymour Hoffman**, tampoco tienen tanto espacio para lucirse pues el espacio restante es para **Hopkins** siempre vestido con sabiduría, elegancia y una indescifrable apariencia.  
  
  

A pesar de las coincidencias observadas en el filme de **Brett Ratner**, encuentro muy intrigante y provocador este largometraje. Alegre por la fidelidad de Hopkins al reparto, y  por las caras nuevas de este thriller. Muy a la altura de secuelas y segundas partes. ¿Quién no quiere cazar a un asesino en serie, o simplemente... ser uno?