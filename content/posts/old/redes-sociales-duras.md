---
title: 'Redes Sociales Duras'
date: 2020-04-25T13:10:00.000+02:00
draft: false
tags : [redes sociales, peces, reddit, tiktok, blogger, facebook, instagram]
---

Realmente, las redes sociales duras: TikTok, Facebook o Instagram, LinkedIn. Cuando empezaremos esta clasificación. Una vez más, vamos a echar mano de las etiquetas, pero ya sabes, las etiquetas son útiles. Las RRSS abiertas serían aparentemente, Reddit, Blogger, y otras más de pirados como nosotros. 

  

Por un lado pienso que no necesito no tener algo para tener el control de cuánto lo uso. Hablemos de los peces, no sé si es una leyenda urbana o no, pero, por lo visto, si les das demasiada comida para peces --esos trocitos de papel biblia que se comen religiosamente--, para ellos, se empachan y se mueren. Ni siquiera me voy a molestar en contrastar esta información. No sé qué etiquetas tienen que tener para ser duras, pero supongo que tienen que ser tendencias de:

*   No colaborativas.
*   Fomentan el narcisismo y la repercusión del exhibicionismo.
*   Des-cultura de la Imagen.

Yo qué sé, ya pensaré en esto.