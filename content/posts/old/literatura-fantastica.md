---
title: 'Literatura fantástica'
date: 2018-12-23T11:18:00.000+01:00
draft: false
tags : [Miguel D'Ors, paterson, poesía cotidiana, Poesía, poesía contemporánea]
---

Que no me llamas porque tienes mucho  
trabajo en el jardín. Y cita en el dentista.  
Y además te llegaron invitados.  
  
Que no podemos vernos  
porque te has apuntado a una piscina,  
porque va a caducarte el DNI,  
porque estás esperando a un fontanero,  
tienes otro cursillo, es miércoles, se ha muerto  
la cuñada de un tío de tu vecina,  
hace mucho calor, bajó la bolsa...  
  
¿Es que estás esperando a que no pase nada  
para quererme? ¿Piensas que el amor  
es cosa sólo de las vacaciones,  
una más de esas que los cursis llaman  
"actividades de ocio y tiempo libre"?  
  
Yo quiero que me quieras en tu vida.  
Con tu vida. Ya ves. A mí el amor,  
lo mismo que el café, no me va solo:  
me gusta con visitas al dentista,  
con sorpresas y líos,  
con coches que de pronto se averían,  
con facturas, lentejas que se queman,  
vinos que están de muerte,  
compromisos, urólogos, podólogos  
y todo eso que es, con todos sus colores,  
la realidad. Lo que suceda fuera de ella,  
_frankly, my dear_, ya sabes...  
  
14-11-2017  
_Manzanas Robadas._ Miguel D'Ors. Renacimiento, 2017.