---
title: 'John H. Newman - "La educación universitaria..."'
date: 2010-10-29T11:39:00.000+02:00
draft: false
tags : [Universidad, John H. Newman, educación, Sentencias]
---

﻿﻿Estrenando la nueva etiqueta(\*)... misteriosamente_ "sentencias":_ en latín algo así como" llevar delante de" algún alguien de renombre. En esta cita el excelentísimo Cardenal Newman o simplemente **John Henry Newman** -beatificado recientemente- habla sobre la educación universitaria (en mi opinión basada en las humanidades), -lo llamaría cita pero lo pronunciaría como sentencia pues me parece más poderosa y subrayada- a lo largo de la sentencia describiendo el personaje del universitario. Atractivo el ideal...

  

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TMqVP2Npy3I/AAAAAAAAAz0/SGmivRW_0YQ/s320/4.gif)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TMqVP2Npy3I/AAAAAAAAAz0/SGmivRW_0YQ/s1600/4.gif)

En la foto. Una de las obras callejeras de _Bansky_

_ "La **educación universitaria** proporciona a la persona capacidad de estar a la altura de las circunstancias en cualquier puesto de trabajo que ocupe. La capacita para acertar en el trato con los demás y comprender su mentalidad, así como manifestar la suya propia. Proporciona capacidad de entenderse con nobleza, sabiendo conllevarse mutuamente. Una persona así formada es capaz de hablar con gente de todas las clases sociales. Sabe cuándo hablar y cuándo callar, conversar y escuchar. Sabe preguntar cosas pertinentes y aunque nada pueda aportar, sabe sacar lecciones de lo que los demás dicen. En todo tiempo está disponible sin ser pesado. Es un compañero agradable, alguien con quien contar. Sabe cuándo estar serio y cuándo alegre. Tiene tacto para decir cosas graciosas así como para volver a la seriedad cuando conviene. Tiene, aun en medio de la multitud, la paz de un espíritu capaz de vivir en el interior de sí mismo, que encuentra en él razones para ser feliz, cuando no las halle en su exterior. Estos dones le sirven en público y le sostienen cuando está aislado, pues sin ellos el éxito es algo trivial, mientras que con ellos incluso el fracaso y la desilusión tienen su encanto."_

  
  
(\*)Posteriormente, cambió a "sentencias".Da en que pensar, no es una persona que estudia sin más para obtener un título y así poder trabajar en lo que desea, es mucho más, y eso estoy descubriendo. Sin embargo aquí no habla de una educación cualquiera, evidentemente depende en gran parte de la universidad, pero supongo que el universitario debe procurarse la suya buscando medios, e incluso con los suyos propios. En definitiva más persona.