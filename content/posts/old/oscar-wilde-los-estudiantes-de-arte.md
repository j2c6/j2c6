---
title: 'Oscar Wilde - A los estudiantes de arte'
date: 2010-08-22T19:59:00.000+02:00
draft: false
tags : [Dorian Gray, Arte, Oscar Wilde, Literatura, belle èpoque]
---

Una breve conferencia, pronunciada por **Wilde** a los estudiantes de arte. En la **_belle èpoque_**  fue toda una referencia, lo comprobamos con "**El Retrato de Dorian Gray**" y algunas de sus poesías; es interesante, una perspectiva... Una breve selección:

  

_El arte es la ciencia de la belleza._

[![](http://3.bp.blogspot.com/_VT_cUiG4gXs/THFljTdSYdI/AAAAAAAAAxc/Nv6a07exB-8/s320/Headlam-OscarWildeSetWhatawonderfulpieceofluck-1719.jpg)](http://materialmiblancoynegro.blogspot.com/2010/08/oscar-wilde-los-estudiantes-de-arte.html)

_Todo cuanto debierais aprender del arte es a reconocer si un cuadro es bueno o malo con solo verlo._

_El arte no es nacional, sino universal._

_La popularidad es la corona de laurel que el mundo teje para el arte malo._

_Si sois fielmente un artista, no seréis portavoz de un siglo, sino el dueño de la eternidad._

_El artista ha sido, y siempre será una bella excepción._

_La peor manera de honrar a Dios es deshonrar al hombre creado a su imagen y que es obra de sus manos._

_Cuando el artista no puede nutrir su mirada de belleza, la belleza se aleja de su trabajo._

_Si un hombre ve en una nube el carro de un ángel, lo pintará de una manera muy poco parecida a una nube._

_¿Será realmente cierto que un bello ambiente resulte necesario para el artista? No lo creo._

_La cosa menos artística en nuestro siglo no es la indiferencia del público por las cosas llamadas feas. Pues para el verdadero artista nada es feo o bello por sí mismo. Él no tiene que ver con los hechos del objeto, sino solo con su apariencia,y esta es cuestión de luz y de sombra, de posición y de valores._

_Lo que vosotros, pintores, tenéis que pintar no son las cosas tales como son, sino como no son._

_No hay objeto, por feo que sea que en determinadas condiciones de luz o de sombra, o en la proximidad de otros objetos, no parezca bello; no hay objeto por bello que sea, que en ciertas ocasiones no parezca feo._

_Existís como artistas, no para copiar la belleza, sino para crearla en vuestro arte._

_El artista joven que solo pinta cosas bella olvida la mitad del mundo._

_Es preferible vivir en una ciudad de temperatura variable que en una ciudad de alrededores maravillosos._

_Toda pintura que no os produce inmediatamente un goce artístico capaz de haceros exclamar: ¡Que bello!, es una mala pintura._

_Si un hombre es un artísta puede pintarlo todo._

_El objetivo del arte es pulsar la cuerda más divina y más secreta que produce música en nuestra alma._

_¿Cuando está terminado un cuadro? Cuando todo rastro de trabajo, así como los medios empleados para lograr el resultado, han desaparecido._

_El arte decorativo pone de manifiesto su material; el arte imaginativo lo anula._

_Un cuadro no tiene más significación que su belleza ni otro mensaje que su alegría._