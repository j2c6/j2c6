---
title: 'Un Ciudadano Ejemplar'
date: 2010-06-25T10:01:00.000+02:00
draft: false
tags : [F. Gary Gray, Jamie Foxx, Gerard Butler, Equilibrium, The Italian Job, Un Ciudadano Ejemplar, Venganza, Cine, Buenas, Kurt Wimmer]
---

Al ver esta película, concluyo con que cada año sale una película de acción esperando a ser vista por nosotros: los amantes del cine. Y aunque pasa desapercibida, al final la encuentras. _Clyde_ ( _**Gerard Butler**_ ) ha sido victima del asesinato de su mujer y de su hija, cuando dos ladrones entraban a robar a su casa. _Nick ( **Jamie Foxx**_ ) el fiscal del caso, acuerda con el autentico asesino que haga una confesión para que el cómplice (y por tanto no asesino) obtenga la pena de muerte, y este salga de la cárcel a los tres años. La justicia ¿es justa?... la respuesta la tiene _Clyde Shelton. **F. Gary Gray**_ director de otros thrillers como _**The Italian Job**_ (2003), o el _Negociador_ (1998), nos deja pasmados ante cada sorpresa que asalta el filme, y nos coloca entre la espada y la pared con un dilema político y social que desenfocara nuestro punto de vista en numerosas ocasiones. Nada más del largometraje, esto es suficiente para montar un plan de película con palomitas recordándonos a otros (**_Venganza_** (2008), _**El Fuego de la Venganza**_ (2004)). Una gran película de acción, bien argumentada con el guión de _**Kurt Wimmer** (**Equilibrium**_ 2002), todo preparado, todo listo, premeditado: _**Un Ciudadano Ejempla**_**r.**

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/TCRisNe5GwI/AAAAAAAAAuw/_0n2AnAJI9s/s320/un-ciudadano-ejemplar-cartel1.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/TCRisNe5GwI/AAAAAAAAAuw/_0n2AnAJI9s/s1600/un-ciudadano-ejemplar-cartel1.jpg)