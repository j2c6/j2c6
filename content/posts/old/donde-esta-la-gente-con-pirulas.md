---
title: '¿Dónde está la gente con pirulas?'
date: 2018-04-03T12:24:00.001+02:00
draft: false
tags : [McDonalds, fe, trabajo, pirulas, creatividad, vocación, aceite, crear, gente que busca]
---

[![](https://4.bp.blogspot.com/-lenan4dmAXk/WsQHEFj30KI/AAAAAAAATEc/8Ho0FMYZnyQgEl4SD-xW8VeNfbUWaaNPACLcBGAs/s400/Captura.PNG)](https://4.bp.blogspot.com/-lenan4dmAXk/WsQHEFj30KI/AAAAAAAATEc/8Ho0FMYZnyQgEl4SD-xW8VeNfbUWaaNPACLcBGAs/s1600/Captura.PNG)

Proyecto de restauración de La Ceramo. Jose Cortés G.

  
Ayer hablé con Jose, un amigo arquitecto con pirulas.  
  
¿Dónde está la gente que tiene pirulas? O se están echando a perder en un trabajo corriente o mientras el universo los tilda de pringaos ellos están silenciosamente trabajando en lo que mejor saben hacer mientras tal vez reparten paellas a domicilio y se acuestan tarde porque tienen fe en sí mismos. Fe indemostrable para los demás. Es una lucha consigo mismos, en la soledad de su cámara y el flexo del escritorio. Algún día oirás hablar de ellos, ya no les ofrecerán trabajillos para ir tirando o les preguntarán con condescendencia si salen adelante, ellos se han hecho a sí mismos. Han mirado dentro de sí, y han ganado. Ahora a veces van a McDonalds pero a tomar patatas fritas (normales) y a recoger su jarra gratuita, y miran con orgullo a aquel que dice 'siguiente', y tras el que se esconde la lucha de un genio con la frente aceitosa, salpicada de conservantes y colorantes y sal industrial. Esa es la batalla que se está librando, y no señor, esa batalla no hace ruido, va buceando hasta el final de la piscina. Sí, ni siquiera nadie está ahí para ver si llegará al otro lado, sólo él sabe cuánto le está costando. Gente que busca y lo hace _lejos del mundanal ruido_.

  

Cuando indagas un poco, te das cuenta de que hay más de lo que parece. Sólo hace falta un plan pequeño, una conversación cercana y te das cuenta. Están ahí.