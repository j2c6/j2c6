---
title: 'Fracture (2007)'
date: 2012-01-30T10:51:00.000+01:00
draft: false
tags : [Anthony Hopkins, Ryan Gosling, Gregory Hoblit, Daniel Payne, Cine, 2000, Rosamund Pike, David Strathairn]
---

[![](http://3.bp.blogspot.com/-OsNWp5GnrXg/TyZn7A_s3bI/AAAAAAAABLY/uno9eHydJfU/s400/rostards.jpg)](http://3.bp.blogspot.com/-OsNWp5GnrXg/TyZn7A_s3bI/AAAAAAAABLY/uno9eHydJfU/s1600/rostards.jpg)Ted Crawford (**Anthony Hopkins**) le pega un tiro a su mujer cuando se entera de que le es infiel. Para Willy Beachum (**Ryan Gosling**), el fiscal que tiene la posibilidad de ascender considerablemente si gana este caso, parece una cosa sencilla. Pero una vez más las cosas no son lo que parecen.

  

¿Porqué? Porque lo tiene todo preparado. Un _thriller_ realmente intrigante de **Gregory Hoblit**. El personaje de **R. Gosling**, interesa por su pinta de triunfador, y aunque Ted Crawford ya huele un poco a las facilidades de **Hopkins** para interpretar a psicópatas (_Veasé_: '**El silencio de los corderos**', 1991) vuelve a derretir la curiosidad del espectador por saber que planea una mente tan impenetrable. Finalmente resulta en mismo juego de siempre, la película confunde al espectador y aunque los recursos, ya muy manidos, son casi rutinarios el público sigue queriendo saber el final de la historia ya que no conoce la respuesta.  
  
  

Las aspiraciones de esta película se ajustan a lo obtenido. Un hilo interesante a pesar de las "florituras" del guión (**Daniel Pyne**) para obtener un filme "inteligente". Sin contar mi debilidad por los filmes judiciales. Afortunadamente no se centran solo en la trama y sangre, sino que también queda hueco para que se expresen los actores. **Rosamund Pike**, **David Strathairn** y los citados **Ryan Gosling**, **A. Hopkins**. Generosamente entretenido.