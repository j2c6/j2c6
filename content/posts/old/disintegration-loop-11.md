---
title: 'Disintegration Loop 1.1'
date: 2020-05-19T18:56:00.006+02:00
draft: false
tags : [twin towers, William Basinski, 11s]
---

Hay un tal William Basinski, con un apellido molón que el 11 de Septiembre de 2001 se plantó en lower Manhattan con su cámara a captar el atardecer humeante tras el desastre de las torres gemelas. Total que el tío, que hace música de ambiente ahí lo dejo, así, sencillamente, qué genial.  

  

[![](https://1.bp.blogspot.com/-siOOJCEHLdU/XsQQlzjuh5I/AAAAAAAAfZQ/Tajom5KvSkgFZXBonXgmel_6fIY7rXrMwCK4BGAsYHg/w320-h320/external-11111111.jpg)](https://1.bp.blogspot.com/-siOOJCEHLdU/XsQQlzjuh5I/AAAAAAAAfZQ/Tajom5KvSkgFZXBonXgmel_6fIY7rXrMwCK4BGAsYHg/external-11111111.jpg)