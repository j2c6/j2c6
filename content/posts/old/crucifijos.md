---
title: 'Crucifijos'
date: 2020-02-03T09:51:00.001+01:00
draft: false
tags : [tótem, claustrofobia, reliquia, crucifijos, canarias, religión]
---

Ayer, buscando el teclado lavable para C., sacamos mi breve colección de crucifijos, había 2 comodines, pero esencialmente:

  

*   Uno dorado y sobado con las esquinas rectas que me dio mi amigo Carlos, el sacerdote.\*
*   Otro pequeño que me dió mi madre, este me encantaba y me daba miedo perderlo.
*   Una copia de este último más grande, de mi confirmación, con fecha de mayo-2008.

  
Estaban metidos en un sobre de cuero que creo que me trajero de canarias, con un sol mirando de frente. Tras un tiempo sin sacarlos, me parecía, todos juntos y hacinados, herramientas supersticiosas. Se veía en ellos el apego que les había tenido. Algunas palabras que me vienen a la mente sin ningún motivo en particular: _reliquia, tótem, claustrofobia_.  
  
\* Este era el que tenía de cabecera y llevaba a todas partes. Lo ponía frente a mí en mi mesa, cuando estudiaba. Me acordaba de cuando, en las meditaciones, el sacerdote jugaba con el crucifijo que llevaba sobre la mesita cubierta de terciopelo a medida.