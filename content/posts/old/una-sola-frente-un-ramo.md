---
title: 'Una sola frente a un ramo'
date: 2019-01-03T10:33:00.001+01:00
draft: false
tags : [Miguel Delibes, Señora de rojo sobre fondo gris, rosa, flor, Literatura, florero, ramo]
---

  

> _¿De quién aprendió entonces que una rosa en un florero puede ser más hermosa que un ramo de rosas o que la belleza podía esconderse en un viejo reloj de pared destripado y lleno de libros? \[...\] Su presencia aligeraba la pesadumbre de vivir._

_Señora de rojo sobre fondo gris._ Miguel Delibes.