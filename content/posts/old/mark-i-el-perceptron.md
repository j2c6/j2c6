---
title: 'Mark I: el perceptrón.'
date: 2020-05-26T10:59:00.001+02:00
draft: false
tags : [Frank Rosenblatt, McCulloch, redes neuronales, perceptrón, biblia]
---

En 1957 Frank Rosenblatt, construyó su Mark I, el Perceptrón, un tipo de computadora que simula en esencia el pensamiento humano, escalando el sistema de funcionamiento de las neuronas, basándose en los modelos de 1943 de McCulloch y Pitts. Rosenblatt se fijó en sus propias neuronas para crear su Mark I, lo que me recuerda a:  

>   
> 
> _ Entonces dijo Dios: Hagamos al hombre a nuestra imagen, conforme a nuestra semejanza_

Gen 1:26   

  

Frank se ahogó años más tarde, en 1971 en bahía de Chesapeake, el día en que cumplía 43 años.