---
title: '#7 COVID-19'
date: 2020-03-17T18:50:00.000+01:00
draft: false
tags : [coronavirus, diario coronavirus, covid-19]
---

Alemania está cada vez más cerca en casos. Pero hemos de hacer hincapié en que el conteo de casos no refleja la realidad. Probablemente haya muchos más casos. En cuanto a la carrera por conseguir un fármaco que sea candidato a vencer el coronavirus tenemos: 

  

*   PharmaMar con el Aplidin, que ya tiene casos efectivos in vitro.
*   Gilead con Remdesivir, que ya inicia pruebas en dos hospitales en España.
*   Zhejiang Hisun con Favilavir, aprobado ya en China para el tratamiento. 
*   Kaletra (lopinavir + ritonavir), ya recomendado por la SEFH y que, por lo visto, ha reducido considerablemente sus niveles de coronavirus.
*   Cloroquina, para la malaria ha demostrado ser eficaz contra en coronavirus, haciendo que el paciente termine siendo menos infeccioso a los pocos días.

Y esto es un poco lo que he encontrado por Internet. No tengo ni idea de en qué fase se encuentra cada uno. Y poco más por hoy. La policía se ha acercado a nuestra rutina musical de las 20 horas, por la llamada de varios vecinos aguafiestas. On-line se sigue presentando esa agresividad y soberbia absurdas.

  

[![](https://1.bp.blogspot.com/-Q7kwV-38Cx8/XnJkRqPbjyI/AAAAAAAAfDQ/958YBiIkoqsm4ziHr7WxUMFWeyBRXlU5wCNcBGAsYHQ/s320/_R072893.jpg)](https://1.bp.blogspot.com/-Q7kwV-38Cx8/XnJkRqPbjyI/AAAAAAAAfDQ/958YBiIkoqsm4ziHr7WxUMFWeyBRXlU5wCNcBGAsYHQ/s1600/_R072893.jpg)

Cola de entrada al supermercado, respetando la distancia de seguridad.