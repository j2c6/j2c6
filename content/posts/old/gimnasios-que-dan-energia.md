---
title: 'Gimnasios que dan energía'
date: 2017-07-13T10:32:00.002+02:00
draft: false
tags : [negocio, idea, carta al periódico, carta]
---

He leído en una carta al semanal XL:

> **Cuando restar suma**  
>   
>   
> 
> Desde la cinta de correr me inquieto. En mi gimnasio, solo un cristal separa las máquinas de la calle y a veces me distraigo observando escenas cotidianas, o me conformo sirviendo de pasatiempo a los transeúntes. Esta mañana ha pasado una anciana cargando unas bolsas que le pesaban más que los años. Iba lenta, fatigada, a punto de desfallecer. Las piernas le reclamaban la poca energía que sus brazos se empeñaban en acaparar. En lo que tardaba yo en correr cien metros, ella apenas daba cuatro o cinco pasos. Ha sido entonces cuando me he dado cuenta de que estaba encarnando un simple ejemplo del funcionamiento más vil del ser humano: yo malgastaba energía en vano, estática, sin ningún fin ni destino, mientras delante de mí la abuelita las estaba pasando canutas para avanzar unos pocos metros. Imbéciles todos, que no vemos (ni queremos ver) que, quitando un poco de aquí y repartiéndolo allí, progresaríamos apaciblemente en la misma dirección. Pero no desesperen porque, aunque débil, siempre acaba asomando una sonrisa entre la negrura, un haz de luz al que aferrarse: mi sufrimiento ha tenido respuesta y una vecina ha salido del portal, ayudándola a entrar. Qué alivio. Irene Barceló Carceller, Tortosa (Tarragona)

Y sí, se desperdicia energía donde, precisamente, sólo distribuyéndola un poco a nuestro alrededor, haría del ambiente algo mejor. Ya se me ocurre como idea sofisticada de negocio, un gimnasio que genere energía eléctrica con la fuerza de los clientes. Es una tontería, creo que es más una cuestión de actitud y aprovechamiento.