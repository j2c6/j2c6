---
title: '"El sol de los Scorta" de Laurent Gaudé'
date: 2014-01-03T23:54:00.000+01:00
draft: false
---

Un bandido, calor, polvo, robos y violaciones. Su descendencia. Los personajes son tratados lacónicamente, mostrando su ferocidad con pocos detalles, cómo si al haber menos palabras los hechos imaginables fueran más terribles. Pero Gaudé tiene muy buenos detalles:

> _Un hombre sucio y cubierto de polvo entraba a casa de los Biscotti, a la hora en que los lagartos sueñan con ser peces y las piedras no pueden reprochárselo._

Aunque es demasiado: "lo sabía", "así era", "nunca fue de otro modo", como reafirmando cada cosa con frases cortas, como si fueran frascos de información breve pero de vital importancia, recalcando todo, blanco o negro, pero no gris. Sus personajes no dudan, se dejan llevar o mandan, no tienen misterio -o por el contrario, lo tienen debido a su simplicidad-, lo saben, nunca sospechan o presienten: lo saben. Sus actitudes son planas, aunque cuando van alcanzando la madurez muestran mayor profundidad y trascendencia: la muerte, el sentido de la vida... Me ponía un poco nervioso que soltara tan poca información: es como si cada frase fuera un _continuará_, y luego no hubiera para tanto, pero tenía _su sú_, aunque nada demasiado especial, de todos modos, aunque quema, poco a poco te bebes la sopa, y está rica. La sequedad y sencillez del pueblo, el amor a la propia sangre y las costumbres. Además para mí tiene el factor exótico italiano, la salsa de tomate y esas cosas. Y adjetivos jugosos y sabrosos, cómo si aquella situación fuera legendariamente calurosa, o árida, o así. Los mayores dan consejo y transmiten su sabiduría  a  los pequeños y ellos guardan respetuoso silencio pensando por ello que reciben el conocimiento, condensado en pocas palabras y que se han convertido en hombres _montepuccianos_. Un hombre que siempre sonríe al ver el producto de la tierra sobre su mesa.  
**C**  

 Laurent Gaudé

_El sol de los Scorta_

Salamandra, Quinteto, Mayo 2008

  

###