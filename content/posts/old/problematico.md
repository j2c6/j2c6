---
title: 'Problemático'
date: 2019-01-28T12:41:00.000+01:00
draft: false
tags : [niño, infancia, problemas, educación]
---

Un niño no es problemático. Un niño tiene problemas.