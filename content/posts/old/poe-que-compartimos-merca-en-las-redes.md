---
title: '¿Por qué compartimos merca en las redes sociales?'
date: 2020-07-14T10:25:00.001+02:00
draft: false
tags : [tiempo]
---

No tengo ni idea. Se sospecha que tiene que ver con un 'eh, estoy aquí en el mundo, soy importante'. Pero imagínate que fuera el equivalente a hablar en una conversación en un grupo de amigos, ¿qué te lleva a hablar? ¿A compartir información personal irrelevante y sumarlo al feed de otros millones de contribuyentes a alimentar el miedo a perderse algo (FOMO)?

  

No lo sé, pero tengo la sensación de que contribuir a ese feed, es peligroso, es alimentar una burbuja y una bola de nieve monumentales. Te quita tiempo, te hace tonto, te deprime, genera ansiedad. ¿Porqué no dejar de quitarle tiempo a los demás? Si quiero que alguien vea mis fotos, en realidad, me vale con que un día las vea en casa, en un libro que habré editado con las mejores fotos del año. ¿Y si Instagram pasa al papel? ¿Y si Twitter se convierte en un diario? ¿Y si Facebook se reduce a algunas llamadas de teléfono y otras quedadas?  

  

¿Y si la mayoría del tiempo e interacciones se reduccen a contactos reales? ¿Por qué no hacen un motor de búsqueda que excluya a los grandes tótems de internet? ¿Por qué no hacemos un esfuerzo en el internet descentralizado?