---
title: 'El ciervo sagrado y el hilo invisible'
date: 2018-04-03T12:49:00.000+02:00
draft: false
tags : [Paul Thomas Anderson, Daniel Day-Lewis, Yorgos Lanthimos, películas, Cine, Thimios Bakatatakis]
---

Dos películas este fin de semana.

  

_El sacrifcio de un ciervo sagrado_. Yorgos Lanthimos. 2017.

  

*   Es como si las personas fueran inhumanas en su cotidianidad, forzadas, correctas, anodinas y toda su verdadera naturaleza apareciera con la violencia y las situaciones extremas en la película.
*   Los planos de la película (Thimios Bakatatakis) impecables acompañados por los movimientos relajados y al mismo tiempo correctos y rígidos de sus protagonistas (por decirlo de alguna manera, relajación controlada), los silencios contenidos, las tomas largas. Todo, hasta la escena en la que Martin, de cara diabólica, juega con los espaguetis mientras explica su realidad. Creo que esta escena se hace repugnante gracias a toda la impecabilidad previa de la película. Simplemente el movimiento del tenedor, ese caos momentáneo de los espaguetis en el aire.

[![](https://1.bp.blogspot.com/-q_cCvmoY15s/WsQDtNLDKRI/AAAAAAAATDs/eTpFVwwm248q3HtB-pQRb8M3VLxOzZeyACLcBGAs/s320/sacrificio-ciervo-poster.jpg)](https://1.bp.blogspot.com/-q_cCvmoY15s/WsQDtNLDKRI/AAAAAAAATDs/eTpFVwwm248q3HtB-pQRb8M3VLxOzZeyACLcBGAs/s1600/sacrificio-ciervo-poster.jpg)

_El hilo invisible_. Paul T. Anderson. 2017.  
  

*   La historia de amor donde, plácidamente, ambos amantes acaban siendo conscientes de las necesidades del otro en un ciclo de: nosamamos - meignoras - teenveneno  - menecesitas - nosamamos. La personalidad Reynolds, la historia y la marca de su madre. Los pequeños detalles de la convivencia.
*   Una vez más Daniel Day-Lewis con su mirada omnipotente sobre ella, todo comprensión y adoración. Pese a que es una mirada caduca, tiene su fuerza. Va más allá de lo humano, contiene los detalles, las maneras, el pensamiento, la paz.
*   La fotografía (del mismo PTA), también muy cuidada, los detalles, los símbolos.

  

[![](https://1.bp.blogspot.com/-NddcfmGJx3g/WsQDB7ZIAZI/AAAAAAAATDU/SIHL2E1r4lAnwYV_-2u9HoImR-WSXGeagCLcBGAs/s320/vlcsnap-2018-04-01-18h16m53s466.png)](https://1.bp.blogspot.com/-NddcfmGJx3g/WsQDB7ZIAZI/AAAAAAAATDU/SIHL2E1r4lAnwYV_-2u9HoImR-WSXGeagCLcBGAs/s1600/vlcsnap-2018-04-01-18h16m53s466.png)

  

[![](https://4.bp.blogspot.com/-yDRbd-s66Q4/WsQDBvy8rhI/AAAAAAAATDQ/olunnHMAtkwPrYqUeH0xEoX4wXnJkgrwwCLcBGAs/s320/vlcsnap-2018-04-01-18h22m02s122.png)](https://4.bp.blogspot.com/-yDRbd-s66Q4/WsQDBvy8rhI/AAAAAAAATDQ/olunnHMAtkwPrYqUeH0xEoX4wXnJkgrwwCLcBGAs/s1600/vlcsnap-2018-04-01-18h22m02s122.png)

  

[![](https://1.bp.blogspot.com/-AHTcYglAgzU/WsQDBrAZmQI/AAAAAAAATDM/s2UKt-6tlwYBKAwlZihKRrpe1aWRPJQvgCLcBGAs/s320/vlcsnap-2018-04-01-18h35m10s503.png)](https://1.bp.blogspot.com/-AHTcYglAgzU/WsQDBrAZmQI/AAAAAAAATDM/s2UKt-6tlwYBKAwlZihKRrpe1aWRPJQvgCLcBGAs/s1600/vlcsnap-2018-04-01-18h35m10s503.png)

  

[![](https://1.bp.blogspot.com/-nTn7vMuctac/WsQDDJny3kI/AAAAAAAATDY/F45CywJViUQTGD6D-y61FrLW8eWpyYrIgCLcBGAs/s320/vlcsnap-2018-04-01-18h49m05s623.png)](https://1.bp.blogspot.com/-nTn7vMuctac/WsQDDJny3kI/AAAAAAAATDY/F45CywJViUQTGD6D-y61FrLW8eWpyYrIgCLcBGAs/s1600/vlcsnap-2018-04-01-18h49m05s623.png)

  
  
Creo que me estoy convirtiendo en una _persona de formas_. El contenido es importante, pero ha de venir en un envoltorio interesante. El argumento ya me da igual, sólo cobra importancia si las formas le hacen merecerlo. (Qué pedante leer esto a posteriori, sin duda tengo una vanidad rara y podrida).