---
title: 'El Retrato de Dorian Gray - Oscar Wilde'
date: 2010-04-21T19:49:00.000+02:00
draft: false
tags : [Dorian Gray, Shakespeare, Sybil Vane, S.XIX, Novela, Narcisismo, Un Clásico, Oscar Wilde, Basil Hallward, Dorian, Literatura, Lord Henry]
---

Leerse un libro puede ser algo gratificante, pero leer  "_**El Retrato de Dorian Gray**_" puede resultar algo "delicioso". Sencillamente, en  cada línea se saborea la _**belle époque**_**,** completamente esteticista, y romántica. No quiero desilusionar a nadie, pero lo he disfrutado tanto...Al hablar de este libro muchas palabras no salen de mi boca porque se que quedan asustadas en la garganta. **Oscar Wilde** acaricia  tantas cuestiones, a lo largo de sus páginas: la belleza, el arte, la juventud, el placer, la verdad... y sus contrarios: la fealdad, la vulgaridad, la vejez, el dolor, la mentira... Se convierte no solo una novela sobre una linea argumental, sino que, a medida que te va absorbiendo el libro, se crean dudas nerviosas sobre las que debes preguntarte . Le he dado cierta confianza a este autor. 

.

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/S886fMprA5I/AAAAAAAAArU/XifabU5Nauo/s320/96210205.JPG)](http://1.bp.blogspot.com/_VT_cUiG4gXs/S886fMprA5I/AAAAAAAAArU/XifabU5Nauo/s1600/96210205.JPG)

Una literatura brillante y viva, donde los diálogos entre los personajes se convierten en toda una reconstrucción de filosofía, ética, estética... y donde las breves descripciones matizan toda la vida, llena de arte y realismo. Los sentimientos, pensamientos, acciones humanas quedan reflejadas a través de los personajes: **Dorian, Henry, Basil, Sybil**, James y otros muchos que hacen de ella una novela inolvidable. El **narcisismo** plasmado en la obra (hubiera puesto narcisos en la foto), es completamente diferente al mostrado posiblemente en otras novelas no tan populares, tiene otro propósito a parte de la admiración del propio lector... Toda la literatura clásica (**Shakespeare**)  y el sentimiento moral se reformulan en la firma de Wilde. Sin embargo toda la paradójica situación creada por el irlandés, y todo el tema estético y artístico que baña la obra, consiguen despertar las mentes dormidas de los lectores para una importante reflexión, de la que se pueden beneficiar. Un libro primaveral, completamente imprescindible.