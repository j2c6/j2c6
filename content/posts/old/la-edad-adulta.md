---
title: 'La edad adulta'
date: 2019-01-03T10:30:00.000+01:00
draft: false
tags : [El día de la Independencia, Richard Ford]
---

  

> _Era cuando uno rencunciaba a tratar a la persona que se quería y se limitaba a aceptarla tal y como era; admitiendo que te gustara._

  

_El Día de la Independencia_. Richard Ford. Anagrama.