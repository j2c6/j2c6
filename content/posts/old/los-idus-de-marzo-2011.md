---
title: 'Los idus de marzo (2011)'
date: 2012-03-11T17:28:00.000+01:00
draft: false
tags : [Lewis Milestone, Marisa Tomei, Ryan Gosling, George Clooney, Cine, Paul Giamatti, 2010, Philip Seymour Hoffman]
---

[![](http://1.bp.blogspot.com/-4AVtwyLS8w0/T1zWQxe0aZI/AAAAAAAABQ4/Z4SqXrM6RfM/s400/rostard.jpg)](http://1.bp.blogspot.com/-4AVtwyLS8w0/T1zWQxe0aZI/AAAAAAAABQ4/Z4SqXrM6RfM/s1600/rostard.jpg)En su cuarto proyecto como director **George Clooney** presenta un drama politico-periodístico, basado en la obra de teatro de **Beau Willimon**: '**_Farragut North_**'. Un reparto en boga con el solicitado **Ryan Gosling**, que vuelve a aparecer tras cederle su puesto **Leonardo  DiCaprio** que se coloca en la línea de producción.

  

Al igual que en '**_Primera plana_**' (1931, **Lewis Milestone**) presenta la realidad del mundo del periodismo, además mezclado con su poder en las campañas presidenciales y dándole un visionado a la parte política. Sin abusar del cinismo y desencanto de encontrarse con las verdades sobre los medios de comunicación, el filme de **Clooney** resulta ser muy realista. No es el clásico alarde de inmoralidad del periodismo, sino un equilibrio entre honestidad y crueldad y esto se comprueba escena a escena donde evidentemente muestra la desafortunada situación de el gran dúo absorbente y demoledor de la sociedad: política y medios de información.

  

Los instrumentos del director han sido utilizados con inteligencia y cautela. El reparto (**Gosling, Seymour Hoffman, Clooney**...) es gran parte del peso de la película y hace posible esta historia que interpreta un guión trabajado y quizá ligeramente pretencioso. Los diálogos elegantes, huyen de los tópicos de esta clase de _thrillers_, con cierta originalidad pero sin excesos. Libre de partidismo enseña lo que le interesa, la carencia de honestidad, la hipocresía y las tramas corruptas de este  mundillo harto representado. Y tal vez fuera eso, no es novedoso ni lo que encarna ni el evidente resultado.

  

La introducción de la película es llevadera, pero no posee la fuerza necesaria para llegar hasta donde le gustaría, ya que a medida que va mostrando las distintas caras del filme no arrastra al espectador a la profundidad que intenta ofrecer con su fábula periodística. A pesar de lo relativamente impredecible del desenlace finalmente capta la atención del espectador tras conocidos y astutos golpes de guión.  
  

La crítica que procura acusar el filme se ve distraída por la relación de Stephen (**Ryan Golsing**) y Molly (**Evan Rachel Wood**), que tiene su repercusión pero le quita importancia, mostrando un caso anecdótico o simbólico que tan solo sugiere tímidamente la situación actual de la política, etc. La referencia a la muerte de **Julio Cesar** con el llamativo título promete algo que después se comprobará como no tan revelador. La conclusión es clara: el filme se queda a medio camino entre el _thriller_ y la denuncia.