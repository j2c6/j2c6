---
title: 'Lentejas simples'
date: 2020-03-19T20:37:00.002+01:00
draft: false
tags : [vegetarianas., receta, cocina, lentejas]
---

Ingredientes:Dos pimientos, cebolla, ajo. Dos zanahorias, rodaja de calabaza y dos patatas. Y lentejas secas.

  

1.  Sofrito: pimiento, con la cebolla y el ajo. Cucharada de pimentón de la Vera. Triturado.
2.  Echar las lentejas lavadas y removemos. Y echamos trozos grandes de calabaza, zanahoria y las patatas enteras para que conserven la forma.
3.  Añadimos agua (o caldo) y buenos pizcas de sal, comino y una hoja de laurel.
4.  Se puede servir con un poco de arroz para que resulte más contundente.
5.  Al hervir 40 minutos, puedo sacar una muestra, con un poco de todo y calabaza y triturarla para que gane espesor y añadirla al guiso.

Se le puede añadir un toque de curry.

  

[![](https://1.bp.blogspot.com/-5SpfO4HG0y0/XnXfH0kDmMI/AAAAAAAAfE8/wbHK-NlMGuIo5kSq8zjGgnT7OMjNmjBaQCNcBGAsYHQ/s320/len.jpg)](https://1.bp.blogspot.com/-5SpfO4HG0y0/XnXfH0kDmMI/AAAAAAAAfE8/wbHK-NlMGuIo5kSq8zjGgnT7OMjNmjBaQCNcBGAsYHQ/s1600/len.jpg)