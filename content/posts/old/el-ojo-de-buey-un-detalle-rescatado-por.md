---
title: 'El ojo de buey, un detalle rescatado por Des Esseintes'
date: 2013-02-01T09:00:00.000+01:00
draft: false
tags : [Ojo de buey, detalles, Joris-Karl Huysmans, A Contrapelo, Des Esseintes]
---

  

[![](http://4.bp.blogspot.com/-SbC1njdeJmU/UQr6CaWZ2xI/AAAAAAAABwM/H1RGKPuXTHE/s1600/ojo+david+miguel+angel+esculturas+del+renacimiento+arte.jpeg)](http://4.bp.blogspot.com/-SbC1njdeJmU/UQr6CaWZ2xI/AAAAAAAABwM/H1RGKPuXTHE/s1600/ojo+david+miguel+angel+esculturas+del+renacimiento+arte.jpeg)

Hay detalles realmente fascinantes en las leyendas posiblemente ciertos o no, pero eso no importa. Son detalles que crean la magia de este mundo, flores que susurran una palabra antes de marchitarse, seres que se convierten en piedra con el primer rayo de sol. En fin, entre el amplio y exquisito museo de pensamiento y arte de '_À Rebours_' entre sus múltiples comentarios, hay uno que me da vueltas en la cabeza, y toda búsqueda al respecto hasta el momento ha sido en vano. El hecho de pensar que los ojos de ciertos animales imprimen en sus pupilas la imagen de lo que estaban observando mientras se les escapaba la vida, es sin duda una idea sublime, palpa algo macabro pero misterioso y poético al mismo tiempo. Si sabe algo, querido lector, de tan seductor elemento, serán bienvenidos sus conocimientos. Aquí copio la referencia de _**Des Esseintes**_:

  

«Publicada en 1867, en la _Revue des  Lettres et des Arts_, esta _Claire Lenoir_ iniciaba un conjunto de relatos agrupados bajo el título genérico de _Histoires moroses_. Sobre un fondo de oscuras especulaciones tomadas del viejo Hegel, se movían unos personajes destartalados, un tal doctor Tribulat Bonhomet, solemne y pueril, una tal Claire Lenoir, ocurrente y siniestra, con gafas azules, abultadas y redondas como monedas de cien céntimos, que cubrían sus ojos casi ciegos.

  

Este relato narraba un simple adulterio, pero se cerraba con una escena de un indescriptible terror, cuando Bonhomet, desplegando las pupilas de Claire en su lecho de muerte, e introduciendo en ellas monstruosas sondas, podía percibir con claridad, reflejada en ellas, la escena del marido blandiendo en sus brazos la cabeza cortada del amante, y vociferando, como un kanaka, un canto de guerra.

  

Basado en una observación más o menos cierta según la cual, los ojos de ciertos animales, como los bueyes, por ejemplo, conservan, hasta el momento de su descomposición, del mismo modo que las placas fotográficas, la imagen de los seres y de las cosas situadas bajo el alcance de de su última mirada, cuando estaban expirando, este cuento se situaba evidentemente en la línea de los de Edgar Poe, del que tomaba la minuciosidad puntillosa en la manera de tratar el asunto y el aspecto de terror.»

  

_**A Contrapelo**_, Joris-Karl Huysmans.

Cátedra, 2000.