---
title: 'R.E.M. - Revolution'
date: 2010-12-22T11:28:00.000+01:00
draft: false
tags : [Rock, R.E.M., Revolución, Rock Alternativo, Música, Michael Stipe]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/TRHSVlhwqGI/AAAAAAAAA2c/8TMVT8M7dZ4/s320/REM.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/TRHSVlhwqGI/AAAAAAAAA2c/8TMVT8M7dZ4/s1600/REM.jpg)

[http://www.goear.com/listen/192586c/revolution-rem](http://www.goear.com/listen/192586c/revolution-rem)

Este grupo de ingleses desde 1980, llevan cambiando el mundo a base de guitarras, y mensajes. Su música dentro del _Rock_, sondea muchos campos dentro del mismo, podría decírseles "los exploradores del rock", tienen canciones muy distintas. Han triunfado considerablemente, aunque con títulos especialmente "destrozados" por la reiteración como "Losing My Religion", cuando tienen muchos otros como: "_Nightswimming_", "_Drive_", "_The_ Sidewinder _Sleeps Tonite_", "Ma_n On The Moon_", " _Everybody __Hurts_" o "_Imitation Of Life_". Sin embargo he preferido no hablar de ninguno de estos, sino de un descubrimiento personal reciente: "_Revolution_". 

Agresiva y distorsionada como una manifestación masiva porque estamos hartos de todo, pero sin pasarse y con estilo. Lleva un ritmo aplicado un poco a la ira con orgullo, cuando lo oyes, tu cabeza asiente irremediablemente, al ritmo. "_Yeah, yeah, yeah... YEAH!_", me gusta mucho, _Michael Stipe_ explica las cosas con fuerza. Las guitarras corean, una melodía constante que quiere decir algo así como: "lo ves, te lo estamos  diciendo". De vez en cuando _Bill Berry_, saca de la manga redobles en caja que aceleran el pulso. El último "Yeaaaaaaaoououuuaaaaaa..." prueba la paciencia del grupo, quizás desesperación. Y en 3.04 minutos estás dentro de la "anarquía" musical de R.E.M.