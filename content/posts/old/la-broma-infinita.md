---
title: 'La Broma Infinita'
date: 2020-08-18T16:32:00.002+02:00
draft: false
tags : [la broma infinita, david foster wallace]
---

 Así, acojonado, miedo al aburrimiento, a no avanzar, a quedarme a mitad y que todo el esfuerzo haya sido en vano, a no conectar, a odiar. Esos son los sentimientos pre-broma. Supongo que frente a ello, siendo valiente y lanzándome, tengo que restarle importancia al aburrimiento, despertar mi capacidad de admiración, apreciar la lentitud, la paciencia, no obligarme a terminarlo, sin expectativas. Leer con calma, sin prisa, como si no hubiera nada más, el último libro de la tierra, y una vez lo leas no podrás leer más. Transpórtalo a todas partes, critícalo, deja tu huella en él. Me llevaré cuentos, y algún ensayo para descansar y volveré con él. Es sólo un maldito libro, y quiero leerlo, saber qué se siente.