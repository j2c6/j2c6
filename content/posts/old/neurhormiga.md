---
title: 'Neurhormiga'
date: 2015-09-18T13:48:00.000+02:00
draft: false
tags : [neuronas, ficción, homiga]
---

Imagina una hormiga. No una hormiga cualquiera, una Camponotus ligniperda, distinguida, casi arácnida, elegante en su movimiento, trabajadora, una hormiga obrera, estéril. Una, hembra, una ella. Ahí está, sola, caminando por su autopista espacial de transporte de alimentos, una irreverencia para las cigarras. Es una hora extraña, parece el amanecer del día de Navidad, no hay tráfico. Pero, es obvio, las hormigas, al menos las camponótidas, no celebran la Navidad. Es simplemente un instante raro en la complicada e instintiva mente colectiva de las formícidas. Va cargada, no conozco hormigas no-atareadas. Esta, Donna, en un bautismo rápido y adecuado según el autor de este lojhrfgñklj, lleva una preciosa y poderosa miga de pan, exquisito, puede que de la parte interna de un bocadillo tipo ‘hora del almuerzo’, un tributo silencioso y despreocupado al reino de los insectos. Y adherida a ella, una vanguardista y absurdamente discreta salpicadura de tomate frito. En realidad, según la proporción, podría tratarse de una hogaza, y no una miga o migaja despreciable para un humano, capaz de alimentar tal vez a una pequeña parte de una colonia, un selecto grupo de individuos, tal vez de índole real.

  

Bien, pues ahora imagina, que esta hormiga, es una neurona: Donna, la neurona. Aquella que fue, como una luciérnaga, la última en apagarse, esperando para llevarme a casa, indicándome el camino mientras las demás, cansadas, de la realeza, dormían oscuramente, no encendidas. Una neurona comprometida con su portador. Una hormiga devota, portadora de un pensamiento que está fermentando una poderosa idea.