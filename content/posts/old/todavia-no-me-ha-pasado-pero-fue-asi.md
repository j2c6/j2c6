---
title: 'Todavía no me ha pasado, pero fue así.'
date: 2010-03-27T17:35:00.000+01:00
draft: false
tags : [Microrrelatos, Arte, La Escalera Que Quemaba Ríos de Tinta]
---

[![](http://1.bp.blogspot.com/_VT_cUiG4gXs/S8cynXYumVI/AAAAAAAAAq0/rJcuQhXh88k/s320/arte-2.jpg)](http://1.bp.blogspot.com/_VT_cUiG4gXs/S8cynXYumVI/AAAAAAAAAq0/rJcuQhXh88k/s1600/arte-2.jpg)  

Todavía no me ha pasado, pero fue así. Me preparé para la ocasión bien elegante, marcando aquella fecha en el calendario, siempre había esperado aquella ocasión,  y estaba muy nervioso, ya que no sabía cómo sería. Por desgracia fui yo solo: entré en aquel edificio moderno y espectacular que crecía frente a mí, y esperé a que en recepción se dieran cuenta de que quería entrar. Cuando accedí al salón había mucha gente. En él distinguí dos tipos: unos que seguían atareados y embotellados en sus conversaciones de gran importancia, y otros que a medida que iba cruzando la gran sala me miraban fijamente. El corazón me latía rápido, y mis nervios estaban jugando un gran papel. Mis ojos se cruzaban con miradas inocentes y melancólicas (algunas); otras egocéntricas y soberbias;  había otras que me miraban incluso con ternura y afecto. Todo era un encanto bajo aquella lámpara de cientos de cristales, que me deslumbraba al alzar la vista. A pesar de toda la belleza que bañaba mis pupilas, yo seguía buscando. Mis zapatos resonaban en el suelo a cada paso. De repente todo se nubló, mi vista desenfocó la realidad: ya no era yo quien conducía mis pisadas. Y me paré. Creo que me temblaban las manos, y el alma me iba a estallar: porque allí estaba ella con todo su encanto. Era mucho más  de lo que había imaginado, ahí permanecía eternamente a la espera de que llegara yo y la mirara y sonriera, como lo había hecho ella durante tanto tiempo. Aquel momento se convirtió en todo para mí. Ese instante me hacia comprender que todo el universo era para ella, y para aquella sonrisa sin palabras casi universal que hacía que nos entendiéramos como si fuéramos hermanos. Toda su fama no significaba nada para mí, era ella sin más: toda su hermosura. Lo que vi era indescriptible…

  

 Ciertamente no sé qué pasó después, pero fue maravilloso. Supongo que esto sería lo que ocurriría al mirar de verdad  a aquella obra de arte, que tantas veces había recortado de algún periódico, y me había guardado en el bolsillo.