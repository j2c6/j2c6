---
title: 'Bajo amenaza (2011)'
date: 2012-01-24T14:37:00.000+01:00
draft: false
tags : [Karl Gajdusek, Nicolas Cage, Nicole Kidman, Cine, 2010, Joel Schumacher]
---

[![](http://4.bp.blogspot.com/-kIZsgFbr2b4/TyVLEBmcDDI/AAAAAAAABLM/MqBskN2ep8g/s400/rostards.jpg)](http://4.bp.blogspot.com/-kIZsgFbr2b4/TyVLEBmcDDI/AAAAAAAABLM/MqBskN2ep8g/s1600/rostards.jpg)

Kyle Miller (**Nicolas Cage**), y Sarah Miller (**Nicole Kidman**) son un matrimonio que vive rodeado de lujo en su espectacular mansión, donde de noche una banda entra a robar todo lo que encuentren.

  

**Joel Schumacher** ('**Un día de furia**', 1993) prolífico director de _thrillers_, vuelve a palpar el fiasco tras algunos fracasos más, arrastrando a actores de la talla de **Kidman** o **Cage**. La idea del guión, algo más que utilizada en el cine, no trae nada nuevo, ni siquiera bajo una nueva perspectiva. Son visibles los intentos de lo contrario, asociando a cierto personaje alguna característica relativamente novedosa, pero los cambios estructurales arañan lo primitivo. Un ritmo de película que no consigue mantener la tensión del espectador, ni siquiera alterando su ritmo cardíaco, por no hablar de la contención del aliento, fenómeno ausente por completo.  
  

Un suspenso para el espectador y para el director. **Karl Gajdusek** recién iniciado y debutante en estos términos; esperemos que cambie su trayectoria tras un comienzo como guionista poco prometedor. Por lo demás una mala noche, mi sincero deseo es que todos se recuperen de este asesinato.