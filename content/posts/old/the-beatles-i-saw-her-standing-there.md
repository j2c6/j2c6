---
title: 'The Beatles - I Saw Her Standing There'
date: 2009-09-15T15:28:00.000+02:00
draft: false
tags : [Please Please Me, Rock, Musica, The Beatles, Comienzos del Rock]
---

[![](http://4.bp.blogspot.com/_VT_cUiG4gXs/Sq-coE_mn0I/AAAAAAAAAjs/JUafeXR9534/s320/2-+BEATLES.jpg)](http://4.bp.blogspot.com/_VT_cUiG4gXs/Sq-coE_mn0I/AAAAAAAAAjs/JUafeXR9534/s1600-h/2-+BEATLES.jpg)  

Creo que es un grupo, como de los mas importantes, de los que todavía no hemos escrito. Es un grupo del que todo el mundo debe conocer sus éxitos, ya que despues de los padres del **rock'n'roll negro**, son ellos los que lo popularizan. Está todo el mundo de acuerdo en que son los Nº1 en clave de rock, y tienen motivos, tienen mas de 20 éxitos en listas de todo el mundo. Si quieres un grupo del que te pueda gustar, casi todo, o todo, este es el tuyo: tienen una fuerza, en sus gritos, en las, voces, en los coros, en los _riffs_, los punteos... Dicen lo que quieren decir, esto es: Rock'n Roll.

[![](http://2.bp.blogspot.com/_VT_cUiG4gXs/Sq-dI1udn5I/AAAAAAAAAj0/Qcfe21GzXqw/s320/untitled.bmp)](http://2.bp.blogspot.com/_VT_cUiG4gXs/Sq-dI1udn5I/AAAAAAAAAj0/Qcfe21GzXqw/s1600-h/untitled.bmp)

Para presentaros el grupo, he decidido, no poner ninguna de las canciones de los famosos recopilatorios, sino el primer tema, del primer disco _**Please Please Me**_, es una canción de su primera etapa, como musicos pioneros en su genero, habla sobre una fiesta, y una chica, lo típico. La pieza no tiene mas, sin embargo tiene una viveza, los gritos, el ritmo, y el rasgueo, es mortal, mucha marcha, como una pista de baile. Y lo comprendes perfectamente, te pones en su lugar, la cancion, dice exactamente lo que el tandem **Lennon/McCartney**, dicen. Bueno no me enrollo mas, esto no es lo mejor que tienen pero si una buena presentación, para que te den ganas de descargarte, la nueva colección de la discografia remasterizada, que acaban de lanzar, disfruta de lo mejor...