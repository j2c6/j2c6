---
title: 'William Saroyan - El Tigre de Tracy'
date: 2011-05-30T18:49:00.000+02:00
draft: false
tags : [Bueno, El Tigre de Tracy, Literatura, William Saroyan, Novela]
---

[![](http://4.bp.blogspot.com/-pEXDXlIaGj0/TePKMIKOl0I/AAAAAAAAA-k/X6zmGMo0OYY/s1600/Pantera-Negra-Americana.jpg)](http://4.bp.blogspot.com/-pEXDXlIaGj0/TePKMIKOl0I/AAAAAAAAA-k/X6zmGMo0OYY/s1600/Pantera-Negra-Americana.jpg)

Tras leer hace algunos años "_La Comedia Humana_" y haberla disfrutado como el primer baño en el mar del verano, oí el rugido de "_El Tigre de Tracy_". Entonces William Saroyan había hecho mucho más que no defraudar, había contado un "cuento" con el que toda la humanidad se ha sentido identificada, algo que perduraría para siempre. 

  

_Thomas Tracy_ va siempre en compañía de un tigre. Un buen día conoce a una _Laura Luthy_ vestida de amarillo y se enamora de ella. Una historia contada de manera muy sencilla, de sentimientos inocentes, puros y humanos. El modo de ocurrir las cosas atrae por su lacónica naturalidad. En Acantilado --la editorial donde lo he leído, un formato pequeño y cómodo-- escriben: "_Nos habla Saroyan sobre el amor, el alma humana y la importancia de creer, no solo con la mente sino con el corazón_". Supongo que _Tracy_ es un hombre auténtico que lucha por lo que cree. La lectura de esta novela se asemeja mucho a la contemplación de un paisaje, tus pupilas van girando para ver lo que tienen delante, y se deleitan sin más y cuando se iban a cerrar tus párpados te das cuenta de que ya has acabado. Se convierte en un misterio para mí el señor Saroyan, ya que algunas piezas de la historia no dejan de inquietarme por su aparición.

  

"_Tracy acompañó a Laura hasta la oficina donde trabajaba de estenógrafa, dos manzanas más abajo de la calle Warren, cerca de los muelles._

_\--¿Mañana?-- dijo, sin saber a qué se refería pero con la esperanza de que_ ella _sí lo supiera._

_\--Sí-- contestó Laura._"  

__ "_El Tigre de Tracy" William Saroyan. ___Acantilado 2010.__