---
title: 'J.F. Kennedy - Discurso Inaugural'
date: 2011-04-20T14:00:00.000+02:00
draft: false
tags : [60', J.F. Kennedy, Discursos]
---

[![](http://4.bp.blogspot.com/-KmWGHBSuOy8/Ta7KNb5A1BI/AAAAAAAAA9k/4ur8xsIQ484/s320/Kennedy.jpg)](http://4.bp.blogspot.com/-KmWGHBSuOy8/Ta7KNb5A1BI/AAAAAAAAA9k/4ur8xsIQ484/s1600/Kennedy.jpg)20 de Enero de **1961**, **Kennedy** es elegido presidente de **EE.UU**. Subió a la tarima, ante los micrófonos, ante el mundo entero, y habló. Su discurso inaugural quedó en las mentes de los ciudadanos de ese país, y emocionó al planeta. Hace 50 años de estas palabras grabadas en la historia; para levantar ideales a seguir, pensamiento político. No es que eche de menos a personas así porque no las he visto nunca en los carteles electorales, pero me gustaría que así fuera. Kennedy no era perfecto, pero su **oratoria** habla con franqueza, de manera sencilla y humana: un discurso sublime. Es evidente que no se puede conocer bien al hombre que sale en las pantallas, pero sí puede darse a conocer. Sentimientos optimistas, luchadores, honestos, patrióticos. La visión que se extrae del _**speech**_ es admirable. Supongamos que un **político** tiene que transmitir tres cosas, Kennedy dijo cuatro.   
  
  
Sin embargo la oratoria no lo es todo.

  

_"Compatriotas:_

[](http://4.bp.blogspot.com/-KmWGHBSuOy8/Ta7KNb5A1BI/AAAAAAAAA9k/4ur8xsIQ484/s1600/Kennedy.jpg)_Celebramos hoy, no la victoria de un partido, sino un acto de libertad —simbólico de un fin tanto como de un comienzo— que significa una renovación a la par que un cambio, pues ante vosotros y ante Dios Todopoderoso he prestado el solemne juramento concebido por nuestros antepasados hace casi 165 años. El mundo es muy distinto ahora. Porque el hombre tiene en sus manos poder para abolir toda forma de pobreza y para suprimir toda forma de vida humana. Y, sin embargo, las convicciones revolucionarias por las que lucharon nuestros antepasados siguen debatiéndose en todo el globo; entre ellas, la convicción de que los derechos del hombre provienen no de la generosidad del Estado, sino de la mano de Dios._

_No olvidemos hoy día que somos los herederos de esa primera revolución. Que sepan desde aquí y ahora amigos y enemigos por igual, que la antorcha ha pasado a manos de una nueva generación de norteamericanos, nacidos en este siglo, templados por la guerra, disciplinados por una paz fría y amarga, orgullosos de nuestro antiguo patrimonio, y no dispuestos a presenciar o permitir la lenta desintegración de los derechos humanos a los que esta nación se ha consagrado siempre, y a los que estamos consagrados hoy aquí y en todo el mundo._

_Que sepa toda nación, quiéranos bien o quiéranos mal, que por la supervivencia y el triunfo de la libertad hemos de pagar cualquier precio, sobrellevar cualquier carga, sufrir cualquier penalidad, acudir en apoyo de cualquier amigo y oponernos a cualquier enemigo.\[...\]_

_La energía, la fe, la devoción que pongamos en esta empresa iluminará a nuestra patria y a todos los que la sirven, y el resplandor de esa llama podrá en verdad iluminar al mundo._

_Así pues, compatriotas: **preguntad, no qué puede vuestro país hacer por vosotros; preguntad qué podéis hacer vosotros por vuestro país.**_

_Conciudadanos del mundo: preguntad, no qué pueden hacer por vosotros los Estados Unidos __de América, sino qué podremos hacer juntos por la libertad del hombre._

_\[...\]No se llevará a cabo todo esto en los primeros 100 días. Tampoco se llevará a cabo en los primeros 1.000 días, ni en la vida de este Gobierno, ni quizá siquiera en el curso de nuestra vida en este planeta. Pero empecemos."_

_ 20 de Enero de 1961. Discurso Inaugural J.F. Kennedy, 35º Presidente de EE.UU_

  
_Recomendado por MB&N._