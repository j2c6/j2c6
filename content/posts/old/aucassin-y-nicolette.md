---
title: 'Aucassin y Nicolette'
date: 2013-02-03T09:00:00.000+01:00
draft: false
tags : [Marianne Stoke, Renacimiento, Walter Pater, Aucassin y Nicolette]
---

En el primer ensayo (**_El Renacimiento_**), Pater habla de cómo van surgiendo esos detalles que componen el encanto del arte, y ya en el siglo XIII se observa cómo entre la tosquedad y rudeza de los escritos, en una _cantefable_ por ejemplo, surge ese encanto, objeto de estudio del esteta. Y en realidad es un placer rescatar esos ingredientes que despiertan tanto disfrute despertaba en los oyentes y lectores de todas las épocas.

  

  

[![](http://2.bp.blogspot.com/-YU4Bjm8NUTw/UQrbEmdaOJI/AAAAAAAABvU/LlYXgR83sN4/s1600/m.jpeg)](http://2.bp.blogspot.com/-YU4Bjm8NUTw/UQrbEmdaOJI/AAAAAAAABvU/LlYXgR83sN4/s1600/m.jpeg)"\[...\] A menos que éste tenga su encanto, a menos que en su factura original haya alguna cualidad puramente artística, el mero gusto por las antigüedades nunca puede aportarle valor estético ni convertirlo en tema adecuado para la crítica estética. La historia de _Aucassin y Nicolette_ tiene algo de dicha cualidad. \[...\] Todo el encanto de la obra está en los detalles, en la especial ligereza y gracia con que se tratan las situaciones y los rasgos sentimentales, sobre todo en los pintorescos fragmentos de la temprana prosa francesa. En todo momento se deja sentir la influencia de esa atmósfera ligera, de super elaborada delicadeza, casi de lascivia, que tan característica es de la poesía de los trovadores. \[...\] Quizás el pasaje más bonito de toda la obra sea el fragmento de prosa que describe su huida:

  

          «Aucassin fue encarcelado, como ya han oído, y Nicolette permaneció encerrada en su cámara. Era verano, en el mes de mayo, cuando los días son cálidos, largos y claros, y las noches recatadas y serenas.

          Una noche Nicolette, que yacía en su lecho, vio la clara luz de la luna por la ventana y oyó el ruiseñor que cantaba en el jardín, y entonces le vino el recuerdo de Aucassin, a quien tanto amaba. Pensó en el conde Garins de Beaucaire, quien la odiaba mortalmente y que para deshacerse de ella, podría hacer en cualquier momento que la quemaran o ahogaran. Se percató de que la vieja encargada de acompañarla estaba dormida; se levantó y se puso la mejor túnica que tenía; cogió las ropas de cama y las toallas y las anudó formando una especie de cuerda, tan larga como dieron de sí. Luego ató el extremo a un pilar de la ventana y se dejó deslizar suavemente hasta el jardín, y lo atravesó en línea recta para alcanzar la ciudad.

          Tenía el pelo rubio y con pequeños rizos, los risueños ojos azul verdosos, la cara clara y pulida, los finos labios muy rojos, los dientes pequeños y blancos; las margaritas que aplastaba a su paso, alcanzando la falda por delante y por detrás parecían oscuras en comparación con su pie, ¡tan blanca era la doncella!

          Llegó a la puerta del jardín y la abrió, y anduvo por las calles de Beaucaire, yendo por el lado oscuro de la calzada para evitar la luz de la luna, que brillaba apaciblemente en el cielo. Anduvo tan deprisa como le era posible, hasta alcanzar la torre donde estaba Aucassin. La torre se sostenía con unos cuantos pilares. Se apretó contra uno de los pilares, se arropó estrechamente en su manto y, con la cabeza metida en una grieta de la torre, que era vieja y estaba en ruinas, oyó a Aucassin llorando amargamente en el interior y, cuando hubo escuchado un rato, comenzó a hablar.» "

  

  

_El Renacimiento,_ **Walter Pater**

Icaria, 1982