---
title: 'Erin Brockovich (2000)'
date: 2012-07-06T13:30:00.000+02:00
draft: false
tags : [Aaron Eckhart, Steven Soderbergh, Julia Roberts, Cine, 2000]
---

[![](http://1.bp.blogspot.com/-1z8nz1om4Kc/T_bMFtlJA5I/AAAAAAAABgA/qfFZL-lSFSE/s400/rostard.jpg)](http://1.bp.blogspot.com/-1z8nz1om4Kc/T_bMFtlJA5I/AAAAAAAABgA/qfFZL-lSFSE/s1600/rostard.jpg)Hay gente que atiende especialmente a una película cuando esta 'basada en hechos reales', pues esta es una de esas películas. **Steven Soderbergh** que ya cosecha una gran variedad de largometrajes se adentra en este caso en un drama judicial y la vida personal de Erin Brockovich.

  

Es un caso lleno de sorprendentes casualidades en una vida difícil de llevar. Desde que Erin (**Julia Roberts**) tiene el accidente de coche, todo va evolucionando gracias a las oportunidades que se le ofrecen -o que ella convierte en oportunidades-. La argumento es bastante interesante, y lo que le falta es suplido por reacciones cómicas de la absorbente protagonista. Está muy bien porque no solo se centra en el caso jurídico, que no es lo esencial, sino que cuando hace falta muestra retazos de la vida de Erin con George (**Aaron Eckhart**), que tiene mucha importancia en el filme.

  

Julia Roberts hace un papel increíble en pantalla. Ha hecho cientos de películas, y despierta un fenómeno que tan solo las grandes actrices -independientemente de su belleza- poseen frente a la cámara en el séptimo arte. Cuando **Julia Roberts** está en pantalla lo llena todo, tiene una presencia absorbente y es difícil estar al tanto de los demás detalles, además de su personaje es ella la que tiene un carácter fuerte y atrayente, de modo que es fácil conectar e identificarse con la protagonista. Esto le da mucha fuerza al filme.

  

En este largometraje además de su habitual rendimiento, se observa algo más. Es sorprendentemente más convincente, parece totalmente metida en el papel, tal vez es porque se trata de un papel más serio y totalmente dramático pero es especialmente brillante. La historia es interesante, y entretenida pero eso -y por desgracia- a la larga se olvida sin más. Lo que queda es una maravillosa Julia Roberts a las órdenes de un gran director.