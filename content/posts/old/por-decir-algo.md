---
title: 'Por decir algo'
date: 2018-06-17T18:46:00.001+02:00
draft: false
tags : [dudas existenciales, soledad, etapas, Revolución]
---

Estamos solos, pero no es el fin del mundo. Respeta la evolución. Crece por dentro.  
  

[![](https://1.bp.blogspot.com/-cULp9NzgDlg/WyaQWNK_wEI/AAAAAAAAXVI/OD9NKFr12okMNwQI7R9Ubjm1PSUxXKfSACLcBGAs/s320/IMG_6597.jpg)](https://1.bp.blogspot.com/-cULp9NzgDlg/WyaQWNK_wEI/AAAAAAAAXVI/OD9NKFr12okMNwQI7R9Ubjm1PSUxXKfSACLcBGAs/s1600/IMG_6597.jpg)