---
title: 'Mama Rosa Restobar'
date: 2020-10-10
draft: false
tags : [restoran, cocina, salidas]
categories : [comida, restoran]
---

Después de entretenernos en las librerías del centro, fuimos a cenar —bastante tarde para las restricciones caóticas y vergonzosas del COVID-19— al Mama Rosa. Nos lo habían recomendado los vecinos de Legazpi muchas veces y son conocidos por sus milanesas.

Llegamos 21:45 y nos pusieron dentro —una pena, porque fuera hacía una temperatura genial, pero estaba todo lleno— , limpiaron una mesa que quedaba bajo una lamparita y pedimos coca-cola light, una Provoleta y una milanesa Napolitana con base de ternera. 

Preocupados por cuándo podrían servirla nos indicaron los tiempos y atentos a que nos gustara todo, extremadamente amables. La comida, casera, estaba muy rica, y pudimos estarde charla tranquilamente hasta el paseo a casa. 31,65€.



*Restobar Mama Rosa*
_Alejandro Saint Aubin 1_
28045 Madrid