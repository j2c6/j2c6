---
title: 'Inicio'
date: 2020-10-09
draft: false
tags : [about]
categories : [blog]
---


Haciendo una prueba sobre las posibilidades de este blog. He pensado en ir prescindiendo poco a poco de esas compañías que no tratan demasiado bien a la privacidad y los datos de comportamiento de sus usuarios. Este formato me gusta, tienes un control total sobre el resultado, y aunque no es impresionante, es simple, y <cite>admite[^1]</cite> notas al pie.


![Imagen de ejemplo](/001.png)


### Apartado 2

Aquí podría explicarme después de haber estado introduciendo el tema e incluso poner alguna <cite>nota[^2]</cite> que otra.


[^1]: Como esta, que resulta bastante molona y puede dar muchas posibilidades.
[^2]: Esta sería muy absurda, bajar hasta abajo para leerla, sin nigún sentido ni posibilidad de aclarar nada, pero divertido. 

