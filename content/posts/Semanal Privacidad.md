---
title: 'Semanal Privacidad'
date: 2020-10-10
draft: false
tags : [about]
categories : [privacidad, vigilancia]
---


Esta maravillosa semana empezó después de estar leyendo el libro de Shoshana <cite>Zuboff[^1]</cite>, y tuvo algunas pequeñas acciones como consecuencia:

* Lunes: borrado de WhatsApp y paso a Telegram.
* Martes: investigación de migraciones de blogs.
* Miércoles: backup del blog. Cuentas de protonmail.
* Jueves: publicación del blog en las GitLab pages con Hugo.
* Viernes: vaciado de la cuenta de Gmail. Cambiar los mails de algunos servicios.
* Sábado: instalación de Tor.


Indirectamente, el uso del móvil ha caído a una media de una hora diaria y un despego relevante de dicho dispositivo. He pedido <cite>otros[^2]</cite> libros relacionados y a su vez han llegado <cite>[^3]</cite>.


[^1]: El libro de Shoshana Zuboff _The Age of Surveillance Capitalism_ (_«La era del capitalismo de vigilancia»_) se publicó el 15 de enero de 2019.
[^2]: 	(1) _Vigilancia permanente_, Edward Snowden 
		(2) _Mindf*ck: Cambridge Analytica. La trama para desestabilizar el <cite>mundo[^4]</cite>_, Christopher Wylie 
		(3) _Superpotencias de la inteligencia artificial: China, Silicon Valley y el nuevo orden mundial_, Kai Fu Lee
[^3]:  (1) _Privacy Is Power: Why and How You Should Take Back Control of Your Data_,
Carissa Véliz
 (2) _Calling Bullshit: The Art of Scepticism in a Data-Driven World_,
Jevin D. West
[^4]: Los subtítulos de verdad, son para ponerlos de coletilla de Misión Imposible, espero que sean órdenes de la editorial.