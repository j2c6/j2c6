---
title: 'Con lo que te gusta el sol'
date: 2020-10-12
draft: false
tags : [poesía,poema, sol, sombra]
categories : [poesía]
---

	cerca de las flores amarillas
	hay restos de comida,
	el suelo está por barrer,
	y no sé de qué clase soy
	¿soy de clase media?
	Lo que me importa es
	sacar el plásico reciclable
	tres días por semana,
	la contemplación de la casa
	y esos ruiditos que haces
	cuando estás absorta en tus cosas
	sabiéndo que yo soy esa sombra seria
	de la que te enamoraste un día
	tú, con lo que te gusta el sol.
